#!/usr/bin/env python3

""" Python script to read an IPC file and report the channels statistics.

Report on IPC senders by type. Report on any receivers.

IPC statistics are stored in a dictionary, keyed by fec name.
Entries are a dictionary with entries for
    location
    num SHMEM ipc channels
    num PCIE (Dolphin) ipc channels
    num RFM0 (long-range Dolphin, X-arm) ipc channels
    num RFM1 (long-range Dolphin, Y-arm) ipc channels

Licensing:
You should have received a copy of the licensing terms for this
software included in the file "LICENSE" located in the top-level
directory of this package.  If you did not, you can view a copy
at http://dcc.ligo.org/M1500244/ LICENSE

D.Barker Apr2019 LHO
"""

import configparser
import argparse

# location RFM counters
RFM_EX_TO_CS = 0
RFM_EY_TO_CS = 0
RFM_CS_TO_EX = 0
RFM_CS_TO_EY = 0

# main dictionary of ipc statistics
ipc_stats = {}


def print_report():
    """ print_report
    """

    # print the accumulated stats
    print("{:>12} {:>3} {:>5} {:>5} {:>5} {:>5}".format("name",
                                                        "loc",
                                                        "shmem",
                                                        "pcie",
                                                        "rfm0",
                                                        "rfm1",
                                                        ))
    for item in ipc_stats:
        print("{:>12} {:>3} {:>5d} {:>5} {:>5} {:>5}".format(item,
                                                             ipc_stats[item]['loc'],
                                                             ipc_stats[item]['shmem'],
                                                             ipc_stats[item]['pcie'],
                                                             ipc_stats[item]['rfm0'],
                                                             ipc_stats[item]['rfm1'],
                                                             ))
    print("EX<-CS {:2d} {:2d} CS->EY".format(RFM_CS_TO_EX, RFM_CS_TO_EY))
    print("EX->CS {:2d} {:2d} CS<-EY".format(RFM_EX_TO_CS, RFM_EY_TO_CS))


if __name__ == '__main__':
    """ main program
    """

    return_code = 0

    config = configparser.ConfigParser()
    # Argument handling
    parser = argparse.ArgumentParser(description="report on H1 IPC channels")
    parser.add_argument("path")
    args = parser.parse_args()

    # read the ipc file
    try:
        config.read(args.path)
    except configparser.DuplicateSectionError as ex:
        print(ex)
        print ("ERROR: There are multiple config sections with the same name, this happens when multiple IPCs have the same name")
        exit(1)


    num_dolphin_cs = 0
    num_dolphin_ex = 0
    num_dolphin_ey = 0
    num_rfm_0 = 0
    num_rfm_1 = 0
    num_shmem = 0
    pcie_counter = 0
    rfm0_counter = 0
    rfm1_counter = 0

    for section in config.sections():
        # print "Channel %s" % section
        fec = config.get(section, 'ipcHost')

        ipc_stats[fec] = {"loc": 'EY', "shmem": 0, "pcie": 0, "rfm0": 0, "rfm1": 0}

        entry_ipcnum = int(config.get(section, 'ipcNum'))
        if config.get(section, 'ipcType') == 'SHMEM':
            ipc_stats[fec]['shmem'] += 1
        elif config.get(section, 'ipcType') == 'PCIE':
            if entry_ipcnum != pcie_counter:
                print("Warning: PCIE ipcNum not contiguous at ipcNum {}".format(entry_ipcnum))
                return_code =1
                pcie_counter = entry_ipcnum
            pcie_counter += 1
            ipc_stats[fec]['pcie'] += 1
        elif config.get(section, 'ipcType') == 'RFM0':
            if entry_ipcnum != rfm0_counter:
                print("Warning: RFM0 ipcNum not contiguous at ipcNum {}".format(entry_ipcnum))
                return_code =1
                rfm0_counter = entry_ipcnum
            rfm0_counter += 1
            ipc_stats[fec]['rfm0'] += 1
            if ipc_stats[fec]['loc'] == "CS":
                RFM_CS_TO_EX += 1
            else:
                RFM_EX_TO_CS += 1
        elif config.get(section, 'ipcType') == 'RFM1':
            if entry_ipcnum != rfm1_counter:
                print("Warning: RFM1 ipcNum not contiguous at ipcNum {}".format(entry_ipcnum))
                return_code = 1
                rfm1_counter = entry_ipcnum
            rfm1_counter += 1
            ipc_stats[fec]['rfm1'] += 1
            if ipc_stats[fec]['loc'] == "CS":
                RFM_CS_TO_EY += 1
            else:
                RFM_EY_TO_CS += 1

    print_report()

    exit(return_code)
