################################################################
# EPICS Channel Access
#
#
# caGet - returns values for a list of record names
#   Example:
#     my @rcrds = ("C1:LSC-PD1_DC_TRAMP", "C1:LSC-PD1_DC_GAIN");
#     my @vals = caGet(@rcrds);
# 
# caPut - returns success
#   arguments are a list of names, and a list of values
#   Example:
#     my @rcrds = ("C1:LSC-PD1_DC_TRAMP", "C1:LSC-PD1_DC_GAIN");
#     my @vals = (5, 1);
#     my $isok = caPut(@rcrds, @vals);
# 
# caToggle - returns 1 if the channel is 0 or 1,  else returns channels' value
#   arguments are a list of names
#   Example:
#     my @rcrds = ("C1:LSC-PD1_STATUS", "C1:LSC-PD2_STATUS");
#     my @status = caToggle(@rcrds);
# 
# caSwitch
#   arguments are a list of names, and a list of values
#   Syntax matches ezcaswitch
#   Example:
#     my @rcrds = ("C1:LSC-PD1_DC", "C1:LSC-PD1_DC");
#     my @vals = ("FM1 ON OUTPUT ON", "FM1 OFF OUTPUT OFF");
#     my $isok = caSwitch(@rcrds, @vals);
# 
# caStep
#    arguments are a pairwise list of channel-steps
#    with an optional $delay (in seconds) appended to end as a hash-ref
#    Syntax is closely matched with ezcastep
#    Example:
#        no warnings 'qw';
#        caStep(qw(M1:HPI-HAMX_ACTOUT_H1_OFFSET +1.0,10 M1:HPI-HAMX_ACTOUT_H2_OFFSET +2.0),{delay=>1.2})
#   Info:
#        This subroutine has a ton of hidden features related to steps. If you specify only
#        step increments without a number of steps in the first pair of channel/step, it defaults
#        to 1 step.  If you don't specify number of increments, it'll default to whatever number
#        of steps is specified in the first pair. Steps can take on the following format:
#        +2.2     *2.2    -2.2    /2.2    +2.2dB
#        +2.2,10  *2.2,1  -2.2,5  /2.2,9  +2.2dB,4
# 7/12/2011: Updated caPut
# Programmer: Giordon Stark
# 8/16/2011: caPut user input handling
# Author: Chris Kucharczyk
# Details:
#	caPut now has a required "doWait" argument which allows
#	one to execute commands in parallel (false) or serial (true)
# 8/31/2011: caStep functionality
# Author: Giordon Stark
# Details: CaTools can step/ramp values simlar to ezcastep input functionality.
################################################################

package CaTools;
use Exporter;
@ISA = ('Exporter');
@EXPORT = qw(&caGet &caPut &caToggle &caSwitch &caSwitchS &caStep);

# This uses the CAP5 perl code to do channel access
# Assumes @INC includes epics  (calling function should have "use stdenv.pm;") 
use CA;
use Time::HiRes qw(usleep);
#use Switch;
use 5.010;
# require careful code
use strict;
#caStep uses given/when
use feature ":5.10";

my $CA_timeout = 5;


################################ Catch the Cntrl-C
$SIG{INT} = sub {

  # Currently don't have anything to do here
  exit;
};
    

################################ caGet
sub caGet {
  my @chans;
  my $ii;
  my @vals;

  # Initialize all the channels
  for ($ii=0; $ii < @_; $ii++) {
	$chans[$ii] = CA->new($_[$ii]);
  }
  # Flush the buffers and connect.
  
 eval {CA->pend_io($CA_timeout);};
 if($@){
	for ($ii=0; $ii<@_;$ii++){
	print STDERR "--$_[$ii]-- didn't load.\n" unless $chans[$ii]->is_connected;
        }
 }
	  
  
  # Queue the get request
  for ($ii=0; $ii < @_; $ii++) {
    $chans[$ii]->get if $chans[$ii]->is_connected;
  }
  # Flush the buffers and get the data.
  CA->pend_io($CA_timeout);

  # Load the output
  for ($ii=0; $ii < @_; $ii++) {
      # required to handle enums.
      my $v = $chans[$ii]->is_connected ? $chans[$ii]->value : '';
	  push(@vals, $v);
  }
  return @vals;
}

############################### caPut
# This function handles user input and sends that information to
# caPutHelper, which does all the actual work
# To call with waiting, include a hash reference at the end of the form
# {parameter=>value}
# Note: Requires at least 5.010 or higher
sub caPut{
	my @input = @_;
	# Initialize all optional arguments to their default values
	my $doWait = 'no_wait';	
	if(ref($input[$#input]) eq 'HASH'){
		my $optRef = pop @input;
		if(%$optRef ~~ /wait/) { $doWait = $optRef->{'wait'}; }
	}
	given ($doWait){
		when ('wait') 	{ $doWait = 1; }
		when ('no_wait'){ $doWait = 0; }
		default		{ die "Error: improper wait parameter $doWait\n"; }
	}		
	die "Error: The number of channels must equal the number of values.\n" if (@input % 2);
	my $length = @input / 2;
	die "Error: must have at least one channel/value pair specified to input.\n" unless (@input > 1);
	# Extract the first and last half of input array
	my @channels = @input[0 .. ($length-1)];
	my @values = @input[$length .. $#input];
	foreach my $chan_name (@channels){
		die "Error: channel 'name' not string.\n" unless ( $chan_name =~ m/^[A-Z][0-9]:.+/ )
	}
	foreach my $value (@values){
		die "Error: value undefined.\n" unless ( defined $value );
	}
	my @chanval = (@channels, @values);
	caPutHelper(\@chanval, $doWait);
}

################################ caPutHelper
my %_put_done = ();#this handles a list of what's been changed or not

sub caPutHelper {
  # shift input parameters in order
  my $chanval_ref = shift;
  my $doWait = shift;
  # replace the input array with a full array of channels and values
  @_ = @$chanval_ref;
  my @chans;
  my $ii;
  
  my $n = @_ / 2;
  my $put_timeout = $n * 0.020 + 1;
  # Initialize all the channels
  for ($ii=0; $ii < $n; $ii++) {
    $chans[$ii] = CA->new($_[$ii]);
    $_put_done{$_[$ii]} = 0;
  }
  # Flush the buffers and connect.
  eval {CA->pend_io($put_timeout);};
  if ($@) {
	print "Error: $@\n";  
    for ($ii = 0; $ii < $n; $ii++) {
	print STDERR "--$_[$ii]--  didn't connect.\n" unless $chans[$ii]->is_connected;
      }
  }

  # Queue the put request
  
  for ($ii=0; $ii < $n; $ii++) {
     my $data = $_[$ii+$n];
     if (length($data)> 39) {
	print STDERR "ERROR: String argument for channel $_[$ii] is too long.\n\t  $data\n";
        $data = substr($data,0, 39); 
     } 
     if($doWait == 1) { 
	$chans[$ii]->put_callback(\&caPut_callback,$data) if $chans[$ii]->is_connected;
      } else {
 	$chans[$ii]->put($data) if $chans[$ii]->is_connected;
      } 	
  }
  # Flush the buffers 
  CA->pend_io($CA_timeout);


    my $catchBadOnes = 0;
    if($doWait == 1){
        for($ii=0; $ii < $n; $ii++){
	    $catchBadOnes = 0;
            while($_put_done{$_[$ii]} != 0){
	        $catchBadOnes += 1;
	        CA->pend_event(0.001);
            }
	    print "Channel $_[$ii] changed in $catchBadOnes successive iterations.\n" unless $catchBadOnes == 0;
            }
    } else {
      usleep(1);    # this seems to be necessary,  but I don't know why....
      # If I don't include it,  then caSwitch will fail if the calling script exits immediately
      # afterwards.  A usleep is sufficient to complete whatever has to finish.
    }
    return 1;
}

sub caPut_callback {
  my ($chan,$status,$data) = @_;
  $_put_done{$chan} = 1;
}

################################ caToggle
our $callback_data;
sub caToggle {
  my $var = $_[0];
  my $val;

  my $chan = CA->new($var);
  CA->pend_io($CA_timeout);
  
  # Register a callback with the CA handler
  $chan->get_callback(\&toggle_callback, "DBR_LONG");
  # Note that callback processing uses pend_event
  CA->pend_event(0.1);
  
  if ($callback_data == 0) {
    $chan->put(1);
    $val = 0;
  } elsif( $callback_data == 1) {
    $chan->put(0);
    $val = 0;
  } else {
    $val = $callback_data;  # This case handles a large numeric
  }
  CA->pend_io($CA_timeout);
  usleep(1);    # this seems to be necessary,  but I don't know why....
  # If I don't include it,  then caSwitch will fail if the calling script exits immediately
  # afterwards.  A usleep is sufficient to complete whatever has to finish.

  return $val;
      
}

sub toggle_callback {
  my ($chan, $status, $data) = @_;
  die $status if $status;
  $callback_data = $data;
}

################################ caSwitch
sub caSwitch {
  my (@sw1, @sw2, @sw1r, @sw2r);
  my $ii;
  my $n = int(@_ / 2);
  
  # Initialize all the input channels
  for ($ii=0; $ii < $n; $ii++) {
    $sw1[$ii] = CA->new("$_[$ii]_SW1");
    $sw2[$ii] = CA->new("$_[$ii]_SW2");
    $sw1r[$ii] = CA->new("$_[$ii]_SW1R");
    $sw2r[$ii] = CA->new("$_[$ii]_SW2R");
  }
  # Flush the buffers and connect to server.
  CA->pend_io($CA_timeout);
  
  # Get the current readback values
  for ($ii=0; $ii < $n; $ii++) {
    $sw1r[$ii]->get;
    $sw2r[$ii]->get;
  }
  # Flush the buffers and connect to server.
  CA->pend_io($CA_timeout);
  
  # build the command word doing lots of nifty binary
  for ($ii =0; $ii < $n; $ii++) {
      
    my $ch1tmp = 0;   # These are temp variables used to build the control word
    my $ch2tmp = 0;
    my $ch1mask = 0;  # These are mask variables used during command catenation
    my $ch2mask = 0;  
    my $ch1word = 0;  # These are the control words that get written.
    my $ch2word = 0;
    my $rdbk1 = $sw1r[$ii]->value; 
    my $rdbk2 = $sw2r[$ii]->value; 


    my @sp = split(/\W+/, $_[$ii+$n]);
    foreach my $item (@sp) {
	$item = uc($item);
    SWITCH: {
	if ($item =~ /INPUT/)  {  $ch1tmp = $ch1tmp  | 1<<2 ; last SWITCH; }
	if ($item =~ /OFFSET/) {  $ch1tmp = $ch1tmp  | 1<<3 ; last SWITCH; }
	if ($item =~ /FM1$/) {  $ch1tmp = $ch1tmp  | 1<<4 ;  last SWITCH; }
	if ($item =~ /FM2/) {  $ch1tmp = $ch1tmp  | 1<<6;  last SWITCH; }
	if ($item =~ /FM3/) {  $ch1tmp = $ch1tmp  | 1<<8; last SWITCH; }
	if ($item =~ /FM4/) {  $ch1tmp = $ch1tmp  | 1<<10; last SWITCH; }
	if ($item =~ /FM5/) {  $ch1tmp = $ch1tmp  | 1<<12; last SWITCH; }
	if ($item =~ /FM6/) {  $ch1tmp = $ch1tmp  | 1<<14;last SWITCH; }
	if ($item =~ /FM7/) {  $ch2tmp = $ch2tmp  | 1<<0; last SWITCH; }
	if ($item =~ /FM8/) {  $ch2tmp = $ch2tmp  | 1<<2;last SWITCH; }
	if ($item =~ /FM9/) {  $ch2tmp = $ch2tmp  | 1<<4; last SWITCH; }
	if ($item =~ /FM10/) {  $ch2tmp = $ch2tmp  | 1<<6;  last SWITCH; }
	if ($item =~ /LIMIT/)    {  $ch2tmp = $ch2tmp  | 1<<8; last SWITCH; }
	if ($item =~ /DECIMATE/) {  $ch2tmp = $ch2tmp  | 1<<9; last SWITCH; }
	if ($item =~ /OUTPUT/)   {  $ch2tmp = $ch2tmp  | 1<<10; last SWITCH; }
	if ($item =~ /HOLD/)     {  $ch2tmp = $ch2tmp  | 1<<11; last SWITCH; }
	if ($item =~ /ALL/)    {  
			     	$ch1tmp = $ch1tmp | 21852; # binary code 
        			$ch2tmp = $ch2tmp | 3925;  # binary code
				last SWITCH; }
	if ($item =~ /FMALL/) {    
				$ch1tmp = $ch1tmp | 21840; # binary code
				$ch2tmp = $ch2tmp | 85;
				last SWITCH; }	
	if ($item =~ /ON/) {
	    # We turn on these guys.
	    $ch1word = ($ch1word & ~$ch1tmp) | (~$rdbk1 & $ch1tmp);
	    $ch2word = ($ch2word & ~$ch2tmp) | (~$rdbk2 & $ch2tmp);
	    $ch1tmp = 0;
	    $ch2tmp = 0;
	    last SWITCH;
	}
	if ($item =~ /OFF/) {
	    # We turn off these guys.
	    $ch1word = ($ch1word & ~$ch1tmp) | ($rdbk1 & $ch1tmp);
	    $ch2word = ($ch2word & ~$ch2tmp) | ($rdbk2 & $ch2tmp);
	    $ch1tmp = 0;
	    $ch2tmp = 0;
	    last SWITCH;
	}
	print "Bad command string didn't match any of: \n";
        print "     FM1..10,  INPUT, OFFSET, LIMIT, DEC, OUTPUT, HOLD \n";
      }
  }
    
    
    # Put the values into the queeue
    $sw1[$ii]->put($ch1word);
    $sw2[$ii]->put($ch2word);
  }
  
  # Flush the buffers and connect to server.
  CA->pend_io($CA_timeout);
  usleep(1);    # this seems to be necessary,  but I don't know why....
  # If I don't include it,  then caSwitch will fail if the calling script exits immediately
  # afterwards.  A usleep is sufficient to complete whatever has to finish.
  return 1;
}

################################ caSwitchS
sub caSwitchS {
  my (@sw1s, @sw2s);
  my $ii;
  my $n = int(@_ / 2);

  # Initialize all the input channels
  for ($ii=0; $ii < $n; $ii++) {
    $sw1s[$ii] = "$_[$ii]_SW1S";
    $sw2s[$ii] = "$_[$ii]_SW2S";
  }
  
  for ($ii =0; $ii < $n; $ii++) {
    my $on_command = $_[$ii + $n];
    my $ch1word = 0;  # These are the control words that get written.
    my $ch2word = 0;
    my @sp = split(/\W+/, $_[$ii+$n]);
    foreach my $item (@sp){
	if ($item !~ /^(INPUT|OFFSET|FM([1-9]0?|ALL)|LIMIT|DECIMATE|OUTPUT|HOLD|ALL)$/) {
        	print "Bad command string didn't match any of: \n";
	        print "     FM1..10,  INPUT, OFFSET, LIMIT, DEC, OUTPUT, HOLD \n";
      	}
    }
    if ($on_command =~ /INPUT/)   { $ch1word += 1<<2 ; }
    if ($on_command =~ /OFFSET/)  {  $ch1word += 1<<3 ; }
    if ($on_command =~ /FM1\b/) {  $ch1word += 1<<4 ; }
    if ($on_command =~ /FM2/)     {  $ch1word += 1<<6;  }
    if ($on_command =~ /FM3/)     {  $ch1word += 1<<8;  }
    if ($on_command =~ /FM4/)     {  $ch1word += 1<<10; }
    if ($on_command =~ /FM5/)     {  $ch1word += 1<<12; }
    if ($on_command =~ /FM6/)     {  $ch1word += 1<<14; }
    if ($on_command =~ /FM7/)     {  $ch2word += 1<<0;  }
    if ($on_command =~ /FM8/)     {  $ch2word += 1<<2;  }
    if ($on_command =~ /FM9/)     {  $ch2word += 1<<4;  }
    if ($on_command =~ /FM10/)    {  $ch2word += 1<<6;  }
    if ($on_command =~ /LIMIT/)   {  $ch2word += 1<<8;  }
    if ($on_command =~ /DECIMATE/){  $ch2word += 1<<9;  }
    if ($on_command =~ /OUTPUT/)  {  $ch2word += 1<<10; }
    if ($on_command =~ /HOLD/)    {  $ch2word += 1<<11; }
    if ($on_command =~ /\bALL/)   {  $ch1word += 0b0101010101011100; # binary 
    	                             $ch2word += 0b0000111101010101;  }
    if ($on_command =~ /FMALL/)   {  $ch1word += 0b0101010101010000; # binary
             	              	     $ch2word += 0b0000000001010101;    }

    push @sw1s, $ch1word;
    push @sw2s, $ch2word; 
  }

  caPut(@sw1s, {wait=>'wait'});
  caPut(@sw2s, {wait=>'wait'});
  return 1;
}

################################ caStep

sub linear {
        my ($currValue,$factor) = @_;
        return $currValue+=$factor;
}

sub geometric {
        my ($currValue,$factor) = @_;
        return $currValue*=$factor;
}

sub caStep {
    my @input = @_;
    # Initialize all optional arguments to their default values
    my $delay = 1.0;
    if(ref($input[$#input]) eq 'HASH'){
            my $optRef = pop @input;
            if(%$optRef ~~ /delay/) { $delay = $optRef->{'delay'}; }
    }
    if($delay <= 0){die 'Delay must be a non-zero positive number.';}
    unless($#input%2){die 'You\'re missing a channel/step pair. We found an odd number of arguments.';}

    my(@channels,@steps);
    #go pair by pair and push to their respective channel/steps
    for(my $i=0;$i<$#input;$i+=2){
        push(@channels,$input[$i]);
        push(@steps,$input[$i+1]);
    }

    #this is for default steps for those that don't specify it
    my $default_numSteps;

    for my $ii (0 .. $#channels){
            my($channel,$step,$action,$factor,$numSteps) = ($channels[$ii],$steps[$ii]);
            given($step){
                when(m/^(\+|-|\*|\/)([^dB]*?)(?:,([0-9]*))?$/){
                    #print "No dB: ",$1,",",$2,"\n";
                    ($action,$factor,$numSteps) = ($1,$2,$3);
                }
                when(m/^(\+|-|\*|\/)(.*?)dB(?:,([0-9]*))?$/){
                     #print "dB: ",$1,",",$2,"\n";
                     #Here, we have a dB gain.
                     # dB = 20*log(magnitude)
                        # thus a +dB is a *magnitude b/c:
                        #db1+db2 = 20*[log(m1)+log(m2)]
                        #        = 20*log(m1*m2)
                    ($action,$factor,$numSteps) = ("*",10.0**($2/20.0),$3);
                }
                default {
                    die "Error with $channel: $step is not valid.";
                }
            }

            unless(defined $default_numSteps){
                #this is the first channel
                $default_numSteps = defined $numSteps?$numSteps:1;
            }

            #if this channel doesn't define $numSteps, then
            #we'll set to default based on first channel
            #and if first channel doesn't define, then it's 1.
            $numSteps = defined $numSteps?$numSteps:$default_numSteps;

            #at this point, the script doesn't care whether we have a dB gain
            # or not. It just needs to understand how to compute things.

            if($action eq "+"){$factor *= 1.0; $action = sub{ linear($_[0],$factor) };}
            elsif($action eq "-"){$factor *= -1.0; $action = sub { linear($_[0],$factor) };}
            elsif($action eq "*"){$factor *= 1.0; $action = sub { geometric($_[0],$factor) };}
            elsif($action eq "/"){$factor **= -1.0; $action = sub { geometric($_[0],$factor) };}

            $steps[$ii] = {"channel"=>$channel,"string"=>$step,"action"=>$action,"factor"=>$factor,"numSteps"=>$numSteps};
    }

    #at this point, @steps is an array of hashes so we can easily reference appropriate actions!
    #       this loop goes on infinitely until we stop script execution or
    #@steps is an empty array, which ony happens if we execute all of our steps to their
    #       maximum counts (IE: numSteps == 0 -> delete element from @steps)

    #step 1 - get all initial values
    my @values = caGet(map $_->{channel}, @steps);

    #step 2 - record all initial values
    for my $i (0 .. $#values){
        $steps[$i]{value} = $values[$i];
        print $steps[$i]->{channel},"=",$steps[$i]->{value},"\n"
    }

    #step 3 - start looping
    while(@steps = grep $_->{numSteps} > 0, @steps){
        for my $step(@steps){
            $step->{value} = $step->{action}($step->{value});
            $step->{numSteps} -= 1;
            print $step->{channel},"=",$step->{value},"\n";
        }
        caPut( map{my $k = $_; map $_->{$k}, @steps} qw(channel value) );
        use Time::HiRes;#move to top
        sleep($delay);
    }

};
################################ end properly
1;

