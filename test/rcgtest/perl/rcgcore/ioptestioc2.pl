#!/usr/bin/perl -w -I /opt/cdscfg
##
##
use threads;
use threads::shared;
BEGIN { 
 $epicsbase = $ENV{'EPICS_BASE'};
        $epicslib = $epicsbase . "/lib/linux-x86_64";
        $epicsperl = $epicsbase . "/lib/perl";
        $rcgdir = $ENV{'RCG_DIR'};
        print "EPICSBASE = $epicsbase \n";
        print "EPICSLIB = $epicslib \n";
        print "EPICSPERL = $epicsperl \n";
        push @INC,"/opt/cdscfg/tst/stddir.pl";
        push @INC,"$epicsperl";
        push @INC,"$epicslib";
        push @INC,"$rcgdir/test/rcgtest/perl/modules";
        push @INC,"$rcgdir/rcgtest/perl";
        push @INC,"/usr/lib/x86_64-linux-gnu/perl5/5.28/auto";
        push @INC,"/usr/lib/x86_64-linux-gnu/perl5/5.28";
        push @INC,"$rcgdir/src/perldaq";
}
#use stdenv;
use CaTools;
use Time::HiRes qw( usleep );
use POSIX qw(ceil floor);
use File::Path;

use strict;


caPut("X2:IOP-SAM_TEST_STAT_1",2);

my $isok;
my @tresult;

(my $sec,my $min, my $hour, my $day, my $mon, my $year, my $wday, my $yday, my $isdst) = localtime(time);
$year += 1900;
my @abbr = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
print "\n\nSTARTING RCG CORE RUNTIME CODE TESTING **********************\n";
print "TIME: $hour:$min:$sec  $abbr[$mon]  $day $year \n\n";
print "KILLING EXISTING TASKS and (RE)STARTING IOP  **********************\n";
caPut("X2:IOP-SAM_TEST_STATUS","KILLING TASKS");

system("rtcds stop x2atstim02");
system("rtcds stop x2atstim04");
system("rtcds stop x2atstim16");
system("rtcds stop x2atstim32");
sleep(7);

my @strEpics = ("X2:FEC-110_RCG_VERSION","X2:FEC-110_MSG","X2:FEC-110_MSGDAQ");
my @strEpicsVals;
@strEpicsVals = caGet(@strEpics);

print "Testing RCG version number: $strEpicsVals[0]\n\n";


sleep(3);
caPut("X2:FEC-110_DACDT_ENABLE",1);
sleep(3);
# Need to bypass the DACKILL to get DAC duotone outputs
caPut("X2:IOP-SAM_DACKILL_HAM3_RESET",1);
sleep(2);
caPut("X2:IOP-SAM_DACKILL_HAM4_RESET",1);
sleep(3);
caPut("X2:FEC-110_DIAG_RESET",1);
sleep(3);

my $status = 0;

my @pcievals = ("X2:FEC-110_PCIE_STAT_0",
                "X2:FEC-110_PCIE_STAT_1",
                "X2:FEC-110_PCIE_STAT_2",
                "X2:FEC-110_PCIE_STAT_3",
                "X2:FEC-110_PCIE_STAT_4",
                "X2:FEC-110_PCIE_STAT_5",
                "X2:FEC-110_PCIE_STAT_6",
                "X2:FEC-110_PCIE_STAT_7",
                "X2:FEC-110_PCIE_STAT_8",
                "X2:FEC-110_PCIE_STAT_9");
my @slotstatus = ("X2:FEC-110_LPT_MON_0",
                  "X2:FEC-110_LPT_MON_1",
                  "X2:FEC-110_LPT_MON_2",
                  "X2:FEC-110_LPT_MON_3",
                  "X2:FEC-110_LPT_MON_4",
                  "X2:FEC-110_LPT_MON_5",
                  "X2:FEC-110_LPT_MON_6",
                  "X2:FEC-110_LPT_MON_7",
                  "X2:FEC-110_LPT_MON_8",
                  "X2:FEC-110_LPT_MON_9");
my @slotconfig = ("X2:FEC-110_LPT_MON_10",
                  "X2:FEC-110_LPT_MON_11",
                  "X2:FEC-110_LPT_MON_12",
                  "X2:FEC-110_LPT_MON_13",
                  "X2:FEC-110_LPT_MON_14",
                  "X2:FEC-110_LPT_MON_15",
                  "X2:FEC-110_LPT_MON_16",
                  "X2:FEC-110_LPT_MON_17",
                  "X2:FEC-110_LPT_MON_18",
                  "X2:FEC-110_LPT_MON_19");
my @diagVals;
my @diagSigs = ("X2:FEC-110_CPU_METER",
		"X2:FEC-110_CPU_METER_MAX",
		"X2:FEC-110_USR_TIME",
		"X2:FEC-110_ADC_WAIT",
		"X2:FEC-110_IRIGB_TIME",
		"X2:FEC-110_DUOTONE_TIME",
		"X2:FEC-110_DUOTONE_TIME_DAC",
		"X2:FEC-110_STATE_WORD",
		"X2:FEC-110_TIME_ERR",
		"X2:FEC-110_DIAG_WORD");
@diagVals = caGet(@diagSigs);

caPut("X2:IOP-SAM_TEST_STATUS","Verifying IOP Operation");
#Verify code is running by checking STATE_WORD bits 0 and 1
my $run = $diagVals[3];
my $runReq = "STATE & 3 = 0";
my $runTest;
 if(($diagVals[7] & 0x3) == 0)
{
	print "OK \n";
	$runTest = "PASS";
} else {
print "Verifying FE code started \t\t----\t ";
	print "FAIL !!!!!!!! EXITING\n";
	$runTest = "FAiL";
	exit(-1);
}

my @ioVals;
my @ioSigs = ("X2:FEC-110_ADC_STAT_0",
	      "X2:FEC-110_ADC_STAT_1",
	      "X2:FEC-110_ADC_STAT_2",
	      "X2:FEC-110_DAC_STAT_0",
	      "X2:FEC-110_DAC_STAT_1",
	      "X2:FEC-110_DAC_STAT_2",
	      "X2:FEC-110_DAC_STAT_3",
	      "X2:FEC-110_DAC_STAT_4");

print "Verifying ADC/DAC Modules Mapped \t----\t ";
@ioVals = caGet(@ioSigs);

my $ii;
my $err = 0;

#Checking bit 0 indicates whether or not IOP found and mapping I/O card.
my $tIo = $diagVals[3];
my $tIoReq = "             ";;
my $tIoTest = "PASS";;
for($ii=0;$ii<8;$ii++)
{
	if(!($ioVals[$ii] & 0x1)) { $err ++; }
}

if($err)
{
	print "FAIL !!!!!!!! EXITING\n";
	$tIoTest = "FAIL";;
	$status = -1;
}

caPut("X2:IOP-SAM_TEST_STATUS","Testing IOP Timing");
#Check sync src is TDS; means timing control card found and should
#sync up properly.
print "Verifying Timing Sync Src = TDS \t----\t ";
my $tSrc = $diagVals[8];
my $tSrcReq = "    LTDS      ";;
my $tSrcTest;
if($tSrc[8] != 32)
{
	print "FAIL !!!!!!!! EXITING\n";
	$tSrcTest = "FAIL";
	exit(-1);
} else {
	print "OK\n";
	$tSrcTest = "PASS";
}

print "\nVerifying Basic Timing ************************************** \n";

#Time between cycles should be 15usec
my $adcWait = $diagVals[3];
my $adcWaitReq = "   15 +/- 2  ";;
my $adcWaitTest;
if(($diagVals[3] > 12) && ($diagVals[3] < 17))
{
	$adcWaitTest = "PASS";
} else {
	$adcWaitTest = "FAIL";
	$status = -1;
}

#Time to process ADC data should be less than 5usec
my $adcTime = $diagVals[2];
my $adcTimeReq = "    <8 usec  ";;
my $adcTimeTest;
if($diagVals[2] < 8)
{
	$adcTimeTest = "PASS";
} else {
	$adcTimeTest = "FAIL";
	$status = -1;
}

#The total processing time of the IOP should be less than 10usec
my $pTime = $diagVals[1];
my $pTimeReq = "   <10 usec  ";;
my $pTimeTest;
if($diagVals[1] < 10)
{
	$pTimeTest = "PASS";
} else {
	$pTimeTest = "FAIL";
	$status = -1;
}
#Check ADC Duotone timing = 5 usec
my $dtTime = $diagVals[5];
my $dtTimeReq = "    5 usec   ";;
my $dtTimeTest;
if($diagVals[5] != 5)
{
	$dtTimeTest = "FAIL";
	$status = -1;
} else {
	$dtTimeTest = "PASS";
}

#Check IRIG-B timing - should be 13 usec plus or minus
my $irigTime = $diagVals[4];
my $irigTimeReq = "  11-13 usec ";;
my $irigTimeTest;
if(($diagVals[4] > 10) && ($diagVals[4] < 14))
{
	$irigTimeTest = "PASS";
} else {
	$irigTimeTest = "FAIL";
	$status = -1;
}

# Check DAC duotone timing - 70usec plus or minus
my $ddtTime = $diagVals[6];
my $ddtTimeReq = "  69-71 usec ";;
my $ddtTimeTest;
if(($diagVals[6] > 68) && ($diagVals[6] < 72))
{
	$ddtTimeTest = "PASS";
} else {
	$ddtTimeTest = "FAIL";
	$status = -1;
}


caPut("X2:IOP-SAM_TEST_STATUS","CLOCK SHIFT TEST +1 clk");
# CLOCK TIME SHIFT TESTING *************************************
print "\n\nSTARTING CYCLE SHIFT TESTING ******************************\n";
print "\tAdding 1 clock cycle\n";
caPut("X2:FEC-110_BUMP_CYCLE",1);
sleep(5);
@diagVals = caGet(@diagSigs);
#ADC duotone time should shift +15usec
my $bdtTime = $diagVals[5];
my $bdtTimeReq = "20 +/- 2 usec";;
my $bdtTimeTest;
if(($diagVals[5] > 18) && ($diagVals[5] < 22) && ($diagVals[7] & 2))
{
	$bdtTimeTest = "PASS";
} else {
	$bdtTimeTest = "FAIL";
	$status = -1;
}

#IRIG-B Timing should shift -15usec.
my $birigTime = $diagVals[4];
my $birigTimeReq = "99995 - 99999";;
my $birigTimeTest;
if(($diagVals[4] > 999995) && ($diagVals[4] < 999999))
{
	$birigTimeTest = "PASS";
} else {
	$birigTimeTest = "FAIL";
	$status = -1;
}
#DAC duotone should shift +20usec
my $bddtTime = $diagVals[6];
my $bddtTimeReq = "87-91 usec";;
my $bddtTimeTest;
if(($diagVals[6] > 86) && ($diagVals[6] < 92) && ($diagVals[7] & 2))
{
	$bddtTimeTest = "PASS";
} else {
	$bddtTimeTest = "FAIL";
	$status = -1;
}

caPut("X2:IOP-SAM_TEST_STATUS","CLOCK SHIFT TEST -1 clk");
# Skip one cycle to get back to normal
print "\tSkipping 1 clock cycle\n";
caPut("X2:FEC-110_BUMP_CYCLE",-1);
sleep(3);
# Skip one cycle to simulate clock loss
print "\tSkipping 1 clock cycle\n";
caPut("X2:FEC-110_BUMP_CYCLE",-1);
sleep(3);
@diagVals = caGet(@diagSigs);
#ADC duotone time should shift -15usec
my $cdtTime = $diagVals[5];
my $cdtTimeReq = "-10 +/- 2 usec";;
my $cdtTimeTest;
if(($diagVals[5] > -11) && ($diagVals[5] < -7) && ($diagVals[7] & 2))
{
	$cdtTimeTest = "PASS";
} else {
	$cdtTimeTest = "FAIL";
	$status = -1;
}
#IRIG-B Timing should shift +15usec.
my $cirigTime = $diagVals[4];
my $cirigTimeReq = "26 - 28 usec";;
my $cirigTimeTest;
if(($diagVals[4] > 25) && ($diagVals[4] < 29) && ($diagVals[7] & 2))
{
	$cirigTimeTest = "PASS";
} else {
	$cirigTimeTest = "FAIL";
	$status = -1;
}
#DAC duotone should shift -20usec
my $cddtTime = $diagVals[6];
my $cddtTimeReq = "51-53 usec";;
my $cddtTimeTest;
if(($diagVals[6] > 50) && ($diagVals[6] < 54))
{
	$cddtTimeTest = "PASS";
} else {
	$cddtTimeTest = "FAIL";
	$status = -1;
}
print "END CYCLE SHIFT TESTING ***********************************\n";
caPut("X2:FEC-110_BUMP_CYCLE",1);
# END CLOCK TIME SHIFT TEST ************************************

# CHANNEL HOP / ADC TIMEOUT TESTING *************************************
print "\n\nSTARTING CHANNEL HOP / ADC TIMEOUT TESTING ****************\n";
print "\tCausing Channel Hop\n";
caPut("X2:IOP-SAM_TEST_STATUS","CHANNEL HOP TEST ");
caPut("X2:FEC-110_BUMP_ADC",4);
sleep(9);
@diagVals = caGet(@diagSigs);
sleep(2);
@diagVals = caGet(@diagSigs);

#Channel hop detection should show up in state word bit 0 (code stopped) and bit 2 (channel hop)
my $chop = $diagVals[7]; #& 0x5;
my $chopReq = "code exit 0";;
my $chopTest;
if(($diagVals[7] & 0x1) && ($diagVals[7] & 0x4))
{
	$chopTest = "PASS";
} else {
	$chopTest = "FAIL";
	$status = -1;
}
sleep(1);
print "\tRestarting IOP  \n";
system("sudo rmmod x2iopsam");
sleep(2);
system("sudo insmod /opt/rtcds/tst/x2/target/x2iopsam/bin/x2iopsam.ko");
sleep(30);

print "\tCausing ADC Timeout\n";
caPut("X2:IOP-SAM_TEST_STATUS","ADC TIMEOUT TEST ");
caPut("X2:FEC-110_BUMP_ADC",-4);
sleep(5);
@diagVals = caGet(@diagSigs);
#ADC timeout 
my $adcto = $diagVals[9] & 0x1;
my $adctoReq = "code exit 1";;
my $adctoTest;
if($diagVals[9] & 0x1)
{
	$adctoTest = "PASS";
} else {
	$adctoTest = "FAIL";
	$status = -1;
}
sleep(1);
print "\tRestarting IOP  \n";
system("sudo rmmod x2iopsam");
sleep(2);
system("sudo insmod /opt/rtcds/tst/x2/target/x2iopsam/bin/x2iopsam.ko");
sleep(5);


print "END CHANNEL HOP / ADC TIMEOUT TESTING *********************\n";

# TESTING WITH CONTROL CODE MODULES AT 32K, 16K, 4K AND 2K *************************************
# Need to bypass the DACKILL to get DAC duotone outputs
caPut("X2:IOP-SAM_DACKILL_HAM3_RESET",1);
sleep(2);
caPut("X2:IOP-SAM_DACKILL_HAM4_RESET",1);
sleep(3);
print "\n\nSTARTING CONTROL PROCESS TESTING ****************\n";
print "\tStarting added test models at 32K, 16K, 4K and 2K  \n";
caPut("X2:IOP-SAM_TEST_STATUS","RESTART IOP PROCESS");
system("rtcds stop x2iopsam");
system("rtcds start x2iopsam");
caPut("X2:IOP-SAM_TEST_STATUS","START 2/4/16/32K PROCESSES");
system("rtcds start x2atstim32");
system("rtcds start x2atstim16");
system("rtcds start x2atstim04");
system("rtcds start x2atstim02");
sleep(5);
caPut("X2:IOP-SAM_DACKILL_HAM3_RESET",1);
caPut("X2:IOP-SAM_DACKILL_HAM4_RESET",1);
caPut("X2:ATS-TIM32_DACKILL_RESET",1);
sleep(2);

caPut("X2:IOP-SAM_TEST_STATUS","2/4/16/32K TESTING ");
# VERIFY SUBPROCESSES SYNC TO IOP  *************************************
$err = 0;
my @diagValsSync;
my @diagSigsSync = ("X2:FEC-111_TIME_ERR",
		"X2:FEC-112_TIME_ERR",
		"X2:FEC-113_TIME_ERR",
		"X2:FEC-114_TIME_ERR");

@diagValsSync = caGet(@diagSigsSync);
my $sSync = 0x8;
my $sSyncReq = "  IOP (8)   ";;
my $sSyncTest = "PASS";
for($ii=0;$ii<4;$ii++)
{
	if($diagValsSync[$ii] != 0x8) { 
		$sSync = $diagValsSync[$ii];
		$sSyncTest = "FAIL";
		$status = -1; 
	}
	
}
# VERIFY ADC/DAC Status  *************************************
@ioVals = caGet(@ioSigs);

$err = 0;

#Check ADC modules
my $sAdc = 31;
my $tAdc = 31;
my $sAdcReq = "     OK=31    ";;
my $sAdcTest = "PASS";
for($ii=0;$ii<3;$ii++)
{
	if($ioVals[$ii] != $tAdc) { $status = -1; $sAdc = $ioVals[$ii]; $sAdcTest  = "FAIL";}
}
#Check 18bit DAC modules
my $sDac = 0xf;
my $sDacReq = "     OK=287  ";;
my $tDac = 287;
my $sDacTest = "PASS";
for($ii=3;$ii<8;$ii++)
{
	if($ioVals[$ii] != $tDac) { $status = -1; $sDac = $ioVals[$ii]; $sDacTest = "FAIL";}
}

# VERIFY Slave Cycle Times  *************************************
$err = 0;
my @cycleVals;
my @cycleSigs = ("X2:FEC-111_ADC_WAIT",
		  "X2:FEC-112_ADC_WAIT",
		  "X2:FEC-113_ADC_WAIT",
		  "X2:FEC-114_ADC_WAIT");
my @cycleNormHi = (33,64,248,492);
my @cycleNormLo = (27,58,242,484);
@cycleVals = caGet(@cycleSigs);
my @sTimeTest = ("PASS","PASS","PASS","PASS");
for($ii=0;$ii<4;$ii++)
{
	if(($cycleVals[$ii] < $cycleNormLo[$ii]) || ($cycleVals[$ii] > $cycleNormHi[$ii])) 
	{ 
		$sTimeTest[$ii] = "FAIL";
		$status = -1;;
	}
	
}
# VERIFY Slave TestPoint Counts = 0  			*************************************
# If not zero at this point, may be problem with mbuf   *************************************
$err = 0;
my @tpVals;
my @tpSigs = ("X2:FEC-110_TP_CNT",
		  "X2:FEC-111_TP_CNT",
		  "X2:FEC-112_TP_CNT",
		  "X2:FEC-113_TP_CNT",
		  "X2:FEC-114_TP_CNT");
@tpVals = caGet(@tpSigs);
my $sTpReq = "      0      ";
my $sTp = 0;
my $sTpTest = "PASS";
for($ii=0;$ii<5;$ii++)
{
	if($tpVals[$ii] != 0) 
	{ 
		$sTpTest = "FAIL";
		$status = -1;;
		$sTp ++;
	}
	
}
# VERIFY Slave CPU Time  			*************************************
# Models are lightly loaded, so higher than 
# 	expected time indicates other problems   *************************************
my @cpumVals;
my @cpumSigs = ("X2:FEC-111_CPU_METER",
		  "X2:FEC-112_CPU_METER",
		  "X2:FEC-113_CPU_METER",
		  "X2:FEC-114_CPU_METER");
my @cpumNormHi = (10,12,16,20);
@cpumVals = caGet(@cpumSigs);
my @sCpuTest = ("PASS","PASS","PASS","PASS");
for($ii=0;$ii<4;$ii++)
{
	if($cpumVals[$ii] > $cpumNormHi[$ii]) 
	{  
		$sCpuTest[$ii] = "FAIL";
		$status = -1;;
	}
	
}

# VERIFY All DAC Outputs are zero	*************************************
# Testing to verify no memory feed thru to DAC outputs, which
# may be caused by bad mbuf or decimation bleed thru. ***********************
$err = 0;
my @swSetsOn = ("OUTPUT ON","OUTPUT ON","OUTPUT ON","OUTPUT ON",
		"OUTPUT ON","OUTPUT ON","OUTPUT ON","OUTPUT ON","OUTPUT ON","OUTPUT ON");
my @swSetsOff = ("OUTPUT OFF","OUTPUT OFF","OUTPUT OFF","OUTPUT OFF",
		 "OUTPUT OFF","OUTPUT OFF","OUTPUT OFF","OUTPUT OFF","OUTPUT OFF","OUTPUT OFF");
my @filters = ("X2:ATS-TIM32_T1_ADC_FILTER_1","X2:ATS-TIM16_T1_ADC_FILTER_1",
		"X2:ATS-TIM04_T1_ADC_FILTER_1","X2:ATS-TIM02_T1_ADC_FILTER_1",
		"X2:ATS-TIM32_T1_DAC_FILTER_3","X2:ATS-TIM32_T1_DAC_FILTER_4","X2:ATS-TIM32_T1_DAC_FILTER_5",
		"X2:ATS-TIM32_T1_DAC_FILTER_6","X2:ATS-TIM32_T1_DAC_FILTER_7","X2:ATS-TIM32_T1_DAC_FILTER_8");
my @ats16swSetsOff = ("OUTPUT OFF","OUTPUT OFF","OUTPUT OFF","OUTPUT OFF","OUTPUT OFF","OUTPUT OFF","OUTPUT OFF","OUTPUT OFF",
		 "OUTPUT OFF","OUTPUT OFF","OUTPUT OFF","OUTPUT OFF","OUTPUT OFF","OUTPUT OFF","OUTPUT OFF","OUTPUT OFF");
my @ats16filters = ("X2:ATS-TIM16_T1_DAC_FILTER_1","X2:ATS-TIM16_T1_DAC_FILTER_2",
		"X2:ATS-TIM16_T1_DAC_FILTER_3","X2:ATS-TIM16_T1_DAC_FILTER_4","X2:ATS-TIM16_T1_DAC_FILTER_5",
		"X2:ATS-TIM16_T1_DAC_FILTER_6","X2:ATS-TIM16_T1_DAC_FILTER_7","X2:ATS-TIM16_T1_DAC_FILTER_8",
		"X2:ATS-TIM16_T2_DAC_FILTER_1","X2:ATS-TIM16_T2_DAC_FILTER_2",
		"X2:ATS-TIM16_T2_DAC_FILTER_3","X2:ATS-TIM16_T2_DAC_FILTER_4","X2:ATS-TIM16_T2_DAC_FILTER_5",
		"X2:ATS-TIM16_T2_DAC_FILTER_6","X2:ATS-TIM16_T2_DAC_FILTER_7","X2:ATS-TIM16_T2_DAC_FILTER_8");
my @dacVals;
my @dacOutputs = ("X2:FEC-110_DAC_OUTPUT_0_1","X2:FEC-110_DAC_OUTPUT_0_2","X2:FEC-110_DAC_OUTPUT_0_3","X2:FEC-110_DAC_OUTPUT_0_4",
		"X2:FEC-110_DAC_OUTPUT_0_5","X2:FEC-110_DAC_OUTPUT_0_6",
		"X2:FEC-110_DAC_OUTPUT_1_0","X2:FEC-110_DAC_OUTPUT_1_1","X2:FEC-110_DAC_OUTPUT_1_2","X2:FEC-110_DAC_OUTPUT_1_3",
		"X2:FEC-110_DAC_OUTPUT_1_4","X2:FEC-110_DAC_OUTPUT_1_5","X2:FEC-110_DAC_OUTPUT_1_6","X2:FEC-110_DAC_OUTPUT_1_7",
		"X2:FEC-110_DAC_OUTPUT_2_0","X2:FEC-110_DAC_OUTPUT_2_1","X2:FEC-110_DAC_OUTPUT_2_2","X2:FEC-110_DAC_OUTPUT_2_3",
		"X2:FEC-110_DAC_OUTPUT_2_4","X2:FEC-110_DAC_OUTPUT_2_5","X2:FEC-110_DAC_OUTPUT_2_6","X2:FEC-110_DAC_OUTPUT_2_7");

# Turn off the timing output signals to DAC modules.
$isok = caSwitch(@filters,@swSetsOff);
$isok = caSwitch(@ats16filters,@ats16swSetsOff);
sleep(9);
@dacVals = caGet(@dacOutputs);
my $sDac0 = 0;
my $sDac0Req = "      0      ";
my $sDac0Test = "PASS";
# Turn on the timing output signals to DAC modules.
$isok = caSwitch(@filters,@swSetsOn);

# LOOPBACK TIMING  TESTING *************************************
my @timingVals;
my @timingSigs = ("X2:FEC-110_TIMING_TEST_64K",
		  "X2:FEC-110_TIMING_TEST_64KA",
		  "X2:FEC-110_TIMING_TEST_64KB",
		  "X2:FEC-110_TIMING_TEST_32K",
		  "X2:FEC-110_TIMING_TEST_32KA",
		  "X2:FEC-110_TIMING_TEST_32KB",
		  "X2:FEC-110_TIMING_TEST_16K",
		  "X2:FEC-110_TIMING_TEST_16KA",
		  "X2:FEC-110_TIMING_TEST_16KB",
		  "X2:FEC-110_TIMING_TEST_04K",
		  "X2:FEC-110_TIMING_TEST_04KA",
		  "X2:FEC-110_TIMING_TEST_04KB",
		  "X2:FEC-110_TIMING_TEST_02K",
		  "X2:FEC-110_TIMING_TEST_02KA",
		  "X2:FEC-110_TIMING_TEST_02KB");
my @timingNorms = (61,76,61,122,137,122,198,198,183,488,503,534,610,625,610);
@timingVals = caGet(@timingSigs);
my @sLoopTest = ("PASS","PASS","PASS","PASS","PASS","PASS","PASS","PASS","PASS","PASS","PASS","PASS","PASS","PASS","PASS");
for($ii=0;$ii<10;$ii++)
{
	if($timingVals[$ii] != $timingNorms[$ii]) { 
		$sLoopTest[$ii]  = "FAIL";
		$status = -1;;
	}
	
}
# END OF TEST - RETURN ERROR STATUS ****************************************************

open(OUTM,">/tmp/rcgtest/data/rcgCoreTestData.dox") || die "cannot open test data file for writing";
print OUTM <<END;
/*!	\\page rcgCoreTest RCG Core Test Report
*	\\verbatim
*************************************************************************************************
RCG CORE RUNTIME CODE TEST REPORT   *************************************************************
TIME: $hour:$min:$sec  $abbr[$mon]  $day $year
Testing SVN version number: $strEpicsVals[0]
*************************************************************************************************

PURPOSE: Verify the basic operation of IOP and User Application software timing and I/O.

OVERVIEW: This test is used to check the primary diagnostics provided by the RCG runtime code and
verify all information is within spec. This script also performs some "distructive testing" to 
verify that the code properly detects errors and deals with them appropriately.

TEST REQUIREMENTS:
This test script (userapps/cds/test/scripts/x1diagtest.pl) is designed to run on x2ats in the
LLO CDS test system. This script will automatically load the latest installed versions of:
	- x2iopsam (IOP model)
	- x2atstim32 (32K test model)
	- x2atstim16 (16K test model)
	- x2atstim04 ( 4K test model)
	- x2atstim02 ( 2K test model)
This test also requires that 16bit and 18bit DAC loopback chassis be connected to the designated 
I/O chassis.

TEST PROCEDURE:
	- Kill all running processes.
	- Load and start IOP model.
	- Verify all I/O modules detected.
	- Verify IOP timing via IOP EPICS diagnostic channels.
	- Perform destructive testing:
		- Force add/skip of ADC module clocks.
		- Force ADC channel hopping
		- Force ADC timeout
	- Test timing of user app models.
		- Load and start test apps.
		- Verify that they found I/O properly.
		- Verify sync to IOP
		- Check cycle times to verify operation at proper rate.
		- Check CPU times and compare to past averages.
	- Test I/O
		- Confirm loop backtimes from 16bit and 18bit DAC modules with codes running
		  at different rates.    

MEDM SCREENS:
    - X2DIAGTEST.adl

-------------------------------------------------------------------------------------------------

      TEST				REQUIREMENT		MEASURE		PASS/FAIL
-------------------------------------------------------------------------------------------------
Verify IOP is running		----	$runReq	----	$run	----	$runTest
Verity IOP I/O Mapping		----	$tIoReq	----	$tIo	----	$tIoTest
Verify IOP Timing
	Time Sync Source	----	$tSrcReq	----	$tSrc	----	$tSrcTest
	Code cycle time		----	$adcWaitReq	----	$adcWait	----	$adcWaitTest
	ADC Data Process Time	----	$adcTimeReq	----	$adcTime	----	$adcTimeTest
	Total Processing Time	----	$pTimeReq	----	$pTime	---- 	$pTimeTest
	ADC Duotone Timing	----	$dtTimeReq	----	$dtTime	----	$dtTimeTest
	IRIG-B Timing   	----	$irigTimeReq	----	$irigTime	----	$irigTimeTest
	DAC Duotone TIming	----	$ddtTimeReq	----	$ddtTime	----	$ddtTimeTest

Verify Detection of Clocking Errors ****
Add a 64K clock pulse
	ADC Duotone Timing      ----	$bdtTimeReq	----	$bdtTime	----    $bdtTimeTest
        IRIG-B Timing   	----	$birigTimeReq	----	$birigTime	----    $birigTimeTest
        DAC Duotone Timing      ----	$bddtTimeReq	----	$bddtTime	----    $bddtTimeTest
Skip a 64K clock pulse
	ADC Duotone Timing      ----    $cdtTimeReq	----	$cdtTime	----    $cdtTimeTest
        IRIG-B Timing   	----    $cirigTimeReq	----	$cirigTime	----    $cirigTimeTest
        DAC Duotone Timing      ----    $cddtTimeReq	----	$cddtTime	----    $cddtTimeTest

Verify Detection of ADC Channel Hopping
	Force channel hop	----	$chopReq	----	$chop	----	$chopTest

Verify Detection of ADC Timeout (loss of clocks)
	Force bad ADC clock	----	$adctoReq	----	$adcto	----	$adctoTest

Start Testing with Slave Processes

Verify Slave Sync to IOP	----	$sSyncReq	----	$sSync	----	$sSyncTest
Verify I/O Connections
	ADC Modules     	----	$sAdcReq	----	$sAdc	----	$sAdcTest
	DAC Modules     	----	$sDacReq	----	$sDac	----	$sDacTest
Verify Cycle Times
	32K Model       	----	$cycleNormLo[0] to $cycleNormHi[0]	----	$cycleVals[0]	----	$sTimeTest[0]
	16K Model       	----	$cycleNormLo[1] to $cycleNormHi[1]	----	$cycleVals[1]	----	$sTimeTest[1]
	 4K Model       	----	$cycleNormLo[2] to $cycleNormHi[2]	----	$cycleVals[2]	----	$sTimeTest[2]
	 2K Model       	----	$cycleNormLo[3] to $cycleNormHi[3]	----	$cycleVals[3]	----	$sTimeTest[3]

Check & Record CPU Times	
	32K Model       	----	< $cpumNormHi[0]		----	$cpumVals[0]	----	$sCpuTest[0]
	16K Model       	----	< $cpumNormHi[1]		----	$cpumVals[1]	----	$sCpuTest[1]
	 4K Model       	----	< $cpumNormHi[2]		----	$cpumVals[2]	----	$sCpuTest[2]
	 2K Model       	----	< $cpumNormHi[3]		----	$cpumVals[3]	----	$sCpuTest[3]

Check & Record ADC->DAC LoopBack Times
- 18 Bit DAC Times
	IOP Model		----	$timingNorms[0]		----	$timingVals[0]	----	$sLoopTest[0]
	32K Model		----	$timingNorms[3]		----	$timingVals[3]	----	$sLoopTest[3]
	16K Model		----	$timingNorms[6]		----	$timingVals[6]	----	$sLoopTest[6]
	 4K Model		----	$timingNorms[9]		----	$timingVals[9]	----	$sLoopTest[9]
	 2K Model		----	$timingNorms[12]		----	$timingVals[12]	----	$sLoopTest[12]
- 16 Bit DAC Times
	IOP Model		----	$timingNorms[1]		----	$timingVals[1]	----	$sLoopTest[1]
	32K Model		----	$timingNorms[4]		----	$timingVals[4]	----	$sLoopTest[4]
	16K Model		----	$timingNorms[7]		----	$timingVals[7]	----	$sLoopTest[7]
	 4K Model		----	$timingNorms[10]		----	$timingVals[10]	----	$sLoopTest[10]
	 2K Model		----	$timingNorms[13]		----	$timingVals[13]	----	$sLoopTest[13]
- 20 Bit DAC Times
	IOP Model		----	$timingNorms[2]		----	$timingVals[2]	----	$sLoopTest[2]
	32K Model		----	$timingNorms[5]		----	$timingVals[5]	----	$sLoopTest[5]
	16K Model		----	$timingNorms[8]		----	$timingVals[8]	----	$sLoopTest[8]
	 4K Model		----	$timingNorms[11]		----	$timingVals[11]	----	$sLoopTest[11]
	 2K Model		----	$timingNorms[14]		----	$timingVals[14]	----	$sLoopTest[14]

Perform Basic Memory Checks
	Verify No TP Set	----	$sTpReq	----	$sTp	----	$sTpTest

       \\endverbatim
*\/

END
if($status == 0)
{
print "\nDIAG TEST COMPLETE - NO ERRORS\n\n\n";
print OUTM "\nDIAG TEST COMPLETE - NO ERRORS\n";
caPut("X2:IOP-SAM_TEST_STATUS","TEST COMPLETE - SUCCESS");
caPut("X2:IOP-SAM_TEST_STAT_1",1);
} else {
print "\nDIAG TEST COMPLETE - ERRORS !!!!!!!!!!!!!!!!!!!!!!!!!!!!\n";
print OUTM "\nDIAG TEST COMPLETE - ERRORS !!!!!!!!!!!!!!!!!!!!!!!!\n";
caPut("X2:IOP-SAM_TEST_STATUS","TEST COMPLETE - FAIL !!!!");
caPut("X2:IOP-SAM_TEST_STAT_1",0);
}
close OUTM;
system("cat /tmp/rcgtest/data/rcgCoreTestData.dox");
sleep(10);
caPut("X2:IOP-SAM_TEST_STATUS","TEST SYSTEM - IDLE");
system("sudo systemctl restart rts-daqd");
exit($status);
