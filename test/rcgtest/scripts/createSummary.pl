#!/usr/bin/perl -w 
##
##

my @testfiles = ("/tmp/rcgtest/data/biotest.dox", 
                "/tmp/rcgtest/data/dactest.dox",
                "/tmp/rcgtest/data/daqFiltTestData.dox", 
                "/tmp/rcgtest/data/decFiltTestData.dox",
                "/tmp/rcgtest/data/duotoneTestData.dox", 
                "/tmp/rcgtest/data/fmc2TestData.dox",
                "/tmp/rcgtest/data/fmcTestData.dox", 
                "/tmp/rcgtest/data/inputFiltTestData.dox",
                "/tmp/rcgtest/data/matrixTestData.dox", 
                "/tmp/rcgtest/data/rampmatrixTest.dox",
                "/tmp/rcgtest/data/simPartsTest.dox",
                "/tmp/rcgtest/data/eicTest.dox",
                "/tmp/rcgtest/data/xextpTest.dox",
                "/tmp/rcgtest/data/phase.dox",
                "/tmp/rcgtest/data/oscTest.dox",
                "/tmp/rcgtest/data/dktTest.dox",
                 "/tmp/rcgtest/data/sfmTestData.dox");
my@tests = ("BioTest: ","DAC test: ","DAQ Filter Test: ","Decimation Filter Test: ",
            "Duotone Test: ", "FMC2 Test: ","FMC Test: ","Input FIlter Test: ","Matrix Test: ",
            "Ramp Matrix: ","Simulink Parts: ","EPICS Input w/Control: ",
            "ExcTp Test: ","Phase Test","Oscillator Test: ","DACKILL Timed: ","Filter Module Test: ");
my $testpattern = "TEST SUMMARY";
my $pass = "PASS";
my $fail = "FAIL";

my $i = 0;
my $results_file = "/tmp/rcgtest/data/results.dox";
open(RESULTS,">$results_file");

print RESULTS "\/\*\!  \\page testsummary RCG Test Summary\n\n";
print RESULTS "\\verbatim \n";

$largest_string = length($tests[3]);
$largest_string += 4;

for(@testfiles) {
open(FILE,"<$_");
    $s = $tests[$i];
    $spaces = $largest_string - length($s);
    for($j=0;$j<$spaces;$j++) {
        $s .= " ";
    }
    print RESULTS "$s";

while(<FILE>) {
    my($line) = $_;
    if(/\Q$testpattern/ and /\Q$pass/) 
    {
        $s = "PASS";
        print RESULTS "$s\n";
    }
    if(/\Q$testpattern/ and /\Q$fail/) 
    {
        $s = "FAIL";
        print RESULTS "$s\n";
    }
}
close(FILE);
$i ++;
}
print RESULTS "\\endverbatim \n";
print RESULTS "\*\/";
close(RESULTS);
