#!/bin/bash

src_path=$1

export RTS_VERSION=${RTS_VERSION:-"undefined version"}

cd $src_path
doxygen "${src_path}/test/rcgtest/doc/doxygen.cfg"
cd /tmp/rcgtest/doc/latex
make pdf
cp /tmp/rcgtest/doc/latex/refman.pdf "${src_path}/test/rcgtest/doc/rcgtest.pdf"
