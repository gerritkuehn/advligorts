# track history of autotest runs
import json
import os.path as path


class History(object):
    def __init__(self, started=False, finished=False, passed=None):
        self.started = started
        self.finished = finished
        self.passed = passed

    def todict(self):
        return {k: v for k, v in self.__dict__.items() if k[0] != "_"}


def load_histories(history_path):
    """
    Return dictionary of History objects with keys as git SHA hash for the commit
    that was used for the test.

    :param history_path: a string.  Path to history file.
    :return: Dictionary of <git SHA1 hash>: History object
    """
    if path.exists(history_path):
        with open(history_path, "rt") as f:
            raw = json.load(f)
    else:
        raw = {}
    return {k: History(**v) for k, v in raw.items()}


def save_histories(history_path, histories):
    """
    Write the histories to a json file.

    :param history_path: path to json file to save histories.  Will be overwritten.
    :param histories: A dictionary of <git SHA1 hash>: History objects
    :return: None
    """

    todict = {k: v.todict() for k, v in histories.items()}
    with open(history_path, "wt") as f:
        json.dump(todict, f)


def add_history(history_path, key, history):
    """
    Add a history to a history file. If the file doesn't exist, it will be created.

    If there's already a history in the file with the same sha_hash, that entry will be overwritten.

    :param history_path: string. path to history file
    :param key: A unique key identifying the history
    :param history: History object
    :return: None
    """
    histories = load_histories(history_path)
    histories[key] = history
    save_histories(history_path, histories)
