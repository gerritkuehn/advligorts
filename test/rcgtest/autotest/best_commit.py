#!/usr/bin/python3
# find the most worthy commit for testing.
#
# 1. Most recent tag that looks like a clean version number
# 2. Most recent tag that looks like a version number
# 3. master branch
# 4. branches not merged to master, starting with latest updated, excluding packaging branches

import re

from collections import namedtuple
from git.repo.fun import BadName
import time

named_commit_by_hash = {}

class ParentIter(object):
    def __init__(self, commit):
        self.commit = commit
        self.count = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.count < len(self.commit.parents):
            old = self.count
            self.count += 1
            return get_named_commit(self.commit.parents[old])
        else:
            raise StopIteration()

class NamedCommit(object):
    def __init__ (self, names, commit):
        self.names = names
        self.commit = commit
        self.children = []
        self._parents_iter = ParentIter(self.commit)
        self._parents = None

        global named_commit_by_hash
        named_commit_by_hash[commit.hexsha] = self

    def __str__(self):
        date_sec = self.commit.committed_date
        committer = self.commit.committer
        ctime = time.gmtime(date_sec)
        date = time.strftime("%Y-%m-%d", ctime)
        return f"{date} {committer} {self.hexsha} named {', '.join(self.names)}"

    @property
    def parents(self):
        if self._parents is None:
            self._parents = list(self._parents_iter)
        return self._parents

    @property
    def hexsha(self):
        return self.commit.hexsha


def get_named_commit(commit):
    """
    Return a named commit for a hash if it already exists
    
    create a new one (with empty name) if it doesn't.
    :param commit: 
    :return: 
    """
    global named_commit_by_hash
    if commit.hexsha in named_commit_by_hash:
        return named_commit_by_hash[commit.hexsha]
    else:
        return NamedCommit([], commit)



bad_tags = ["4.2.7_dev2"]

def find_recent_dirty_tag(repo):
    """
    Find the most recent tag that looks like version number but is not clean.

    :param repo: a git.Repo pointing at an advligorts repository
    :return: the tag or None if nothing is found
    """
    verclean_re = re.compile(r"^\d+\.\d+\.\d+$")
    ver_re = re.compile(r"^\d+\.\d+\.\d+\w*$")

    clean_tags = [t for t in repo.tags if verclean_re.search(t.name)]

    tags = [t for t in repo.tags if ver_re.search(t.name) and t not in clean_tags and t.name not in bad_tags]
    tags = sorted(tags, reverse=True, key=lambda t: t.commit.committed_date)
    if len(tags) > 0:
        return tags[0]
    else:
        return None

def find_recent_clean_tag(repo):
    """
    Find the most recent tag that looks like version number and has nothing extra in it.

    :param repo: a git.Repo pointing at an advligorts repository
    :return: the tag or None if nothing is found
    """
    ver_re = re.compile(r"^\d+\.\d+\.\d+$")

    tags = [t for t in repo.tags if ver_re.search(t.name) if t.name not in bad_tags]
    tags = sorted(tags, reverse=True, key=lambda t: t.commit.committed_date)
    if len(tags) > 0:
        return tags[0]
    else:
        return None

def is_ancestor_of(parent, child):
    """
    Find out if parent is an ancestor commit of child
    specifically, returns true if parent and child are the same.
    :param parent: a git.Commit
    :param child: a git.Commit
    :return: True if parent is an ancestor, or the same commit as child
    """
    visited = set()
    stack = [child]
    while len(stack) > 0:
        commit = stack.pop()
        visited.add(commit)
        if commit == parent:
            return True
        for p in commit.parents:
            if p not in visited:
                stack.append(p)

    return False

def find_non_ancestors(commits, child):
    """
    Modify a set of commits to include only non-ancestors of child.
    :param commits: a set of git.Commit
    :param child: a git Commit
    :return: None
    """
    visited = set()
    stack = [child]
    while len(stack) > 0:
        commit = stack.pop()
        visited.add(commit)
        commits.discard(commit)
        for p in commit.parents:
            if p not in visited:
                stack.append(p)

def find_descendents(repo, parent):
    """
    Return a set of commits that descendents of parent, including parent
    :param commits: a set of commits
    :param parent:
    :return: A set of descendents of parent
    """

    commits = find_deserving_branches(repo)

    master = find_commit_by_ref(repo, "origin/master")
    master.names.append("origin/master")

    commits.append(master)

    commits = set(commits)

    stack = list(commits)

    while len(stack) > 0:
        commit = stack.pop()
        for p in commit.parents:
            p.children = []
            if p not in commits:
                stack.append(p)
                commits.add(p)

    for commit in commits:
        for p in commit.parents:
            p.children.append(commit)

    children = set([parent])

    stack = [parent]

    while len(stack) > 0:
        p = stack.pop()
        for c in p.children:
            if c not in children:
                stack.append(c)
                children.add(c)
    return children

def branch_commit_name(branch):
    """
    return a string useful for naming specific points on a branch
    """
    return f"{branch.name}_{branch.commit.committed_datetime.isoformat()}"


def find_deserving_branches(repo):
    """
    Find most deserving branches for testing.
    :param repo: a git.Repo object
    :return: a list of NamedCommits, named after branch + commit time.  Sorted by commit time.
    master and package branches omitted.  Ancestors of master ommitted.
    """

    master = repo.heads.master

    skip_res = [
        re.compile(r"^master$"),
        re.compile(r"^debian/"),
        re.compile(r"^HEAD$")
        ]

    branches = []


    # get remote branches
    for r in repo.remotes:
        for ref in r.refs:
            base_name = ref.name.split("/",1)[1]
            skip = False
            for reg in skip_res:
                if reg.search(base_name):
                    skip = True
                    break
            if not skip:
                branches.append(ref)

    commits = set([b.commit for b in branches])
    find_non_ancestors(commits, master.commit)
    find_non_ancestors(commits,repo.remotes[0].refs["debian/buster"].commit)
    branches = [b for b in branches if b.commit in commits]
    branches = sorted(branches, reverse=True, key=lambda b: b.commit.committed_date)

    return [NamedCommit([b.name, branch_commit_name(b)], b.commit) for b in branches]

def find_branch_by_name(repo, branchname):
    """
    Find a branch given a name
    :param branchname: a string
    :return: the branch as a NamedCommit or None
    """
    components = branchname.split("/")
    for r in repo.remotes:
        for ref in r.refs:
            ref_comps = ref.name.split("/")
            if len(ref_comps) >= len(components):
                tail = ref_comps[-len(components):]
                if "/".join(tail) == branchname:
                    return NamedCommit([branchname], ref.commit)

def find_commit_by_ref(repo, ref):
    """
    Finds a commit by reference and returns a named commit
    :param repo:
    :param ref: A reference, such as an SHA hash, or maybe a tag
    :return: NamedCommit (names are empty) or None if not found
    """
    try:
        commit = repo.commit(ref)
    except BadName:
        return None

    return NamedCommit([], commit)


def get_deserving_commits(repo):
    """
    Get a list of NamedCommits in order of priority we want to try.

    :param repo: a git.Repo object
    :return: a list of NamedCommits.  First item is highest priority.
    """

    commits = []

    # latest clean version
    tag = find_recent_clean_tag(repo)
    if tag is not None:
        commits.append( NamedCommit([tag.name], tag.commit))

    # latest unclean version
    tag = find_recent_dirty_tag(repo)
    if tag is not None:
        commits.append( NamedCommit([tag.name], tag.commit))

    # master branch
    master = find_commit_by_ref(repo, "origin/master")
    if master is not None:
        commits.append(NamedCommit(["master","origin/master"], master.commit))

    # other branches
    commits += find_deserving_branches(repo)

    return commits

def test(repo):
    commits = get_deserving_commits(repo)
    print("\n".join([str(c) for c in commits]))

    print(repo.remotes[0].refs["branch-2.1"].name)

    start = find_commit_by_ref(repo, '9c27eaf65a8037ce44bdf0bbb6d1c49489aa84ca')

    print("children")
    children = find_descendents(repo, start)
    for c in children:
        print(str(c))


if __name__ == "__main__":
    from git import Repo

    repo = Repo("/tmp/advligorts")
    #repo = Repo.clone_from('https://git.ligo.org/cds/advligorts.git', "/tmp/advligorts")
    #repo = Repo("/home/erik.vonreis/projects/advligorts")
    test(repo)