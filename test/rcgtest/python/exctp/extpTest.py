#!/usr/bin/python3
# Test report generation routines

from subprocess import call
from epics import PV
import sys
import time
import os
import os
import os.path as path

rcg_dir = os.environ['RCG_DIR']
sys.path.insert(0,path.join(rcg_dir, 'test/rcgtest/python'))
from cdstestsupport import testreport
from cdstestsupport import dtt, SFM

tr = testreport('xextpTest','EXC and TP Part',112)
tr.sectionhead('Purpose')
ptxt = ' Verify the operation of RCG EXC and TP parts. \n'
tr.sectiontext(ptxt)

tr.sectionhead('Overview')
otxt = 'This test uses the GDS diag tool to select and inject signals into six EXC parts, as shown in\n'
otxt += 'in the following Matlab diagram. The EXC and TP output signals will then be read in and run \n'
otxt += 'through an FFT to verify proper frequency and amplitude.'
tr.sectiontext(otxt)
#tr.image('exctest')

tr.sectionhead('Test Requirements')
trtxt = 'This test script (extpTest.py) was designed to run on the LLO DAQ test system. It requires that the \n'
trtxt += 'x2ats computer is running the x2atstim16.mdl software. \n'
tr.sectiontext(trtxt)

tr.sectionhead('Test MEDM Screen')
trtxt = 'Test screen is /opt/rtcds/userapps/trunk/cds/test/medm//X2ATS_EXC_TEST_CUSTOM.adl \n'
tr.sectiontext(trtxt)
#tr.image('extptestMedm')


tr.sectionhead('Test Procedure')
tptxt = ''
tr.sectiontext(tptxt)

# Step 1: **********************************************************************************************************************
tr.teststate.value = 'EXCTP - Initializing'
tr.sectionhead('Step 1: Initialize test signals')
tr.teststeptxt = 'a) Clear the GDS TP Table. \n'
os.system("echo tp clear 112 \\* | /usr/bin/diag -l > /dev/null 2>&1");
time.sleep(1)
tr.teststeptxt = 'b) Set EXC to TP matrices to aline EXC inputs to TP outputs: \n'
matrixchans = ['X2:ATS-TIM16_T2_EXC_TEST_1_1','X2:ATS-TIM16_T2_EXC_TEST_1_2','X2:ATS-TIM16_T2_EXC_TEST_2_1','X2:ATS-TIM16_T2_EXC_TEST_2_2',
	       'X2:ATS-TIM16_T2_EXC_TEST1_1_1','X2:ATS-TIM16_T2_EXC_TEST1_1_2','X2:ATS-TIM16_T2_EXC_TEST1_2_1','X2:ATS-TIM16_T2_EXC_TEST1_2_2',
	       'X2:ATS-TIM16_T2_EXC_TEST2_1_1','X2:ATS-TIM16_T2_EXC_TEST2_1_2','X2:ATS-TIM16_T2_EXC_TEST2_2_1','X2:ATS-TIM16_T2_EXC_TEST2_2_2']
matrixvals = [1.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 1.0]

testword = "X2:IOP-SAM_TEST_STAT_16"


testwordepics = PV(testword)
testwordepics.value = 2


ii = 0
for chan in matrixchans:
	ev = PV(chan)
	ev.value = matrixvals[ii]
	tr.teststeptxt += '\t - ' + chan + ' = ' + repr(matrixvals[ii]) + '\n'
	ii += 1

time.sleep(2)

tr.teststeptxt += '\nc) Preselect some EXC/TP. This is done just to move some GDS TP slots around to verify \n'
tr.teststeptxt += '   the test does not pass simply because EXC/TP are not in first/adjacent GDS selection \n'
tr.teststeptxt += '   slots, as seen during initial testing. \n' 
tr.teststeptxt += '\t - X2:ATS-TIM16_T1_ADC_FILTER_8_IN1 \n' 
tr.teststeptxt += '\t - X2:ATS-TIM16_T1_ADC_FILTER_8_EXC \n' 
tr.teststeptxt += '\t - X2:ATS-TIM16_T2_EXC_TEST_TP4 \n' 
tr.teststeptxt += '\t - X2:ATS-TIM16_T1_ADC_FILTER_8_IN2 \n' 
tr.teststeptxt += '\t - X2:ATS-TIM16_T2_EXC_TEST_IN1 \n' 

os.system("echo tp set X2:ATS-TIM16_T1_ADC_FILTER_8_IN1 | /usr/bin/diag -l >/dev/null 2>&1");
os.system("echo tp set X2:ATS-TIM16_T1_ADC_FILTER_8_EXC | /usr/bin/diag -l >/dev/null 2>&1");
os.system("echo tp set X2:ATS-TIM16_T2_EXC_TEST_TP4 | /usr/bin/diag -l >/dev/null 2>&1");
os.system("echo tp set X2:ATS-TIM16_T1_ADC_FILTER_8_IN2 | /usr/bin/diag -l >/dev/null 2>&1");
os.system("echo tp set X2:ATS-TIM16_T2_EXC_TEST_IN1 | /usr/bin/diag -l >/dev/null 2>&1");
os.system("echo tp set X2:ATS-TIM16_T2_EXC_TEST_IN6 | /usr/bin/diag -l >/dev/null 2>&1");

tr.teststeptxt += '\nd) Verify that, though selected, EXC are not yet active: \n'
ev = PV('X2:FEC-112_STATE_WORD')
excstatus = int(ev.value) & 256
if excstatus > 0:
	excstate = 'ON \t FAIL'
	tr.teststatus = -1
else:
	excstate = 'OFF '

tr.teststeptxt += '\t - EXC State = ' + excstate + '\n'
tr.sectionstep()

# Step 2: **********************************************************************************************************************
tr.teststate.value = 'EXCTP - Run Excitations'
tr.sectionhead('Step 2: Run GDS diag tool to inject/measure EXC and TP signals')

time.sleep(5)
reftemplates = [path.join(rcg_dir, 'test/rcgtest/python/exctp/template2'),'oscTest10','oscTest6500']
gdsdatafiles = ['/tmp/rcgtest/tmp/exctestgds','oscTest10','oscTest6500']
testsignals = ['EXC1','EXC2','EXC3','EXC4','EXC5','EXC6','TP1','TP2','TP3','TP4','TP5','TP6']
dataorder = [4,5,1,2,3,0,8,7,6,10,9,11]
gdstpchans = ['X2:FEC-112_GDS_MON_0', 'X2:FEC-112_GDS_MON_1', 'X2:FEC-112_GDS_MON_2', 'X2:FEC-112_GDS_MON_3', 'X2:FEC-112_GDS_MON_4',
	      'X2:FEC-112_GDS_MON_5', 'X2:FEC-112_GDS_MON_6', 'X2:FEC-112_GDS_MON_7', 'X2:FEC-112_GDS_MON_8', 'X2:FEC-112_GDS_MON_9',
	      'X2:FEC-112_GDS_MON_10', 'X2:FEC-112_GDS_MON_11', 'X2:FEC-112_GDS_MON_12', 'X2:FEC-112_GDS_MON_13', 'X2:FEC-112_GDS_MON_14']


testdiag = dtt()

tr.teststeptxt = 'a) Create the dtt command file -> dtt.cmd \n'
tr.teststeptxt += '\trestore template1.xml \n'
tr.teststeptxt += '\trun -w \n'
tr.teststeptxt += '\tsave exctestgds.xml \n'
tr.teststeptxt += '\tquit \n\n'
testdiag.createcmd(reftemplates[0],gdsdatafiles[0])

tr.teststeptxt += '\nb) Run diag using the generated command file. \n'
testdiag.rundttnowait()

ev = PV('X2:FEC-112_GDS_MON_14')
while (ev.value < 1):
	time.sleep(1)

tr.teststate.value = 'EXCTP - Record Data'
tr.teststeptxt += '\nc) Record selected TP numbers \n'
gdstpnum = []
for chan in gdstpchans:
	ev = PV(chan)
	gdstpnum.append(int(ev.value))
tr.teststeptxt += '\t' + repr(gdstpnum[0]) + '\t' + repr(gdstpnum[1]) + '\t' + repr(gdstpnum[2]) + '\t' + repr(gdstpnum[3]) + '\t' + repr(gdstpnum[4]) + '\n'
tr.teststeptxt += '\t' + repr(gdstpnum[5]) + '\t' + repr(gdstpnum[6]) + '\t' + repr(gdstpnum[7]) + '\t' + repr(gdstpnum[8]) + '\t' + repr(gdstpnum[9]) + '\n'
tr.teststeptxt += '\t' + repr(gdstpnum[10]) + '\t' + repr(gdstpnum[11]) + '\t' + repr(gdstpnum[12]) + '\t' + repr(gdstpnum[13]) + '\t' + repr(gdstpnum[14]) + '\n'

tr.teststeptxt += '\nd) Verify EXC bit of STATEWORD comes ON.\n'
ev = PV('X2:FEC-112_STATE_WORD')
excstatus = int(ev.value) & 256
if excstatus > 0:
	excstate = 'ON'
else:
	excstate = 'OFF \t FAIL'
	tr.teststatus = -1

tr.teststeptxt += '\t - EXC State = ' + excstate + '\n'
tr.sectionstep()


ev = PV('X2:FEC-112_GDS_MON_14')
while (ev.value > 0):
	time.sleep(1)

# Step 3: **********************************************************************************************************************
tr.teststate.value = 'EXCTP - Read FFT Data'
tr.sectionhead('Step 3: Read FFT data for EXC signals and verify frequency and amplitude.')
tr.teststeptxt = 'Signal \t\t Frequency \t\t Amplitude \t\t PASS/FAIL \n'
tr.teststeptxt += '\t\tREQ\tMeasured\tREQ\tMeasured \n'

testdiag.xml2txtM(gdsdatafiles[0],12)

reqfreq = [100,200,300,400,500,600,100,200,300,400,500,600]
freqtolerance = 0.2
#reqamp = [1954,3898,5854,7809,9746,11711,1954,3898,5854,7809,9746,11711]
#reqamp = [1986,3963,5950,7938,9906,11904,1986,3963,5950,7938,9906,11904]
reqamp = [2888,5764,8654,11545,14408,17313,2888,5764,8654,11545,14408,17313]
amptolerance = 2


for ii in range(0,6):
	testdiag.datafile = '/tmp/rcgtest/tmp/exctestgds' + repr(dataorder[ii]) + '.data'
	testdiag.readdata(0,1)
	freqchkhi = reqfreq[ii] + freqtolerance
	freqchklo = reqfreq[ii] - freqtolerance
	ampchkhi = reqamp[ii] + amptolerance
	ampchklo = reqamp[ii] - amptolerance
	fpf = 'PASS'
	if testdiag.peakfreq > freqchkhi or testdiag.peakfreq < freqchklo:
		fpf = 'FAIL'
		tr.teststatus = -1
	if testdiag.peakamp > ampchkhi or testdiag.peakamp < ampchklo:
		fpf = 'FAIL'
		tr.teststatus = -1
	tr.teststeptxt += '\t' + testsignals[ii] + '\t' + repr(reqfreq[ii]) + '\t' + repr(testdiag.peakfreq) + '\t\t' + repr(reqamp[ii]) + '\t' + repr(testdiag.peakamp) + '\t' + fpf + '\n'
tr.sectionstep()

# Step 4: **********************************************************************************************************************
tr.sectionhead('Step 4: ')
tr.teststeptxt = 'Plot EXC spectrum: \n'
tr.sectionstep()
tr.image("xextp")

# Step 5: **********************************************************************************************************************
tr.teststate.value = 'EXCTP - Measure Freq'
tr.sectionhead('Step 5: ')
tr.teststeptxt = 'Measure TP frequency/amplitude: \n'
tr.teststeptxt += 'Signal \t\t Frequency \t\t Amplitude \t\t PASS/FAIL \n'
tr.teststeptxt += '\t\tREQ\tMeasured\tREQ\tMeasured \n'

for ii in range(6,12):
	testdiag.datafile = '/tmp/rcgtest/tmp/exctestgds' + repr(dataorder[ii]) + '.data'
	testdiag.readdata(0,1)
	freqchkhi = reqfreq[ii] + freqtolerance
	freqchklo = reqfreq[ii] - freqtolerance
	ampchkhi = reqamp[ii] + amptolerance
	ampchklo = reqamp[ii] - amptolerance
	fpf = 'PASS'
	if testdiag.peakfreq > freqchkhi or testdiag.peakfreq < freqchklo:
		fpf = 'FAIL'
		tr.teststatus = -1
	if testdiag.peakamp > ampchkhi or testdiag.peakamp < ampchklo:
		fpf = 'FAIL'
		tr.teststatus = -1
	tr.teststeptxt += '\t' + testsignals[ii] + '\t' + repr(reqfreq[ii]) + '\t' + repr(testdiag.peakfreq) + '\t\t' + repr(reqamp[ii]) + '\t' + repr(testdiag.peakamp) + '\t' + fpf + '\n'
tr.sectionstep()

# Step 6: **********************************************************************************************************************
tr.sectionhead('Step 6: ')
tr.teststeptxt = 'Plot TP spectrum: \n'
tr.sectionstep()
tr.image("xextp2")

# Step 7: **********************************************************************************************************************
tr.teststate.value = 'EXCTP - Clear TP'
tr.sectionhead('Step 7: Clear Testpoints and Verify all EXC have been reset to zero')
tr.teststeptxt = 'a) Run tp clear command with diag -l. \n'
os.system("echo tp clear 112 \\* | /usr/bin/diag -l > /dev/null 2>&1");
tr.teststeptxt += 'b) Verify that all EXC have been set back to a zero value by reading EPICS channels connected at output. \n'
tr.teststeptxt += '\t\tEPICS Channel\t\t\tValue\tPass/Fail \n'
excepicschans = ['X2:ATS-TIM16_T2_EXC_TEST_OUT1', 'X2:ATS-TIM16_T2_EXC_TEST_OUT2', 'X2:ATS-TIM16_T2_EXC_TEST_OUT3',
		 'X2:ATS-TIM16_T2_EXC_TEST_OUT4', 'X2:ATS-TIM16_T2_EXC_TEST_OUT5', 'X2:ATS-TIM16_T2_EXC_TEST_OUT6']
time.sleep(4)
for chan in excepicschans:
	pf = 'PASS'
	ev = PV(chan)
	if int(ev.value) != 0:
		pf = 'FAIL'
		tr.teststatus = -1
	tr.teststeptxt += '\t-' + chan + '\t\t' + repr(int(ev.value)) + '\t' + pf + '\n'
tr.sectionstep()

if(tr.teststatus == 0):
    tr.sectionhead('RCG-1300-T TEST SUMMARY:  PASS') # *********************************************
else:
    tr.sectionhead('RCG-1300-T TEST SUMMARY:  FAIL') #*********************************************
tr.sectionstep()

testwordepics.value = 0
if tr.teststatus == 0:
    testwordepics.value = 1
tr.teststate.value = 'TEST SYSTEM - Idle'
tr.close()
testdiag.plot(path.join(rcg_dir, 'test/rcgtest/python/exctp/test.plt'))
os.system("cat /tmp/rcgtest/data/xextpTest.dox");
sys.exit(tr.teststatus)
