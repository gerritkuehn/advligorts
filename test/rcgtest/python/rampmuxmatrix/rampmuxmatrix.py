#!/usr/bin/python3
# Test report generation routines

from subprocess import call
from epics import PV
from epics import caget, caput
import sys
import time
import os
import os.path as path

rcg_dir = os.environ["RCG_DIR"]
python_path = path.join(rcg_dir, 'test/rcgtest/python')
sys.path.insert(0,python_path)
from cdstestsupport import testreport
from cdstestsupport import dtt


print ('starting test')
testword = "X2:IOP-SAM_TEST_STAT_7"
testwordepics = PV(testword)
testwordepics.value = 2

tr = testreport('rampmatrixTest','Ramp Mux Matrix Part',112)
# Standard Header for all Test Scripts ****************************************************************
tr.sectionhead('Purpose')
s = ' Verify the operation of RCG Ramp Mux Matrix Part. \n'
tr.sectiontext(s)

tr.sectionhead('Overview')
s = ' The Ramp Mux Matrix part provides extended features for matrices:\n'
s += '	New settings are sent all together when matrix load is requested.\n'
s += '  New settings are ramped over time as opposed to instantaneous change. \n'
tr.sectiontext(s)

tr.sectionhead('Test Requirements')
s = 'This test was designed to run on the LLO DAQ test stand:\n'
s += '  - Model x2atstim16 must be running on x2ats computer.\n'
s += '  - DAQ system must be operational. \n'
tr.sectiontext(s)

tr.sectionhead('Test Model')
s = 'RCG components for testing are located in x2atstim16.mdl'
tr.sectiontext(s)

tr.sectionhead('Related MEDM Screen')
s = 'Test MEDM screen is at /opt/rtcds/userapss/trunk/cds/test/medm \n'
tr.sectiontext(s)
tr.image('rampmuxmatrixMedm')
# END Standard Header for all Test Scripts ************************************************************

tr.sectionhead('Test Procedure')
s = ''
tr.sectiontext(s)

diag = dtt()
tr.teststate.value = 'RAMP MATRIX TEST'
timenow = int(diag.gpstime.value)
time.sleep(64)
starttime = timenow
print ('timenow =  ',starttime)
stoptime = timenow +2
buffers = diag.nds.fetch(starttime,stoptime,{'X2:ATS-TIM16_T1_RMM_OUT_FILT_1_OUT_DQ'})
#sys.exit(tr.teststatus)
# Start the test ******************************************

ii = 0;

 
matoutChans = ["X2:ATS-TIM16_T1_RMM_OUT1", "X2:ATS-TIM16_T1_RMM_OUT2", "X2:ATS-TIM16_T1_RMM_OUT3", "X2:ATS-TIM16_T1_RMM_OUT4"]
matinChans = ["X2:ATS-TIM16_T1_RMM_IN1", "X2:ATS-TIM16_T1_RMM_IN2", "X2:ATS-TIM16_T1_RMM_IN3", "X2:ATS-TIM16_T1_RMM_IN4"]
matinVals = [10,20,30,40]
matvalueChans = [
		"X2:ATS-TIM16_T1_RMM1_1_1", "X2:ATS-TIM16_T1_RMM1_1_2", "X2:ATS-TIM16_T1_RMM1_1_3", "X2:ATS-TIM16_T1_RMM1_1_4",
		"X2:ATS-TIM16_T1_RMM1_2_1", "X2:ATS-TIM16_T1_RMM1_2_2", "X2:ATS-TIM16_T1_RMM1_2_3", "X2:ATS-TIM16_T1_RMM1_2_4",
		"X2:ATS-TIM16_T1_RMM1_3_1", "X2:ATS-TIM16_T1_RMM1_3_2", "X2:ATS-TIM16_T1_RMM1_3_3", "X2:ATS-TIM16_T1_RMM1_3_4",
		"X2:ATS-TIM16_T1_RMM1_4_1", "X2:ATS-TIM16_T1_RMM1_4_2", "X2:ATS-TIM16_T1_RMM1_4_3", "X2:ATS-TIM16_T1_RMM1_4_4"
		]
matrampChans = [
		"X2:ATS-TIM16_T1_RMM1_RAMPING_1_1", "X2:ATS-TIM16_T1_RMM1_RAMPING_1_2",
		"X2:ATS-TIM16_T1_RMM1_RAMPING_1_3", "X2:ATS-TIM16_T1_RMM1_RAMPING_1_4",
		"X2:ATS-TIM16_T1_RMM1_RAMPING_2_1", "X2:ATS-TIM16_T1_RMM1_RAMPING_2_2",
		"X2:ATS-TIM16_T1_RMM1_RAMPING_2_3", "X2:ATS-TIM16_T1_RMM1_RAMPING_2_4",
		"X2:ATS-TIM16_T1_RMM1_RAMPING_3_1", "X2:ATS-TIM16_T1_RMM1_RAMPING_3_2",
		"X2:ATS-TIM16_T1_RMM1_RAMPING_3_3", "X2:ATS-TIM16_T1_RMM1_RAMPING_3_4",
		"X2:ATS-TIM16_T1_RMM1_RAMPING_4_1", "X2:ATS-TIM16_T1_RMM1_RAMPING_4_2",
		"X2:ATS-TIM16_T1_RMM1_RAMPING_4_3", "X2:ATS-TIM16_T1_RMM1_RAMPING_4_4"
		]

matrampVals = [5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5]

matsetChans = [
		"X2:ATS-TIM16_T1_RMM1_SETTING_1_1", "X2:ATS-TIM16_T1_RMM1_SETTING_1_2",
		"X2:ATS-TIM16_T1_RMM1_SETTING_1_3", "X2:ATS-TIM16_T1_RMM1_SETTING_1_4",
		"X2:ATS-TIM16_T1_RMM1_SETTING_2_1", "X2:ATS-TIM16_T1_RMM1_SETTING_2_2",
		"X2:ATS-TIM16_T1_RMM1_SETTING_2_3", "X2:ATS-TIM16_T1_RMM1_SETTING_2_4",
		"X2:ATS-TIM16_T1_RMM1_SETTING_3_1", "X2:ATS-TIM16_T1_RMM1_SETTING_3_2",
		"X2:ATS-TIM16_T1_RMM1_SETTING_3_3", "X2:ATS-TIM16_T1_RMM1_SETTING_3_4",
		"X2:ATS-TIM16_T1_RMM1_SETTING_4_1", "X2:ATS-TIM16_T1_RMM1_SETTING_4_2",
		"X2:ATS-TIM16_T1_RMM1_SETTING_4_3", "X2:ATS-TIM16_T1_RMM1_SETTING_4_4"
		]
matsetVals = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]
matloadChan = PV('X2:ATS-TIM16_T1_RMM1_LOAD_MATRIX')
matRampTime = PV('X2:ATS-TIM16_T1_RMM1_TRAMP')
matloadVals = [1,1,1,1]
matrixClear = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]



tr.sectionhead('Step 1: Initialize Test Settings')
tr.teststeptxt = '\ta) Set matrix inputs to 10,20,30,40 \n'
tr.setEpicsVals(matinChans,matinVals)
time.sleep(2)
tr.teststeptxt += '\tb) Set all matrix settings to zero (0.0) \n'
tr.setEpicsVals(matsetChans,matrixClear)
time.sleep(2)
tr.teststeptxt += '\tc) Set matrix ramping time to 1 second \n'
matRampTime.value = 1
time.sleep(1)
tr.teststeptxt += '\td) Load new matrix settings and wait for ramping to complete \n'
matloadChan.value = 1
time.sleep(3)
tr.teststeptxt += '\te) Read new matrix settings and verify they are all zero. \n'
tr.testEpicsVals(matvalueChans,matrixClear)
tr.teststeptxt += '\tf) Verify Matrix outputs are all zero. \n'
matoutReq = [0,0,0,0]
tr.testEpicsVals(matoutChans,matoutReq)
tr.sectionstep()


tr.sectionhead('Step 2: Individually Set and Verify Matrix Elements')
tr.teststeptxt = '\ta) Set ramp to 5 \n'
tr.setEpicsVals(matsetChans,matsetVals)
time.sleep(2)
tr.teststeptxt += '\tb) Load Matrix \n'
matRampTime.value = 5
time.sleep(1)
tr.teststeptxt += '\tc) Verify Ramping is in progress. \n'
matloadChan.value = 1
time.sleep(7)
tr.teststeptxt += '\td) Verify final setting. \n'
tr.testEpicsVals(matvalueChans,matsetVals)
matoutReq = [300,700,1100,1500]
tr.testEpicsVals(matoutChans,matoutReq)
tr.sectionstep()



# Test Rows-Columns of 4x4 matrix
print ('Verify Matrix Rows Columns   \n')

# Initialize all matrix settings to 0 and load
matsetVals = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
tr.setEpicsVals(matsetChans,matsetVals)
matloadChan.value = 1
time.sleep(7)

for data2 in range(0,4):


	matsetVals[data2] = 1.0

	if data2 > 0:
		matsetVals[data2 - 1] = 0
		tr.setEpicsVals(matsetChans, matsetVals)
	# Load new matrix settings
	matloadChan.value = 1
	time.sleep(2)
	# Read and verify ramp in progress signals are correct
	tr.getEpicsVals(matrampChans)
	s = ''
	for data in tr.arraydata:
	    s += repr(data) + ' '
	s+= '\n'
	print ('Ramp data = ',s)
	time.sleep(7)
	# Read and verify output signals are correct
	tr.getEpicsVals(matoutChans)
	for data in tr.arraydata:
		s += repr(data) + ' '
	s+= '\n'
	print ('Matrix Output data = ',s)

# Test entire row set to 1
matsetVals = [1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0]
tr.setEpicsVals(matsetChans, matsetVals)
matloadChan.value = 1
time.sleep(2)
tr.getEpicsVals(matrampChans)
s = ''
for data in tr.arraydata:
	s += repr(data) + ' '
s+= '\n'
print ('Ramp data = ',s)
time.sleep(7)
tr.getEpicsVals(matoutChans)
for data in tr.arraydata:
	s += repr(data) + ' '
s+= '\n'
print ('Matrix Output data = ',s)

# clear all matrix settings to 0 and load
matsetVals = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
tr.setEpicsVals(matsetChans,matsetVals)
matloadChan.value = 1
time.sleep(7)

# Test entire column set to 1
matsetVals = [1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0]
tr.setEpicsVals(matsetChans, matsetVals)
matloadChan.value = 1
time.sleep(2)
tr.getEpicsVals(matrampChans)
s = ''
for data in tr.arraydata:
	s += repr(data) + ' '
s+= '\n'
print ('Ramp data = ',s)
time.sleep(7)
tr.getEpicsVals(matoutChans)
for data in tr.arraydata:
	s += repr(data) + ' '
s+= '\n'
print ('Matrix Output data = ',s)

# clear all matrix settings to 0 and load
matsetVals = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
tr.setEpicsVals(matsetChans,matsetVals)
matloadChan.value = 1
time.sleep(7)

# Load diagonal 1's into matrix cols
matsetVals = [1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1]
tr.setEpicsVals(matsetChans, matsetVals)
matloadChan.value = 1
time.sleep(2)
tr.getEpicsVals(matrampChans)
s = ''
for data in tr.arraydata:
	s += repr(data) + ' '
s+= '\n'
print ('Ramp data = ',s)
time.sleep(7)
tr.getEpicsVals(matoutChans)
for data in tr.arraydata:
	s += repr(data) + ' '
s+= '\n'
print ('Matrix Output data = ',s)

# clear all matrix settings to 0 and load
matsetVals = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
tr.setEpicsVals(matsetChans,matsetVals)
matloadChan.value = 1
time.sleep(7)

# Test entire Matrix set to 1
matsetVals = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
tr.setEpicsVals(matsetChans, matsetVals)
matloadChan.value = 1
time.sleep(2)
tr.getEpicsVals(matrampChans)
s = ''
for data in tr.arraydata:
	s += repr(data) + ' '
s+= '\n'
print ('Ramp data = ',s)
time.sleep(7)
tr.getEpicsVals(matoutChans)
for data in tr.arraydata:
	s += repr(data) + ' '
s+= '\n'
print ('Matrix Output data = ',s)

# clear all matrix settings to 0 and load
matsetVals = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
tr.setEpicsVals(matsetChans,matsetVals)
matloadChan.value = 1
time.sleep(7)

tr.sectionstep()


tr.sectionhead('Step 3: Verify spline ramping')
tr.teststeptxt = '\ta) Set ramp to 1 \n'
tr.teststeptxt += '\tb) Load Matrix \n'
tr.teststeptxt += '\tc) Read Data from DAQ System \n'
tr.teststeptxt += '\td) Compare Data to Reference Data \n'


tr.setEpicsVals(matinChans,matinVals)
time.sleep(2)

matsetVals = [2000,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
tr.setEpicsVals(matsetChans,matsetVals)
time.sleep(2)

# Set matrix ramp time to 1 seconds
matRampTime.value = 1
time.sleep(1)
# Load matrix values
matloadChan.value = 1
timenow = int(diag.gpstime.value)
time.sleep(90)
starttime = timenow
stoptime = timenow +2
buffers = diag.nds.fetch(starttime,stoptime,{'X2:ATS-TIM16_T1_RMM_OUT_FILT_1_OUT_DQ'})

rampdata = []
rampdata.append(0.0)
stpt = 0
endpt = 0
ii = 0
for data in buffers[0].data:
	if stpt != 0 and endpt == 0:
		rampdata.append(data)
	if data > 0.0 and stpt == 0:
		rampdata.append(data)
		stpt = ii
	if data >= 20000 and endpt == 0:
		rampdata.append(data)
		endpt = ii
	ii += 1
#print 'write ramp data file'
#for data in rampdata:
#    print 'data =  ',data
totalpts = endpt - stpt -1
tr.teststeptxt += '\t\t1) RCG-1021-T Calculate ramp time and compare with requirement.\n'
totaltime = float(totalpts)/16384.0
tr.teststeptxt += '\t\t\tReq = 0.98 to 1.02 seconds      Measure = ' + repr(totaltime) + ' seconds\n'
tr.teststeptxt += '\t\t2) RCG-1020-T Compare ramp data with template data.\n'
diag.datacompare(rampdata,path.join(rcg_dir, 'test/rcgtest/python/rampmuxmatrix/rampReference.data'),1,0.1)
tr.teststeptxt += '\t\t\tErrors = ' + repr(diag.errors) + '\n'
if diag.errors:
	tr.teststatus = -1

diag.datafile = '/tmp/rcgtest/tmp/ramp.data'
diag.rawdata2file(rampdata)

tr.sectionstep()

diag.plot('rmm.plt')

tr.image('rmmRamp')

testwordepics.value = 0
if tr.teststatus == 0:
	testwordepics.value = 1

if(tr.teststatus == 0):
	tr.sectionhead('TEST SUMMARY:  PASS') # *********************************************
else:
	tr.sectionhead('TEST SUMMARY:  FAIL') #*********************************************
tr.sectionstep()


tr.close()
# Turn INPUT switch back ON to leave FM in default state
tr.teststate.value = 'TEST SYSTEM - IDLE'
call(["cat","/tmp/rcgtest/data/rampmatrixTest.dox"])
sys.exit(tr.teststatus)

