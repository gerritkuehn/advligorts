#!/usr/bin/env python
# Test report generation routines

from subprocess import call
from cdstestsupport import testreport
from cdstestsupport import dtt, SFM
from epics import PV
from epics import caget, caput
from math import sin,cos,radians
import sys
import time

tr = testreport('sdf','Setpoint Monitor',91)
# Standard Header for all Test Scripts ****************************************************************
tr.sectionhead('Purpose')
s = ' Verify the operation of SDF. \n'
tr.sectiontext(s)

tr.sectionhead('Overview')
s = '1) Load the safe.snap file and verify no errors. \n'
s += '\t- Verify appropriate number/names of chans not monitored, diff, not init, not found, alarm sets \n'
s += '2) Load the matempty.snap file using Load EDB only. \n'
s += '\t- Verify diffs show up with proper count. \n'
s += '3) Request to load file which does not exist. \n'
s += '\t- Verify proper error messages and readbacks of files loaded. \n'
s += '4) Load files with errant lines. \n'
s += '\t- Verify file loads but errant lines captured and reported. \n'

s += ' \n'
tr.sectiontext(s)

tr.sectionhead('Test Requirements')
s = ' \n'
s += ' \n'
tr.sectiontext(s)

tr.sectionhead('Test Model')
s = ' \n'
s += ' \n'
tr.sectiontext(s)
# tr.image('rampmuxmatrixModel')

tr.sectionhead('Related MEDM Screen')
s = 'Test MEDM screen is at /opt/rtcds/userapss/trunk/cds/test/medm/ \n'
tr.sectiontext(s)
# tr.image('rampmuxmatrixMedm')
# END Standard Header for all Test Scripts ************************************************************

tr.sectionhead('Test Procedure')
s = ''
tr.sectiontext(s)

pdiag = dtt()
# Start the test ******************************************

ii = 0;

tr.teststate.value = 'SDF TEST'

sdfReqFilename = PV('H1:FEC-91_SDF_NAME')
sdfTableLoaded = PV('H1:FEC-91_SDF_LOADED')
sdfEpicsLoaded = PV('H1:FEC-91_SDF_LOADED_EDB')
sdfFileSetCnt = PV('H1:FEC-91_SDF_FILE_SET_CNT')

sdfFileChans = ['H1:FEC-91_SDF_NAME','H1:FEC-91_SDF_LOADED','H1:FEC-91_SDF_LOADED_EDB']

# File load commands
# SDF_LOAD_DB_ONLY        4
# SDF_READ_ONLY           2
# SDF_RESET               3
# SDF_LOAD_PARTIAL        1

sdfNumReadbacks = ['H1:FEC-91_SDF_FULL_CNT','H1:FEC-91_SDF_DIFF_CNT',
		 'H1:FEC-91_SDF_UNMON_CNT','H1:FEC-91_SDF_UNINIT_CNT','H1:FEC-91_SDF_DROP_CNT',
		 'H1:FEC-91_SDF_ALARM_CNT', 'H1:FEC-91_SDF_FILE_SET_CNT']

sdfFileLoad = PV('H1:FEC-91_SDF_RELOAD')
sdfdbSetPts = PV('H1:FEC-91_SDF_FULL_CNT')
sdfdiffs = PV('H1:FEC-91_SDF_DIFF_CNT')
sdfunmon = PV('H1:FEC-91_SDF_UNMON_CNT')
sdfuninit = PV('H1:FEC-91_SDF_UNINIT_CNT')
sdfnotfound = PV('H1:FEC-91_SDF_DROP_CNT')
sdfalarms = PV('H1:FEC-91_SDF_ALARM_CNT')
sdffilesets = PV('H1:FEC-91_SDF_FILE_SET_CNT')
sdfSort = PV('H1:FEC-91_SDF_SORT')

sdfTableChans = ['H1:FEC-91_SDF_SP_STAT0','H1:FEC-91_SDF_SP_STAT1','H1:FEC-91_SDF_SP_STAT2','H1:FEC-91_SDF_SP_STAT3',
		 'H1:FEC-91_SDF_SP_STAT4','H1:FEC-91_SDF_SP_STAT5','H1:FEC-91_SDF_SP_STAT6','H1:FEC-91_SDF_SP_STAT7',
		 'H1:FEC-91_SDF_SP_STAT8','H1:FEC-91_SDF_SP_STAT9','H1:FEC-91_SDF_SP_STAT10','H1:FEC-91_SDF_SP_STAT11',
		 'H1:FEC-91_SDF_SP_STAT12','H1:FEC-91_SDF_SP_STAT13','H1:FEC-91_SDF_SP_STAT14','H1:FEC-91_SDF_SP_STAT15',
		 'H1:FEC-91_SDF_SP_STAT16','H1:FEC-91_SDF_SP_STAT17','H1:FEC-91_SDF_SP_STAT18','H1:FEC-91_SDF_SP_STAT19',
		 'H1:FEC-91_SDF_SP_STAT20','H1:FEC-91_SDF_SP_STAT21','H1:FEC-91_SDF_SP_STAT22','H1:FEC-91_SDF_SP_STAT23',
		 'H1:FEC-91_SDF_SP_STAT24','H1:FEC-91_SDF_SP_STAT25','H1:FEC-91_SDF_SP_STAT26','H1:FEC-91_SDF_SP_STAT27',
		 'H1:FEC-91_SDF_SP_STAT28','H1:FEC-91_SDF_SP_STAT29','H1:FEC-91_SDF_SP_STAT30','H1:FEC-91_SDF_SP_STAT31',
		 'H1:FEC-91_SDF_SP_STAT32','H1:FEC-91_SDF_SP_STAT33','H1:FEC-91_SDF_SP_STAT34','H1:FEC-91_SDF_SP_STAT35',
		 'H1:FEC-91_SDF_SP_STAT36','H1:FEC-91_SDF_SP_STAT37','H1:FEC-91_SDF_SP_STAT38','H1:FEC-91_SDF_SP_STAT39']
sdfTableBurt = ['H1:FEC-91_SDF_SP_STAT0_BURT','H1:FEC-91_SDF_SP_STAT1_BURT','H1:FEC-91_SDF_SP_STAT2_BURT','H1:FEC-91_SDF_SP_STAT3_BURT',
		 'H1:FEC-91_SDF_SP_STAT4_BURT','H1:FEC-91_SDF_SP_STAT5_BURT','H1:FEC-91_SDF_SP_STAT6_BURT','H1:FEC-91_SDF_SP_STAT7_BURT',
		 'H1:FEC-91_SDF_SP_STAT8_BURT','H1:FEC-91_SDF_SP_STAT9_BURT','H1:FEC-91_SDF_SP_STAT10_BURT','H1:FEC-91_SDF_SP_STAT11_BURT',
		 'H1:FEC-91_SDF_SP_STAT12_BURT','H1:FEC-91_SDF_SP_STAT13_BURT','H1:FEC-91_SDF_SP_STAT14_BURT','H1:FEC-91_SDF_SP_STAT15_BURT',
		 'H1:FEC-91_SDF_SP_STAT16_BURT','H1:FEC-91_SDF_SP_STAT17_BURT','H1:FEC-91_SDF_SP_STAT18_BURT','H1:FEC-91_SDF_SP_STAT19_BURT',
		 'H1:FEC-91_SDF_SP_STAT20_BURT','H1:FEC-91_SDF_SP_STAT21_BURT','H1:FEC-91_SDF_SP_STAT22_BURT','H1:FEC-91_SDF_SP_STAT23_BURT',
		 'H1:FEC-91_SDF_SP_STAT24_BURT','H1:FEC-91_SDF_SP_STAT25_BURT','H1:FEC-91_SDF_SP_STAT26_BURT','H1:FEC-91_SDF_SP_STAT27_BURT',
		 'H1:FEC-91_SDF_SP_STAT28_BURT','H1:FEC-91_SDF_SP_STAT29_BURT','H1:FEC-91_SDF_SP_STAT30_BURT','H1:FEC-91_SDF_SP_STAT31_BURT',
		 'H1:FEC-91_SDF_SP_STAT32_BURT','H1:FEC-91_SDF_SP_STAT33_BURT','H1:FEC-91_SDF_SP_STAT34_BURT','H1:FEC-91_SDF_SP_STAT35_BURT',
		 'H1:FEC-91_SDF_SP_STAT36_BURT','H1:FEC-91_SDF_SP_STAT37_BURT','H1:FEC-91_SDF_SP_STAT38_BURT','H1:FEC-91_SDF_SP_STAT39_BURT']
reqUnChans = ['H1:FE3-TIM16_T2_PR_FILTER_1_GAIN','H1:FE3-TIM16_T2_PR_FILTER_1_LIMIT',
	      'H1:FE3-TIM16_T2_PR_FILTER_1_OFFSET','H1:FE3-TIM16_T2_PR_FILTER_1_TRAMP',
	      'H1:FE3-TIM16_T2_WFS_FILTER_1_GAIN','H1:FE3-TIM16_T2_WFS_FILTER_1_LIMIT',
	      'H1:FE3-TIM16_T2_WFS_FILTER_1_OFFSET','H1:FE3-TIM16_T2_WFS_FILTER_1_SW1S',
	      'H1:FE3-TIM16_T2_WFS_FILTER_1_SW2S', 'H1:FE3-TIM16_T2_WFS_FILTER_1_TRAMP',
	      'H1:FE3-TIM16_T2_WFS_PHASE1_D','H1:FE3-TIM16_T2_WFS_PHASE1_R',
	      'H1:FE3-TIM16_T1_STRTEST_1','H1:FE3-TIM16_T1_STRTEST_2','H1:FE3-TIM16_T1_STRTEST_3',
	      'H1:FE3-TIM16_T1_STRTEST_4','H1:FE3-TIM16_T1_STRTEST_5']
reqNfChans = ['H1:FEC-91_SDF_MON_COUNT.HSV','H1:FEC-91_SDF_MON_COUNT.LSV','H1:FEC-91_SDF_MON_COUNT.HIGH','H1:FEC-91_SDF_MON_COUNT.LOW']
reqNiChans = ['H1:FE3-TIM16_EI2TOP','H1:FE3-TIM16_T2_PHASER_I',
	      'H1:FE3-TIM16_T2_PR_FILTER_2_GAIN', 'H1:FE3-TIM16_T2_PR_FILTER_2_LIMIT','H1:FE3-TIM16_T2_PR_FILTER_2_OFFSET',
	      'H1:FE3-TIM16_T2_PR_FILTER_2_SW1S','H1:FE3-TIM16_T2_PR_FILTER_2_SW2S','H1:FE3-TIM16_T2_PR_FILTER_2_TRAMP',
	      'H1:FE3-TIM16_T2_WFS_FILTER_2_GAIN', 'H1:FE3-TIM16_T2_WFS_FILTER_2_LIMIT','H1:FE3-TIM16_T2_WFS_FILTER_2_OFFSET',
	      'H1:FE3-TIM16_T2_WFS_FILTER_2_SW1S','H1:FE3-TIM16_T2_WFS_FILTER_2_SW2S','H1:FE3-TIM16_T2_WFS_FILTER_2_TRAMP',
	     ]

reqsp = 900
reqdiff = 0
requnmon = 17
requninit =14 
reqnotfound = 4
reqalarms = 7460 

#STEP 1 ***************************************************************************************************
tr.sectionhead('Step 1: Load safe settings')
tr.teststeptxt = '\n\ta) Load the SDF request record with safe:\n'
sdfReqFilename.value = 'safe'
reqsdffile = 'safe'
reqtablefile = 'safe'
reqedbload = 'safe'
time.sleep(1)
tr.teststeptxt = '\n\tb) Request an SDF file reload - Load Table and EDB:\n'
sdfFileLoad.value = 1
time.sleep(1)
tr.sectionstep()

#STEP 2 ***************************************************************************************************
tr.sectionhead('Step 2: Check SDF readbacks to verify SDF file loaded as expected')
reqvals = [900,0,17,14,4,7440,897];
tr.teststeptxt = '\ta) Verify channel count readbacks \n'
tr.testEpicsVals(sdfNumReadbacks,reqvals)

x = int(3);
filereq = ['safe','safe','safe'];
tr.teststeptxt += '\n\tb) Verify file settings readbacks:\n'
tr.testEpicsStr(sdfFileChans,filereq,3,0)
tr.sectionstep()
	
        
#STEP 3 ***************************************************************************************************
x = int(sdfunmon.value)
print 'x = ' + repr(x) +'\n'
wd = []
tr.sectionhead('Step 3: Verify SDF Unmonitored channel list')
sdfSort.value = 3
time.sleep(2)
tr.teststeptxt = '\n\ta) Read and compare unmonitored channel list:\n'
tr.teststeptxt += '\n\n\n******************\n'
tr.testEpicsStr(sdfTableChans,reqUnChans,x,1)
tr.sectionstep()

#STEP 4 ***************************************************************************************************
tr.sectionhead('Step 4: Verify SDF channels not found list')
tr.teststeptxt = '\n\ta) Read and compare channels not found list:\n'
x = int(sdfnotfound.value)
sdfSort.value = 1
time.sleep(2)
tr.teststeptxt += '\n\n\n******************\n'
tr.testEpicsStr(sdfTableChans,reqNfChans,x,1)
tr.sectionstep()


#STEP 5 ***************************************************************************************************
tr.sectionhead('Step 5: Verify SDF channels not initialized list')
tr.teststeptxt = '\n\ta) Read and compare channels not initialized list:\n'
x = int(sdfuninit.value)
sdfSort.value = 2
time.sleep(2)
tr.teststeptxt += '\n\n\n******************\n'
tr.testEpicsStr(sdfTableChans,reqNiChans,x,1)
tr.sectionstep()

# Close out the test **********************************************************************************
tr.close()
# Turn INPUT switch back ON to leave FM in default state
tr.teststate.value = 'TEST SYSTEM - IDLE'
call(["cat","/opt/rtcds/userapps/trunk/cds/test/data/sdf.dox"])
sys.exit(tr.teststatus)


