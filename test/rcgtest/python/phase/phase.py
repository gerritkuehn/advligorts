#!/usr/bin/python3
# Test report generation routines

from subprocess import call
from epics import PV
from epics import caget, caput
from math import sin,cos,radians
import sys
import time
import os
import os.path as path

rcg_dir = os.environ['RCG_DIR']
sys.path.insert(0,path.join(rcg_dir,'test/rcgtest/python'))
from cdstestsupport import testreport
from cdstestsupport import dtt, SFM


tr = testreport('phase','Phase Rotater Part',112)
testword = "X2:IOP-SAM_TEST_STAT_10"
testwordepics = PV(testword)
testwordepics.value = 2

# Standard Header for all Test Scripts ****************************************************************
tr.sectionhead('Purpose')
s = ' Verify the operation of RCG Phase Part. \n'
tr.sectiontext(s)

tr.sectionhead('Overview')
s = 'This block replicates an I&Q phase rotator used in the LIGO LSC control \n'
s += 'software (i.e., it is used to change the phase of the input values by a \n'
s += 'specific phase angle). \n\n'
s += 'The EPICS code reads in the user variable and calculates the sine and \n'
s += 'cosine for this entered value.  These two values (sinPhase, cosPhase) are \n'
s += 'then passed to the real-time software, which performs the following \n'
s += 'calculations: \n\n'
s += '   Out1 = In1 * cosPhase + In2 * sinPhase \n'
s += '   Out2 = In2 * cosPhase - In1 * sinPhase \n'
tr.sectiontext(s)

tr.sectionhead('Test Requirements')
s = 'This test was designed to run on the CIT DAQ test stand:\n'
s += '  - Model x1atstim16 must be running on scipe20 computer.\n'
s += '  - DAQ system must be operational. \n'
tr.sectiontext(s)

tr.sectionhead('Test Model')
s = 'RCG components for testing are located in x1atstim16.mdl'
tr.sectiontext(s)
# tr.image('rampmuxmatrixModel')

tr.sectionhead('Related MEDM Screen')
s = 'Test MEDM screen is at /opt/rtcds/userapss/trunk/cds/test/medm/PHASE_TEST.adl \n'
tr.sectiontext(s)
# tr.image('rampmuxmatrixMedm')
# END Standard Header for all Test Scripts ************************************************************

tr.sectionhead('Test Procedure')
s = ''
tr.sectiontext(s)

pdiag = dtt()
# Start the test ******************************************

ii = 0;

tr.teststate.value = 'PHASE PART TEST'

phaseSetting = PV('X2:ATS-TIM16_T2_PHASER_1')
phaseI = PV('X2:ATS-TIM16_T2_PR_FILTER_1_INMON')
phaseQ = PV('X2:ATS-TIM16_T2_PR_FILTER_2_INMON')
phaseIn10 = PV('X2:ATS-TIM16_T2_ADC_FILTER_15_OUTMON')
phaseIn20 = PV('X2:ATS-TIM16_T2_ADC_FILTER_16_OUTMON')

testvals = [1.0,2.0,3.0,4.0]

#STEP 1 ***************************************************************************************************
tr.sectionhead('Step 1: Initialize Test Settings')
tr.teststeptxt = '\ta)Filter modules are used as inputs to the PHASE part and need to be initialized:\n'
tr.teststeptxt += '\t\tPhase In1 = X2:ATS-TIM16_T2_ADC_FILTER_15\n'
tr.teststeptxt += '\t\t\t-Input Switch OFF\n'
tr.teststeptxt += '\t\t\t-Offset Switch ON\n'
tr.teststeptxt += '\t\t\t-Offset Value 2.0\n'
tr.teststeptxt += '\t\t\t-Gain Value 1.0\n'
tr.teststeptxt += '\t\t\t-Limit Switch OFF\n'
tr.teststeptxt += '\t\t\t-Output Switch ON\n'
phaseIn1Filt = SFM('X2:ATS-TIM16_T2_ADC_FILTER_15')
phaseIn1Filt.inputsw('OFF')
phaseIn1Filt.offsetsw('ON')
phaseIn1Filt.offset.value = 2.0
phaseIn1Filt.gain.value = 1.0
phaseIn1Filt.limitsw('OFF')
phaseIn1Filt.outputsw('ON')
tr.teststeptxt += '\t\tPhase In2 = X2:ATS-TIM16_T2_ADC_FILTER_16\n'
tr.teststeptxt += '\t\t\t-Input Switch OFF\n'
tr.teststeptxt += '\t\t\t-Offset Switch ON\n'
tr.teststeptxt += '\t\t\t-Offset Value 2.0\n'
tr.teststeptxt += '\t\t\t-Gain Value 1.0\n'
tr.teststeptxt += '\t\t\t-Limit Switch OFF\n'
tr.teststeptxt += '\t\t\t-Offset Switch ON\n'
phaseIn2Filt = SFM('X2:ATS-TIM16_T2_ADC_FILTER_16')
phaseIn2Filt.inputsw('OFF')
phaseIn2Filt.offsetsw('ON')
phaseIn2Filt.offset.value = 1.0
phaseIn2Filt.gain.value = 1.0
phaseIn2Filt.limitsw('OFF')
phaseIn2Filt.outputsw('ON')
tr.teststeptxt += '\tb)Filter modules are connected to the PHASE part outputs. The filter INMON signals\n'
tr.teststeptxt += '\t  will be used to verify the PHASE outputs:\n'
tr.teststeptxt += '\t\tPhase Out1 = X2:ATS-TIM16_T2_PR_FILTER_1\n'
tr.teststeptxt += '\t\t\t-Input Switch ON\n'
phaseOut1Filt = SFM('X2:ATS-TIM16_T2_PR_FILTER_1')
phaseOut1Filt.inputsw('ON')
tr.teststeptxt += '\t\tPhase Out2 = X2:ATS-TIM16_T2_PR_FILTER_2\n'
tr.teststeptxt += '\t\t\t-Input Switch ON\n'
phaseOut2Filt = SFM('X2:ATS-TIM16_T2_PR_FILTER_2')
phaseOut2Filt.inputsw('ON')

tr.teststeptxt += '\tc) The Phase part is X2:ATS-TIM16_T2_PHASER_1\n'
tr.teststeptxt += '\t\tInitialize phase to zero (0.0)\n'
tr.teststeptxt += '\n'
phaseSetting.value = 0.0
time.sleep(1)
tr.sectionstep()

#STEP 2 ***************************************************************************************************
tr.sectionhead('Step 2: Verify PHASE Part Operation')
tr.teststeptxt = '\n\ta) Step the PHASE input (in degrees):\n'
tr.teststeptxt += '\t\t- Range: 0 to 180 degrees\n'
tr.teststeptxt += '\t\t- Step Size: 1.0 \n'
tr.teststeptxt += '\t\t- Time between steps: 1 second \n'
tr.teststeptxt += '\n\tb) At each incremental input step:\n'
tr.teststeptxt += '\n\t\t- Calculate what the PHASE part Out1 and Out2 values should be.\n'
tr.teststeptxt += '\n\t\t- Read in the actual PHASE part Out1 and Out2 values.\n'
tr.teststeptxt += '\n\t\t- Compare calculated vs read in values.\n'
tr.teststeptxt += '\n\t\t- Report any differences calculated vs read signals.\n'
starttime = int(pdiag.gpstime.value)
phaseIn1 = phaseIn10.value
phaseIn2 = phaseIn20.value
tolerance = 0.000000001
phaseOut1Read = []
phaseOut2Read = []
tr.teststeptxt += '\n\tSET \tREAD OUT1\tCALC OUT1\tREAD OUT2\tCALC OUT2\tTEST RESULT\n'
for val in range(0,180):
    phaseSetting.value = val
    myIphase = phaseIn1 * cos(radians(val)) + phaseIn2 * sin(radians(val))
    myQphase = phaseIn2 * cos(radians(val)) - phaseIn1 * sin(radians(val))
    myIP = round(myIphase,9)
    myQP = round(myQphase,9)
    myIphaseUpper = myIP + tolerance;
    myIphaseLower = myIP - tolerance;
    myQphaseUpper = myQP + tolerance;
    myQphaseLower = myQP - tolerance;
    time.sleep(1)
    testResult = 'PASS'
    phaseOut1Read.append(phaseI.value)
    phaseOut2Read.append(phaseQ.value)
    p1 = round(phaseI.value,9)
    if p1 > myIphaseUpper or p1 < myIphaseLower:
        testResult = 'FAIL'
        tr.teststatus = -1
    p2 = round(phaseQ.value,9)
    if p2 > myQphaseUpper or p2 < myQphaseLower:
        testResult = 'FAIL'
        tr.teststatus = -1
    tr.teststeptxt += '\t' + repr(phaseSetting.value) + '\t' + repr(p1) + '\t' + repr(myIP) + '\t' + repr(p2) + '\t' + repr(myQP) + '\t' + testResult + ' \n'
tr.teststeptxt += '\n'
stoptime = int(pdiag.gpstime.value)
tr.sectionstep()

#STEP 3 ***************************************************************************************************
# Need to wait for data to frame
time.sleep(70)
tr.sectionhead('Step 3: Verify DAQ data')
tr.teststeptxt = '\ta) Read back DAQ data from the following channels: \n'
tr.teststeptxt += '\t\t1) X2:ATS-TIM16_T2_PHASER_1 = PHASE INPUT \n'
tr.teststeptxt += '\t\t2) X2:ATS-TIM16_T2_PHASER_1_SIN = EPICS generated SIN value sent to FE code \n'
tr.teststeptxt += '\t\t3) X2:ATS-TIM16_T2_PHASER_1_COS = EPICS generated COS value sent to FE code \n'
tr.teststeptxt += '\t\t4) X2:ATS-TIM16_T2_PR_FILTER_1_INMON = PHASE part OUT1 \n'
tr.teststeptxt += '\t\t5) X2:ATS-TIM16_T2_PR_FILTER_2_INMON = PHASE part OUT2 \n\n'
buffer1 = pdiag.nds.fetch(starttime,stoptime,['X2:ATS-TIM16_T2_PHASER_1'])
buffer2 = pdiag.nds.fetch(starttime,stoptime,['X2:ATS-TIM16_T2_PHASER_1_SIN'])
buffer3 = pdiag.nds.fetch(starttime,stoptime,['X2:ATS-TIM16_T2_PHASER_1_COS'])
buffer4 = pdiag.nds.fetch(starttime,stoptime,['X2:ATS-TIM16_T2_PR_FILTER_1_INMON'])
buffer5 = pdiag.nds.fetch(starttime,stoptime,['X2:ATS-TIM16_T2_PR_FILTER_2_INMON'])
tr.teststeptxt += '\tb) Verify DAQ PHASE SIN and COS values match calculated SIN and COS values: \n'
tr.teststeptxt += '\n\tSet \tCALC SIN\tDAQ SIN\t\tDAQ COS\tCALC COS\tTEST RESULT\n'
i = 0
dum = -1
rawdataI = []
rawdataQ = []
rawdataIout = []
rawdataQout = []
tolerance = 0.0000001
for val in buffer1[0].data:
    if val != dum and i+1 < len(buffer2[0].data) and i+1 < len(buffer3[0].data) and i+1 < len(buffer4[0].data) and i+1 < len(buffer5[0].data):
        rawdataI.append(buffer2[0].data[i+1])
        rawdataQ.append(buffer3[0].data[i+1])
        rawdataIout.append(buffer4[0].data[i+1])
        rawdataQout.append(buffer5[0].data[i+1])
        mysin = round(sin(radians(val)),8)
        mysinUpper = mysin + tolerance
        mysinLower = mysin - tolerance
        mycos = round(cos(radians(val)),8)
        mycosUpper = mycos + tolerance
        mycosLower = mycos - tolerance
        testResult = 'PASS'
        testSin = round(buffer2[0].data[i+1],8)
        if testSin > mysinUpper or testSin < mysinLower:
            testResult = 'FAIL'
            tr.teststatus = -1
        testCos = round(buffer3[0].data[i+1],8)
        if testCos > mycosUpper or testCos < mycosLower:
            testResult = 'FAIL'
            tr.teststatus = -1
        tr.teststeptxt += '\t' + repr(val) + '\t' + repr(mysin) + '\t' + repr(testSin) + '\t' + repr(testCos) + '\t' + repr(mycos) + '\t' + testResult
        tr.teststeptxt += '\n'
        dum = val
    i += 1

i = 0


tr.sectionstep()

pdiag.datafile = '/tmp/rcgtest/tmp/phaseI.data'
pdiag.rawdata2file(rawdataI)
pdiag.datafile = '/tmp/rcgtest/tmp/phaseQ.data'
pdiag.rawdata2file(rawdataQ)
pdiag.datafile = '/tmp/rcgtest/tmp/phaseIout.data'
pdiag.rawdata2file(rawdataIout)
pdiag.datafile = '/tmp/rcgtest/tmp/phaseQout.data'
pdiag.rawdata2file(rawdataQout)

#STEP 4 ***************************************************************************************************
tr.sectionhead('Step 4: Plot DAQ data')
pdiag.plot(path.join(rcg_dir, 'test/rcgtest/python/phase/phase.plt'))
tr.image('/tmp/rcgtest/images/phase')
tr.image('/tmp/rcgtest/images/phaseOut')
tr.teststeptxt = '\tTest complete at ' + repr(stoptime) + '\n'
tr.sectionstep()

if(tr.teststatus == 0):
    tr.sectionhead('RCG-1060-T TEST SUMMARY:  PASS') # *********************************************
else:
    tr.sectionhead('RCG-1060-T TEST SUMMARY:  FAIL') #*********************************************
tr.sectionstep()



time.sleep(2)

# Cleanup
phaseIn1Filt.inputsw('ON')
phaseIn1Filt.offsetsw('OFF')
phaseIn1Filt.offset.value = 0.0
phaseIn1Filt.outputsw('ON')
phaseIn2Filt.inputsw('ON')
phaseIn2Filt.offsetsw('OFF')
phaseIn2Filt.offset.value = 0.0
phaseIn2Filt.outputsw('ON')
testwordepics.value = 0
if tr.teststatus == 0:
    testwordepics.value = 1



tr.close()
# Turn INPUT switch back ON to leave FM in default state
tr.teststate.value = 'TEST SYSTEM - IDLE'
call(["cat","/tmp/rcgtest/data/phase.dox"])
sys.exit(tr.teststatus)

