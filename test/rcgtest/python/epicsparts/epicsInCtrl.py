#!/usr/bin/python3
# Test EPICS INPUT w/CONTROL (EIC) part

import epics
import time
import string
import sys
import os
import os.path as path

rcg_dir = os.environ['RCG_DIR']
from epics import PV
from subprocess import call
sys.path.insert(0,path.join(rcg_dir, 'test/rcgtest/python'))
from cdstestsupport import testreport


testword = "X2:IOP-SAM_TEST_STAT_17"
testwordepics = PV(testword)
testwordepics.value = 2

tr = testreport('eicTest','EPICS INPUT W/CONTROL',112)
# Standard Header for all Test Scripts ****************************************************************
tr.sectionhead('Purpose')
pt = 'Test EPICS Input w/Control Part introduced in RCG V2.8. \n'
tr.sectiontext(pt)

tr.sectionhead('Overview')
ovt = 'This test verifies the operation of the Epics Input w/Control (EIC) part as defined in the RCG. \n'
ovt += 'Both Local Control (LC), control from within the real-time code, and Remote Control (RC), via \n'
ovt += 'normal EPICS channel access, are tested. \n'
tr.sectiontext(ovt)

tr.sectionhead('Requirements')
rt = 'This test is designed to run on the CIT DAQ test stand. \n'
rt += '\t- x1atstim16 running on scipe20 computer. \n'
rt += '\t- Test script epicsInCtrl.py in userapps/cds/test/scripts. \n'
tr.sectiontext(rt)

tr.sectionhead('Related MEDM Screens')
rmt = 'Test MEDM screen is at /opt/rtcds/userapss/trunk/cds/test/medm \n'
tr.sectiontext(rmt)
tr.image('eicTestMedm')

tr.sectionhead('Test Model')
tmt = 'Test model is x1atstim16.mdl\n'
tr.sectiontext(tmt)
tr.image('eicTestModel')
# END Standard Header for all Test Scripts ************************************************************


# Define EPICS Variables *****************************************************************************
# Define EPICS input/wctrl part variable. 
epicCtrl = PV('X2:ATS-TIM16_T1_EPIC_IN_CTRL.VAL')
# Set test inputs/output
eicTestMask = PV('X2:ATS-TIM16_T1_EIC_MASK.VAL')	# Sets MASK Input
eicTestInput = PV('X2:ATS-TIM16_T1_EIC_IN.VAL')		# Sets Local Control Input
eicTestOut = PV('X2:ATS-TIM16_T1_EIC_OUT.VAL')		# Displays output from Epics In/w control part.

rcInputs = [20.0,50.0]
lcInputs = [10.0,100.0,545.55]

testStatus = 0;
# END Define EPICS Variables *************************************************************************
tr.sectionhead('Test Procedure\n\n')

# Start the test ******************************************
tr.sectionhead('Step 1: Initialize Values \n')

eicTestMask.value = 0;	# Reset input mask; allows remote control ie direct EPICS input to EIC part
epicCtrl.value = rcInputs[0]	# Set EIC value via EPICS (Remote control)
eicTestInput.value = lcInputs[0]	# Set EIC remote control test value
stxt = '\tMask = ' + repr(eicTestMask.value) + '\n\tLocal Control (LC) input = ' + repr(eicTestInput.value) + '\n\tEpics (Remote Control (RC)) input = ' + repr(epicCtrl.value) + ' \n'
tr.sectiontext(stxt)

ii = 0;
tr.sectionhead('Step 2: RCG-0700-T Verify Remote Control operation.')
stxt = '\tRequirement: Output testpoint and Epics part RC input match. \n\n'
ii = 0;
for rcInput in rcInputs:
	epicCtrl.value = rcInput	# Set EIC value via EPICS (Remote control)
	time.sleep(1)			# Wait for EPICS value to be transmitted and set.
	if eicTestOut.value != rcInput:
		passFail = 'FAIL'
		testStatus = -1
	else:
		passFail = 'PASS'
	stxt += '\tRC input = ' + repr(rcInput) + ' \tOutput = ' + repr(eicTestOut.value) + '\t' + passFail + '\n'
tr.sectiontext(stxt)

tr.sectionhead('Step 3: RCG-0701-T Verify Local Control operation.')
stxt = '\tRequirement: Both output testpoint and Epics part RC input match LC input. \n\n'
eicTestMask.value = 1;	# Set input mask for local control
time.sleep(1)		# Wait for EPICS value to be transmitted and set.
# Read the inputs
for lcInput in lcInputs:
	eicTestInput.value = lcInput	# Set EIC value via EPICS (Remote control)
	time.sleep(1)			# Wait for EPICS value to be transmitted and set.
	if eicTestOut.value != lcInput:
		passFail = 'FAIL'
		testStatus = -1
	else:
		passFail = 'PASS'
	if epicCtrl.value != lcInput:
		passFail = 'FAIL'
		testStatus = -1
	stxt += '\tLC input = ' + repr(eicTestInput.value) + ' \tOutput = ' + repr(eicTestOut.value) + '\t RC = ' + repr(epicCtrl.value) + '\t' + passFail + '\n'
tr.sectiontext(stxt)

tr.sectionhead('\nStep 4: RCG-0702-T Return to RC \n')
stxt = '\t Requirement: RC value remains equal to LC value loaded prior to removing MASK set.\n'
eicTestMask.value = 0;	# Reset input mask
time.sleep(1)		# Wait for EPICS value to be transmitted and set.
eicTestInput.value = lcInputs[0]	# Set EIC remote control test value
if eicTestOut.value != lcInputs[2]:
	passFail = 'FAIL'
else:
	passFail = 'PASS'
	if epicCtrl.value != lcInputs[2]:
		passFail = 'FAIL'
		testStatus = -1
stxt += '\tLC input = ' + repr(eicTestInput.value) + ' \tOutput = ' + repr(eicTestOut.value) + '\t RC = ' + repr(epicCtrl.value) + '\t' + passFail + '\n'

testwordepics.value = 0
if tr.teststatus == 0:
    testwordepics.value = 1

tr.sectiontext(stxt)
tr.sectionstep()

if(tr.teststatus == 0):
    tr.sectionhead('TEST SUMMARY:  PASS') # *********************************************
else:
    tr.sectionhead('TEST SUMMARY:  FAIL') #*********************************************
tr.sectionstep()

tr.close()
# Print standard test report
# Print standard test report to file for doxygen
call(["cat","/tmp/rcgtest/data/eicTest.dox"])
print (testStatus)
sys.exit(testStatus)
