import sys
import numpy as np
from channel import DacChannel, AdcChannel
from rate_params import RateParams
import pickle
import importlib

if len(sys.argv) > 1:
    model_name = sys.argv[1]
    print(f"testing model {model_name}")
    model_class = importlib.import_module(model_name + "_pybind")
else:
    print(f"need a model name as an argument")
    sys.exit(1)



from demod_component import DemodComponent

model = model_class.create_instance()

rate_params = RateParams(rate_hz=model.get_model_rate_Hz(), duration_s=1.0)

demod_freq_Hz = 300

class DemodPart(object):
    def __init__(self, name, num_signals, adc_start_chan, dac_start_chan, rate_params, demod_freq_hz):
        self.name = name
        self.num_signals = num_signals
        self.adc_start_chan = adc_start_chan
        self.dac_start_chan = dac_start_chan
        self.rate_params = rate_params
        self.demod_freq_hz = demod_freq_hz

        # filled in during setup
        self.components = None

        # filled in by checking
        self.result = None

        self.setup()

    def setup(self):

        self.components = []
        for i in range(self.num_signals):
            comp = DemodComponent(i * 0.5 + 1, self.demod_freq_hz, i + 1,)
            self.components.append(comp)
            freq_hz = comp.base_freq_hz + comp.freq_offset_hz
            signal = comp.amp*np.sin(self.rate_params.time_s * freq_hz * 2.0 * np.pi)
            chan = self.adc_start_chan + i
            model.set_adc_channel_generator(chan.card, chan.channel, model_class.CannedSignalGenerator(canned_data=signal))

    def check(self):
        """
        Check output of part.  Print some results.

        :return: True if all tests passed.
        """
        for comp_index in range(len(self.components)):
            comp = self.components[comp_index]
            i_channel = self.dac_start_chan + (2*comp_index)
            q_channel = i_channel + 1
            comp.out_i = model.get_dac_output_by_id(i_channel.card, i_channel.channel)
            comp.out_q = model.get_dac_output_by_id(q_channel.card, q_channel.channel)


        # positive only frequencies
        pos_freqs_Hz = self.rate_params.freq_Hz[:len(self.rate_params.freq_Hz)//2]

        print(f"Checking {self.name}")

        result = True
        for comp in self.components:
            result = comp.check(100.0, self.rate_params) and result

        self.result = result
        self.summary()
        print()
        print("***********************************")
        return result

    def summary(self):
        print(f"{self.name}: ", self.result and "PASS" or "FAIL")

class DemodRotatePart(object):
    def __init__(self, name, num_freqs, adc_chan, dac_start_chan, rate_params, demod_freq_hz):
        self.name = name
        self.num_freqs = num_freqs
        self.adc_chan = adc_chan
        self.dac_start_chan = dac_start_chan
        self.rate_params = rate_params
        self.demod_freq_hz = demod_freq_hz

        # filled in during setup
        self.components = None

        # filled in by checking
        self.result = None

        self.setup()

    def setup(self):

        amp = 1.0

        self.components = []
        for i in range(self.num_freqs):
            comp = DemodComponent(amp, self.demod_freq_hz - 1, i + 1,)
            self.components.append(comp)

        # one less, so first demodulation should produce 1 Hz
        sig_hz = self.demod_freq_hz - 1
        signal = amp*np.sin(self.rate_params.time_s * sig_hz * 2.0 * np.pi)
        model.set_adc_channel_generator(self.adc_chan.card, self.adc_chan.channel, model_class.CannedSignalGenerator(canned_data=signal))

    def check(self):
        """
        Check output of part.  Print some results.

        :return: True if all tests passed.
        """
        for comp_index in range(len(self.components)):
            comp = self.components[comp_index]
            i_channel = self.dac_start_chan + (2*comp_index)
            q_channel = i_channel + 1
            comp.out_i = model.get_dac_output_by_id(i_channel.card, i_channel.channel)
            comp.out_q = model.get_dac_output_by_id(q_channel.card, q_channel.channel)


        # positive only frequencies
        pos_freqs_Hz = self.rate_params.freq_Hz[:len(self.rate_params.freq_Hz)//2]

        print(f"Checking {self.name}")

        result = True
        for comp in self.components:
            result = comp.check(100.0, self.rate_params) and result

        self.result = result
        self.summary()
        print()
        print("***********************************")
        return result

    def summary(self):
        print(f"{self.name}: ", self.result and "PASS" or "FAIL")



demod_parts = [
    DemodPart("RCG-1090-T DemodDecim8x", 8, AdcChannel(0,0), DacChannel(0,0), rate_params, demod_freq_Hz),
    DemodPart("RCG-1091-T DemodDecim16x", 16, AdcChannel(0,9), DacChannel(1,0), rate_params, demod_freq_Hz),
]

demod_rotate_parts = [
    DemodRotatePart("RCG-1092-T DemodRotate8x", 8, AdcChannel(0,25), DacChannel(3,0), rate_params, demod_freq_Hz),
    DemodRotatePart("RCG-1090-T DemodRotate16x", 16, AdcChannel(0,25), DacChannel(4,0), rate_params, demod_freq_Hz),
    ]

model.set_var("DEMODFREQ", demod_freq_Hz)
# print(model.get_var("DEMODFREQ"))

model.record_dac_output(True)

# run model
model.run_model(len(rate_params.time_s))

all_passed = True

for demod_part in demod_parts:
    all_passed = demod_part.check() and all_passed

for demod_rotate_part in demod_rotate_parts:
    all_passed = demod_rotate_part.check() and all_passed

print()
print("Test Summary")
for demod_part in demod_parts:
    demod_part.summary()
for demod_rotate_part in demod_rotate_parts:
    demod_rotate_part.summary()


gauss_chan = DacChannel(6,0)
gauss_output = model.get_dac_output_by_id(gauss_chan.card,gauss_chan.channel)

# with open("gauss.pickle", "wb") as f:
#     pickle.dump(gauss_output, f)

with open("gauss.pickle", "rb") as f:
    gauss_expected = pickle.load(f)

# some differences caused by changing floating point instructions with -fma, -sse3 etc options on the compiler
# these are mostly changes in the last bit, but the bifurcations in the gauss function can lead to errors > 1E-7
gauss_result = np.isclose(gauss_output, gauss_expected, rtol = 1e-6, atol=1e-15)

for i in range(len(gauss_result)):
    if not gauss_result[i]:
        print(f"{i}\t{gauss_output[i]}\t{gauss_expected[i]}\t{gauss_expected[i] - gauss_output[i]}")

print(f"RCG-1081-T gaussian output test: ", gauss_result.all() and "PASS" or "FAIL")

all_passed = gauss_result.all() and all_passed

if all_passed:
    sys.exit(0)
else:
    sys.exit(1)

