


class Channel(object):

    channels_per_card = 0

    """Represents a channel on a DAC or ADC card"""
    def __init__(self, card, channel):
        """
        :param card: the card number where the channel lives
        :param channel: the channel number on the card
        """
        assert(self.channels_per_card > 0)
        assert(channel < self.channels_per_card)
        assert(card >= 0)
        assert(channel >= 0)

        self.card = card
        self.channel = channel


    def __str__(self):
        return f"(card: {self.card}, channel: {self.channel}"

    def __repr__(self):
        return f"Channel(card={self.card}, channel={self.channel})"

    def __add__(self, x):
        """
        hard-coded for 16 channel dacs
        """
        new_chan = self.channel + x
        new_card = self.card
        while new_chan >= self.channels_per_card:
            new_chan -= self.channels_per_card
            new_card += 1
        return self.__class__(new_card, new_chan)

class DacChannel(Channel):
    channels_per_card = 16

    def __init__(self, card, channel):
        super().__init__(card, channel)

class AdcChannel(Channel):
    channels_per_card = 32

    def __init__(self, card, channel):
        super().__init__(card, channel)

def test():
    d = DacChannel(0,15)
    a = AdcChannel(0,31)

    d += 1
    a += 1

    print(d)
    print(a)

    assert(d.card == 1)
    assert(d.channel == 0)

    assert(a.card == 1)
    assert(a.channel == 0)

    print("Channel objects PASS")


if __name__ == "__main__":
    test()