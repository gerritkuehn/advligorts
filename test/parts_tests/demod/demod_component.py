from math_funcs import abs_fft, max_two
from test_funcs import print_result

class DemodComponent(object):
    def __init__(self, amp, base_freq_hz, freq_offset_hz, skip_q=False):
        self.amp = float(amp)
        self.freq_offset_hz = float(freq_offset_hz)
        self.base_freq_hz = float(base_freq_hz)
        self.out_i = None
        self.out_q = None
        self.skip_q = skip_q

    def __str__(self):
        return (f"A={self.amp} f={self.freq_offset_hz} Hz demod={self.base_freq_hz} Hz")


    def check_channel(self, chan_name, peaks, peak_tolerance_Hz):
        """Check I or Q channel output

            Print results.
            :param chan_name: either 'I' or 'Q'
            :param peaks: (1st peak Hz, 1st peak Mag, 2nd peak Hz, 2nd peak mag) as returned by max_two()
            :param peak_tolerance_Hz: tolerance of peak freq check
            :return: Return True if all trets passed.
        """

        # check peak frequency matches offset frequency
        result = print_result(
            f"Demod {self.base_freq_hz} Hz {self.freq_offset_hz} Hz off. {chan_name} channel peak freq.",
            self.freq_offset_hz,
            peaks[0],
            "near",
            peak_tolerance_Hz
        )

        # check second peak is small compared to first
        min_ratio = 5.0
        if peaks[3] != 0.0:
            ratio = peaks[1] / peaks[3]
        else:
            ratio = 10000000000.0
        result = print_result(
            f"Demod {self.base_freq_hz} Hz {self.freq_offset_hz} Hz off. {chan_name} peak ratio (1st to 2nd)",
            min_ratio,
            ratio,
            ">=",
        ) and result



        return result


    def check(self, max_freq_Hz, rate_params):
        """
        Check output for a single band

        :param max_freq_Hz: the highest frequency to include in the peak search
        :param rate_params: a RateParams object describing the rates of the test
        :return: True if all tests passed.
        """

        fft_i = abs_fft(self.out_i)
        fft_q = abs_fft(self.out_q)

        # maximum index to search for peaks
        max_index = int(max_freq_Hz / rate_params.dHz)

        peaks_i = max_two(rate_params.freq_Hz[:max_index], fft_i[:max_index])
        peaks_q = max_two(rate_params.freq_Hz[:max_index], fft_q[:max_index])

        # check I channel
        result = self.check_channel('I', peaks_i, rate_params.dHz / 2)

        # skip nyquist frequency: don't know how to check it.
        if not self.skip_q:
            result = self.check_channel('Q', peaks_q, rate_params.dHz / 2) and result


        # check amplitude
        # again, skip nyquist demod
        if not self.skip_q:
            amp = peaks_i[1] + peaks_q[1]
        else:
            amp = peaks_i[1]


        # gain = number of points in fft / 2.  We dropped the negative frequency amplitudes
        expected_amp = self.amp * (rate_params.rate_hz) * rate_params.duration_s / 2

        amp_tolerance = expected_amp*0.020

        result = print_result(
            f"Demod {self.base_freq_hz} Hz {self.freq_offset_hz} Hz off. amplitude",
            expected_amp,
            amp,
            "near",
            amp_tolerance
        ) and result

        print("")

        return result
