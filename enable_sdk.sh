#!/usr/bin/env bash

if [[ "$(basename -- "$0")" == "enable_sdk.sh" ]]; then
    >&2 echo "ERROR: Don't run $0, source it, to enable the repo it resides in. Ex 'source $0'"
    exit 1
fi

SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)
export RCG_SRC=${SCRIPT_DIR}
echo "RCG_SRC=${SCRIPT_DIR}"

VERSION_HASH=$( echo ${SCRIPT_DIR} | md5sum )
export RTS_VERSION=${VERSION_HASH:0:10}
echo "RTS_VERSION=${VERSION_HASH:0:10}"

export CDS_SRC=${SCRIPT_DIR}/src/include/
echo "CDS_SRC=${SCRIPT_DIR}/src/include/"

echo ${VERSION_HASH:0:10} > ${SCRIPT_DIR}/rcg-version
