//
// Created by jonathan.hanks on 12/20/19.
//
#include "simple_pv.h"
#include "simple_epics.hh"
#include "simple_pv_internal.hh"

#include <algorithm>
#include <cstring>
#include <memory>
#include <vector>

#include "fdManager.h"

namespace
{

    const char* string_value_dummy_val = "";
} // namespace

extern "C" {

simple_pv_handle
simple_pv_server_create( const char* prefix, SimplePV* pvs, int pv_count )
{
    const char* dummy = "";
    if ( pv_count < 0 || ( !pvs && pv_count > 0 ) )
    {
        return nullptr;
    }
    const std::string prefix_ = ( prefix ? prefix : "" );

    std::unique_ptr< simple_epics::Server > server =
        simple_epics::detail::make_unique_ptr< simple_epics::Server >(
            prefix_ );

    auto pv_server = server.get( );
    std::for_each(
        pvs,
        pvs + pv_count,
        [ &prefix_, pv_server ]( const SimplePV& pv ) -> void {
            if ( !pv.name || !pv.data )
            {
                return;
            }
            switch ( pv.pv_type )
            {
            case SIMPLE_PV_INT: {
                pv_server->addPV( simple_epics::pvIntAttributes(
                    prefix_ + pv.name,
                    reinterpret_cast< std::atomic< int >* >( pv.data ),
                    std::make_pair< int, int >( pv.alarm_low, pv.alarm_high ),
                    std::make_pair< int, int >( pv.warn_low, pv.warn_high ) ) );
            }
            break;
            case SIMPLE_PV_STRING: {
                pv_server->addPV( simple_epics::pvStringAttributes(
                    prefix_ + pv.name, reinterpret_cast< char* >( pv.data ) ) );
            }
            break;
            case SIMPLE_PV_DOUBLE: {
                pv_server->addPV( simple_epics::pvDoubleAttributes(
                    prefix_ + pv.name,
                    reinterpret_cast< std::atomic< double >* >( pv.data ),
                    std::make_pair( pv.alarm_low, pv.alarm_high ),
                    std::make_pair( pv.warn_low, pv.warn_high ) ) );
            }
            break;
            }
        } );

    return server.release( );
}

int
simple_pv_server_add_int32( simple_pv_handle server, SimpleInt32PV pv )
{
    auto server_ = reinterpret_cast< simple_epics::Server* >( server );
    if ( server_ == nullptr )
    {
        return 0;
    }
    if ( pv.name == nullptr || pv.data == nullptr )
    {
        return 0;
    }
    try
    {
        server_->addPV( simple_epics::pvIntAttributes(
            server_->prefix( ) + pv.name,
            reinterpret_cast< std::atomic< std::int32_t >* >( pv.data ),
            std::make_pair( pv.alarm_low, pv.alarm_high ),
            std::make_pair( pv.warn_low, pv.warn_high ),
            ( pv.read_write == SIMPLE_PV_READ_ONLY
                  ? simple_epics::PVMode::ReadOnly
                  : simple_epics::PVMode::ReadWrite ) ) );
    }
    catch ( ... )
    {
        return 0;
    }
    return 1;
}

int
simple_pv_server_add_uint16( simple_pv_handle server, SimpleUInt16PV pv )
{
    auto server_ = reinterpret_cast< simple_epics::Server* >( server );
    if ( server_ == nullptr )
    {
        return 0;
    }
    if ( pv.name == nullptr || pv.data == nullptr )
    {
        return 0;
    }
    try
    {
        server_->addPV( simple_epics::pvUShortAttributes(
            server_->prefix( ) + pv.name,
            reinterpret_cast< std::atomic< std::uint16_t >* >( pv.data ),
            std::make_pair( pv.alarm_low, pv.alarm_high ),
            std::make_pair( pv.warn_low, pv.warn_high ),
            ( pv.read_write == SIMPLE_PV_READ_ONLY
                  ? simple_epics::PVMode::ReadOnly
                  : simple_epics::PVMode::ReadWrite ) ) );
    }
    catch ( ... )
    {
        return 0;
    }
    return 1;
}

int
simple_pv_server_add_float32( simple_pv_handle server, SimpleFloat32PV pv )
{
    auto server_ = reinterpret_cast< simple_epics::Server* >( server );
    if ( server_ == nullptr )
    {
        return 0;
    }
    if ( pv.name == nullptr || pv.data == nullptr )
    {
        return 0;
    }
    try
    {
        server_->addPV( simple_epics::pvFloatAttributes(
            server_->prefix( ) + pv.name,
            reinterpret_cast< std::atomic< float >* >( pv.data ),
            std::make_pair( pv.alarm_low, pv.alarm_high ),
            std::make_pair( pv.warn_low, pv.warn_high ),
            ( pv.read_write == SIMPLE_PV_READ_ONLY
                  ? simple_epics::PVMode::ReadOnly
                  : simple_epics::PVMode::ReadWrite ) ) );
    }
    catch ( ... )
    {
        return 0;
    }
    return 1;
}

int
simple_pv_server_add_float64( simple_pv_handle server, SimpleFloat64PV pv )
{
    auto server_ = reinterpret_cast< simple_epics::Server* >( server );
    if ( server_ == nullptr )
    {
        return 0;
    }
    if ( pv.name == nullptr || pv.data == nullptr )
    {
        return 0;
    }
    try
    {
        server_->addPV( simple_epics::pvDoubleAttributes(
            server_->prefix( ) + pv.name,
            reinterpret_cast< std::atomic< double >* >( pv.data ),
            std::make_pair( pv.alarm_low, pv.alarm_high ),
            std::make_pair( pv.warn_low, pv.warn_high ),
            ( pv.read_write == SIMPLE_PV_READ_ONLY
                  ? simple_epics::PVMode::ReadOnly
                  : simple_epics::PVMode::ReadWrite ) ) );
    }
    catch ( ... )
    {
        return 0;
    }
    return 1;
}

int
simple_pv_server_add_string( simple_pv_handle server, SimpleStringPV pv )
{
    auto server_ = reinterpret_cast< simple_epics::Server* >( server );
    if ( server_ == nullptr )
    {
        return 0;
    }
    if ( pv.name == nullptr || pv.io.write_fn == nullptr )
    {
        return 0;
    }
    try
    {
        server_->addPV( simple_epics::pvStringAttributes(
            server_->prefix( ) + pv.name,
            pv.io,
            ( pv.read_write == SIMPLE_PV_READ_ONLY
                  ? simple_epics::PVMode::ReadOnly
                  : simple_epics::PVMode::ReadWrite ) ) );
    }
    catch ( ... )
    {
        return 0;
    }
    return 1;
}

void
simple_pv_server_update( simple_pv_handle server )
{
    auto server_ = reinterpret_cast< simple_epics::Server* >( server );
    if ( server_ == nullptr )
    {
        return;
    }
    server_->update( );
    fileDescriptorManager.process( 0. );
}

void
simple_pv_server_destroy( simple_pv_handle* server )
{
    if ( server == nullptr || *server == nullptr )
    {
        return;
    }
    std::unique_ptr< simple_epics::Server > server_(
        reinterpret_cast< simple_epics::Server* >( *server ) );
}

StringValue
string_value_create( const char* input )
{
    StringValue sv{ };
    sv.str = string_value_dummy_val;
    if ( input )
    {
        auto  len = strlen( input ) + 1;
        char* val = static_cast< char* >( malloc( len ) );
        if ( val )
        {
            std::strcpy( val, input );
            sv.str = val;
        }
    }
    return sv;
}

void
string_value_destroy( StringValue* sv )
{
    if ( !sv )
    {
        return;
    }
    if ( sv->str && sv->str != string_value_dummy_val )
    {
        std::free(
            const_cast< void* >( static_cast< const void* >( sv->str ) ) );
    }
    sv->str = nullptr;
}
}
