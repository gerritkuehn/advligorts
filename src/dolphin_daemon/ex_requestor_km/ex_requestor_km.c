#include <net/sock.h>
#include <linux/module.h>
#include <linux/netlink.h>
#include <linux/skbuff.h>
#include <linux/kthread.h>  // for threads

#include "daemon_messages.h"


struct sock *nl_sock = NULL;

static struct task_struct *g_thread = 0;
static char thread_name[] = {"test thread"};

volatile void * g_read_addr = 0;
volatile void * g_write_addr = 0;

static void netlink_test_recv_msg(struct sk_buff *skb)
{
    struct nlmsghdr *nlh;
    int pid;
    all_dolphin_daemon_msgs_union any_msg;

    nlh = (struct nlmsghdr *)skb->data;
    pid = nlh->nlmsg_pid; // pid of sending process 


    any_msg.as_hdr = (dolphin_mc_header *) nlmsg_data(nlh);

    if ( !check_mc_header_valid(any_msg.as_hdr, nlmsg_len(nlh)) ) //Did not get a good header
    {
        printk(KERN_ERR "netlink_test: Malformed message, too short or bad preamble\n");
        return;
    }

    if(any_msg.as_hdr->interface_id != RT_KM_INTERFACE_ID)
        return; //Msg not ment for us


    switch(any_msg.as_hdr->msg_id)
    {
        case DOLPHIN_DAEMON_ALLOC_RESP:
            if ( !check_alloc_resp_valid(any_msg.as_alloc_resp, nlmsg_len(nlh)) )
            {
                printk(KERN_ERR "netlink_test: Got a DOLPHIN_DAEMON_ALLOC_RESP, but message was not valid, discarding.\n");
                return;
            }

            //TODO: Don't print so much
            printk(KERN_INFO "netlink_test: Received %d pyload bytes from pid %d, sizeof(volatile void *) %lu\n", 
                   nlmsg_len(nlh), pid, sizeof(volatile void *));
            printk(KERN_INFO "netlink_test: msg_type %u, status %u, num_addrs %u\n", 
                   any_msg.as_alloc_resp->header.msg_id, any_msg.as_alloc_resp->status, any_msg.as_alloc_resp->num_addrs);

            //Loop over all the returned addrs
            for(int i=0; i < any_msg.as_alloc_resp->num_addrs; ++i)
            {
                printk(KERN_INFO "netlink_test: \t read addr: 0x%lx\n", (long unsigned int)any_msg.as_alloc_resp->addrs[i].read_addr);
                printk(KERN_INFO "netlink_test: \t write addr 0x%lx\n", (long unsigned int)any_msg.as_alloc_resp->addrs[i].write_addr);
                g_read_addr = any_msg.as_alloc_resp->addrs[i].read_addr;
                g_write_addr = any_msg.as_alloc_resp->addrs[i].write_addr;
            }


        break;

        default:
            printk(KERN_ERR "netlink_test: Got a message with ID %u, which we don't support, discarding.\n", any_msg.as_hdr->msg_id);
            return;
        break;

   }



}



int multicast_thread_fn( void * pv )
{

    struct sk_buff *skb_out;
    struct nlmsghdr *nlh;
    int res;
    dolphin_mc_alloc_req * alloc_req;

    unsigned num_segments = 1; //TODO:
    unsigned total_payload_sz = GET_ALLOC_REQ_SZ(num_segments);

    //Send one request
    skb_out = nlmsg_new( total_payload_sz, 0);
    if (!skb_out) {
        printk(KERN_ERR "netlink_test: Failed to allocate new skb\n");
        while(!kthread_should_stop())
        {
            msleep(1000);
        }
        return 0;
    }

    //Allocate and send message
    nlh = nlmsg_put(skb_out, 0, 0, NLMSG_DONE, total_payload_sz, 0);


    alloc_req = (dolphin_mc_alloc_req*) nlmsg_data(nlh);
    alloc_req->header.msg_id = DOLPHIN_DAEMON_ALLOC_REQ;
    alloc_req->header.preamble_seq = DOLPHIN_DAEMON_MSG_PREAMBLE;
//    alloc_req->segment_sz_bytes = 8192;
    alloc_req->num_segments = num_segments;
    for(int i =0; i < num_segments; ++i)
    {
        alloc_req->segments[i].segment_id = 1;
        alloc_req->segments[i].adapter_num = 0;
        alloc_req->segments[i].segment_sz_bytes = 8192;

    } 

    res = nlmsg_multicast(nl_sock, skb_out, 0, DOLPHIN_DAEMON_REQ_GRP_ID, GFP_KERNEL);
    skb_out = NULL;


    int c = 0;
    while(!kthread_should_stop()) 
    {
        /*
        if(g_read_addr != 0)
        {
            printk("Read from returned pointer : %d\n", *( ((volatile int *) g_read_addr)+1024 ) ); //+Sync offset
        }*/

        if(g_write_addr != 0)
        {
            *( ((volatile int *) g_write_addr)+1024 ) = c;
        }

        msleep(1000);
        ++c;
    }
    return 0;
}



void send_free_req( void )
{
    struct sk_buff *skb_out;
    struct nlmsghdr *nlh;
    int res;
    dolphin_mc_free_all_req * free_req;

    unsigned total_payload_sz = sizeof( dolphin_mc_free_all_req );

    //Send one request
    skb_out = nlmsg_new( total_payload_sz, 0);
    if (!skb_out) {
        printk(KERN_ERR "netlink_test: Failed to allocate new skb\n");
        return;
    }

    //Allocate and send message
    nlh = nlmsg_put(skb_out, 0, 0, NLMSG_DONE, total_payload_sz, 0);


    free_req = (dolphin_mc_free_all_req*) nlmsg_data(nlh);
    free_req->header.msg_id = DOLPHIN_DAEMON_FREE_REQ;
    free_req->header.preamble_seq = DOLPHIN_DAEMON_MSG_PREAMBLE;

    res = nlmsg_multicast(nl_sock, skb_out, 0, DOLPHIN_DAEMON_REQ_GRP_ID, GFP_KERNEL);
    skb_out = NULL;

}


static int __init netlink_test_init(void)
{
    printk(KERN_INFO "netlink_test: Init module\n");

    struct netlink_kernel_cfg cfg = {
        .input = netlink_test_recv_msg,
    };

    nl_sock = netlink_kernel_create(&init_net, DOLPHIN_DAEMON_REQ_LINK_ID, &cfg);
    if (!nl_sock) {
        printk(KERN_ALERT "netlink_test: Error creating socket.\n");
        return -10;
    }

    g_thread = kthread_run(multicast_thread_fn, NULL, thread_name);
    if( !g_thread )
    {
        printk(KERN_ERR  " - Could not create the reader kthread.\n");
        return -5;
    }


    return 0;
}

static void __exit netlink_test_exit(void)
{
    printk(KERN_INFO "netlink_test: Exit module\n");

    if(g_thread)
        kthread_stop(g_thread);

    send_free_req();

    netlink_kernel_release(nl_sock);
}

module_init(netlink_test_init);
module_exit(netlink_test_exit);

MODULE_LICENSE("Dual MIT/GPL");
