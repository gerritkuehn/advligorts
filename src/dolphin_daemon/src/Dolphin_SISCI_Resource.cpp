#include "Dolphin_SISCI_Resource.hpp"

//libspdlog-dev
#include "spdlog/spdlog.h"

//Dolphin helper defines
#define NO_CALLBACK         NULL
#define NO_FLAGS            0

#include <mutex>

unsigned Dolphin_SISCI_Resource::_usage_count = 0;
std::mutex Dolphin_SISCI_Resource::_data_mutex;
const unsigned Dolphin_SISCI_Resource::_local_adapter_num = 0;
unsigned   Dolphin_SISCI_Resource::_local_node_id;

std::unique_ptr< Dolphin_SISCI_Resource > Dolphin_SISCI_Resource::create_instance()
{
    sci_error_t error;

    std::unique_ptr< Dolphin_SISCI_Resource > me_ptr (new Dolphin_SISCI_Resource());


    std::lock_guard<std::mutex> guard( Dolphin_SISCI_Resource::_data_mutex );
    if( Dolphin_SISCI_Resource::_usage_count == 0 )
    {
        // Initialize the SISCI library 
        SCIInitialize(NO_FLAGS, &error);
        if (error != SCI_ERR_OK) {
            spdlog::error("Dolphin_SISCI_Resource::init_dolphin() - SCIInitialize failed - Error code: 0x{:x}", error);
            SCITerminate();
            return nullptr;
        }

        // Get local nodeId 
        SCIGetLocalNodeId(_local_adapter_num,
                          &_local_node_id,
                          NO_FLAGS,
                          &error);

        if (error != SCI_ERR_OK) {
            spdlog::error("Dolphin_SISCI_Resource::create_instance() - Could not find the local adapter {}", _local_adapter_num);
            SCITerminate();
            return nullptr;
        }

    }
    ++Dolphin_SISCI_Resource::_usage_count;

    // Open a virtual dev descriptor 
    SCIOpen(&me_ptr->_v_dev, NO_FLAGS, &error);
    if (error != SCI_ERR_OK) {
        if (error == SCI_ERR_INCONSISTENT_VERSIONS) {
            spdlog::error("Dolphin_SISCI_Resource::init_dolphin() - Version mismatch between SISCI user library and SISCI driver");
        }
        spdlog::error("Dolphin_SISCI_Resource::init_dolphin() - SCIOpen failed - Error code 0x{:x}", error);
        return nullptr;
    }

    return me_ptr;
}

Dolphin_SISCI_Resource::~Dolphin_SISCI_Resource()
{
    sci_error_t error;
    
    std::lock_guard<std::mutex> guard( Dolphin_SISCI_Resource::_data_mutex );
   
    clean_up_segment();

    // Close the file descriptor 
    SCIClose(_v_dev, NO_FLAGS, &error);
    --Dolphin_SISCI_Resource::_usage_count;

    //If this is the last usage, clean up library
    if ( Dolphin_SISCI_Resource::_usage_count == 0)
    {
        spdlog::info("! Calling SCITerminate() !");
        SCITerminate();
    }

}


DOLPHIN_ERROR_CODES Dolphin_SISCI_Resource::connect_mc_segment(unsigned segment_id, unsigned segment_size)
{
    sci_error_t error;

    if ( !supports_multicast() )
    {
        spdlog::error("Dolphin_SISCI_Resource::connect_mc_segment() - Called, but dolphin network does not support multicast.");
        return DOLPHIN_ERROR_NO_MCAST_SUPPORT;
    }

    // Create local reflective memory segment 
    SCICreateSegment(_v_dev, &_local_segment, segment_id, segment_size, NO_CALLBACK, NULL, SCI_FLAG_BROADCAST, &error);
    if (error == SCI_ERR_OK) {
        spdlog::info("Dolphin_SISCI_Resource::connect_mc_segment() - Local segment (id=0x{:x}, size={}) is created.", segment_id, segment_size);
    } else {
        spdlog::error("Dolphin_SISCI_Resource::connect_mc_segment() - SCICreateSegment failed - Error code 0x{:x}", error);
        return DOLPHIN_ERROR_SEGMENT_SETUP_ERROR;
    }

    // Prepare the segment 
    SCIPrepareSegment(_local_segment, _local_adapter_num, SCI_FLAG_BROADCAST, &error);
    if (error == SCI_ERR_OK) {
        spdlog::info("Dolphin_SISCI_Resource::connect_mc_segment() - Local segment (id=0x{:x}, size={}) is prepared.", segment_id, segment_size);
    } else {
        spdlog::error("Dolphin_SISCI_Resource::connect_mc_segment() - SCIPrepareSegment failed: {} 0x{:x}.", SCIGetErrorString(error), error);
        return DOLPHIN_ERROR_SEGMENT_SETUP_ERROR;
    }

    /* Map local segment to user space - this is the address to read back data from the reflective memory region */
    volatile void * readAddr = SCIMapLocalSegment(_local_segment, &_local_map, 0, segment_size, NULL, NO_FLAGS, &error);
    if (error == SCI_ERR_OK) {
        spdlog::info("Local segment (id=0x{:x}) is mapped to user space at 0x{:x}", segment_id, (unsigned long)readAddr);
    } else {
        spdlog::error("SCIMapLocalSegment failed - Error code 0x{:x}", error);
        return DOLPHIN_ERROR_SEGMENT_SETUP_ERROR;
    }

    // Set the segment available 
    SCISetSegmentAvailable(_local_segment, _local_adapter_num, NO_FLAGS, &error);
    if (error != SCI_ERR_OK) {
        spdlog::error("Dolphin_SISCI_Resource::connect_mc_segment() - SCISetSegmentAvailable failed: {} ({})",SCIGetErrorString(error), error);
        return DOLPHIN_ERROR_SEGMENT_SETUP_ERROR;
    }

    //Store the kernel side read address
    _read_addr = get_kernel_read_addr();

    //Not mapping the segment into userspace (for now)
    

    SCIConnectSegment(_v_dev, &_remote_segment, DIS_BROADCAST_NODEID_GROUP_ALL, segment_id, _local_adapter_num,
                      NO_CALLBACK, NULL, SCI_INFINITE_TIMEOUT, SCI_FLAG_BROADCAST, &error);
    if (error != SCI_ERR_OK)
    {
        spdlog::error("Dolphin_SISCI_Resource::connect_mc_segment() - SCIConnectSegment, failed to connect for write addr: {} 0x{:x}",
                      SCIGetErrorString(error), error);
        return DOLPHIN_ERROR_SEGMENT_SETUP_ERROR;
    }

    //TODO: Figure out how to get write kernel address


    mc_segment_initialized = true;
    _segment_id = segment_id;

    return DOLPHIN_ERROR_OK;
}

int Dolphin_SISCI_Resource::get_segment_id() 
{
    if(mc_segment_initialized = false)
        return -1;
    else 
        return _segment_id; 
}

volatile void * Dolphin_SISCI_Resource::kernel_read_addr() 
{ 
    return _read_addr; 
}

volatile void * Dolphin_SISCI_Resource::kernel_write_addr() 
{ 
    return _write_addr; 
}


//Private member functions
bool Dolphin_SISCI_Resource::supports_multicast()
{
    sci_query_adapter_t queryAdapter = {0};
    sci_error_t         error;
    unsigned int        mcast_max_groups = 0;

    queryAdapter.localAdapterNo = _local_adapter_num;
    queryAdapter.subcommand     = SCI_Q_ADAPTER_MCAST_MAX_GROUPS;
    queryAdapter.data           = &mcast_max_groups;

    SCIQuery(SCI_Q_ADAPTER, &queryAdapter, NO_FLAGS, &error);

    // Multicast support is present if the number of multicast groups is not zero 
    if (mcast_max_groups != 0) return true;
    else return false;
}

volatile void * Dolphin_SISCI_Resource::get_kernel_read_addr()
{
    sci_error_t         error;
    sci_query_local_segment_t qlseg;
    qlseg.subcommand = SCI_Q_LOCAL_SEGMENT_VIRTUAL_KERNEL_ADDR;
    qlseg.segment=_local_segment;

    SCIQuery(SCI_Q_LOCAL_SEGMENT, &qlseg, NO_FLAGS, &error);

    if (error != SCI_ERR_OK) return nullptr;

    return (volatile void *)qlseg.data.ioaddr;
}

void Dolphin_SISCI_Resource::clean_up_segment()
{
    sci_error_t         error;
    if (mc_segment_initialized)
    {
        SCIDisconnectSegment(_remote_segment, NO_FLAGS, &error);

        SCIUnmapSegment(_local_map, NO_FLAGS, &error);
        SCISetSegmentUnavailable(_local_segment, _local_adapter_num, NO_FLAGS, &error);
        SCIRemoveSegment(_local_segment, NO_FLAGS, &error);

    }
}


