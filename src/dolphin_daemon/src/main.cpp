#include <iostream>
#include <chrono>
#include <signal.h>

//libspdlog-dev
#include "spdlog/spdlog.h"

#if SPDLOG_VERSION > 10600
#include "spdlog/cfg/env.h"
#endif

#include "LIGO_Thread.hpp"
#include "DolphinKernelProxy.hpp"

using namespace std;


static volatile int g_stop = 0;
void signal_handler(int sig) 
{
    if( sig == SIGINT)
    {
        spdlog::warn("Got exit signal, exiting...");
        g_stop = 1;
    }
}


int main(int argc, char * argv[])
{
    signal(SIGINT, signal_handler);
    signal(SIGHUP, signal_handler);

    #if SPDLOG_VERSION > 10600
    //Load log level from ENV
    spdlog::cfg::load_env_levels();
    #endif

    std::unique_ptr< DolphinKernelProxy > tt = DolphinKernelProxy::create_instance();
    if(tt.get() == nullptr) return 0;

    tt->nonblocking_start();

    spdlog::info("Started DolphinKernelProxy thread... Waiting for mcast netlink requests.");

    while( !g_stop )
    {
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }

    tt->signal_stop_and_join();

    return 0;
}
