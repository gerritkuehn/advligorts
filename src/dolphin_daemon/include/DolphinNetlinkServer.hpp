#ifndef LIGO_DOLPHIN_NETLINK_SERVER_HPP
#define LIGO_DOLPHIN_NETLINK_SERVER_HPP

#include "LIGO_Thread.hpp"
#include "Dolphin_SISCI_Resource.hpp"
#include "daemon_messages.h"


#include <linux/netlink.h>
#include <sys/socket.h>
#include <memory>
#include <map>


class DolphinNetlinkServer : public LIGO_Thread
{
    public:

        static std::unique_ptr< DolphinNetlinkServer > create_instance();

        virtual ~DolphinNetlinkServer();

        void thread_body();

        //Initialization Helpers
        bool init_sockets();

        //Message Handlers 
        void handle_free_all_req( dolphin_mc_free_all_req *  );
        void handle_alloc_req( dolphin_mc_alloc_req * );


    private:

        DolphinNetlinkServer();
        void build_and_send_alloc_resp_error(DOLPHIN_ERROR_CODES status);
        void send_netlink_message(void * msg_ptr, unsigned sz_bytes);

        //Netlink Socket Info
        struct sockaddr_nl _src_addr;
        int _sock_fd;

        //Dolphin Storage
        std::map< unsigned, std::unique_ptr< Dolphin_SISCI_Resource > > _dolphin_resources;


};


#endif //LIGO_DOLPHIN_NETLINK_SERVER_HPP
