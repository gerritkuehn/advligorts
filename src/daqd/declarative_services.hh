//
// Created by jonathan.hanks on 6/12/23.
//

#ifndef DAQD_TRUNK_DECLARATIVE_SERVICES_HH
#define DAQD_TRUNK_DECLARATIVE_SERVICES_HH

#include <algorithm>
#include <iterator>
#include <map>
#include <memory>
#include <sstream>
#include <vector>

namespace declarative_config
{
    class service_base
    {
    public:
        service_base( ) = default;
        virtual ~service_base( ) = default;

        virtual std::string name( ) = 0;

        virtual size_t      size( ) const noexcept = 0;
        virtual std::string at( const std::string& index ) const = 0;
        std::string         at_first( const std::string& index ) const;
        virtual std::vector< std::string > fields( ) const = 0;

        static std::unique_ptr< service_base >
        get_service( const std::string& name );

        static std::vector< std::string > get_services( );
    };

    class service_configuration_number_client : public service_base
    {
    public:
        service_configuration_number_client( ) : service_base( )
        {
        }
        ~service_configuration_number_client( ) override = default;

        std::string
        name( ) override
        {
            return "config_number_client";
        }
        size_t
        size( ) const noexcept override
        {
            return 1;
        }
        std::string
        at( const std::string& index ) const override
        {
            if ( index == "server" )
            {
                return server;
            }
            throw std::out_of_range( "invalid index" );
        }
        std::vector< std::string >
        fields( ) const override
        {
            return std::vector< std::string >( { "server" } );
        }

        std::string server{ "" };
    };

    class service_frame_writer : public service_base
    {
    public:
        service_frame_writer( ) : service_base( )
        {
        }
        ~service_frame_writer( ) override = default;
        std::string
        name( ) override
        {
            return "frame_writer";
        }

        size_t
        size( ) const noexcept override
        {
            return 4;
        }
        std::vector< std::string >
        fields( ) const override
        {
            return std::vector< std::string >{
                "frame_dir",
                "frame_type",
                "seconds_per_frame",
                "checksum_dir",
            };
        }
        std::string
        at( const std::string& index ) const override
        {
            if ( index == "frame_dir" )
            {
                return frame_dir;
            }
            else if ( index == "frame_type" )
            {
                return frame_type;
            }
            else if ( index == "seconds_per_frame" )
            {
                std::ostringstream os{ };
                os << seconds_per_frame;
                return os.str( );
            }
            else if ( index == "checksum_dir" )
            {
                return checksum_dir;
            }
            throw std::out_of_range( "invalid index" );
        }

        std::string frame_dir{ "" };
        std::string frame_type{ "" };
        std::string checksum_dir{ "" };
        int         seconds_per_frame{ 64 };
    };

    class service_strend_writer : public service_base
    {
    public:
        service_strend_writer( ) : service_base( )
        {
        }
        ~service_strend_writer( ) override = default;
        std::string
        name( ) override
        {
            return "strend_writer";
        }

        size_t
        size( ) const noexcept override
        {
            return 4;
        }
        std::vector< std::string >
        fields( ) const override
        {
            return std::vector< std::string >{
                "frame_dir",
                "frame_type",
                "minutes_per_frame",
                "checksum_dir",
            };
        }
        std::string
        at( const std::string& index ) const override
        {
            if ( index == "frame_dir" )
            {
                return frame_dir;
            }
            else if ( index == "frame_type" )
            {
                return frame_type;
            }
            else if ( index == "minutes_per_frame" )
            {
                std::ostringstream os{ };
                os << minutes_per_frame;
                return os.str( );
            }
            else if ( index == "checksum_dir" )
            {
                return checksum_dir;
            }
            throw std::out_of_range( "invalid index" );
        }

        std::string frame_dir{ "" };
        std::string frame_type{ "" };
        std::string checksum_dir{ "" };
        int         minutes_per_frame{ 10 };
        int
        seconds_per_frame( ) const noexcept
        {
            return minutes_per_frame * 60;
        }
    };

    class service_mtrend_writer : public service_base
    {
    public:
        service_mtrend_writer( ) : service_base( )
        {
        }
        ~service_mtrend_writer( ) override = default;
        std::string
        name( ) override
        {
            return "mtrend_writer";
        }

        size_t
        size( ) const noexcept override
        {
            return 4;
        }
        std::vector< std::string >
        fields( ) const override
        {
            return std::vector< std::string >{
                "frame_dir",
                "frame_type",
                "minutes_per_frame",
                "checksum_dir",
            };
        }
        std::string
        at( const std::string& index ) const override
        {
            if ( index == "frame_dir" )
            {
                return frame_dir;
            }
            else if ( index == "frame_type" )
            {
                return frame_type;
            }
            else if ( index == "minutes_per_frame" )
            {
                std::ostringstream os{ };
                os << minutes_per_frame;
                return os.str( );
            }
            else if ( index == "checksum_dir" )
            {
                return checksum_dir;
            }
            throw std::out_of_range( "invalid index" );
        }

        std::string frame_dir{ "" };
        std::string frame_type{ "" };
        std::string checksum_dir{ "" };
        int         minutes_per_frame{ 60 };
        int
        seconds_per_frame( ) const noexcept
        {
            return minutes_per_frame * 60;
        }
    };

    class service_raw_mtrend_writer : public service_base
    {
    public:
        service_raw_mtrend_writer( ) : service_base( )
        {
        }
        ~service_raw_mtrend_writer( ) override = default;
        std::string
        name( ) override
        {
            return "raw_mtrend_writer";
        }

        size_t
        size( ) const noexcept override
        {
            return 2;
        }
        std::vector< std::string >
        fields( ) const override
        {
            return std::vector< std::string >{
                "trend_dir",
                "save_period_minutes",
            };
        }
        std::string
        at( const std::string& index ) const override
        {
            if ( index == "trend_dir" )
            {
                return trend_dir;
            }
            else if ( index == "save_period_minutes" )
            {
                std::ostringstream os{ };
                os << save_period_minutes;
                return os.str( );
            }
            throw std::out_of_range( "invalid index" );
        }

        std::string trend_dir{ "" };
        int         save_period_minutes{ 5 };
        int
        save_period_seconds( ) const noexcept
        {
            return save_period_minutes * 60;
        }
    };

    class service_nds1 : public service_base
    {
    public:
        service_nds1( ) : service_base( )
        {
        }
        ~service_nds1( ) override = default;
        std::string
        name( ) override
        {
            return "nds1";
        }

        size_t
        size( ) const noexcept override
        {
            return 9;
        }
        std::vector< std::string >
        fields( ) const override
        {
            return std::vector< std::string >{
                "job_dir",     "trend_dirs", "port",
                "frame_dir",   "frame_type", "strend_dir",
                "strend_type", "mtrend_dir", "mtrend_type",
            };
        }
        std::string
        at( const std::string& index ) const override
        {
            if ( index == "job_dir" )
            {
                return job_dir;
            }
            else if ( index == "trend_dirs" )
            {
                std::ostringstream os{ };
                auto               first{ true };
                for ( const auto& entry : trend_dirs )
                {
                    os << ( !first ? "," : "" ) << entry;
                    first = false;
                }
                auto tmp{ os.str( ) };
                return tmp;
            }
            else if ( index == "port" )
            {
                std::ostringstream os{ };
                os << port;
                return os.str( );
            }
            else if ( index == "frame_dir" )
            {
                return frame_dir;
            }
            else if ( index == "frame_type" )
            {
                return frame_type;
            }
            else if ( index == "strend_dir" )
            {
                return strend_dir;
            }
            else if ( index == "strend_type" )
            {
                return strend_type;
            }
            else if ( index == "mtrend_dir" )
            {
                return mtrend_dir;
            }
            else if ( index == "mtrend_type" )
            {
                return mtrend_type;
            }
            throw std::out_of_range( "invalid index" );
        }

        std::string                job_dir{ "/var/run/nds" };
        std::vector< std::string > trend_dirs{ };
        std::string                frame_dir{ "" };
        std::string                frame_type{ "" };
        std::string                strend_dir{ "" };
        std::string                strend_type{ "" };
        std::string                mtrend_dir{ "" };
        std::string                mtrend_type{ "" };
        int                        port{ 8088 };
    };

    class service_epics : public service_base
    {
    public:
        service_epics( ) : service_base( )
        {
        }
        ~service_epics( ) override = default;
        std::string
        name( ) override
        {
            return "epics";
        }

        size_t
        size( ) const noexcept override
        {
            return 1;
        }
        std::vector< std::string >
        fields( ) const override
        {
            return std::vector< std::string >{
                "prefix",
            };
        }
        std::string
        at( const std::string& index ) const override
        {
            if ( index == "prefix" )
            {
                return prefix;
            }
            throw std::out_of_range( "invalid index" );
        }

        std::string prefix{ "" };
    };

    class service_gds_broadcast : public service_base
    {
    public:
        service_gds_broadcast( ) : service_base( )
        {
        }
        ~service_gds_broadcast( ) override = default;
        std::string
        name( ) override
        {
            return "gds_broadcast";
        }

        size_t
        size( ) const noexcept override
        {
            return 6;
        }
        std::vector< std::string >
        fields( ) const override
        {
            return std::vector< std::string >{
                "address",       "network",   "port",
                "broadcast_ini", "frame_dir", "frame_type",
            };
        }
        std::string
        at( const std::string& index ) const override
        {
            if ( index == "address" )
            {
                return address;
            }
            else if ( index == "network" )
            {
                return network;
            }
            else if ( index == "port" )
            {
                std::ostringstream os{ };
                os << port;
                return os.str( );
            }
            else if ( index == "broadcast_ini" )
            {
                return broadcast_ini;
            }
            else if ( index == "frame_dir" )
            {
                return frame_dir;
            }
            else if ( index == "frame_type" )
            {
                return frame_type;
            }
            throw std::out_of_range( "invalid index" );
        }

        std::string address{ "" };
        std::string network{ "" };
        int         port{ 7096 };
        std::string broadcast_ini{ "/etc/advligorts/gds_broadcast.ini" };
        std::string frame_dir{ "/dev/shm" };
        std::string frame_type{ "" };
    };

    class service_daqd_control : public service_base
    {
    public:
        service_daqd_control( ) : service_base( )
        {
        }
        ~service_daqd_control( ) override = default;
        std::string
        name( ) override
        {
            return "daqd_control";
        }

        size_t
        size( ) const noexcept override
        {
            return 2;
        }
        std::vector< std::string >
        fields( ) const override
        {
            return std::vector< std::string >{
                "binary_port",
                "text_port",
            };
        }
        std::string
        at( const std::string& index ) const override
        {
            if ( index == "binary_port" )
            {
                std::ostringstream os{ };
                os << binary_port;
                return os.str( );
            }
            else if ( index == "text_port" )
            {
                std::ostringstream os{ };
                os << text_port;
                return os.str( );
            }
            throw std::out_of_range( "invalid index" );
        }

        int binary_port{ 8088 };
        int text_port{ 8087 };
    };

    class field_override_service : public service_base
    {
    public:
        field_override_service( std::shared_ptr< service_base >      source,
                                std::map< std::string, std::string > overrides )
            : service_base( ), source_{ source }, overrides_{ overrides }
        {
        }
        ~field_override_service( ) override = default;

        std::string
        name( ) override
        {
            return source_->name( );
        }

        size_t
        size( ) const noexcept override
        {
            return source_->size( );
        }
        std::string
        at( const std::string& index ) const override
        {
            auto val = source_->at( index );
            if ( !val.empty( ) )
            {
                return val;
            }
            auto it = overrides_.find( index );
            if ( it != overrides_.end( ) )
            {
                val = it->second;
            }
            return val;
        }
        std::vector< std::string >
        fields( ) const override
        {
            return source_->fields( );
        }

    private:
        std::map< std::string, std::string > overrides_{ };
        std::shared_ptr< service_base >      source_{ };
    };

} // namespace declarative_config

#endif // DAQD_TRUNK_DECLARATIVE_SERVICES_HH
