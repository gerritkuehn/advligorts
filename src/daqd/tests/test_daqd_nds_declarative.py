
import os
import os.path
import time
import typing

import integration

integration.Executable(name="fe_simulated_streams", hints=["../fe_stream_test/fe_simulation",], description="data stream generator")
integration.Executable(name="daqd", hints=[], description="daqd")
integration.Executable(name="nds", hints=["../nds",], description="nds")
integration.Executable(name="fe_stream_check_nds", hints=["../fe_stream_test",], description="verification program")

ini_dir = integration.state.temp_dir('ini_files')
frames_dir = integration.state.temp_dir('frames')
os.mkdir(os.path.join(frames_dir, "full"))
nds_dir = integration.state.temp_dir('nds')
master_file = os.path.join(ini_dir, "master")
testpoint_file = ""
daqdrc_file = os.path.join(ini_dir, "daqd.yaml")

integration.transform_text_file(input='nds_test.yaml', output=daqdrc_file,
                                substitutions=[('MASTER', master_file),
                                               ('TESTPOINT', testpoint_file),
                                               ('FRAMES', frames_dir),
                                               ('NDS', nds_dir)])

fe_simulated_streams = integration.Process("fe_simulated_streams",
                                           ["-i", ini_dir,
                                            "-M", master_file,
                                            "-b", "local_dc",
                                            "-m", "100",
                                            "-k", "400",
                                            "-R", "50"])

daqd = integration.Process("daqd",
                           ["-c", daqdrc_file])

nds = integration.Process("nds",
                          ["--rundir", nds_dir])

fe_stream_check = None

def find_frame(root_dir:str) -> typing.Optional[str]:
    for dirs, dirnames, filenames in os.walk(root_dir):
        for filename in filenames:
            if filename.endswith('.gwf'):
                if os.path.basename(filename).startswith('.'):
                    continue
                return filename
    return None

def wait_for_frames(root_dir:str, timeout:float=30.0):

    end_time = time.time() + timeout
    def do_wait():
        while True:
            if find_frame(root_dir) is not None:
                return
            if time.time() > end_time:
                raise RuntimeError('Timed out while waiting for frame files')
            time.sleep(1.0)

    return do_wait

def run_check(root_dir:str):
    def do_check():
        global fe_stream_check
        frame = find_frame(root_dir=root_dir)
        if frame is None:
            raise RuntimeError("No frame found")
        gps = frame.split('-')[-2]
        fe_stream_check = integration.Process("fe_stream_check_nds",
                                              ["-c", "30",
                                               "-t", gps])
        fe_stream_check.run()

    return do_check


def check_ok(timeout:float):
    end_time = time.time() + timeout
    def do_check():
        global fe_stream_check
        while True:
            state = fe_stream_check.state()
            if state == integration.Process.STOPPED:
                fe_stream_check.ignore()
                return
            elif state == integration.Process.FAILED:
                raise RuntimeError('Data check failed')
            print("state is {0}".format(state))

            if time.time() > end_time:
                raise RuntimeError('Timed out while waiting for nds check to complete')
            time.sleep(0.5)

    return do_check

def echo(msg):
    def wrapper():
        print(msg)
    return wrapper

integration.Sequence(
    [
        integration.state.preserve_files,
        integration.require_readable(files=["/dev/gpstime", "/dev/mbuf"], description="required device files"),
        fe_simulated_streams.run,
        integration.wait(),
        daqd.run,
        integration.wait_tcp_server(port=8088, timeout=20),
        echo("daqd is up now"),
        nds.run,
        integration.wait(),
        wait_for_frames(root_dir=frames_dir, timeout=50),
        nds.ignore,
        daqd.ignore,
        fe_simulated_streams.ignore,
        run_check(root_dir=frames_dir),
        check_ok(timeout=50),
    ]
)