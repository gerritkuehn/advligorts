//
// Created by jonathan.hanks on 5/26/20.
//
#include "catch.hpp"

#include "declarative_config.hh"
#include "daqd_config.hh"
#include "raii.hh"

#include <iterator>
#include <sstream>

namespace
{
    std::istringstream
    gen_input( const char* input )
    {
        return std::istringstream{ input };
    }
} // namespace

TEST_CASE( "You can test a config filename to see if it might be a "
           "declarative/yamle file" )
{
    REQUIRE(
        declarative_config::is_declarative_config_file( "abc_config.yaml" ) );
    REQUIRE(
        !declarative_config::is_declarative_config_file( "abc_configrc" ) );
}

TEST_CASE( "The config must be a map type" )
{
    static const char* input = "---"
                               "5";
    auto               test_input = gen_input( input );
    REQUIRE_THROWS( declarative_config::parse( test_input ) );
}

TEST_CASE( "We can parse a config in yaml" )
{
    static const char* input =
        "---\n"
        "parameters:\n"
        "  # a comment here\n"
        "  stack_size: 102400\n"
        "  debug: 6\n"
        "  zero_bad_data: true\n"
        "  ignored: \"unknown\"\n"
        "detector:\n"
        "  site: TST\n"
        "  ifo: T1\n"
        "  longitude: -90.77\n"
        "  ignored: false\n"
        "  midpoints:\n"
        "    - 2000\n"
        "    - 2000\n"
        "inputs:\n"
        "  type: ini\n"
        "  ini_list: \"/etc/advligorts/master.ini\"\n"
        "  test_points: \"/etc/advligorts/testpoint.par\"\n"
        "services:\n"
        "  - service: strend_writer\n"
        "    minutes_per_frame: 15\n"
        "    ignored: \"hi there\"\n"
        "  - service: daqd_control\n"
        "    binary_port: 8090\n"
        "    text_port: 8091\n";
    auto test_input = gen_input( input );
    auto config = declarative_config::parse( test_input );

    REQUIRE( config.detector.name == "TST" );
    REQUIRE( config.detector.prefix == "T1" );

    REQUIRE( config.parameters.size( ) == 4 );
    REQUIRE_NOTHROW( config.parameters.at( "stack_size" ) );
    REQUIRE_NOTHROW( config.parameters.at( "debug" ) );
    REQUIRE_NOTHROW( config.parameters.at( "zero_bad_data" ) );
    REQUIRE_NOTHROW( config.parameters.at( "ignored" ) );
    REQUIRE( config.detector.longitude == -90.77 );
    REQUIRE( config.inputs.master_file_path == "/etc/advligorts/master.ini" );
    REQUIRE( config.inputs.testpoint_par_path ==
             "/etc/advligorts/testpoint.par" );
    REQUIRE( config.services.size( ) == 2 );
    REQUIRE_NOTHROW( config.find_service( "strend_writer" ) );
    REQUIRE( config.find_service( "strend_writer" )->size( ) == 4 );
    REQUIRE(
        config.find_service( "strend_writer" )->at( "minutes_per_frame" ) ==
        "15" );
    REQUIRE( config.find_service( "strend_writer" )->at( "checksum_dir" ) ==
             "" );
    REQUIRE_NOTHROW( config.find_service( "daqd_control" ) );
    REQUIRE( config.find_service( "daqd_control" )->size( ) == 2 );
    REQUIRE( config.find_service( "daqd_control" )->at( "binary_port" ) ==
             "8090" );
    REQUIRE( config.find_service( "daqd_control" )->at( "text_port" ) ==
             "8091" );
}

TEST_CASE( "We can parse a partial detector definition" )
{
    auto input = R"(---
detector:
  site: TST
  ifo: X6
)";
    auto test_input = gen_input( input );
    auto yaml_config = declarative_config::parse( test_input );
    REQUIRE( yaml_config.detector.name == "TST" );
    REQUIRE( yaml_config.detector.prefix == "X6" );
}
#include <iostream>
TEST_CASE(
    "We don't crash with explicit nulls in detector, parameters, inputs" )
{
    auto inputs = std::vector< std::string >{ "parameters: null",
                                              "detector: null",
                                              "inputs: null" };
    for ( const auto& input : inputs )
    {
        std::cerr << "checking " << input << "\n";
        auto test_input = gen_input( input.c_str( ) );
        REQUIRE_THROWS_AS( declarative_config::parse( test_input ),
                           std::runtime_error );
    }
}

TEST_CASE( "We can parse a input with a null testpoint field" )
{
    auto input = R"(---
detector:
  site: TST
  ifo: X6

inputs:
  type: ini
  testpoints: null
)";
    auto test_input = gen_input( input );
    auto yaml_config = declarative_config::parse( test_input );
    REQUIRE( yaml_config.inputs.testpoint_par_path == "" );
}

TEST_CASE( "We can parse a DC config in yaml and convert it into something the "
           "daqd can work with" )
{
    auto input = R"(---
parameters:
  zero_bad_data: false
  log: 1
  buffer_size: 100

detector:
  site: TST
  ifo: X6
  longitude: -90.7742
  latitude: 39.5629
  elevation: 0.0
  azimuths:
    - 1.1
    - 4.7123
  altitudes:
    - 1.0
    - 2.0
  midpoints:
    - 2000
    - 2000

services:
  - service: epics
  - service: daqd_control
)";
    auto test_input = gen_input( input );
    auto yaml_config = declarative_config::parse( test_input );
    auto config = daqd_config::to_config( yaml_config );

    using daqd_config::action;

    auto expected_service = std::vector< action >{
        action::CONFIGURE_CHANNELS,
        action::CONFIGURE_LOCATION,
        action::MAIN,
        action::EPICS,
        action::PROFILER,
        action::PRODUCER,
        action::DAQD_PROTOCOL,
        action::DAQD_TEXT_PROTOCOL,
        action::FINAL_CRC,
    };
    REQUIRE( config.actions == expected_service );
}

TEST_CASE( "We can set checksum dirs for the frame writers" )
{
    auto input = R"(---
parameters:
  zero_bad_data: false
  log: 1
  buffer_size: 100

detector:
  site: TST
  ifo: X6
  longitude: -90.7742
  latitude: 39.5629
  elevation: 0.0
  azimuths:
    - 1.1
    - 4.7123
  altitudes:
    - 1.0
    - 2.0
  midpoints:
    - 2000
    - 2000

services:
  - service: frame_writer
    checksum_dir: /custom1
  - service: strend_writer
    checksum_dir: /custom2
  - service: mtrend_writer
    checksum_dir: /custom3
)";
    auto test_input = gen_input( input );
    auto yaml_config = declarative_config::parse( test_input );

    REQUIRE( yaml_config.find_service( "frame_writer" )->at( "checksum_dir" ) ==
             "/custom1" );
    REQUIRE(
        yaml_config.find_service( "strend_writer" )->at( "checksum_dir" ) ==
        "/custom2" );
    REQUIRE(
        yaml_config.find_service( "mtrend_writer" )->at( "checksum_dir" ) ==
        "/custom3" );
}

TEST_CASE( "overlay_derived_defaults will fill in frame type information from "
           "the detector structure" )
{
    struct TestCase
    {
        TestCase( ) = default;
        TestCase( std::string pre,
                  std::string raw,
                  std::string strend,
                  std::string mtrend,
                  std::string broadcast ) noexcept
            : prefix{ std::move( pre ) },
              expected_raw{ std::move( raw ) },
              expected_strend{ std::move( strend ) },
              expected_mtrend{ std::move( mtrend ) },
              expected_broadcast{ std::move( broadcast ) }
        {
        }
        TestCase( TestCase&& ) noexcept = default;
        TestCase( const TestCase& ) = default;
        TestCase&   operator=( TestCase&& ) noexcept = default;
        TestCase&   operator=( const TestCase& ) = default;
        std::string prefix{ };
        std::string expected_raw{ };
        std::string expected_strend{ };
        std::string expected_mtrend{ };
        std::string expected_broadcast{ };
    };
    std::vector< TestCase > test_cases{
        TestCase{ "X1", "X-X1_R-", "X-X1_T-", "X-X1_M-", "X-R-" },
        TestCase{ "X6", "X-X6_R-", "X-X6_T-", "X-X6_M-", "X-R-" },
        TestCase{ "H1", "H-H1_R-", "H-H1_T-", "H-H1_M-", "H-R-" },
        TestCase{ "L1", "L-L1_R-", "L-L1_T-", "L-L1_M-", "L-R-" },
    };
    for ( const auto& test_case : test_cases )
    {
        declarative_config::config_t config{ };
        config.detector.prefix = test_case.prefix;
        config.services.emplace_back(
            raii::make_unique_ptr<
                declarative_config::service_frame_writer >( ) );
        config.services.emplace_back(
            raii::make_unique_ptr<
                declarative_config::service_strend_writer >( ) );
        config.services.emplace_back(
            raii::make_unique_ptr<
                declarative_config::service_mtrend_writer >( ) );
        config.services.emplace_back(
            raii::make_unique_ptr< declarative_config::service_nds1 >( ) );
        config.services.emplace_back(
            raii::make_unique_ptr<
                declarative_config::service_gds_broadcast >( ) );
        REQUIRE( config.find_service( "frame_writer" )->at( "frame_type" ) ==
                 "" );
        REQUIRE( config.find_service( "strend_writer" )->at( "frame_type" ) ==
                 "" );
        REQUIRE( config.find_service( "mtrend_writer" )->at( "frame_type" ) ==
                 "" );
        REQUIRE( config.find_service( "nds1" )->at( "frame_type" ) == "" );
        REQUIRE( config.find_service( "nds1" )->at( "strend_type" ) == "" );
        REQUIRE( config.find_service( "gds_broadcast" )->at( "frame_type" ) ==
                 "" );

        REQUIRE( config.find_service( "nds1" )->at( "frame_dir" ) == "" );
        REQUIRE( config.find_service( "nds1" )->at( "strend_dir" ) == "" );
        REQUIRE( config.find_service( "nds1" )->at( "mtrend_dir" ) == "" );

        REQUIRE( config.find_service( "frame_writer" )->at( "frame_dir" ) ==
                 "" );
        REQUIRE( config.find_service( "strend_writer" )->at( "frame_dir" ) ==
                 "" );
        REQUIRE( config.find_service( "mtrend_writer" )->at( "frame_dir" ) ==
                 "" );

        declarative_config::overlay_derived_defaults( config );

        REQUIRE( config.find_service( "frame_writer" )->at( "frame_type" ) ==
                 test_case.expected_raw );
        REQUIRE( config.find_service( "strend_writer" )->at( "frame_type" ) ==
                 test_case.expected_strend );
        REQUIRE( config.find_service( "mtrend_writer" )->at( "frame_type" ) ==
                 test_case.expected_mtrend );
        REQUIRE( config.find_service( "nds1" )->at( "frame_type" ) ==
                 test_case.expected_raw );
        REQUIRE( config.find_service( "nds1" )->at( "strend_type" ) ==
                 test_case.expected_strend );
        REQUIRE( config.find_service( "nds1" )->at( "mtrend_type" ) ==
                 test_case.expected_mtrend );
        REQUIRE( config.find_service( "gds_broadcast" )->at( "frame_type" ) ==
                 test_case.expected_broadcast );

        REQUIRE( config.find_service( "nds1" )->at( "frame_dir" ) ==
                 config.find_service( "frame_writer" )->at( "frame_dir" ) );
        {
            auto nds1_val = config.find_service( "nds1" )->at( "strend_dir" );
            auto strend_writer_val =
                config.find_service( "strend_writer" )->at( "frame_dir" );
            REQUIRE( nds1_val == strend_writer_val );
            REQUIRE(
                config.find_service( "nds1" )->at( "strend_dir" ) ==
                config.find_service( "strend_writer" )->at( "frame_dir" ) );
        }
        REQUIRE( config.find_service( "nds1" )->at( "mtrend_dir" ) ==
                 config.find_service( "mtrend_writer" )->at( "frame_dir" ) );
        REQUIRE( config.find_service( "nds1" )->at( "frame_dir" ) ==
                 "/frames/full" );
        REQUIRE( config.find_service( "nds1" )->at( "strend_dir" ) ==
                 "/frames/trend/second" );
        REQUIRE( config.find_service( "nds1" )->at( "mtrend_dir" ) ==
                 "/frames/trend/minute" );
    }
}

TEST_CASE( "overlay_defaults will also fill in defaults for raw trend writers "
           "and nds1 if no information is specified" )
{
    declarative_config::config_t config{ };
    config.detector.name = "TST";
    config.detector.prefix = "X6";
    config.services.emplace_back(
        raii::make_unique_ptr<
            declarative_config::service_raw_mtrend_writer >( ) );
    config.services.emplace_back(
        raii::make_unique_ptr< declarative_config::service_nds1 >( ) );

    REQUIRE( config.find_service( "raw_mtrend_writer" )->at( "trend_dir" ) ==
             "" );
    REQUIRE( config.find_service( "nds1" )->at_first( "trend_dirs" ) == "" );

    declarative_config::overlay_derived_defaults( config );

    REQUIRE( config.find_service( "raw_mtrend_writer" )->at( "trend_dir" ) ==
             "/frames/trend/raw" );
    REQUIRE( config.find_service( "nds1" )->at_first( "trend_dirs" ) ==
             "/frames/trend/raw" );
}

TEST_CASE( "overlay_defaults should error if there is a fatal difference "
           "between nds1 and raw_mtrend_writer configs" )
{
    declarative_config::config_t config{ };
    config.detector.name = "TST";
    config.detector.prefix = "X6";
    {
        auto ptr = raii::make_unique_ptr<
            declarative_config::service_raw_mtrend_writer >( );
        ptr->trend_dir = "/raw_trend";
        config.services.emplace_back( std::move( ptr ) );
    }
    {
        auto ptr = raii::make_unique_ptr< declarative_config::service_nds1 >( );
        ptr->trend_dirs = std::vector< std::string >{ "/frames/trend/raw" };
        config.services.emplace_back( std::move( ptr ) );
    }

    REQUIRE( config.find_service( "raw_mtrend_writer" )->at( "trend_dir" ) ==
             "/raw_trend" );
    REQUIRE( config.find_service( "nds1" )->at_first( "trend_dirs" ) ==
             "/frames/trend/raw" );

    REQUIRE_THROWS_AS( declarative_config::overlay_derived_defaults( config ),
                       std::runtime_error );
}

TEST_CASE( "overlay_defaults should error if there is a fatal difference "
           "between nds1 and strend_writer configs" )
{
    declarative_config::config_t config{ };
    config.detector.name = "TST";
    config.detector.prefix = "X6";
    {
        auto ptr = raii::make_unique_ptr<
            declarative_config::service_strend_writer >( );
        ptr->frame_dir = "/strend";
        config.services.emplace_back( std::move( ptr ) );
    }
    {
        auto ptr = raii::make_unique_ptr< declarative_config::service_nds1 >( );
        ptr->strend_dir = "/frames/trend/second";
        config.services.emplace_back( std::move( ptr ) );
    }

    REQUIRE( config.find_service( "strend_writer" )->at( "frame_dir" ) ==
             "/strend" );
    REQUIRE( config.find_service( "nds1" )->at_first( "strend_dir" ) ==
             "/frames/trend/second" );

    REQUIRE_THROWS_AS( declarative_config::overlay_derived_defaults( config ),
                       std::runtime_error );
}

TEST_CASE( "overlay_defaults should error if there is a fatal difference "
           "between nds1 and frame_writer configs" )
{
    declarative_config::config_t config{ };
    config.detector.name = "TST";
    config.detector.prefix = "X6";
    {
        auto ptr = raii::make_unique_ptr<
            declarative_config::service_frame_writer >( );
        ptr->frame_dir = "/frames";
        config.services.emplace_back( std::move( ptr ) );
    }
    {
        auto ptr = raii::make_unique_ptr< declarative_config::service_nds1 >( );
        ptr->frame_dir = "/frames/full";
        config.services.emplace_back( std::move( ptr ) );
    }

    REQUIRE( config.find_service( "frame_writer" )->at( "frame_dir" ) ==
             "/frames" );
    REQUIRE( config.find_service( "nds1" )->at_first( "frame_dir" ) ==
             "/frames/full" );

    REQUIRE_THROWS_AS( declarative_config::overlay_derived_defaults( config ),
                       std::runtime_error );
}