//
// Created by jonathan.hanks on 5/26/20.
//

#include "declarative_config.hh"

#include <algorithm>
#include <cmath>

#include <initializer_list>
#include <iostream>
#include <type_traits>

#include <boost/algorithm/string/predicate.hpp>
#include <yaml-cpp/yaml.h>

namespace
{
    const std::string DEFAULT_RAW_FRAME_DIR{ "/frames/full" };
    const std::string DEFAULT_STREND_FRAME_DIR{ "/frames/trend/second" };
    const std::string DEFAULT_MTREND_FRAME_DIR{ "/frames/trend/minute" };
    const std::string DEFAULT_RAW_MTREND_DIR{ "/frames/trend/raw" };
} // namespace

namespace YAML
{
    template <>
    struct convert< std::shared_ptr<
        declarative_config::service_configuration_number_client > >
    {
        static bool
        decode(
            const Node& node,
            std::shared_ptr<
                declarative_config::service_configuration_number_client >& rhs )
        {
            if ( !node.IsMap( ) )
            {
                return false;
            }
            auto tmp = std::make_shared<
                declarative_config::service_configuration_number_client >( );
            if ( node[ "server" ] )
            {
                tmp->server = node[ "server" ].as< std::string >( );
            }
            rhs = std::move( tmp );
            return true;
        }
    };

    template <>
    struct convert<
        std::shared_ptr< declarative_config::service_frame_writer > >
    {
        static bool
        decode(
            const Node&                                                  node,
            std::shared_ptr< declarative_config::service_frame_writer >& rhs )
        {
            if ( !node.IsMap( ) )
            {
                return false;
            }
            auto tmp =
                std::make_shared< declarative_config::service_frame_writer >( );
            if ( node[ "frame_dir" ] )
            {
                tmp->frame_dir = node[ "frame_dir" ].as< std::string >( );
            }
            if ( node[ "frame_type" ] )
            {
                tmp->frame_type = node[ "frame_type" ].as< std::string >( );
            }
            if ( node[ "seconds_per_frame" ] )
            {
                tmp->seconds_per_frame =
                    node[ "seconds_per_frame" ].as< int >( );
            }
            if ( node[ "checksum_dir" ] )
            {
                tmp->checksum_dir = node[ "checksum_dir" ].as< std::string >( );
            }
            rhs = std::move( tmp );
            return true;
        }
    };

    template <>
    struct convert<
        std::shared_ptr< declarative_config::service_strend_writer > >
    {
        static bool
        decode(
            const Node&                                                   node,
            std::shared_ptr< declarative_config::service_strend_writer >& rhs )
        {
            if ( !node.IsMap( ) )
            {
                return false;
            }
            auto tmp = std::make_shared<
                declarative_config::service_strend_writer >( );
            if ( node[ "frame_dir" ] )
            {
                tmp->frame_dir = node[ "frame_dir" ].as< std::string >( );
            }
            if ( node[ "frame_type" ] )
            {
                tmp->frame_type = node[ "frame_type" ].as< std::string >( );
            }
            if ( node[ "minutes_per_frame" ] )
            {
                tmp->minutes_per_frame =
                    node[ "minutes_per_frame" ].as< int >( );
            }
            if ( node[ "checksum_dir" ] )
            {
                tmp->checksum_dir = node[ "checksum_dir" ].as< std::string >( );
            }
            rhs = std::move( tmp );
            return true;
        }
    };

    template <>
    struct convert<
        std::shared_ptr< declarative_config::service_mtrend_writer > >
    {
        static bool
        decode(
            const Node&                                                   node,
            std::shared_ptr< declarative_config::service_mtrend_writer >& rhs )
        {
            if ( !node.IsMap( ) )
            {
                return false;
            }
            auto tmp = std::make_shared<
                declarative_config::service_mtrend_writer >( );
            if ( node[ "frame_dir" ] )
            {
                tmp->frame_dir = node[ "frame_dir" ].as< std::string >( );
            }
            if ( node[ "frame_type" ] )
            {
                tmp->frame_type = node[ "frame_type" ].as< std::string >( );
            }
            if ( node[ "minutes_per_frame" ] )
            {
                tmp->minutes_per_frame =
                    node[ "minutes_per_frame" ].as< int >( );
            }
            if ( node[ "checksum_dir" ] )
            {
                tmp->checksum_dir = node[ "checksum_dir" ].as< std::string >( );
            }
            rhs = std::move( tmp );
            return true;
        }
    };

    template <>
    struct convert<
        std::shared_ptr< declarative_config::service_raw_mtrend_writer > >
    {
        static bool
        decode(
            const Node& node,
            std::shared_ptr< declarative_config::service_raw_mtrend_writer >&
                rhs )
        {
            if ( !node.IsMap( ) )
            {
                return false;
            }
            auto tmp = std::make_shared<
                declarative_config::service_raw_mtrend_writer >( );
            if ( node[ "trend_dir" ] )
            {
                tmp->trend_dir = node[ "trend_dir" ].as< std::string >( );
            }
            if ( node[ "save_period_minutes" ] )
            {
                tmp->save_period_minutes =
                    node[ "save_period_minutes" ].as< int >( );
            }
            rhs = std::move( tmp );
            return true;
        }
    };

    template <>
    struct convert< std::shared_ptr< declarative_config::service_nds1 > >
    {
        static bool
        decode( const Node&                                          node,
                std::shared_ptr< declarative_config::service_nds1 >& rhs )
        {
            if ( !node.IsMap( ) )
            {
                return false;
            }
            auto tmp = std::make_shared< declarative_config::service_nds1 >( );
            if ( node[ "job_dir" ] )
            {
                tmp->job_dir = node[ "job_dir" ].as< std::string >( );
            }
            if ( node[ "trend_dirs" ] )
            {
                tmp->trend_dirs =
                    node[ "trend_dirs" ].as< std::vector< std::string > >( );
            }
            if ( node[ "port" ] )
            {
                tmp->port = node[ "port" ].as< int >( );
            }
            if ( node[ "frame_dir" ] )
            {
                tmp->frame_dir = node[ "frame_dir" ].as< std::string >( );
            }
            if ( node[ "frame_type" ] )
            {
                tmp->frame_type = node[ "frame_type" ].as< std::string >( );
            }
            if ( node[ "strend_dir" ] )
            {
                tmp->strend_dir = node[ "strend_dir" ].as< std::string >( );
            }
            if ( node[ "strend_type" ] )
            {
                tmp->strend_type = node[ "strend_type" ].as< std::string >( );
            }
            rhs = std::move( tmp );
            return true;
        }
    };

    template <>
    struct convert< std::shared_ptr< declarative_config::service_epics > >
    {
        static bool
        decode( const Node&                                           node,
                std::shared_ptr< declarative_config::service_epics >& rhs )
        {
            if ( !node.IsMap( ) )
            {
                return false;
            }
            auto tmp = std::make_shared< declarative_config::service_epics >( );
            if ( node[ "prefix" ] )
            {
                tmp->prefix = node[ "prefix" ].as< std::string >( );
            }
            rhs = std::move( tmp );
            return true;
        }
    };

    template <>
    struct convert<
        std::shared_ptr< declarative_config::service_gds_broadcast > >
    {
        static bool
        decode(
            const Node&                                                   node,
            std::shared_ptr< declarative_config::service_gds_broadcast >& rhs )
        {
            if ( !node.IsMap( ) )
            {
                return false;
            }
            auto tmp = std::make_shared<
                declarative_config::service_gds_broadcast >( );
            if ( node[ "address" ] )
            {
                tmp->address = node[ "address" ].as< std::string >( );
            }
            if ( node[ "network" ] )
            {
                tmp->network = node[ "network" ].as< std::string >( );
            }
            if ( node[ "port" ] )
            {
                tmp->port = node[ "port" ].as< int >( );
            }
            if ( node[ "broadcast_ini" ] )
            {
                tmp->broadcast_ini =
                    node[ "broadcast_ini" ].as< std::string >( );
            }
            if ( node[ "frame_dir" ] )
            {
                tmp->frame_dir = node[ "frame_dir" ].as< std::string >( );
            }
            if ( node[ "frame_type" ] )
            {
                tmp->frame_type = node[ "frame_type" ].as< std::string >( );
            }
            rhs = std::move( tmp );
            return true;
        }
    };

    template <>
    struct convert<
        std::shared_ptr< declarative_config::service_daqd_control > >
    {
        static bool
        decode(
            const Node&                                                  node,
            std::shared_ptr< declarative_config::service_daqd_control >& rhs )
        {
            if ( !node.IsMap( ) )
            {
                return false;
            }
            auto tmp =
                std::make_shared< declarative_config::service_daqd_control >( );
            if ( node[ "binary_port" ] )
            {
                tmp->binary_port = node[ "binary_port" ].as< int >( );
            }
            if ( node[ "text_port" ] )
            {
                tmp->text_port = node[ "text_port" ].as< int >( );
            }
            rhs = std::move( tmp );
            return true;
        }
    };

    template <>
    struct convert< declarative_config::service_ptr >
    {
        static bool
        decode( const Node& node, declarative_config::service_ptr& rhs )
        {
            if ( !node.IsMap( ) )
            {
                return false;
            }
            auto node_type = node[ "service" ].as< std::string >( );
            if ( node_type == "config_number_client" )
            {
                rhs = node.as< std::shared_ptr<
                    declarative_config::
                        service_configuration_number_client > >( );
                return true;
            }
            else if ( node_type == "frame_writer" )
            {
                rhs = node.as< std::shared_ptr<
                    declarative_config::service_frame_writer > >( );
                return true;
            }
            else if ( node_type == "strend_writer" )
            {
                rhs = node.as< std::shared_ptr<
                    declarative_config::service_strend_writer > >( );
                return true;
            }
            else if ( node_type == "mtrend_writer" )
            {
                rhs = node.as< std::shared_ptr<
                    declarative_config::service_mtrend_writer > >( );
                return true;
            }
            else if ( node_type == "raw_mtrend_writer" )
            {
                rhs = node.as< std::shared_ptr<
                    declarative_config::service_raw_mtrend_writer > >( );
                return true;
            }
            else if ( node_type == "nds1" )
            {
                rhs = node.as<
                    std::shared_ptr< declarative_config::service_nds1 > >( );
                return true;
            }
            else if ( node_type == "epics" )
            {
                rhs = node.as<
                    std::shared_ptr< declarative_config::service_epics > >( );
                return true;
            }
            else if ( node_type == "gds_broadcast" )
            {
                rhs = node.as< std::shared_ptr<
                    declarative_config::service_gds_broadcast > >( );
                return true;
            }
            else if ( node_type == "daqd_control" )
            {
                rhs = node.as< std::shared_ptr<
                    declarative_config::service_daqd_control > >( );
                return true;
            }
            return false;
        }
    };
} // namespace YAML

namespace
{
    enum class ParamType
    {
        Int,
        Double,
        Bool,
        String,
    };
    using parameter_check_t =
        std::vector< std::pair< std::string, ParamType > >;
    using service_check_t = std::map< std::string, parameter_check_t >;

    class service_description
    {
    public:
        service_description( int                id_val,
                             std::string        name_val,
                             std::vector< int > depends_val )
            : id_{ id_val }, name_{ std::move( name_val ) }, depends_{
                  std::move( depends_val )
              }
        {
            std::for_each(
                depends_.begin( ),
                depends_.end( ),
                [ this ]( const int dep_id ) {
                    if ( dep_id >= id_ )
                    {
                        throw std::runtime_error(
                            "A service is being declared with a dependency id "
                            "that could cause a cycle" );
                    }
                } );
        }

        service_description( const service_description& ) = default;

        service_description( service_description&& ) = default;

        service_description& operator=( const service_description& ) = default;

        service_description& operator=( service_description&& ) = default;

        int
        id( ) const
        {
            return id_;
        }

        const std::string&
        name( ) const
        {
            return name_;
        }

        const std::vector< int >&
        depends( ) const
        {
            return depends_;
        }

    private:
        int                id_;
        std::string        name_;
        std::vector< int > depends_;
    };

    const parameter_check_t allowed_global_params{
        { "thread_stack_size_mb", ParamType::Int },
        { "dcu_status_check", ParamType::Bool },
        { "debug", ParamType::Int },
        { "log", ParamType::Int },
        { "zero_bad_data", ParamType::Bool },
    };

    void
    require_map(
        const YAML::Node& node,
        const char* msg = "A map is required in this portion of the config" )
    {
        if ( node.IsMap( ) )
        {
            return;
        }
        throw std::runtime_error( msg );
    }

    declarative_config::parameter_t
    extract_parameter( const YAML::Node& yaml_val, ParamType type )
    {
        switch ( type )
        {
        case ParamType::Int:
            return yaml_val.as< int >( );
        case ParamType::Double:
            return yaml_val.as< double >( );
        case ParamType::Bool:
            return yaml_val.as< bool >( );
        case ParamType::String:
            return yaml_val.as< std::string >( );
        }
        throw std::runtime_error( "Invalid type of parameter" );
    }

    declarative_config::parameter_map
    extract_parameters( const char*              debug_prefix,
                        const YAML::Node&        node,
                        const parameter_check_t& accepted_params )
    {
        declarative_config::parameter_map parameters;
        require_map( node, "Parameter lists are maps with named keys" );
        for ( const auto& input_param : node )
        {
            std::string name = input_param.first.as< std::string >( );
            auto        it = std::find_if(
                accepted_params.begin( ),
                accepted_params.end( ),
                [ &name ]( const std::pair< std::string, ParamType >& entry )
                    -> bool { return name == entry.first; } );
            if ( it == accepted_params.end( ) )
            {
                parameters.insert(
                    std::make_pair( name,
                                    extract_parameter( input_param.second,
                                                       ParamType::String ) ) );
            }
            else
            {
                parameters.insert( std::make_pair(
                    name,
                    extract_parameter( input_param.second, it->second ) ) );
            }
        }
        return parameters;
    }

    declarative_config::detector_t
    extract_detector( const YAML::Node& input )
    {
        declarative_config::detector_t detector{ };
        require_map( input, "The detector section must be a map object" );
        for ( const auto& entry : input )
        {
            auto name = entry.first.as< std::string >( );
            if ( name == "site" )
            {
                detector.name = entry.second.as< std::string >( );
            }
            else if ( name == "ifo" )
            {
                detector.prefix = entry.second.as< std::string >( );
            }
            else if ( name == "longitude" )
            {
                detector.longitude = entry.second.as< double >( );
            }
            else if ( name == "latitude" )
            {
                detector.latitude = entry.second.as< double >( );
            }
            else if ( name == "elevation" )
            {
                detector.elevation = entry.second.as< double >( );
            }
            else if ( name == "azimuths" )
            {
                detector.azimuths =
                    entry.second.as< std::array< double, 2 > >( );
            }
            else if ( name == "altitudes" )
            {
                detector.altitudes =
                    entry.second.as< std::array< double, 2 > >( );
            }
            else if ( name == "midpoints" )
            {
                detector.midpoints =
                    entry.second.as< std::array< double, 2 > >( );
            }
            else
            {
                std::cout
                    << "Ignoring unexpected field in the detector section '"
                    << name << "'" << std::endl;
            }
        }
        return detector;
    }

    declarative_config::channel_directory_t
    extract_inputs( const YAML::Node& input )
    {
        declarative_config::channel_directory_t inputs{ };
        require_map( input, "The inputs section must be a map" );
        for ( const auto& entry : input )
        {
            auto name = entry.first.as< std::string >( );
            if ( name == "ini_list" )
            {
                inputs.master_file_path = entry.second.as< std::string >( );
            }
            else if ( name == "testpoints" )
            {
                inputs.testpoint_par_path =
                    ( entry.second.IsNull( )
                          ? ""
                          : entry.second.as< std::string >( ) );
            }
            else
            {
                std::cout << "Ignoring unexpected field in the inputs section '"
                          << name << "'" << std::endl;
            }
        }
        return inputs;
    }

    class as_string_visitor : public boost::static_visitor< std::string >
    {
    public:
        template < typename T >
        std::string
        operator( )( const T& val ) const
        {
            std::ostringstream os;
            os << val;
            return os.str( );
        }
    };

    void
    default_frame_and_buffer_sizes( declarative_config::config_t& conf )
    {
        int seconds_in_frame{ 0 };
        try
        {
            auto spf =
                conf.find_service( "frame_writer" )->at( "seconds_per_frame" );
            std::istringstream input( spf );
            input >> seconds_in_frame;
        }
        catch ( std::runtime_error& err )
        {
            seconds_in_frame = 16;
        }
        auto buffer_param_it = conf.parameters.find( "buffer:seconds" );
        if ( seconds_in_frame > 0 && buffer_param_it == conf.parameters.end( ) )
        {
            auto min_buf = seconds_in_frame +
                std::max( static_cast< int >( std::ceil(
                              static_cast< double >( seconds_in_frame ) *
                              0.1 ) ),
                          4 );
            conf.parameters[ "buffer:seconds" ] = min_buf;
        }
    }

    void
    default_frame_types( declarative_config::config_t& conf )
    {
        using declarative_config::field_override_service;

        if ( conf.detector.prefix.empty( ) )
        {
            return;
        }
        if ( conf.detector.prefix[ 0 ] < 'A' ||
             conf.detector.prefix[ 0 ] > 'Z' )
        {
            return;
        }
        std::string start( conf.detector.prefix.c_str( ), 1 );
        auto        raw = start + "-" + conf.detector.prefix + "_R-";
        auto        s_trend = start + "-" + conf.detector.prefix + "_T-";
        auto        m_trend = start + "-" + conf.detector.prefix + "_M-";
        auto        broadcast = start + "-R-";

        for ( auto& service : conf.services )
        {
            if ( service->name( ) == "frame_writer" &&
                 service->at( "frame_type" ).empty( ) )
            {
                std::map< std::string, std::string > overrides{ };
                overrides[ "frame_type" ] = raw;
                auto overlay = std::make_shared< field_override_service >(
                    service, overrides );
                service = overlay;
            }
            else if ( service->name( ) == "strend_writer" &&
                      service->at( "frame_type" ).empty( ) )
            {
                std::map< std::string, std::string > overrides{ };
                overrides[ "frame_type" ] = s_trend;
                auto overlay = std::make_shared< field_override_service >(
                    service, overrides );
                service = overlay;
            }
            else if ( service->name( ) == "mtrend_writer" &&
                      service->at( "frame_type" ).empty( ) )
            {
                std::map< std::string, std::string > overrides{ };
                overrides[ "frame_type" ] = m_trend;
                auto overlay = std::make_shared< field_override_service >(
                    service, overrides );
                service = overlay;
            }
            else if ( service->name( ) == "nds1" &&
                      ( service->at( "frame_type" ).empty( ) ||
                        service->at( "strend_type" ).empty( ) ||
                        service->at( "mtrend_type" ).empty( ) ) )
            {
                std::map< std::string, std::string > overrides{ };
                if ( service->at( "frame_type" ).empty( ) )
                {
                    overrides[ "frame_type" ] = raw;
                }
                if ( service->at( "strend_type" ).empty( ) )
                {
                    overrides[ "strend_type" ] = s_trend;
                }
                if ( service->at( "mtrend_type" ).empty( ) )
                {
                    overrides[ "mtrend_type" ] = m_trend;
                }
                if ( !overrides.empty( ) )
                {
                    auto overlay = std::make_shared< field_override_service >(
                        service, overrides );
                    service = overlay;
                }
            }
            else if ( service->name( ) == "gds_broadcast" &&
                      service->at( "frame_type" ).empty( ) )
            {
                std::map< std::string, std::string > overrides{ };
                overrides[ "frame_type" ] = broadcast;
                auto overlay = std::make_shared< field_override_service >(
                    service, overrides );
                service = overlay;
            }
        }
    }

    std::function< bool( declarative_config::service_ptr& ) >
    service_predicate( const std::string& name )
    {
        return [ name ]( declarative_config::service_ptr& p ) -> bool {
            return p->name( ) == name;
        };
    };

    declarative_config::service_ptr
    lookup_service( declarative_config::config_t& config,
                    const std::string&            name )
    {
        auto it = std::find_if( config.services.begin( ),
                                config.services.end( ),
                                service_predicate( name ) );
        if ( it != config.services.end( ) )
        {
            return *it;
        }
        return nullptr;
    }

    int
    lookup_service_index( declarative_config::config_t& config,
                          const std::string&            name )
    {
        for ( int i = 0; i < config.services.size( ); ++i )
        {
            if ( config.services[ i ]->name( ) == name )
            {
                return i;
            }
        }
        return -1;
    }

    void
    replace_service( declarative_config::config_t&          config,
                     const declarative_config::service_ptr& old_service,
                     declarative_config::service_ptr        new_service )
    {
        auto i = lookup_service_index( config, old_service->name( ) );
        if ( i < 0 )
        {
            throw std::runtime_error(
                "Unable to find requested service in config" );
        }
        config.services[ i ] = std::move( new_service );
    }

    void
    default_fw_dirs( declarative_config::config_t& conf )
    {
        using declarative_config::field_override_service;

        std::map< std::string, std::string > mappings{
            { "frame_writer", DEFAULT_RAW_FRAME_DIR },
            { "strend_writer", DEFAULT_STREND_FRAME_DIR },
            { "mtrend_writer", DEFAULT_MTREND_FRAME_DIR },
        };
        for ( const auto& entry : mappings )
        {
            auto i = lookup_service_index( conf, entry.first );
            if ( i < 0 )
            {
                continue;
            }
            std::map< std::string, std::string > overrides{ };
            if ( conf.services[ i ]->at( "frame_dir" ).empty( ) )
            {
                overrides[ "frame_dir" ] = entry.second;
            }
            if ( !overrides.empty( ) )
            {
                auto overlay = std::make_shared< field_override_service >(
                    conf.services[ i ], overrides );
                conf.services[ i ] = overlay;
            }
        }
    }

    void
    default_nds1_dirs( declarative_config::config_t& conf )
    {
        using declarative_config::field_override_service;

        auto i = lookup_service_index( conf, "nds1" );
        if ( i < 0 )
        {
            return;
        }
        std::map< std::string, std::string > overrides{ };
        if ( conf.services[ i ]->at( "frame_dir" ).empty( ) )
        {
            overrides[ "frame_dir" ] = DEFAULT_RAW_FRAME_DIR;
        }
        if ( conf.services[ i ]->at( "strend_dir" ).empty( ) )
        {
            overrides[ "strend_dir" ] = DEFAULT_STREND_FRAME_DIR;
        }
        if ( conf.services[ i ]->at( "mtrend_dir" ).empty( ) )
        {
            overrides[ "mtrend_dir" ] = DEFAULT_MTREND_FRAME_DIR;
        }
        if ( !overrides.empty( ) )
        {
            auto overlay = std::make_shared< field_override_service >(
                conf.services[ i ], overrides );
            conf.services[ i ] = overlay;
        }
    }

    void
    default_raw_mtrend_dirs( declarative_config::config_t& conf )
    {
        using declarative_config::field_override_service;

        auto raw_mtrend_writer = lookup_service( conf, "raw_mtrend_writer" );
        if ( !raw_mtrend_writer )
        {
            return;
        }
        if ( raw_mtrend_writer->at( "trend_dir" ).empty( ) )
        {
            std::map< std::string, std::string > overrides{
                { "trend_dir", DEFAULT_RAW_MTREND_DIR }
            };
            auto override_ptr = std::make_shared< field_override_service >(
                raw_mtrend_writer, overrides );
            replace_service(
                conf, raw_mtrend_writer, std::move( override_ptr ) );
        }
    }

    void
    default_nds1_mtrend_dirs( declarative_config::config_t& conf )
    {
        using declarative_config::field_override_service;

        auto nds1 = lookup_service( conf, "nds1" );
        if ( !nds1 )
        {
            return;
        }
        if ( nds1->at( "trend_dirs" ).empty( ) )
        {
            std::map< std::string, std::string > overrides{
                { "trend_dirs", DEFAULT_RAW_MTREND_DIR }
            };
            auto override_ptr =
                std::make_shared< field_override_service >( nds1, overrides );
            replace_service( conf, nds1, std::move( override_ptr ) );
        }
    }

    std::string
    select_compatible_entry( const std::string& a,
                             const std::string& b,
                             const std::string& def_val )
    {
        if ( a == b )
        {
            return ( a.empty( ) ? def_val : a );
        }
        if ( a.empty( ) )
        {
            return b;
        }
        if ( b.empty( ) )
        {
            return a;
        }
        throw std::runtime_error(
            "Mismatching frame file directories specified between the frame "
            "writers and nds1" );
    }

    void
    default_frame_directories( declarative_config::config_t& config )
    {
        using declarative_config::field_override_service;

        declarative_config::service_ptr frame_writer{ lookup_service(
            config, "frame_writer" ) };
        declarative_config::service_ptr strend_writer{ lookup_service(
            config, "strend_writer" ) };
        declarative_config::service_ptr mtrend_writer{ lookup_service(
            config, "mtrend_writer" ) };
        declarative_config::service_ptr nds1{ lookup_service( config,
                                                              "nds1" ) };

        auto lookup = []( declarative_config::service_ptr& p,
                          const std::string& key ) -> std::string {
            if ( !p )
            {
                return "";
            }
            return p->at( key );
        };

        if ( !frame_writer && !strend_writer && !mtrend_writer && !nds1 )
        {
            return;
        }
        if ( ( frame_writer || strend_writer || mtrend_writer ) && !nds1 )
        {
            default_fw_dirs( config );
            return;
        }
        if ( ( !frame_writer && !strend_writer && !mtrend_writer ) && nds1 )
        {
            default_nds1_dirs( config );
            return;
        }
        auto frame_dir =
            select_compatible_entry( lookup( frame_writer, "frame_dir" ),
                                     lookup( nds1, "frame_dir" ),
                                     DEFAULT_RAW_FRAME_DIR );
        auto strend_dir =
            select_compatible_entry( lookup( strend_writer, "frame_dir" ),
                                     lookup( nds1, "strend_dir" ),
                                     DEFAULT_STREND_FRAME_DIR );

        auto mtrend_dir =
            select_compatible_entry( lookup( mtrend_writer, "frame_dir" ),
                                     lookup( nds1, "mtrend_dir" ),
                                     DEFAULT_MTREND_FRAME_DIR );

        if ( frame_writer && frame_writer->at( "frame_dir" ) != frame_dir )
        {
            std::map< std::string, std::string > overrides{ { "frame_dir",
                                                              frame_dir } };
            auto override_ptr = std::make_shared< field_override_service >(
                frame_writer, overrides );
            replace_service( config, frame_writer, std::move( override_ptr ) );
        }
        if ( strend_writer && strend_writer->at( "frame_dir" ) != strend_dir )
        {
            std::map< std::string, std::string > overrides{ { "frame_dir",
                                                              strend_dir } };
            auto override_ptr = std::make_shared< field_override_service >(
                strend_writer, overrides );
            replace_service( config, strend_writer, std::move( override_ptr ) );
        }
        if ( mtrend_writer && mtrend_writer->at( "frame_dir" ) != mtrend_dir )
        {
            std::map< std::string, std::string > overrides{ { "frame_dir",
                                                              mtrend_dir } };
            auto override_ptr = std::make_shared< field_override_service >(
                mtrend_writer, overrides );
            replace_service( config, mtrend_writer, std::move( override_ptr ) );
        }
        if ( nds1 )
        {
            std::map< std::string, std::string > overrides{ };
            if ( nds1->at( "frame_dir" ) != frame_dir )
            {
                overrides[ "frame_dir" ] = frame_dir;
            }
            if ( nds1->at( "strend_dir" ) != strend_dir )
            {
                overrides[ "strend_dir" ] = strend_dir;
            }
            if ( nds1->at( "mtrend_dir" ) != mtrend_dir )
            {
                overrides[ "mtrend_dir" ] = mtrend_dir;
            }
            if ( !overrides.empty( ) )
            {
                auto override_ptr = std::make_shared< field_override_service >(
                    nds1, overrides );
                replace_service( config, nds1, std::move( override_ptr ) );
            }
        }
    }

    void
    default_raw_trend_directories( declarative_config::config_t& config )
    {
        using declarative_config::field_override_service;

        declarative_config::service_ptr raw_mtrend_writer{ lookup_service(
            config, "raw_mtrend_writer" ) };
        declarative_config::service_ptr nds1{ lookup_service( config,
                                                              "nds1" ) };

        auto lookup = []( declarative_config::service_ptr& p,
                          const std::string& key ) -> std::string {
            if ( !p )
            {
                return "";
            }
            return p->at_first( key );
        };

        if ( !raw_mtrend_writer && !nds1 )
        {
            return;
        }
        if ( raw_mtrend_writer && !nds1 )
        {
            default_raw_mtrend_dirs( config );
            return;
        }
        if ( !raw_mtrend_writer && nds1 )
        {
            default_nds1_mtrend_dirs( config );
            return;
        }
        auto raw_mtrend_dir =
            select_compatible_entry( lookup( raw_mtrend_writer, "trend_dir" ),
                                     lookup( nds1, "trend_dirs" ),
                                     DEFAULT_RAW_MTREND_DIR );
        if ( raw_mtrend_writer->at( "trend_dir" ) != raw_mtrend_dir )
        {
            std::map< std::string, std::string > overrides{
                { "trend_dir", raw_mtrend_dir }
            };
            auto override_ptr = std::make_shared< field_override_service >(
                raw_mtrend_writer, overrides );
            replace_service(
                config, raw_mtrend_writer, std::move( override_ptr ) );
        }
        auto nds1_first = nds1->at_first( "trend_dirs" );
        if ( nds1_first != raw_mtrend_dir )
        {
            if ( nds1->at( "trend_dirs" ) != nds1_first )
            {
                throw std::runtime_error(
                    "There is a discrepancy between the raw_mtrend_writer and "
                    "the nds1 trend directories.  Please reconcile this" );
            }
            std::map< std::string, std::string > overrides{
                { "trend_dirs", raw_mtrend_dir }
            };
            auto override_ptr =
                std::make_shared< field_override_service >( nds1, overrides );
            replace_service( config, nds1, std::move( override_ptr ) );
        }
    }

} // namespace

namespace declarative_config
{
    bool
    is_declarative_config_file( const std::string& fname )
    {
        return boost::algorithm::ends_with( fname, ".yaml" );
    }

    config_t
    parse( std::istream& stream )
    {
        config_t         config{ };
        const YAML::Node input{ YAML::Load( stream ) };
        require_map( input,
                     "The config object does not parse to a map type, it "
                     "cannot be processed." );
        for ( const auto& section : input )
        {
            auto section_name = section.first.as< std::string >( );
            if ( section_name == "parameters" )
            {
                config.parameters = extract_parameters(
                    "parameters", section.second, allowed_global_params );
            }
            else if ( section_name == "detector" )
            {
                config.detector = extract_detector( section.second );
            }
            else if ( section_name == "inputs" )
            {
                config.inputs = extract_inputs( section.second );
            }
            else if ( section_name == "services" )
            {
                config.services = section.second.as<
                    std::vector< declarative_config::service_ptr > >( );
            }
            else
            {
                std::cout << "Ignoring unexpected section '" << section_name
                          << "'" << std::endl;
            }
        }
        return config;
    }

    void
    overlay_derived_defaults( config_t& conf )
    {
        default_frame_and_buffer_sizes( conf );

        default_frame_types( conf );

        default_frame_directories( conf );

        default_raw_trend_directories( conf );
    }

    service_ptr
    config_t::find_service( const std::string& name )
    {
        auto it = std::find_if(
            services.begin( ),
            services.end( ),
            [ &name ]( declarative_config::service_ptr& p ) -> bool {
                return p->name( ) == name;
            } );
        if ( it == services.end( ) || !*it )
        {
            throw std::runtime_error( "not found" );
        }
        return *it;
    }

    std::string
    as_string( const parameter_t& param )
    {
        return boost::apply_visitor( as_string_visitor( ), param );
    }
} // namespace declarative_config
