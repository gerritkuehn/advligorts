static char *versionId = "Version $Id$" ;
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: testpoint						*/
/*                                                         		*/
/* Module Description: implements functions for handling test points	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef __EXTENSIONS__
#define __EXTENSIONS__
#endif
#ifndef _BSD_SOURCE
#define _BSD_SOURCE
#endif
#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE
#endif

#include "dtt/gdsutil.h"
#include "awgtpman.h"


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Includes: 								*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <errno.h>

#include <pthread.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>

#include "dtt/rmorg.h"
#include "dtt/testpoint_direct.h"
#include "shared_memory.h"
#include "testpoint_structs.h"
#include "dtt/testpoint_server.h"

#include "rtestpoint.h"

#include "dtt/rmapi.h"
#include "dtt/gdssched.h"



/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Constants: _NETID		  net protocol used for rpc		*/
/*            _MAX_INDEX_CACHE	  size of tp index cache		*/
/*            _KEEP_ALIVE_RATE	  keep alive rate in sec		*/
/*            _TP_CLEAR_ALL	  clear all test points			*/
/*            PRM_FILE		  parameter file name			*/
/*            PRM_SECTION	  parameter file section heading	*/
/*            PRM_ENTRY1	  parameter file host name entry	*/
/*            PRM_ENTRY2	  parameter file rpc program # entry	*/
/*            PRM_ENTRY3	  parameter file rpc version # entry	*/
/*            PRM_ENTRY4	  parameter file controls sysem name    */
/*                                                         		*/
/*----------------------------------------------------------------------*/

#define _MAX_INDEX_CACHE	3
#define _KEEP_ALIVE_RATE	5

#define PRM_FILE		gdsPathFile ("/param", "testpoint.par")
#define PRM_SECTION		gdsSectionSite ("node%i")
#define PRM_ENTRY1		"hostname"
#define PRM_ENTRY2		"prognum"
#define PRM_ENTRY3		"progver"
#define PRM_ENTRY4		"system"



   struct tpIndex_t {
      testpoint_t	tp[TP_MAX_NODE][TP_MAX_INTERFACE][DAQ_GDS_MAX_TP_NUM];
      taisec_t		time;
      int		epoch;
   };
   typedef struct tpIndex_t tpIndex_t;

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Types: tpNode_t - node type for rpc client				*/
/*        tpIndex_t - test point index chache				*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
struct tpNode_t {
    int		valid;
    int		duplicate;
    int		id;
    char		hostname[80];
    unsigned long	prognum;
    unsigned long	progver;
};
typedef struct tpNode_t tpNode_t;



/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Globals: tp_init		whether client was already init.	*/
/*          tpNodes		test point nodes		 	*/
/*          tpNum		Number of reachable test point nodes 	*/
/*          tpmux		mutex to protect index cache		*/
/*          tpindexcur		cursor into index cache			*/
/*          tpindexlist		test point index cache (organized as a	*/
/*          			ring buffer)				*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

   static int			tp_init = 0;
   static tpNode_t		tpNode[TP_MAX_NODE];
   static int			tpNum = 0;

   static mutexID_t		tpmux;
   static int			tpindexcur = 0;
   static tpIndex_t		tpindexlist[_MAX_INDEX_CACHE];


   static scheduler_t*		tpsched;



/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: initTestpoint				*/
/*                                                         		*/
/* Procedure Description: initializes test point interface		*/
/*                                                         		*/
/* Procedure Arguments: void						*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
static void initTestpoint (void)
{

    int		node;		/* node id */

    /* test if already initialized */
    if (tp_init != 0) {
        return;
    }

    tpNum = 0;

    for (node = 0; node < TP_MAX_NODE; node++) {
        /* make section header */
        tpNode[node].valid = 0;
        /*#if (_TESTPOINT_DIRECT != 0)
           if ((_TESTPOINT_DIRECT & (1 << node)) != 0) {
              tpNode[node].valid = 1;*/

    }

    /* intialized */
    tp_init = 1;

}


   __init__(initTestpoint);
   __fini__(finiTestpoint);



/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: getIndexDirect				*/
/*                                                         		*/
/* Procedure Description: gets a test point index			*/
/*                                                         		*/
/* Procedure Arguments: node, interface, time, epoch, tp list & length	*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 if not			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   int tpGetIndexDirect (int node, int tpinterface, testpoint_t tp[], 
                     int tplen, taisec_t time, int epoch)
   {

      int		len;		/* length of index */
      int 		cur;		/* index into index cache */


      /* check node */
      if ((node < 0) || (node >= TP_MAX_NODE)) {
         return -2;
      }
   
      /* check interface */
      if ((tpinterface < 0) || (tpinterface >= TP_MAX_INTERFACE)) {
         return -2;
      }
   
      /* test if node is included in test point direct */
      if ((_TESTPOINT_DIRECT & (1 << node)) == 0) {
         return -1;
      }
   
      /* get length and address of index */
      len = DAQ_GDS_MAX_TP_NUM;
      if (len > tplen) {
         len = tplen;
      }
      if (len < 0) {
         return -3;
      }
   
      /* get mutex */
      MUTEX_GET (tpmux);
   
      /* search for appropriate index */
      cur = tpindexcur;
      do {
         if (time >= tpindexlist[cur].time) {
            break;
         }
         cur = (cur + _MAX_INDEX_CACHE - 1) % _MAX_INDEX_CACHE;
      } while (cur != tpindexcur);
      /* request too far in the past */
      if (time < tpindexlist[cur].time) {
         printf ("tp request time %li error for %li\n", time, 
                 tpindexlist[cur].time);
         MUTEX_RELEASE (tpmux);
         return -4;
      }
   
      /* copy list */
      if (tp != NULL) {
         memcpy (tp, tpindexlist[cur].tp[node][tpinterface], 
                len * sizeof (testpoint_t));
      }
   
      /* release mutex and return */
      MUTEX_RELEASE (tpmux);
      return len;
   }













#if !defined(_NO_KEEP_ALIVE) && !defined(_NO_TESTPOINTS)

#endif



/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: readTestpoints				*/
/*                                                         		*/
/* Procedure Description: reads test point indexes			*/
/*                                                         		*/
/* Procedure Arguments: scheduler task, time, epoch, argument		*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 if not			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static int readTestpoints (schedulertask_t* info, taisec_t time, 
                     int epoch, void* arg)
   {
      int		node;		/* node index */
      int		i;		/* interface index */
      int		j;		/* index into test point list */

      /*printf("read testpoints\n");*/
      /* get mutex and increase cursor */
      MUTEX_GET (tpmux);
      tpindexcur = (tpindexcur + 1) % _MAX_INDEX_CACHE;
   
      /* set time and epoch */
      tpindexlist[tpindexcur].time = time + 1;
      tpindexlist[tpindexcur].epoch = 0;
      /*printf ("read TP at %li (%i)\n", time, epoch);*/
   
      for (node = 0; node < TP_MAX_NODE; node++) {
      	 /* loop over interfaces */
         for (i = 0; i < TP_MAX_INTERFACE; i++) {
	    /*printf("read testpoint cache for node %d\n", node);*/
            switch(i)
            {
            case TP_EX_INTERFACE:
                memcpy( tpindexlist[ tpindexcur ].tp[ node ][ i ],
                        (void*)shmemTestpointCfg->excitations,
                        DAQ_GDS_MAX_TP_NUM * sizeof( testpoint_t ) );
                break;
            case TP_TP_INTERFACE:
                memcpy( tpindexlist[ tpindexcur ].tp[ node ][ i ],
                        (void*)shmemTestpointCfg->testpoints,
                        DAQ_GDS_MAX_TP_NUM * sizeof( testpoint_t ) );
                break;
            }

         }
      }
      MUTEX_RELEASE (tpmux);
      return 0;
   }





/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: testpoint_cleanup				*/
/*                                                         		*/
/* Procedure Description: cleans up the test point client interface	*/
/*                                                         		*/
/* Procedure Arguments: void						*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/       
   void testpoint_cleanup (void) 
   {

      if (tp_init <= 1) {
         return;
      }

      if (tp_init == 2) {
         closeScheduler (tpsched, 0);
         tpsched = NULL;
      }

      tp_init = 1;
   }





/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: finiTestpoint				*/
/*                                                         		*/
/* Procedure Description: cleans up test point interface		*/
/*                                                         		*/
/* Procedure Arguments: void						*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static void finiTestpoint (void)
   {

      if (tp_init == 0) {
         return;
      }
      if (tp_init > 1) {
         /*tp_init = 3; make sure we don't call closeScheduler */
         testpoint_cleanup ();
      }
   

      MUTEX_GET (tpmux);
      MUTEX_DESTROY (tpmux);

   
      tp_init = 0;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: testpoint_direct				*/
/*                                                         		*/
/* Procedure Description: opens interface to direct testpoint control   */
/*                                                         		*/
/* Procedure Arguments: void						*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 if failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
int testpoint_direct (void)
{

    int		node;		/* node id */
    struct timeval	timeout;	/* timeout for probe */
    CLIENT*		clnt;		/* client rpc handle */
    int		status;		/* rpc status */
#if !defined(_NO_KEEP_ALIVE)
    int		keepAliveNum;	/* # of keep alives */
#endif

    /* already initialized */
    if (tp_init == 2) {
        return tpNum;
    }

    gdsDebug ("start test point client");

    /* intialize interface first */
    if (tp_init == 0) {
        initTestpoint();
        if (tp_init == 0) {
            gdsError (GDS_ERR_MEM, "failed to initialze test points");
            return -1;
        }
        /* Log the version ID */
        printf("testpoint_client %s\n", versionId) ;
    }


    /* install heartbeat */

    if (installHeartbeat (NULL, use_gps_time) < 0) {
        gdsError (GDS_ERR_MEM, "failed to install heartbeat");
        return -2;
    }

    /* create scheduler */
    tpsched = createScheduler (0, NULL, NULL);
    if (tpsched == NULL) {
        gdsError (GDS_ERR_MEM,
                  "failed to create test point scheduler");
        return -3;
    }


    timeout.tv_sec = RPC_PROBE_WAIT;
    timeout.tv_usec = 0;
    node = getNode();
    tpNode[node].valid = 1;


    /* start keep alive task */
#if !defined(_NO_KEEP_ALIVE)
    keepAliveNum = 0;
    for (node = 0; node < TP_MAX_NODE; node++) {
        if (tpNode[node].valid &&
            ((_TESTPOINT_DIRECT & (1 << node)) == 0)) {
            keepAliveNum++;
        }
    }
    if (keepAliveNum > 0) {
        schedulertask_t	task;	/* task info */
        /* setup task info structure for tp read task*/
        SET_TASKINFO_ZERO (&task);
        task.flag = SCHED_REPEAT | SCHED_WAIT | SCHED_ASYNC;
        task.waittype = SCHED_WAIT_IMMEDIATE;
        task.repeattype = SCHED_REPEAT_INFINITY;
        task.repeatratetype = SCHED_REPEAT_EPOCH;
        task.repeatrate = _KEEP_ALIVE_RATE * NUMBER_OF_EPOCHS;
        task.synctype = SCHED_SYNC_EPOCH;
        task.syncval = 0;
        task.func = keepAlive;

        /* schedule test point read task */
        if (scheduleTask (tpsched, &task) < 0) {
            closeScheduler (tpsched, 3 * _EPOCH);
            tpsched = NULL;
            gdsError (GDS_ERR_MEM,
                      "failed to create test point read task");
            return -6;
        }
    }
#endif

    /* install test point read task */

    {
         schedulertask_t	task;	/* task info */

         /* setup task info structure for tp read task*/
         SET_TASKINFO_ZERO (&task);
         task.flag = SCHED_REPEAT | SCHED_WAIT;
         task.waittype = SCHED_WAIT_IMMEDIATE;
         task.repeattype = SCHED_REPEAT_INFINITY;
         task.repeatratetype = SCHED_REPEAT_EPOCH;
         task.repeatrate = NUMBER_OF_EPOCHS;
         task.synctype = SCHED_SYNC_EPOCH;
         task.syncval = TESTPOINT_VALID1;
         task.func = readTestpoints;

         /* schedule test point read task */
         if (scheduleTask (tpsched, &task) < 0) {
            closeScheduler (tpsched, 3 * _EPOCH);
            tpsched = NULL;
            gdsError (GDS_ERR_MEM,
                     "failed to create test point read task");
            return -7;
         }
      }


    tp_init = 2;
    return tpNum;

}
