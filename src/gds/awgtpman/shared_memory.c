//
// Created by erik.vonreis on 4/27/21.
//

#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include "shared_memory.h"
#include "shmem_all.h"
#include "cds-shmem.h"

//
static const char *shared_memory_prefix=SHMEM_MBUF_PREFIX;



// AWG data shared memory

volatile AWG_DATA* shmemAwgData = NULL;

shmem_handle awg_data_shmem_handle = NULL;

void
closeAwgDataSharedMemory()
{
    if(NULL != awg_data_shmem_handle)
    {
        shmem_close(awg_data_shmem_handle);
    }
}

int
OpenAwgDataSharedMemory( const char* model_name )
{
    char         buff[ 256 ];
    shmem_handle handle = NULL;

    strncpy( buff, shared_memory_prefix, sizeof(buff));
    buff[ sizeof( buff ) - 1 ] = 0;
    strncat( buff, model_name, sizeof( buff ) - strlen(buff) - 1 );
    strncat( buff, SHMEM_AWG_SUFFIX, sizeof( buff ) - strlen(buff) - 1 );

    handle = shmem_open( buff, SHMEM_AWG_SIZE_MB );
    shmemAwgData = shmem_mapping( handle );
    AWG_DATA_INIT( shmemAwgData );
    awg_data_shmem_handle = handle;
    atexit(closeAwgDataSharedMemory);
    return ( shmemAwgData != NULL ) ? -1 : 0;
}


// IO from IOP shared memory
volatile IO_MEM_DATA* ioMemData = NULL;

// Pointers into the shared memory for the cycle and time (coming from the IOP
// (e.g. x00)) these are just convenient pointers into some part of ioMemData.
volatile unsigned int* ioMemDataCycle = NULL;
volatile unsigned int* ioMemDataGPS = NULL;
volatile unsigned int* ioMemDataRate_sps = NULL;
volatile uint32_t *ioFrontEndData = NULL;

shmem_handle io_mem_shmem_handle = NULL;

void
closeIoMemSharedMemory()
{
    if(NULL != io_mem_shmem_handle)
    {
        shmem_close(io_mem_shmem_handle);
    }
}

int
OpenIoMemSharedMemory(int initialize )
{
    volatile void* iomem_base = NULL;
    shmem_handle   handle = NULL;

    char buff[256];

    strncpy(buff, shared_memory_prefix, sizeof(buff));
    buff[sizeof(buff)-1] = 0;
    strncat(buff, SHMEM_IOMEM_NAME, sizeof(buff) - strlen(buff) - 1);

    handle = shmem_open( buff, SHMEM_IOMEM_SIZE_MB );
    iomem_base = shmem_mapping( handle );

    ioMemData = (volatile IO_MEM_DATA*)( iomem_base + IO_MEM_DATA_OFFSET );
    io_mem_shmem_handle = handle;
    atexit(closeIoMemSharedMemory);

    if (initialize) {
        ioMemData->struct_version = IO_MEM_DATA_VERSION;
    }
    else {
        if (ioMemData->struct_version != IO_MEM_DATA_VERSION) {
            fprintf(stderr, "Error: The ioMemData->struct_version is %d, but we expect %d.\n"
                            " awgtpman needs to be rebuilt with the new version. \n",
                    ioMemData->struct_version, IO_MEM_DATA_VERSION);
            ioMemData = NULL;
        }
    }

    return ( ioMemData != NULL ) ? -1 : 0;
}


volatile TESTPOINT_CFG * shmemTestpointCfg = NULL;

shmem_handle testpoint_cfg_shmem_handle = NULL;

void
closeTestpointCFGSharedMemory()
{
    if(NULL != testpoint_cfg_shmem_handle)
    {
        shmem_close(testpoint_cfg_shmem_handle);
    }
}

int
OpenTestpointCfgSharedMemory(const char *model_name)
{
    char            buff[256];
    shmem_handle    handle = NULL;

    strncpy( buff, shared_memory_prefix, sizeof(buff));
    buff[sizeof(buff)-1] = 0;
    strncat( buff, model_name, sizeof( buff) - strlen(buff) - 1);
    strncat( buff, SHMEM_TESTPOINT_SUFFIX, sizeof(buff) - strlen(buff) - 1);

    handle = shmem_open(buff, SHMEM_TESTPOINT_SIZE_MB );
    shmemTestpointCfg = shmem_mapping( handle );
    TESTPOINT_CFG_INIT(shmemTestpointCfg);
    testpoint_cfg_shmem_handle = handle;
    atexit(closeTestpointCFGSharedMemory);
    return ( shmemTestpointCfg != NULL ) ? -1 : 0;
}

shmem_handle io_frontend_shmem_handle = NULL;

void
closeIOFrontEndData()
{
    if(NULL != io_frontend_shmem_handle)
    {
        shmem_close(io_frontend_shmem_handle);
    }
}

int
OpenIoFrontEndData(const char *model_name)
{
    char            buff[256];
    shmem_handle    handle = NULL;

    strncpy( buff, shared_memory_prefix, sizeof(buff));
    buff[sizeof(buff)-1] = 0;
    strncat( buff, model_name, sizeof( buff) - strlen(buff) - 1);

    handle = shmem_open(buff, SHMEM_EPICS_SIZE_MB );
    ioFrontEndData = (volatile uint32_t *) shmem_mapping( handle );
    io_frontend_shmem_handle = handle;
    atexit(closeIOFrontEndData);
    return ( ioFrontEndData != NULL ) ? -1 : 0;
}

int
SetSharedMemoryPrefix(const char *prefix)
{
    if(!strncmp(prefix, SHMEM_MBUF_PREFIX, strlen(SHMEM_MBUF_PREFIX)-3))
    {
        shared_memory_prefix = SHMEM_MBUF_PREFIX;
    }
    else if(!strncmp(prefix, SHMEM_POSIX_PREFIX, strlen(SHMEM_POSIX_PREFIX)-3))
    {
        shared_memory_prefix = SHMEM_POSIX_PREFIX;
    }
    else  //don't know this prefix
    {
        return 0;
    }
    return 1;
}
