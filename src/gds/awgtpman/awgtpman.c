static char *versionId = "Version $Id$" ;

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>
#include "dtt/gdsmain.h"
#include "dtt/gdstask.h"
#include "dtt/testpoint_server.h"
#include "dtt/awg.h"
#include "dtt/awg_server.h"
#include "dtt/gdsprm.h"
#include "dtt/rmapi.h"
#include "drv/cdsHardware.h"
#include "modelrate.h"
#include "shmem_awg.h"
#include "shared_memory.h"

#define _PRIORITY_TPMAN		60
#define _PRIORITY_AWG		60
#define _TPMAN_NAME		"tTPmgr"
#define _AWG_NAME		"tAWGmgr"

char myParFile[256];

char archive_storage[256];
char *archive = archive_storage;

char site_prefix_storage[2];
char *site_prefix = site_prefix_storage;

char target[256];


/**
 * Model rate in Hz.  This may not be properly defined
 * in the case where only  a frequency multiplier was given.
 */
int model_rate_Hz = 0;

/**
 * Number of values in a page, where a page will hold all data for one
 * epoch for one channel at the model rate, so page_size is model_rate_Hz / epochs_per_sec
 */
int page_size = 0;

/* Control system name */
char system_name[PARAM_ENTRY_LEN];

// Set to one when we are in sync with master (x00)
int inSyncMaster = 0;

// when true, get timing from the gpstime kernel module
// when false, get timing from IOP model through shared memory.
int use_gps_time = 0;


CDS_HARDWARE cdsPciModules;



   int main (int argc, char* argv[])
   {
      char 		c;		/* flag */
      int		errflag = 0;	/* error flag */
      int		attr;	        /* task creation attribute */
      taskID_t		tpmanTID = 0;
      taskID_t		awgTID = 0;
      int		run_awg = 1;
      int		run_tpman = 1;
      int 		lckall = 0;

      /* awgtpman by default tries to read the model rate from the INI files
       * for the model.  Override with the -r command.
       */
      int               use_file_model_rate = 1;

      {
	 int i ;
	 char   hostname[128] ;

	 for(i = 0; i < argc; i++)
	 {
	    fprintf(stderr, "%s ", argv[i]) ;
	 }
	 fprintf(stderr, "started ") ;
	 if (gethostname(hostname, sizeof(hostname)) == 0)
	 {
	    fprintf(stderr, "on host %s ", hostname) ;
	 }
	 fprintf(stderr, "hostid %lx ", gethostid()) ;
	 fprintf(stderr, "\nawgtpman %s\n", versionId) ;
      }
   
      system_name[0] = 0;
      while ((c = getopt (argc, argv, "h?tar:01248ws:l:m:g")) != EOF) {
         if(isdigit(c))
         {
             printf("\nERROR: Model rate multiplier can no longer be specified on the command line.\n\n"
                     "Use -r option to set model rate\n\n");
             errflag = 1;
         }
         switch (c) {
	    case 'w':
		lckall = 1;
		break;
	    case 's':
		if (strlen(optarg) > (PARAM_ENTRY_LEN-2)) {
			printf("System name is too long\n");
			exit(1);
		}
		strcpy(system_name, optarg);
		break;
            /* help */
            case 'h':
            case '?':
               {
                  errflag = 1;
                  break;
               }
	    case 't':
	       {
		 run_awg = 0;
		 break;
	       }
	    case 'a':
	       {
		 run_tpman = 0;
		 break;
	       }
            case 'g':
               {
                 use_gps_time = 1;
                 break;
               }
	    case 'l':
	    	{
			if (0 == freopen(optarg, "w", stdout)) {
				perror("freopen");
				exit(1);
			}
			setvbuf(stdout, NULL, _IOLBF, 0);
			stderr = stdout;
			break;
		}
            case 'm':
                {
                    if(!SetSharedMemoryPrefix(optarg))
                    {
                        printf("\nERROR: '%s' is not a valid shared memory prefix\n\n", optarg);
                        errflag = 1;
                    }
                    break;
                }
            case 'r':
                {
                    use_file_model_rate = 0;
                    model_rate_Hz = atoi(optarg);
                    break;
                }
         }
      }

       if (errflag) {
           printf ("Usage: awgtpman\n"
                   "    Starts awg and tpman on a unix machine\n"
                   "    -h : help\n"
                   "    -l <file_name> : specify log file name\n"
                   "    -s <system_name> : specify control system name (model name)\n"
                   "    -t : run tpman, no awg\n"
                   "    -a : run awg, no tpman\n"
                   "    -w : lock all pages in memory\n"
                   "    -g : use gpstime.  Default is to get timing from IOP model.\n"
                   "    -m <shared_mem_prefix>: select shared memory type\n"
                   "        'mbuf': mbuf shared memory usable by kernel objects such as real-time models (default)\n"
                   "        'shm': POSIX shared memory.  Userspace only.  Portable.  Does not require a kernel module.\n"
                   "    -r <model_rate> : Override the automatically determined model rate in Hz.\n"
                   "                      Rate is in Hz.  Rate must be a multiple of 16.\n");
           return 1;
       }


       //setup the target directory.  Look for TARGET env, first, then failing that, build an
      //old style directory, then just fail.
      if(getenv("TARGET"))
      {
          snprintf(target, sizeof(target), "%s", getenv("TARGET"));
      }
      else if(getenv("IFO") && getenv("SITE"))
      {
          char site_upper[4], site_lower[4];
          char ifo_upper[3], ifo_lower[3];

          printf("WARNING: TARGET environment variable not found\n");
          printf("\tBuilding an old style target path\n");
          snprintf(site_upper, sizeof site_upper, "%s", getenv("SITE"));
          snprintf(ifo_upper, sizeof ifo_upper, "%s", getenv("IFO"));
          printf("SITE=%s, IFO=%s\n", site_upper, ifo_upper);
          int lc=0;
          while(( site_lower[lc] = (char)tolower(site_upper[lc])))
          {
              lc++;
          }
          lc=0;
          while(( ifo_lower[lc] = (char)tolower(ifo_upper[lc])))
          {
              lc++;
          }
          printf("site=%s, ifo=%s\n", site_lower, ifo_lower);
          snprintf( target,
                    sizeof( target ),
                    "/opt/rtcds/%s/%s",
                    site_lower,
                    ifo_lower );
      }
      else
      {
          fprintf(stderr, "ERROR: TARGET environment variable not found.\n"
            "\tTARGET must be set to the base target directory for the IFO\n"
            "\te.g. /opt/rtcds/tst/x2, so that \"$TARGET/target/gds/param\"\n"
            "\tis a directory containing the \".par\" parameter files.\n");
          exit(2);
      }
      printf("Target directory is %s\n", target);

      int rate_hz=0, dcuid=0;
      if(use_file_model_rate)
      {
          char tpdir[256];
          snprintf(tpdir, sizeof tpdir, "%s/target/gds/param", target);
          printf("Looking for model rate in %s\n", tpdir);
          get_model_rate_dcuid( &rate_hz, &dcuid, system_name, tpdir);

          if(!rate_hz)
          {
              fprintf(stderr, "ERROR: model rate for %s could not be read from param file.\n", system_name);
              fprintf(stderr, "\tCheck the spelling of the system name (argument to -s).\n");
              fprintf(stderr, "\tCheck that the TARGET environment variable is set so that the directory\n"
                               "\t\"$TARGET/target/gds/param\" contains the parameter files for the model.\n");
              fprintf(stderr, "\tUse the -r argument to specify the model rate from the command line.\n");
              exit(-1);
          }

          model_rate_Hz = rate_hz;  // save to global

      }
      else
      {
          // model rate set in options
          printf("Command line override of model rate to %d Hz\n", model_rate_Hz);
      }

      if(model_rate_Hz <= 0)
      {
          fprintf(stderr, "Model rate %d Hz must be positive.\n", model_rate_Hz);
          exit(-2);
      }

      if(model_rate_Hz > MAX_MODEL_RATE_SPS)
      {
         fprintf(stderr, "Model rate %d Hz is greater than the maximum %d Hz.\n",
                  model_rate_Hz, MAX_MODEL_RATE_SPS);
         exit(-2);
      }

      if( 0 != ( model_rate_Hz % NUMBER_OF_EPOCHS ))
      {
          fprintf(stderr, "Model rate %d Hz must be a multiple of %d epochs per second.\n",
                  model_rate_Hz, NUMBER_OF_EPOCHS);
          exit(-2);
      }

      printf("%d Hz system\n", model_rate_Hz);
   
      /* help */

      if (!run_awg && !run_tpman) exit(0);

      if (lckall) {
      	// Lock all current and future process pages in memory
      	mlockall(MCL_FUTURE);
      }

      if(!OpenAwgDataSharedMemory(system_name))
      {
          fprintf(stderr, "Failed to open mbuf for awg streaming.\n");
          _exit(2);
      }



      if (run_awg) {
        nice(-20);
      }

      // site_prefix takes on the first letter of the system name (as upper case)
      site_prefix_storage[1] = 0;
      site_prefix_storage[0] = toupper(system_name[0]);

      snprintf(myParFile, sizeof(myParFile),"%s/target/gds/param/tpchn_%s.par", target, system_name);
      printf("My config file is %s\n", myParFile);
      snprintf(archive_storage, sizeof archive_storage, "%s/target/gds", target);
      printf("Archive storage is %s\n", archive);

      if(!OpenIoMemSharedMemory(use_gps_time))
      {
          fprintf(stderr, "Failed to open IOMem shared memory.\n");
          _exit(2);
      }

      if(!OpenTestpointCfgSharedMemory(system_name))
      {
          fprintf(stderr, "Failed to open testpoint configuration shared memory.\n");
          _exit(2);
      }

      if(!OpenIoFrontEndData(system_name))
      {
          fprintf(stderr, "Failed to open legacy front end shared memory.\n");
          _exit(2);
      }

      // Make sure there is no other copy running already
      { 
      	int g,g1;
        g = (int)ioFrontEndData[0];
	sleep(1);
        g1 = (int)ioFrontEndData[0];
	if (g != g1) {
		fprintf(stderr, "Another copy already running! Will not start a second copy.\n");
		_exit(1);
	}
      }

      // Find the first ADC card
      // Master will map ADC cards first, then DAC and finally DIO


      if(!use_gps_time)
      {
          if ( ioMemData->totalCards <= 0 )
          {
              // Wait for the master to come up
              printf( "Waiting for the IOP to start\n" );
              while ( ioMemData->totalCards <= 0 )
              {
                  sleep( 2 );
              }
              sleep(1);
          }
          printf( "Total PCI cards from the master: %d\n",
                  ioMemData->totalCards );
          sleep( 2 );
          if ( ioMemData->totalCards > 0 )
          {
              printf( "Model %d = %d\n", 0, ioMemData->model[ 0 ] );
              printf( "Found card at %d\n", ioMemData->ipc[ 0 ] );
              cdsPciModules.adcType[ 0 ] = ioMemData->model[ 0 ];
              cdsPciModules.adcConfig[ 0 ] = ioMemData->ipc[ 0 ];
              cdsPciModules.adcCount = 1;
          }
          if ( !cdsPciModules.adcCount )
          {
              printf( "No ADC cards found - exiting\n" );
              _exit( 1 );
          }

          int ll = cdsPciModules.adcConfig[ 0 ];
          ioMemDataCycle = &ioMemData->iodata[ ll ][ 0 ].cycle;
          ioMemDataGPS = &ioMemData->gpsSecond;
          ioMemDataRate_sps = &ioMemData->mem_data_rate_hz;

          printf( "mem_data_rate_hz is %u\n", ioMemData->mem_data_rate_hz );
          printf( "iodata index=%d\n", ll );
          printf( "IO_MEM_DATA_SIZE=%d\n", sizeof( IO_MEM_DATA ) );
          if(ioMemData->mem_data_rate_hz <= 0)
          {
              fprintf(stderr,"*** WARNING: IOP data rate was not detected.\n"
                      "\tEither the IOP is not running, or there is a version mismatch betwen the IOP and awgtpman\n");
          }
      }
      else
      {
          ioMemDataGPS = &ioMemData->gpsSecond;
          ioMemDataCycle = &ioMemData->iodata[0][0].cycle;
          ioMemDataRate_sps = &ioMemData->mem_data_rate_hz;
      }

      if (run_tpman) {

	if (!run_awg) return testpoint_server();

        /* Start TP Manager */
        printf ("Spawn testpoint manager\n");
        attr = PTHREAD_CREATE_DETACHED | PTHREAD_SCOPE_SYSTEM;
        if (taskCreate (attr, _PRIORITY_TPMAN, &tpmanTID,
           _TPMAN_NAME, (taskfunc_t) testpoint_server, 0) < 0) {
	   printf ("Error: Unable to spawn testpoint manager\n");
           return 1;
        }
        extern int testpoint_manager_node;
        int tm_count=0;
        while( testpoint_manager_node < 0)
        {
            if(tm_count > 60)
            {
                printf("Test point manager startup failed; %d\n", testpoint_manager_node);
                return 1;
            }
            sleep( 1 );
            tm_count++;
        }

      }

      if (run_awg) {
        /* Start AWG Manager */
        printf ("Spawn arbitrary waveform generator\n");
        attr = PTHREAD_CREATE_DETACHED | PTHREAD_SCOPE_SYSTEM;
        if (taskCreate (attr, _PRIORITY_AWG, &awgTID,
           _AWG_NAME, (taskfunc_t) awg_server, 0) < 0) {
	   printf ("Error: Unable to spawn arbitrary waveform generator\n");
           return 1;
        }

        int awg_init_count = 0;
        while(!isAwgInit())
        {
            // wait a whole minute to quit
            if(awg_init_count > 60)
            {
                printf("AWG startup failed.\n");
                return 1;
            }
            printf("Waiting for AWG to initialize.\n");
            sleep(1);
            awg_init_count++;
        }

        /* Load AWG parameters here */
       awgLock(1);
      }
      

      fflush(stdout) ;
      fflush(stderr) ;

      /* go to sleep */
      for (;;) {
         sleep (1000);
      }
      /* Never reached */
   }
