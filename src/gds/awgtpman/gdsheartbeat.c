static char *versionId = "Version $Id$" ;
#if !defined(GDS_ONLINE)
#ifndef AVOID_SIGNALS
#define AVOID_SIGNALS
#endif
#endif

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: gdsheartbeat						*/
/*                                                         		*/
/* Procedure Description: provides functions to utilize the system	*/
/* heartbeat; in LIGO 16Hz						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

/* Header File List: */
#include "dtt/gdsutil.h"
#include <time.h>
#include "shared_memory.h"

/* VxWorks */
#include <errno.h>
#include <time.h>
#include <limits.h>
#include <unistd.h>
#if !defined(AVOID_SIGNALS)
#include <signal.h>
#endif
#include <pthread.h>
#include "dtt/gdstask.h"

/* all */
#include "dtt/gdsheartbeat.h"

#include <sys/ioctl.h>
#include <fcntl.h>
#include "../../drv/gpstime/gpstime.h"

#include "dtt/rmapi.h"

/* The total number of DAQ bytes the front-end is sending per second */
/* This is the current number of bytes and it is updated 16 times per second */
unsigned int curDaqBlockSize;

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Signals:  signal used for heartbeat timer (POSIX only)		*/
/*           SIGUSR2                                           		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
#if defined (_USE_POSIX_TIMER)
#define SIGheartbeat SIGUSR2
#endif


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Types: semhb_t	semaphore used for heartbeat			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   typedef pthread_cond_t* semhb_t;


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Globals: semHeartbeat: heartbeat semaphore;				*/
/*          		  gets release every heartbeat	              	*/
/*          heartbeatCount: counts the number of heartbeats received	*/
/*                          since the heartbeat was installed		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static semhb_t semHeartbeat = NULL;
   static unsigned long heartbeatCount = 0;


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Forward declarations: 						*/
/*	initHeartbeat		init of hearbeat interface		*/
/*	finitHearbeat		stops the hearbeat 			*/
/*      								*/
/*----------------------------------------------------------------------*/
   __init__(initHeartbeat);

   __fini__(finiHeartbeat);

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: doHeartbeat					*/
/*                                                         		*/
/* Procedure Description: releases the heartbeat semaphore		*/
/* 									*/
/*                                                         		*/
/* Procedure Arguments: void						*/
/*                                                         		*/
/* Procedure Returns: 0 when successful or error number when failed	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

   int doHeartbeat (void) 
   {
      /* wait for semaphore */
      if (semHeartbeat == 0) return -1;
      if (pthread_cond_broadcast (semHeartbeat) != 0) return -1;

      /* increment interrupt count */
      heartbeatCount++;
      return 0;
   }



/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: setupHeartbeat				*/
/*                                                         		*/
/* Procedure Description: releases the heartbeat semaphore		*/
/*                                                         		*/
/* Procedure Arguments: void						*/
/*                                                         		*/
/* Procedure Returns: 0 when successful or error number when failed	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

   int setupHeartbeat (void)
   {
      static pthread_cond_t semHB;
   
      /* test if heartbeat semaphore is already created */
      if (semHeartbeat != NULL) {
         return -2;
      }
      /* create a new heartbet semaphore */ 
      if (pthread_cond_init (&semHB, NULL) != 0) {
         return -1;
      }
      semHeartbeat = &semHB;
      return 0;
   }



/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: getTimeAndEpoch				*/
/*                                                         		*/
/* Procedure Description: obtains the current time and divides it	*/
/*                        TAI in sec and an epoch count			*/
/*                                                         		*/
/* Procedure Arguments: _tai : pointer to TAI time variable		*/
/*                      _epoch: pointer to epoch variable 		*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static void getTimeAndEpoch (taisec_t* _tai, int* _epoch)
   {
      tainsec_t	tain;	/* tai in nsec */
      tai_t	taibd;	/* broken down time */
      taisec_t	tai;	/* tai of epoch in sec */
      int	epoch;	/* epoch */
   
      /* obtain current time and calculate epoch */
      tain = TAInow();
      TAIsec (tain, &taibd);
      tai = taibd.tai;
      epoch = (taibd.nsec + _EPOCH / 10) / _EPOCH;
      if (epoch >= NUMBER_OF_EPOCHS) {
         epoch -= NUMBER_OF_EPOCHS;
         tai++;
      }
      if (_tai != NULL) {
         *_tai = tai;
      }
      if (_epoch != NULL) {
         *_epoch = epoch;
      }
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: synchWithHeartbeat				*/
/*                                                         		*/
/* Procedure Description: blocks until the next heartbeat		*/
/*                                                         		*/
/* Procedure Arguments: void						*/
/*                                                         		*/
/* Procedure Returns: 0 when successful or error number when failed	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static pthread_mutex_t  dummy = PTHREAD_MUTEX_INITIALIZER;

   int syncWithHeartbeat (void)
   {
      static struct timespec	tick = 
      {_EPOCH / _ONESEC, _EPOCH % _ONESEC};
   
      /* test if heartbeat semaphore is already created */
      if (semHeartbeat == NULL) {
         nanosleep (&tick, NULL);
         return -2;
      }

      /* wait for the next heartbeat */
      pthread_mutex_lock (&dummy);
      pthread_cond_wait (semHeartbeat, &dummy);
      pthread_mutex_unlock (&dummy);
      return 0;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: synchWithHeartbeatEx			*/
/*                                                         		*/
/* Procedure Description: blocks until the next heartbeat		*/
/*                                                         		*/
/* Procedure Arguments: tai: variable to store TAI of heartbeat		*/
/*                      epoch: variable to store epoch count 		*/
/*                                                         		*/
/* Procedure Returns: 0 when successful or error number when failed	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   int syncWithHeartbeatEx (taisec_t* tai, int* epoch)
   {
      int	status;
   
      status = syncWithHeartbeat ();
      if (status == 0) {
         getTimeAndEpoch (tai, epoch);
      }
      return status;
   }


/* Forward declarations */
#if !defined(AVOID_SIGNALS)
   static void defaultISR ();
#endif
   static int  connectHeartbeatISR (void ISR ());


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: installHeartbeat				*/
/*                                                         		*/
/* Procedure Description: installs the interrupt service and reset	*/
/* the heartbeat semaphore.						*/
/*                                                         		*/
/* Procedure Arguments: void						*/
/*                                                         		*/
/* Procedure Returns: 0 when successful or error number when failed	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _TP_DAQD

    /**
     * @brief Get current GPS time from the symmetricom IRIG-B card
     * @param symmetricom_fd, a file descriptor opened to the gpstime device
     * @param frac_nsec, output, returns fraction of a second in nanoseconds
     * @param stt output, status value.  Always zero.
     * @return gps time in seconds.
     */
    static unsigned long symm_gps_time(int symmetricom_fd, unsigned long *frac_nsec, int *stt) {
    // **************************************************************************
        unsigned long t[3];
        ioctl (symmetricom_fd, IOCTL_SYMMETRICOM_TIME, &t);
        t[1] *= 1000;
        t[1] += t[2];
        if (frac_nsec) *frac_nsec = t[1];
        if (stt) *stt = 0;
        // return  t[0] + daqd.symm_gps_offset;
        return  t[0];
    }

   static taskretarg_t provide_gps_time(taskarg_t _)
   {
       printf("Using a thread to simulate IOP timing\n");

       const int virtual_iop_rate = 16; //one tick per cycle
       const int samples_per_cycle = virtual_iop_rate / NUMBER_OF_EPOCHS;
       const int nsec_per_cycle = 1e9 / NUMBER_OF_EPOCHS;

       int symmetricom_fd =  open ("/dev/gpstime", O_RDONLY | O_SYNC);
       if (symmetricom_fd < 0) {
           perror("/dev/gpstime");
           _exit(1);
       }

       unsigned long gps_seconds;
       unsigned long gps_nsec_frac;

       printf("virtual iop rate = %d SPS\n", virtual_iop_rate);
       printf("virtual iop samples per cycle = %d\n", samples_per_cycle);
       printf("nanseconds per cycle = %d\n", nsec_per_cycle);

       // setup shared memory
       *ioMemDataRate_sps = virtual_iop_rate;

       while(1)
       {
            //get the time
            gps_seconds = symm_gps_time(symmetricom_fd, &gps_nsec_frac, NULL);

            //calculate epoch
            int epoch = gps_nsec_frac / nsec_per_cycle;

            //populate shared memory
            *ioMemDataGPS = gps_seconds,
            *ioMemDataCycle = epoch * samples_per_cycle;

            //sleep
            usleep(10000);
       }
   }

   static taskID_t gpstimeTID;

   static int setup_gps_time()
   {


       int attr = PTHREAD_CREATE_DETACHED;
       if (taskCreate (attr, 90, &gpstimeTID, "tGPS", provide_gps_time,
                       (taskarg_t) 1) != 0) {
           return -102;
       }


       return 1;
   }

   int installHeartbeat (void ISR (void), int use_gps_time)
   {
      void (*hbISR) (void);

      /* do not install more than one hearbeat clock */
      switch (setupHeartbeat ()) 
      {
         case 0: 
            {
               /* init successful */
               break;
            }
         case -2: 
            {
               /* already initialized, return immeadiately */
               return 0;
            }
         default:
            {
               /* init failed */
               return -1;
            }
      }

      if(use_gps_time)
      {
          printf("Using gps time driver\n");
          if(!setup_gps_time())
          {
              return -2;
          }
      }
      else
      {
          printf("Using IOP timing\n");
      }
   
      /* assign default ISR if argument is NULL */
   #if !defined(AVOID_SIGNALS)
      if (ISR == NULL) {
         hbISR = defaultISR;
      }
      else {
   #endif
      hbISR = ISR;
   #if !defined(AVOID_SIGNALS)
      }
   #endif
      /* connect target specific ISR */
      return connectHeartbeatISR (hbISR);
   }
#endif


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: defaultISR					*/
/*                                                         		*/
/* Procedure Description: declares the default ISR			*/
/* 									*/
/*                                                         		*/
/* Procedure Arguments: void						*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

/* POSIX: not really an ISR, but a signal handler */

   static int		signalHandlerStatus = 0;
   static taskID_t	hearbeatTID = 0;

#if !defined(AVOID_SIGNALS)
   static void defaultISR (int sig)
   {
      /* do a heartbeat */
      doHeartbeat();
      /* reinstall itself */
      signal (SIGheartbeat, defaultISR);
   }
#endif



/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: connectHeartbeatISR				*/
/*                                                         		*/
/* Procedure Description: connects the ISR				*/
/*                                                         		*/
/* Procedure Arguments: hbISR interrup service routine			*/
/*                                                         		*/
/* Procedure Returns: 0 when successful or error number when failed	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

   /* POSIX: creates a realtime timer and attaches it to a signal */ 

#if !defined(AVOID_SIGNALS)
   void* installSignal (void* noreturn)
   {
      static timer_t	hbtimer;
      struct sigevent	hbsig;
      struct itimerspec	tval;
      struct timespec	now;

      /* create a timer */
      hbsig.sigev_notify = SIGEV_SIGNAL;
      hbsig.sigev_signo = SIGheartbeat;
      hbsig.sigev_value.sival_int = 0;
      if (timer_create (CLOCK_REALTIME, &hbsig, &hbtimer) != 0) {
         signalHandlerStatus = -1;
         return NULL;
      }

      /* get current time */
      if (clock_gettime (CLOCK_REALTIME, &now) != 0) {
         signalHandlerStatus = -2;
         return NULL;
      }

      /* start timer synchronized with the next second */
      tval.it_value = now;
      if (now.tv_nsec > 700000000L) {
         /* too close to the next sec, go one further */
         tval.it_value.tv_sec += 2;
      }
      else {
         tval.it_value.tv_sec += 1;
      }
      tval.it_value.tv_nsec = 0; /* exactly on sec */

      /* set timer to heartbeat rate */
      tval.it_interval.tv_sec = 1 / NUMBER_OF_EPOCHS;
      tval.it_interval.tv_nsec = (1000000000UL / NUMBER_OF_EPOCHS) % 1000000000UL;

       /* connect signal handler */
      if (signal (SIGheartbeat, defaultISR) == SIG_ERR) {
         signalHandlerStatus = -3;
         return NULL;
      }

      /* arm timer */
      if (timer_settime (hbtimer, TIMER_ABSTIME, &tval, NULL) != 0) {
         signalHandlerStatus = -4;
         return NULL;
      }

      if ((int) noreturn) {
         sigset_t	set;
         int		sig;

         /* signal mask */
         if ((sigemptyset (&set) != 0) ||
            (sigaddset (&set, SIGheartbeat) != 0)) {
            signalHandlerStatus = -5;
            return NULL;
         }

         signalHandlerStatus = 1;
         while (1) {
            /* wait for heartbeat signal */
            sigwait (&set, &sig);
            /* check if finished */
            if (signalHandlerStatus == 2) {
               signal (SIGheartbeat, SIG_IGN);
               return NULL;
            }
            /* do a heartbeat */
            doHeartbeat();
         }
      }
      return NULL;
   }
#else

#ifndef _TP_DAQD
   void* installSignal (void* noreturn)
   {

      if (0 == noreturn) {
         return NULL;
      }

      pthread_setcanceltype (PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

      // Sync up to the master clock
      // Find memory buffer of first ADC to be used in secondary application.
      printf("waiting to sync %d\n", *ioMemDataCycle);
      // Spin until cycle 0 detected in first ADC buffer location.
      int spin_cnt = 0;
      struct timespec cycle_wait={0, 10000000}; //10 msec

      //first wait for non-zero then zero.  If no change, we never sync.

      while (*ioMemDataCycle == 0) {
          nanosleep(&cycle_wait, 0);
          spin_cnt++;
          if (spin_cnt >= 6000) {
              fprintf(stderr, "Timed out waiting for cycle counter to change from zero.\n");
              _exit(3);
          }
      }

      while (*ioMemDataCycle != 0) {
          nanosleep(&cycle_wait, 0);
	spin_cnt++;
	if (spin_cnt >= 6000) {
	  fprintf(stderr, "Timed out waiting for cycle counter to return to zero.\n");
          _exit(3);
	}
      }

      // Get GPS seconds from MASTER
      int timeSec = *ioMemDataGPS;
      int timeCycle = 0;
      printf("TimeSec=%d;  cycle=%d\n", timeSec, *ioMemDataCycle);

      int samples_per_second = *ioMemDataRate_sps;
      int wait_rate=0;
      printf("samples_per_second=%d\n", samples_per_second);
      while(samples_per_second <= 0)
      {
          if(wait_rate >= 30)
          {
              fprintf(stderr, "Timed out waiting for the IOP data rate\n");
              _exit(2);
          }
          sleep(1);
          ++wait_rate;
          samples_per_second = *ioMemDataRate_sps;
          printf("samples_per_second=%d\n", samples_per_second);
      }

      int samples_per_cycle = samples_per_second / DAQ_NUM_DATA_BLOCKS_PER_SECOND;

      signalHandlerStatus = 1;

      while (1) {

         /* check if finished */
         if (signalHandlerStatus == 2) {
            return NULL;
         }
         /* do a heartbeat */
         doHeartbeat();

	 /*printf("AWG installSignal loop iteration\n"); */

	timeCycle += samples_per_cycle;
	timeCycle %= samples_per_second;
	
	int sleep_cnt = 0;
        do {
           struct timespec wait = {0, 10000000UL }; // 10 milliseconds
           nanosleep (&wait, NULL);
	   //printf("nanosleeping...\n"); 
	   sleep_cnt++;
	   if (sleep_cnt >= 100) {
		fprintf(stderr, "IOP cycle timeout\n");
		_exit(1);
	   }
        } while(timeCycle?
		*ioMemDataCycle < timeCycle:
		*ioMemDataCycle > (samples_per_second / 2));


       // Send gps seconds to the front-end
       ioFrontEndData[0] = (uint32_t)*ioMemDataGPS;

       // Read the current DAQ block size from the front-end
       curDaqBlockSize = ioFrontEndData[1];
      }
   }
#endif
#endif

#ifndef _TP_DAQD
   static int connectHeartbeatISR (void ISR (void))
   {
      /* for Bajas make sure the realtime clock is intialized.
         :PATHETIC: the timer resolution is determiend by the
         system clock interrupt rate! To make it work properly,
         it has to be a multiple of the heartbeat rate. */

      signalHandlerStatus = 0;
   
      {
         static int	once = 0;
         int		attr;
         struct timespec tick = {0, 100000000};	/* tick */
      
      #if !defined(AVOID_SIGNALS)
         sigset_t	set;
         /* mask heartbeat signal */
         if ((sigemptyset (&set) != 0) ||
            (sigaddset (&set, SIGheartbeat) != 0) ||
            (pthread_sigmask (SIG_BLOCK, &set, NULL) != 0)) {
            return -101;
         }
      #endif
         /* install signal handler in separate task context */
         attr = PTHREAD_CREATE_DETACHED;
         if (taskCreate (attr, 90, &hearbeatTID, "tHB", installSignal, 
            (taskarg_t) 1) != 0) {
            return -102;
         }
         /* wait for signal installation */
         while (signalHandlerStatus == 0) {
            nanosleep (&tick, NULL);
         }
         if (signalHandlerStatus < 0) {
            return signalHandlerStatus;
         }
      
         /* support forking (timers are process specific), so make a new 
            one for each fork'ed child; but only the first time around! */
         if (once == 0) {
            pthread_atfork (NULL, NULL, 
                           (void (*) (void)) connectHeartbeatISR);
         }
         once++;
         return 0;
      }
   }
#endif


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: getHeartbeatCount				*/
/*                                                         		*/
/* Procedure Description: returns the number of heartbeats		*/
/*                                                         		*/
/* Procedure Arguments: void						*/
/*                                                         		*/
/* Procedure Returns: number of heartbeats since heartbeat was installed*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   unsigned long getHeartbeatCount (void)
   {
      return heartbeatCount;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: initHeartbeat				*/
/*                                                         		*/
/* Procedure Description: initilializes heartbeats			*/
/*                                                         		*/
/* Procedure Arguments: void						*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static void initHeartbeat (void) 
   {
   #if !defined(AVOID_SIGNALS)
      sigset_t	set;
      if ((sigemptyset (&set) != 0) ||
         (sigaddset (&set, SIGheartbeat) != 0) ||
         (pthread_sigmask (SIG_BLOCK, &set, NULL) != 0)) {
         return;
      }
   #endif
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: finiHeartbeat				*/
/*                                                         		*/
/* Procedure Description: terminates the heartbeat			*/
/*                                                         		*/
/* Procedure Arguments: void						*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static void finiHeartbeat (void)
   {
   #if defined(_USE_POSIX_TIMER)
      signalHandlerStatus = 2;
   #if !defined(AVOID_SIGNALS)
      signal (SIGheartbeat, SIG_IGN);
   #endif
      taskCancel (&hearbeatTID);
   #endif
   }
