add_compile_options(-lm)

#target_compile_definitions(gds_objs)

add_executable(awgtpman
        awgtpman.c
        awg.c
        awg_server.c
        awgfunc.c
        awgtpman.c
        rmapi.c
        gdstask.c
        gdserr.c
        gdsstring.c
        gdschannel.c
        gdsprm.c
        gdssched.c
        gdsheartbeat.c
        gdsrand.c
        gdsxdr_util.c
        testpoint_server.c
        testpointinfo.c
        testpoint_direct.c
        rpcinc.c
        confserver.c
        sockutil.c
        tconv.c
        shared_memory.c

        ../../util/modelrate.c
        #../../drv/shmem.c
        big_buffers.c
        )

target_include_directories(awgtpman PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}
        ${CMAKE_SOURCE_DIR}/src/include
        ${RPC_OUTPUT_DIR}
        ${CMAKE_SOURCE_DIR}/src/shmem
        ${CMAKE_SOURCE_DIR}/src/drv/mbuf
        )

target_link_libraries(awgtpman PRIVATE
        Threads::Threads
        m
        #testpoint_objs
        rawgapi_rpc
        rtestpoint_rpc
        ${RPC_LIBRARIES}
        driver::shmem
        )

target_compile_definitions(awgtpman PUBLIC
        LIGO_GDS
        _ADVANCED_LIGO
        _AWG_RM
        MAX_CHNNAME_SIZE=60
        _NO_KEEP_ALIVE
        )

install(TARGETS awgtpman DESTINATION bin)

#### Static library for use by DAQD ####
SET(TP_CLIENT_VERSION 1.0.0)
add_library(testpoint STATIC
        testpoint.c
        gdsstring.c
        gdserr.c
        )

target_include_directories(testpoint PUBLIC
        ${CMAKE_CURRENT_SOURCE_DIR}
        ${CMAKE_CURRENT_SOURCE_DIR}/../../include
        ${CMAKE_SOURCE_DIR}/src/shmem
        )

target_compile_definitions(testpoint PRIVATE
        -D_TP_CLIENT_VERSION="${TP_CLIENT_VERSION}"
        )

target_include_directories(testpoint PUBLIC
        ${CMAKE_CURRENT_SOURCE_DIR}
        ${CMAKE_CURRENT_SOURCE_DIR}/../../include
        ${RPC_INCLUDE_DIRS}
        ${RPC_OUTPUT_DIR}
        )

target_link_libraries(testpoint
        rtestpoint_rpc
        ${RPC_LIBRARIES})

set_target_properties(testpoint PROPERTIES
        VERSION
            0.0.0
        PUBLIC_HEADER
            testpoint.h
        PRIVATE_HEADER
            "dtt/gdstask.h;dtt/gdserr.h"
        )

install(TARGETS testpoint
        DESTINATION lib
        PUBLIC_HEADER
            DESTINATION include/advligorts/awgtpman
            COMPONENT libraries
        PRIVATE_HEADER
            DESTINATION include/advligorts/awgtpman
            COMPONENT libraries
        )

add_library(gds_objs OBJECT
        rpcinc.c
        gdstask.c
        sockutil.c
        )

target_compile_definitions(gds_objs PRIVATE
        -D_TP_DAQD
        -D_TESTPOINT_DIRECT=0
        )

target_include_directories(gds_objs PUBLIC
        ${CMAKE_CURRENT_SOURCE_DIR}
        ${CMAKE_SOURCE_DIR}/src/include
        ${RPC_OUTPUT_DIR}
        ${CMAKE_SOURCE_DIR}/src/shmem
        ${RPC_INCLUDE_DIRS}
        )

add_library(gds_daqd_libs STATIC
        #$<TARGET_OBJECTS:testpoint_objs>
        $<TARGET_OBJECTS:gds_objs>
        )

target_link_libraries(gds_daqd_libs
        rtestpoint_rpc
        )

add_library(gds::daqd ALIAS gds_daqd_libs)
