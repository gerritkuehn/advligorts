package main

import (
	"fmt"
	"html/template"
)

var indexHandlerTemplate *template.Template

func init() {
	indexHandlerTemplate = template.New("index")
	_, err := indexHandlerTemplate.Parse(`<!DOCTYPE html>
<html>
<head>
	<title>fe_simulated_streams - admin interface</title>
	<style type="text/css">
		body {
			background-color: lightgray;
		}
	</style>
</head>
<body>
	<p>This is the admin interface to fe_simulated_stream</p>
	<h4>Actions</h4>
	<ul>
		<li><a href="/api/v1/system/status/">Get system status /status/</a></li>
		<li>
			<p>Stop the simulation</p>
			<form action="/api/v1/system/stop/" method="POST">
				<input type="submit" value="Stop Simulation"/>				
			</form>
		</li>
		<li>
			<p>Mutate a model</p>
			<form action="/simple_mutate/" method="POST">
				<label>DcuId</label><input type="text" name="DcuId"/><br/>
				<label>Remove # chans</label><input type="text" name="RemoveCount"/><br/>
				<label>Add # chans</label><input type="text" name="AddCount"/><br/>
				<p>Specific channels to add/remove/protect from removal (space seperated)</p>
				<label>Protect these chans from removal</label><br/>
				<input type="text" name="ProtectedChannels" /><br/>
				<label>Remove these specific channels as part of the remove count</label><br/>
				<input type="text" name="RemoveChannels" /><br/>
				<label>Add these specific channels as part of the add count</label><br/>
				<input type="text" name="AddChannels" /><br/>
				<input type="submit" value="Mutate DCU" />		
			</form>
		</li>
		<li>
			<p>Start/Stop models</p>
			<table>
				<tr><th>Name</th><th>dcuid</th><th>Running Models</th><th>Stopped Models</th></tr>
{{range .DcuStatus}}
	{{if .IsStopped }}
				<tr><td>{{.ModelName}}</td><td>{{.DcuId}}</td><td></td><td>
					<form method="POST" action="/simple_start_dcu/{{.DcuId}}/">
						<input type="submit" value="start" />
					</form></td></tr>
	{{else}}
				<tr><td>{{.ModelName}}</td><td>{{.DcuId}}</td><td>
					<form method="POST" action="/simple_stop_dcu/{{.DcuId}}/">
						<input type="submit" value="stop" />
					</form></td><td></td></tr>
	{{end}}
{{end}}
			</table>
		</li>
		<li>
			<p>Send a Timing Glitch</p>
			<form method="POST" action="/simple_timing_glitch/">
				<label>Delta (cycles)</label> <input type="text" name="DeltaCycles" /><br/>
				<label>Dcus<label><input type="text" name="Dcus"/><br/>
				
				<input type="submit" value="Request Glitch" />
			</form>
		</li>
		<li>
			<p>Time</p>
			<p>Current time {{.Clock.String}}</p>
			{{if .Clock.Paused }}
			<form method="POST" action="/clock/resume/">
				<input type="submit" value="Resume" />
			</form>
			<br/>
			<form method="POST" action="/clock/step/">
				<input type="submit" value="Single step" />
			</form>
			{{else}}
			<form method="POST" action="/clock/stop/">
				<input type="submit" value="Stop" />
			</form>
			{{end}}
		</li>
	</ul>
</body>
</html>
`)
	if err != nil {
		panic(fmt.Sprintf("template parse error - index handler - %v", err))
	}
}
