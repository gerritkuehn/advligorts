package main

import (
	"context"
	"encoding/json"
	"git.ligo.org/cds/advligorts/src/fe_stream_test/fe_simulation"
	"git.ligo.org/cds/advligorts/src/fe_stream_test/fe_simulation/gps"
	"git.ligo.org/cds/advligorts/src/fe_stream_test/fe_simulation/middleware"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
)

func apiOk(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write([]byte("{\"status\": \"ok\"}"))
}

func gotoIndex(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/", http.StatusSeeOther)
}

func indexHandler(w http.ResponseWriter, r *http.Request, sim *fe_simulation.SimulationLoop, clock *gps.Clock) {
	data := struct {
		DcuStatus []fe_simulation.SimulationDcuStatus
		Clock     *gps.Clock
	}{
		DcuStatus: sim.Status(),
		Clock:     clock,
	}
	if err := indexHandlerTemplate.Execute(w, &data); err != nil {
		log.Print(err)
		http.Error(w, "Internal error", http.StatusInternalServerError)
	}
}

func getDcuId(r *http.Request) (int, error) {
	params := middleware.ReMuxParams(r)
	val, err := strconv.ParseInt(params["dcuid"], 10, 32)
	if err != nil {
		return 0, err
	}
	return int(val), nil
}

func systemStatusApiHandler(w http.ResponseWriter, r *http.Request, sim *fe_simulation.SimulationLoop) {
	status := sim.Status()
	w.Header().Set("Content-Type", "application/json")
	data, _ := json.Marshal(&status)
	_, _ = w.Write(data)
}

func systemStopApiHandler(w http.ResponseWriter, r *http.Request, sim *fe_simulation.SimulationLoop, srv *http.Server) {
	sim.Stop()
	go func() {
		srv.Shutdown(context.Background())
	}()
}

func stopDcuApiHandler(w http.ResponseWriter, r *http.Request, sim *fe_simulation.SimulationLoop, onSuccess http.HandlerFunc) {
	dcuId, _ := getDcuId(r)
	sim.StopDcu(dcuId)
	onSuccess(w, r)
}

func startDcuApiHandler(w http.ResponseWriter, r *http.Request, sim *fe_simulation.SimulationLoop, onSuccess http.HandlerFunc) {
	dcuId, _ := getDcuId(r)
	sim.StartDcu(dcuId)
	onSuccess(w, r)
}

func mutateDcuApiHandler(w http.ResponseWriter, r *http.Request, sim *fe_simulation.SimulationLoop, dcuCheck *int, onSuccess http.HandlerFunc) {
	var changes struct {
		DcuId             int
		RemoveCount       int
		AddCount          int
		ProtectedChannels string
		RemoveChannels    string
		AddChannels       string
	}

	if middleware.ParseInput(&changes, r) != nil || (dcuCheck != nil && *dcuCheck != changes.DcuId) {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}
	sim.RequestMutation(fe_simulation.MutationRequest{
		DcuId:             changes.DcuId,
		RemoveCount:       changes.RemoveCount,
		AddCount:          changes.AddCount,
		ProtectedChannels: toStringList(changes.ProtectedChannels),
		RemoveChannels:    toStringList(changes.RemoveChannels),
		AddChannels:       toStringList(changes.AddChannels),
	})
	onSuccess(w, r)
}

func timingGlitchHandler(w http.ResponseWriter, r *http.Request, sim *fe_simulation.SimulationLoop, onSuccess http.HandlerFunc) {
	var rawRequest struct {
		DeltaCycles int
		Dcus        string
	}
	if middleware.ParseInput(&rawRequest, r) != nil {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}
	dcuStrings := strings.Split(rawRequest.Dcus, ",")
	dcus := make([]int, 0, len(dcuStrings))
	for _, input := range dcuStrings {
		if id, err := strconv.ParseInt(input, 10, 32); err == nil {
			dcus = append(dcus, int(id))
		}
	}
	sim.RequestTimingGlitch(fe_simulation.TimingGlitchRequest{
		DeltaCycles: rawRequest.DeltaCycles,
		Dcus:        dcus,
	})
	onSuccess(w, r)
}

func clockStopHandler(w http.ResponseWriter, r *http.Request, clock *gps.Clock, onSuccess http.HandlerFunc) {
	clock.Pause()
	onSuccess(w, r)
}

func clockResumeHandler(w http.ResponseWriter, r *http.Request, clock *gps.Clock, onSuccess http.HandlerFunc) {
	clock.Resume()
	onSuccess(w, r)
}

func clockStepHandler(w http.ResponseWriter, r *http.Request, clock *gps.Clock, onSuccess http.HandlerFunc) {
	clock.Step()
	onSuccess(w, r)
}

func runAdminInterface(sim *fe_simulation.SimulationLoop, address string, clock *gps.Clock) {
	for sim.State() == fe_simulation.SimulationNotStarted {
		time.Sleep(time.Microsecond * 20)
	}

	middle := middleware.Chain(middleware.LoggingHandler)
	mux := middleware.NewReMux()

	srv := &http.Server{}
	srv.Addr = address
	srv.Handler = mux

	mux.HandleFunc("/", middle.Apply(func(w http.ResponseWriter, r *http.Request) {
		indexHandler(w, r, sim, clock)
	}))
	mux.HandleFunc("/simple_mutate/", middle.Apply(func(w http.ResponseWriter, r *http.Request) {
		mutateDcuApiHandler(w, r, sim, nil, gotoIndex)
	})).Methods("POST")
	mux.HandleFunc("/simple_stop_dcu/(?P<dcuid>[0-9]+)/", middle.Apply(func(w http.ResponseWriter, r *http.Request) {
		stopDcuApiHandler(w, r, sim, gotoIndex)
	})).Methods("POST")
	mux.HandleFunc("/simple_start_dcu/(?P<dcuid>[0-9]+)/", middle.Apply(func(w http.ResponseWriter, r *http.Request) {
		startDcuApiHandler(w, r, sim, gotoIndex)
	})).Methods("POST")
	mux.HandleFunc("/simple_timing_glitch/", middle.Apply(func(w http.ResponseWriter, r *http.Request) {
		timingGlitchHandler(w, r, sim, gotoIndex)
	})).Methods("POST")
	mux.HandleFunc("/clock/stop/", middle.Apply(func(w http.ResponseWriter, r *http.Request) {
		clockStopHandler(w, r, clock, gotoIndex)
	})).Methods("POST")
	mux.HandleFunc("/clock/resume/", middle.Apply(func(w http.ResponseWriter, r *http.Request) {
		clockResumeHandler(w, r, clock, gotoIndex)
	})).Methods("POST")
	mux.HandleFunc("/clock/step/", middle.Apply(func(w http.ResponseWriter, r *http.Request) {
		clockStepHandler(w, r, clock, gotoIndex)
	})).Methods("POST")

	mux.HandleFunc("/api/v1/system/status/", middle.Apply(func(w http.ResponseWriter, r *http.Request) {
		systemStatusApiHandler(w, r, sim)
	})).Methods("GET")
	mux.HandleFunc("/api/v1/system/stop/", middle.Apply(func(w http.ResponseWriter, r *http.Request) {
		systemStopApiHandler(w, r, sim, srv)
	})).Methods("POST")
	mux.HandleFunc("/api/v1/dcu/(?P<dcuid>[0-9]+)/stop/", middle.Apply(func(w http.ResponseWriter, r *http.Request) {
		stopDcuApiHandler(w, r, sim, apiOk)
	})).Methods("POST")
	mux.HandleFunc("/api/v1/dcu/(?P<dcuid>[0-9]+)/start/", middle.Apply(func(w http.ResponseWriter, r *http.Request) {
		startDcuApiHandler(w, r, sim, apiOk)
	})).Methods("POST")
	mux.HandleFunc("/api/v1/dcu/(?P<dcuid>[0-9]+)/mutate/", middle.Apply(func(w http.ResponseWriter, r *http.Request) {
		dcuId, _ := getDcuId(r)
		mutateDcuApiHandler(w, r, sim, &dcuId, apiOk)
	})).Methods("POST")
	mux.HandleFunc("/api/v1/system/timing_glitch/", middle.Apply(func(w http.ResponseWriter, r *http.Request) {
		timingGlitchHandler(w, r, sim, apiOk)
	})).Methods("POST")

	err := srv.ListenAndServe()
	log.Print(err)
}
