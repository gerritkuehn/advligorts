package main

import (
	"flag"
	"fmt"
	"git.ligo.org/cds/advligorts/src/fe_stream_test/fe_simulation"
	"git.ligo.org/cds/advligorts/src/fe_stream_test/fe_simulation/gps"
	"log"
)

func parseArgs() fe_simulation.InitialOptions {
	iniPath := flag.String("i", "", "Path to the ini/par file folder to use (should be a full path)")
	masterPath := flag.String("M", "master", "Path to the master file (will be overwritten)")
	mbufName := flag.String("b", "local_dc", "Name of the output shared memory buffer")
	mbufSize := flag.Int("m", 100, "Size in MB of the output shared memory buffer")
	modelSize := flag.Int("k", 700, "Default data rate of each model in kB")
	modelCount := flag.Int("R", 0, "Number of models to simulate [1-247]")
	requestedDcus := flag.String("D", "", "Simulate the given dcus (dcu,dcu,dcu,...)")
	requestedTps := flag.String("t", "", "Enable the given testpoints on the given dcu (dcu:tp#,dcu:tp#,dcu:tp#,...)")
	requestedFailures := flag.String("f", "", "Fail the given dcu's, configs written, but not started (dcu,dcu,dcu,...)")
	individualMbufs := flag.Bool("S", false, "Set if each model should write its own mbuf rmIpc structs")
	shmemPrefix := flag.String("s", "mbuf://", "Set the shmem type mbuf:// or shm://")
	adminIface := flag.String("admin", "", "Set to the host:port to open an http interface on for admin")
	closeDaqOnStop := flag.Bool("close-daq-on-stop", false, "Close _daq buffers when a simulated model is stopped")
	useAWG := flag.Bool("a", false, "Use awgtpman to set testpoints and excitations")
	linkTPs := flag.String("x", "", "A pair of testpoints.  Data is copied from the first to the second with optional noise.\nNumbers are in sequence opened, not testpoints.\nSource must be lower than target. Only works with '-a'")
	noiseAmp := flag.Float64("n", 0, "Amplitude of uniform noise added to data copied with '-x' flag")

	flag.Parse()

	dcuList, err1 := ParseIntList(*requestedDcus)
	tpList, err2 := ParseTPList(*requestedTps)
	failList, err3 := ParseIntList(*requestedFailures)
	linkIntList, err4 := ParseIntList(*linkTPs)

	if err1 != nil || err2 != nil || err3 != nil || err4 != nil {
		log.Fatal("Improper list found in the dcu list, test point list, fail list or testpoint list.")
	}
	if *modelSize < 10 || *modelSize > 3900 {
		log.Fatalf("The model data rate must between [10, 3900]")
	}
	if *mbufSize < 10 || *mbufSize > 140 {
		log.Fatal("The mbuf size must be between between [40, 140]")
	}
	if *modelCount > 0 && len(dcuList) != 0 {
		log.Fatalf("Only specify the model count if an explicit dcu list is not given")
	}
	if *modelCount > 0 {
		dcuList = getNDcus(*modelCount)
	}
	if len(dcuList) == 0 {
		log.Fatal("No dcus identified, either give a concrete dcu list or a model count")
	}

	if len(linkIntList)%2 != 0 {
		log.Fatal("Linked testpoint list must be in pairs a1,a2,b1,b2...")
	}

	var linkTPList []fe_simulation.TPLink

	for a := 0; a < len(linkIntList); a = a + 2 {
		b := a + 1

		from := linkIntList[a]
		to := linkIntList[b]

		if to <= from {
			log.Fatal("'from' linked testpoint must be a lower number than 'to' linked testpoint")
		}

		if to >= 64 {
			log.Fatal("linked testpoints must indicate testpoint slots, not testpoint numbers.  Valid range is 0-63.")
		}

		linkTPList = append(linkTPList,
			fe_simulation.TPLink{
				From: from,
				To:   to,
			})
	}

	models := make([]fe_simulation.ModelParams, 0, len(dcuList))
	for _, dcuid := range dcuList {
		tps := make([]int, 0, 0)
		status := fe_simulation.ModelRunning

		for _, curTp := range tpList {
			if curTp.DCUId == dcuid {
				tps = append(tps, int(curTp.TP))
			}
		}
		for _, failedDcu := range failList {
			if failedDcu == dcuid {
				status = fe_simulation.ModelStopped
				break
			}
		}

		model := fe_simulation.ModelParams{
			Name:       fmt.Sprintf("mod%v", dcuid),
			ModelRate:  2048,
			DataRate:   *modelSize * 1024,
			TPList:     tps,
			Status:     status,
			DCUId:      dcuid,
			LinkTPList: linkTPList,
		}
		models = append(models, model)
	}

	opts := fe_simulation.InitialOptions{
		Models:         models,
		IniRoot:        *iniPath,
		MasterPath:     *masterPath,
		ShmemPrefix:    *shmemPrefix,
		MBufName:       *mbufName,
		MBufSizeMB:     *mbufSize,
		Concentrate:    !*individualMbufs,
		AdminIface:     *adminIface,
		CloseDaqOnStop: *closeDaqOnStop,
		UseAWG:         *useAWG,
		NoiseAmp:       *noiseAmp,
	}

	return opts
}

func main() {
	opts := parseArgs()

	//fmt.Println(opts)

	// 	fmt.Println("Press enter to continue...")
	// 	reader := bufio.NewReader(os.Stdin)
	// 	_, _ = reader.ReadString('\n')

	simLoop, err := fe_simulation.NewSimulationLoop(opts)
	if err != nil {
		log.Fatalf("Unable to create simulation loop %v", err)
	}
	clock, err := gps.NewClock()
	if err != nil {
		log.Fatalf("Unable to create clock, error %v", err)
	}
	defer clock.Close()
	if opts.AdminIface != "" {
		go runAdminInterface(simLoop, opts.AdminIface, clock)
	}
	err = simLoop.Run(clock)
	if err != nil {
		log.Printf("Error running loop: %v", err)
	}
}
