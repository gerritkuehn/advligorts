package fe_simulation

import (
	"fmt"
	"git.ligo.org/cds/advligorts/src/fe_stream_test/fe_simulation/ligo_crc"
	"io"
	"os"
	"strings"
)

type TestpointParEntry struct {
	dcuid      int
	host_name  string
	model_name string
}

type IniManager struct {
	Root   string
	Master string

	masterContents       []string
	testpointParContents []TestpointParEntry
}

func cleanName(name string) string {
	return strings.ReplaceAll(name, ":", "")
}

func NewIniManager(root, master string) IniManager {
	return IniManager{
		Root:                 root,
		Master:               master,
		masterContents:       make([]string, 0, 10),
		testpointParContents: make([]TestpointParEntry, 0, 10),
	}
}

func (m *IniManager) Add(modelName string, dcuId int, modelRate RateHertz, chans, testPoints []Generator) (uint32, error) {
	var crcVal uint32
	var err error
	crcVal, err = m.outputIniFiles(modelName, dcuId, modelRate, chans, testPoints)
	if err != nil {
		return 0, err
	}
	if err = m.addToMaster(modelName); err != nil {
		return 0, err
	}
	var hostname string
	hostname, err = os.Hostname()
	if err != nil {
		hostname = "localhost"
	}
	if err = m.addToTestpointPar(dcuId, modelName, hostname); err != nil {
		return 0, err
	}
	return crcVal, err
}

func (m *IniManager) generateIniName(modelName string) string {
	return fmt.Sprintf("%s/%s.ini", m.Root, modelName)
}

func (m *IniManager) generateParName(modelName string) string {
	return fmt.Sprintf("%s/tpchn_%s.par", m.Root, modelName)
}

func (m *IniManager) outputIniFiles(modelName string, dcuId int, modelRate RateHertz, chans, testPoints []Generator) (uint32, error) {
	cleanedName := cleanName(modelName)
	iniFname := m.generateIniName(cleanedName)
	parFname := m.generateParName(cleanedName)
	fIni, error := os.Create(iniFname)
	if error != nil {
		return 0, error
	}
	defer fIni.Close()

	crc := &ligo_crc.LigoCrc32{}
	iniWriter := io.MultiWriter(fIni, crc)

	fPar, error := os.Create(parFname)
	if error != nil {
		return 0, error
	}
	defer fPar.Close()
	fmt.Fprintf(iniWriter, "[default]\ngain=1.0\nacquire=3\ndcuid=%d\nifoid=0\n", dcuId)
	fmt.Fprintf(iniWriter, "datatype=2\ndatarate=%d\noffset=0\nslope=1.0\nunits=undef\n\n", modelRate.Rate)

	for _, generator := range chans {
		generator.OutputIniEntry(iniWriter)
	}
	for _, generator := range testPoints {
		generator.OutputParEntry(fPar)
	}
	return crc.Sum32(), nil
}

func (m *IniManager) addToMaster(modelName string) error {
	cleanedName := cleanName(modelName)

	iniFile := m.generateIniName(cleanedName)
	parFile := m.generateParName(cleanedName)

	iniExists := false
	parExists := false

	for _, entry := range m.masterContents {
		if entry == iniFile {
			iniExists = true
			break
		}
	}
	for _, entry := range m.masterContents {
		if entry == parFile {
			parExists = true
			break
		}
	}
	if !iniExists {
		m.masterContents = append(m.masterContents, m.generateIniName(cleanedName))
	}
	if !parExists {
		m.masterContents = append(m.masterContents, m.generateParName(cleanedName))
	}
	if !iniExists || !parExists {
		return m.rewriteMaster()
	}
	return nil
}

func (m *IniManager) rewriteMaster() error {
	tmpName := m.Master + ".tmp"

	writeFile := func() error {
		f, err := os.Create(tmpName)
		if err != nil {
			return err
		}
		defer f.Close()
		for _, entry := range m.masterContents {
			_, _ = f.Write([]byte(entry))
			_, _ = f.Write([]byte("\n"))
		}
		return nil
	}
	if err := writeFile(); err != nil {
		return err
	}
	return os.Rename(tmpName, m.Master)
}

func (m *IniManager) addToTestpointPar(dcuId int, modelName string, hostName string) error {
	m.testpointParContents = append(m.testpointParContents,
		TestpointParEntry{
			dcuid:      dcuId,
			model_name: modelName,
			host_name:  hostName,
		})
	return m.rewriteTestpointPar()
}

func (m *IniManager) rewriteTestpointPar() error {
	tpName := fmt.Sprintf("%s/testpoint.par", m.Root)
	tempName := fmt.Sprintf("%s.tmp", tpName)
	writeFile := func() error {
		f, err := os.Create(tempName)
		if err != nil {
			return err
		}
		for _, entry := range m.testpointParContents {
			f.Write([]byte(fmt.Sprintf("[M-node%d]\n", entry.dcuid)))
			f.Write([]byte(fmt.Sprintf("hostname=%s\n", entry.host_name)))
			f.Write([]byte(fmt.Sprintf("system=%s\n", entry.model_name)))
		}
		return nil
	}
	if err := writeFile(); err != nil {
		return err
	}
	return os.Rename(tempName, tpName)
}
