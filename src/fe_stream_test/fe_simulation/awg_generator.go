package fe_simulation

//#include <stdint.h>
//#include "shmem_all.h"
import "C"
import (
	"io"
	"math/rand"
	"unsafe"
)

// move data from the awg buffer for a model
type AwgGenerator struct {
	model *Model
	slot  int
	bchan *BChanGenerator
}

func (g *AwgGenerator) Name() string {
	return "AwgGenerator"
}

func (g *AwgGenerator) FullChannelName() string {
	return "Unknown"
}

func (g *AwgGenerator) BytesPerSec() RateBytes {
	return g.model.DataRate
}

func CalcPageIndex(slot, epoch int) int {
	return slot*C.AWG_EPOCHS_PER_SLOT + epoch%C.AWG_EPOCHS_PER_SLOT
}

func (g *AwgGenerator) Generate(gpsSec, gpsNano int, out uintptr) uintptr {
	rate := g.model.ModelRate.Rate / 16
	epoch := int(gpsNano / (1e9 / 16))
	page_index := CalcPageIndex(g.slot, epoch)
	out_data, end := int32Slice(out, rate)

	if g.model.AWG_data.page[page_index].status == 0 {
		awg_data, _ := int32Slice(uintptr(unsafe.Pointer(&g.model.AWG_data.page[page_index].buf)), rate)
		copy(out_data, awg_data)
	} else {
		for i := range out_data {
			out_data[i] = 0
		}
	}

	if g.bchan != nil {
		float_dat, _ := float32Slice(out, rate)
		g.bchan.Update(float_dat)
	}

	return end
}

func (g *AwgGenerator) OutputIniEntry(writer io.Writer) error {
	return nil
}

func (g *AwgGenerator) OutputParEntry(writer io.Writer) error {
	return nil
}

func (g *AwgGenerator) DataType() int {
	return dataTypeFloat32
}

func (g *AwgGenerator) DataRate() RateHertz {
	return g.model.ModelRate
}

func (g *AwgGenerator) SetBChanGenerator(bchan *BChanGenerator) {
	g.bchan = bchan
}

func NewAwgGenerator(model *Model, slot int, bchan *BChanGenerator) *AwgGenerator {
	return &AwgGenerator{
		model: model,
		slot:  slot,
		bchan: bchan,
	}
}

/// a Generator for testpoints that depend on the data from other test points
/// noiseAmp is amplitude of noise added to the generated output
/// motivating use was testing transfer functions in Diaggui,
/// transfer functions requiring A (input) and B (output) channels
/// that are not identical
type BChanGenerator struct {
	model      *Model
	next_chunk []C.float
	noiseAmp   float64
}

func (g *BChanGenerator) FullChannelName() string {
	return "Unknown"
}

func (g *BChanGenerator) BytesPerSec() RateBytes {
	return g.model.DataRate
}

func (g *BChanGenerator) Generate(gpsSec, gpsNano int, out uintptr) uintptr {
	rate := g.model.ModelRate.Rate / 16
	outData, end := float32Slice(out, rate)

	copy(outData, g.next_chunk)

	return end
}

func (g *BChanGenerator) OutputIniEntry(writer io.Writer) error {
	return nil
}

func (g *BChanGenerator) OutputParEntry(writer io.Writer) error {
	return nil
}

func (g *BChanGenerator) DataType() int {
	return dataTypeFloat32
}

func (g *BChanGenerator) DataRate() RateHertz {
	return g.model.ModelRate
}

func (g *BChanGenerator) Update(input []C.float) {
	g.next_chunk = make([]C.float, len(input), len(input))
	copy(g.next_chunk, input)
	for i, v := range g.next_chunk {
		g.next_chunk[i] = v + (C.float)((rand.Float64()-0.5)*g.noiseAmp*2)
	}
}

func (g *BChanGenerator) Name() string {
	return "BChanGenerator"
}

func NewBChanGenerator(m *Model, noiseAmp float64) BChanGenerator {
	bchan := BChanGenerator{
		model:    m,
		noiseAmp: noiseAmp,
	}
	return bchan
}
