package fe_simulation

// #include <stdint.h>
import "C"
import (
	"errors"
	"fmt"
	"io"
	"strconv"
	"strings"
)

const (
	dataTypeInt16     = 1
	dataTypeInt32     = 2
	dataTypeInt64     = 3
	dataTypeFloat32   = 4
	dataTypeFloat64   = 5
	dataTypeComplex32 = 6
	dataTypeUInt32    = 7
)

// Generator is the basic interface into a simulated stream
type Generator interface {
	Name() string
	FullChannelName() string
	BytesPerSec() RateBytes
	Generate(gpsSec, gpsNano int, out uintptr) uintptr

	OutputIniEntry(writer io.Writer) error
	OutputParEntry(writer io.Writer) error

	DataType() int
	DataRate() RateHertz
}

type BasicGenerator struct {
	GeneratorClassName string
	SignalBaseName     string
	SignalParams       string
	GeneratorDataType  int
	GeneratorDataRate  RateHertz
	DcuId              int
	ChNum              int
}

func (g *BasicGenerator) Name() string {
	return g.GeneratorClassName
}

func (g *BasicGenerator) FullChannelName() string {
	return fmt.Sprintf("%s--%s--%s--%d--%d", g.SignalBaseName, g.GeneratorClassName, g.SignalParams, g.GeneratorDataType, g.GeneratorDataRate.Rate)
}

func (g *BasicGenerator) DataType() int {
	return g.GeneratorDataType
}

func (g *BasicGenerator) DataRate() RateHertz {
	return g.GeneratorDataRate
}

func (g *BasicGenerator) BytesPerSec() RateBytes {
	return NewRateBytes(dataTypeSize(g.GeneratorDataType) * g.GeneratorDataRate.Rate)
}

func (g *BasicGenerator) OutputIniEntry(writer io.Writer) error {
	_, err := fmt.Fprintf(writer, "[%s]\ndatarate=%d\ndatatype=%d\nchnum=%d\n", g.FullChannelName(), g.GeneratorDataRate.Rate, g.GeneratorDataType, g.ChNum)
	return err
}

func (g *BasicGenerator) OutputParEntry(writer io.Writer) error {
	if g.GeneratorDataType != dataTypeFloat32 || g.GeneratorDataRate.Rate != 2048 {
		return errors.New("cannot generate a par entry for a non float or non 2k entry at this time")
	}
	_, err := fmt.Fprintf(writer, "[%s]\nifoid = 1\nrmid = %d\ndcuid = 16\nchnnum = %d\ndatatype = %d\ndatarate = %d\n",
		g.FullChannelName(),
		g.DcuId,
		g.ChNum,
		g.GeneratorDataType,
		g.GeneratorDataRate.Rate)
	return err
}

func dataTypeSize(dataType int) int {
	switch {
	case dataType == dataTypeInt16:
		return 2
	case dataType == dataTypeInt32 || dataType == dataTypeFloat32 || dataType == dataTypeUInt32:
		return 4
	case dataType == dataTypeFloat64 || dataType == dataTypeInt64 || dataType == dataTypeComplex32:
		return 8
	}
	return 0
}

type BasicGeneratorParams struct {
	BaseName string
	DcuId    int
	DataType int
	DataRate RateHertz
	ChNum    int
}

type GPSSecondWithOffset struct {
	BasicGenerator
	offset int
}

func NewGPSSecondWithOffset(params BasicGeneratorParams, offset int) *GPSSecondWithOffset {
	if params.DataType != dataTypeInt32 {
		panic("Only int32 is supported for GPSSecondWithOffset")
	}
	return &GPSSecondWithOffset{
		BasicGenerator: BasicGenerator{
			GeneratorClassName: "gpssoff1p",
			SignalBaseName:     params.BaseName,
			SignalParams:       fmt.Sprintf("%d", offset),
			GeneratorDataType:  params.DataType,
			GeneratorDataRate:  params.DataRate,
			ChNum:              params.ChNum,
			DcuId:              params.DcuId,
		},
		offset: offset,
	}
}

func (g *GPSSecondWithOffset) Generate(gpsSec, gpsNano int, out uintptr) uintptr {
	rate := g.GeneratorDataRate.Rate / 16
	switch g.GeneratorDataType {
	case dataTypeInt32:
		{
			data, end := int32Slice(out, rate)
			offset := g.offset
			for i := 0; i < rate; i++ {
				data[i] = C.int32_t(gpsSec + offset)
			}
			return end
		}
	default:
		panic("operation not defined for given data type")
	}
}

type GPSMod100kSecWithOffsetAndCycle struct {
	BasicGenerator
	offset int
}

func NewGPSMod100kSecWithOffsetAndCycle(params BasicGeneratorParams, offset int) *GPSMod100kSecWithOffsetAndCycle {
	switch params.DataType {
	case dataTypeInt16:
		break
	case dataTypeInt32:
		break
	case dataTypeUInt32:
		break
	case dataTypeFloat32:
		break
	default:
		panic("GPSMod100kSecWithOffsetAndCycle used with unsupported type")
	}
	return &GPSMod100kSecWithOffsetAndCycle{
		BasicGenerator: BasicGenerator{
			GeneratorClassName: "gpssmd100koffc1p",
			SignalBaseName:     params.BaseName,
			SignalParams:       fmt.Sprintf("%d", offset),
			GeneratorDataType:  params.DataType,
			GeneratorDataRate:  params.DataRate,
			ChNum:              params.ChNum,
			DcuId:              params.DcuId,
		},
		offset: offset,
	}
}

func (g *GPSMod100kSecWithOffsetAndCycle) Generate(gpsSec, gpsNano int, out uintptr) uintptr {
	cycle := gpsNano
	if gpsNano > 16 {
	    cycle = gpsNano / 62500000
	}
	rate := g.GeneratorDataRate.Rate / 16
	switch g.GeneratorDataType {
	case dataTypeInt16:
		{
			data, end := int16Slice(out, rate)
			offset := g.offset
			for i := 0; i < rate; i++ {
				data[i] = C.int16_t(((gpsSec%100000)+offset)*100 + cycle)
			}
			return end
		}
	case dataTypeInt32:
		{
			data, end := int32Slice(out, rate)
			offset := g.offset
			for i := 0; i < rate; i++ {
				data[i] = C.int32_t(((gpsSec%100000)+offset)*100 + cycle)
			}
			return end
		}
	case dataTypeUInt32:
		{
			data, end := uint32Slice(out, rate)
			offset := g.offset
			for i := 0; i < rate; i++ {
				data[i] = C.uint32_t(((gpsSec%100000)+offset)*100 + cycle)
			}
			return end
		}
	case dataTypeFloat32:
		{
			data, end := float32Slice(out, rate)
			offset := g.offset
			for i := 0; i < rate; i++ {
				data[i] = C.float(((gpsSec%100000)+offset)*100 + cycle)
			}
			return end
		}
	default:
		panic("operation not defined for given data type")
	}
}

type StaticGenerator struct {
	BasicGenerator
	value float64
}

func NewStaticGenerator(params BasicGeneratorParams, value float64) *StaticGenerator {
	if params.DataType != dataTypeFloat32 {
		panic("StaticGenerator is only supported for float32")
	}
	return &StaticGenerator{
		BasicGenerator: BasicGenerator{
			GeneratorClassName: "static",
			SignalBaseName:     params.BaseName,
			SignalParams:       fmt.Sprintf("%v", value),
			GeneratorDataType:  params.DataType,
			GeneratorDataRate:  params.DataRate,
			DcuId:              params.DcuId,
			ChNum:              params.ChNum,
		},
		value: value,
	}
}

func (g *StaticGenerator) Generate(gpsSec, gpsNano int, out uintptr) uintptr {
	rate := g.GeneratorDataRate.Rate / 16
	switch g.GeneratorDataType {
	case dataTypeFloat32:
		{
			data, end := float32Slice(out, rate)
			val := C.float(g.value)
			for i := 0; i < rate; i++ {
				data[i] = val
			}
			return end
		}
	default:
		panic("operation not defined for given data type")
	}
}

func NewGenerator(name string, dcuId, chId int) (g Generator, err error) {
	defer func() {
		if x := recover(); x != nil {
			g = nil
			err = errors.New("unable to create generator")
		}
	}()
	g, err = newGenerator(name, dcuId, chId)
	return
}

func newGenerator(name string, dcuId, chId int) (Generator, error) {
	invalid := errors.New("invalid generator string")
	parts := strings.Split(name, "--")
	if len(parts) < 4 {
		return nil, invalid
	}
	intParam := func(paramIndex int) int {
		val, err := strconv.ParseInt(parts[2+paramIndex], 10, 32)
		if err != nil {
			panic("param not an int")
		}
		return int(val)
	}
	floatParam := func(paramIndex int) float64 {
		val, err := strconv.ParseFloat(parts[2+paramIndex], 32)
		if err != nil {
			panic("param not an float")
		}
		return val
	}

	baseName := parts[0]
	generatorName := parts[1]
	dataType, err := strconv.ParseInt(parts[len(parts)-2], 10, 32)
	if err != nil {
		return nil, invalid
	}
	dataRate, err := strconv.ParseInt(parts[len(parts)-1], 10, 32)
	if err != nil {
		return nil, invalid
	}

	basicInfo := BasicGeneratorParams{
		BaseName: baseName,
		DcuId:    dcuId,
		DataType: int(dataType),
		DataRate: NewRateHertz(int(dataRate)),
		ChNum:    chId,
	}

	switch generatorName {
	case "gpssoff1p":
		return NewGPSSecondWithOffset(basicInfo, intParam(0)), nil
	case "gpssmd100koffc1p":
		return NewGPSMod100kSecWithOffsetAndCycle(basicInfo, intParam(0)), nil
	case "static":
		return NewStaticGenerator(basicInfo, floatParam(0)), nil
	}
	return nil, invalid
}
