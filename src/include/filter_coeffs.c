// Up/Down Sampling Filter Coeffs
// All systems not running at 64K require up/down sampling to communicate I/O
// data with IOP, which is running at 64K. Following defines the filter coeffs
// for these up/down filters.

/* Recalculated filters in biquad form */

/* Oversamping base rate is 64K */


/* Coeffs for the 256x downsampling filter (256 Hz system)
 * from feCoeff2k with zero/pole freqs rescaled by 1/8 */
double feCoeff256x[ 9 ] = { 0.011167217991503314,
    0.9882793385026947, -0.0001031629897536579, - 0.01103266153096305,  0.0004991756529770797,
    0.9912887317122534, -0.00015706406233884085, -0.006131125346906963, 0.002251815161263604
};


/* Coeffs for the 2x downsampling (32K system) filter */
const double feCoeff2x[ 9 ] = { 0.053628649721183,
                                0.2568759660371100, -0.3225906481359000, 1.2568801238621801,  1.6774135096891700,
                               -0.2061764045745400, -1.0941543149527400, 2.0846376586498803, 2.1966597482716801
};

/*
 * DCC docs of note
 * T1600066 - New RCG 3.0 Decimation (IOP Upsampling / Downsampling) Filter for 16 kHz models
 * T1500075 - A Note on the RCG Decimation (IOP Upsampling / Downsampling) Filters (Prior to RCG 3.0)
 */

/* RCG V3.0 Coeffs for the 4x downsampling (16K system) filter */
const double feCoeff4x[ 9 ] = { 0.054285975,
                                0.3890221, -0.17645085, -0.0417771600000001, 0.41775916,
                                0.52191125, -0.37884382, 1.52190741336686, 1.69347541336686 };


/*
 * Filter design link: https://dcc.ligo.org/LIGO-G2202011
 * These taps are used in decimation filters for 524k ADC date down to 65K
 * and in demodulation parts.
 *
 */
const double feCoeff8x[ 13 ] = { 0.0107429,
                                 0.671270, -0.081200, -0.166670, 0.070231,
                                 0.815860, -0.099388, 0.413760, 0.481383,
                                 -0.234144, -0.234144, 1.765856, 1.765856};

//
// New Brian Lantz 4k decimation filter
const double feCoeff16x[ 9 ] = { 0.010203728365,
                                 0.8052941009065100, -0.0241751519071000, 0.3920490703701900,  0.5612099784288400,
                                 0.8339678987936501, -0.0376022631287799, -0.0131581721533700, 0.1145865116421301
};


/* Coeffs for the 32x downsampling filter (2K system) per Brian Lantz May 5,
 * 2009 */
const double feCoeff32x[ 9 ] = { 0.010581064947739,
                                 0.90444302586137004,  -0.0063413204375699639, -0.056459743474659874, 0.032075154877300172,
                                 0.92390910024681006, -0.0097523655540199261, 0.077383808424050127, 0.14238741130302013
};



