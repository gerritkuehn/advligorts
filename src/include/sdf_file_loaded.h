#ifndef DAQD_TRUNK_SDF_FILE_LOADED_H
#define DAQD_TRUNK_SDF_FILE_LOADED_H

// set or clear and sdf file loaded flag
// this flag is used to trigger BURT_RESTORE
// when the safe.snap or other sdf file is completely loaded

#ifdef __cplusplus
extern "C" {
#endif

extern int  get_sdf_file_loaded( void );
extern void set_sdf_file_loaded( int );

#ifdef __cplusplus
}
#endif


#endif // DAQD_TRUNK_SDF_FILE_LOADED_H
