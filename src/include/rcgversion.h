#ifndef LIGO_RCG_VERSION_H
#define LIGO_RCG_VERSION_H

#define RCG_VERSION_MAJOR 5
#define RCG_VERSION_MINOR 2
#define RCG_VERSION_SUB 1

// Set to 1 for release version, 0 for development version
// When 0, causes version number to be displayed as negative on MEDM screens
#define RCG_VERSION_REL 0

#endif //LIGO_RCG_VERSION_H
