//
// Prototypes for functions in seq_main
//


#ifndef ADVLIGORTS_SEQUENCER_H
#define ADVLIGORTS_SEQUENCER_H

// write a message to site/ifo/logs/<sys>/ioc.log
void logFileEntry(char *message);
void logFileEntryFmt(char *format, ... );

#endif // ADVLIGORTS_SEQUENCER_H
