#include "drv/iop_adc_functions.h"


#include "drv/gsc16ai64.h"
#include "drv/gsc18ai32.h"
#include "drv/gsc18ai64.h"
#include "drv/gsc_adc_common.h"
#include "drv/symmetricomGps.h"
#include "drv/spectracomGPS.h"
#include "drv/ligoPcieTiming.h"
#include "drv/gsc18ao8.h"
#include "drv/plx_9056.h"
#include "fm10Gen.h"//iir_filter_biquad()
#include "controller.h" //cdsPciModules, etc
#include "rts-logger.h"

#ifdef XMIT_DOLPHIN_TIME
#include "../fe/controllerIop.h" //pcieTimer
#endif

#ifdef XMIT_DOLPHIN_TIME
#include <asm/cacheflush.h>
#endif

//
// Global Functions
//
static int first_adc_read = 1;


// Routine for initializing ADC modules on startup
int
iop_adc_init( adcInfo_t* adcinfo )
{
    volatile int* adcDummyData;
    int           ii, jj;
    int           dma_bytes = 32;
    int           card = 0;
    int samples;

    // Determine order to read ADC cards
    // 18bit ADC modules deliver their data 1/2 cycle later
    // than 16bit, so want to wait on their data first.
    for ( jj = 0; jj < cdsPciModules.adcCount; jj++ )
    {
        if ( cdsPciModules.adcType[ jj ] != GSC_18AI32SSC1M &&
             cdsPciModules.adcType[ jj ] != GSC_18AI64SSC )
        {
            adcinfo->adcRdOrder[ card ] = jj;
            card++;
        }
    }
    for ( jj = 0; jj < cdsPciModules.adcCount; jj++ )
    {
        if ( cdsPciModules.adcType[ jj ] == GSC_18AI32SSC1M ||
             cdsPciModules.adcType[ jj ] == GSC_18AI64SSC )
        {
            adcinfo->adcRdOrder[ card ] = jj;
            card++;
        }
    }
    /// \> If IOP,  Initialize the ADC modules
    for ( jj = 0; jj < cdsPciModules.adcCount; jj++ )
    {

        // Preload input memory with dummy variables to test that new ADC data
        // has arrived.
        // Write a dummy 0 to first ADC channel location
        // This location should never be zero when the ADC writes data as it
        // should always have an upper bit set indicating channel 0.
        // Write a number into the last channel which the ADC should never write
        // ie no upper bits should be set in channel 31.
        adcDummyData = cdsPciModules.pci_adc[ jj ];
        switch ( cdsPciModules.adcType[ jj ] )
        {
        case GSC_18AI32SSC1M:
        case GSC_18AI64SSC:
            if(cdsPciModules.adcTimeShift[ jj ] < UNDERSAMPLE)
            {
                samples = cdsPciModules.adcChannels[ jj ]
                    * (UNDERSAMPLE-cdsPciModules.adcTimeShift[jj]);
            }
            else
            {
                samples = cdsPciModules.adcChannels[ jj ] * UNDERSAMPLE;
            }
            dma_bytes = samples * 4;
            plx9056_adc_dma_setup( jj, dma_bytes );
            *adcDummyData = 0x0;
            adcDummyData += samples - 1 ;
            *adcDummyData = DUMMY_ADC_VAL;
            break;
        default:
            dma_bytes = GSAI_DMA_BYTE_COUNT;
            plx9056_adc_dma_setup( jj, dma_bytes );
            *adcDummyData = 0x0;
            adcDummyData += GSAI_64_OFFSET;
            *adcDummyData = DUMMY_ADC_VAL;
            break;
        }

        // Set ADC Present Flag
        pLocalEpics->epicsOutput.statAdc[ jj ] = ADC_MAPPED;
        // Set ADC AutoCal Pass/Fail Flag
        if ( ( cdsPciModules.adcConfig[ jj ] & GSAI_AUTO_CAL_PASS ) != 0 )
            pLocalEpics->epicsOutput.statAdc[ jj ] |= ADC_CAL_PASS;
        // Reset Diag Info
        adcinfo->adcRdTimeErr[ jj ] = 0;
        adcinfo->adcChanErr[ jj ] = 0;
        adcinfo->adcOF[ jj ] = 0;
        for ( ii = 0; ii < MAX_ADC_CHN_PER_MOD; ii++ )
            adcinfo->overflowAdc[ jj ][ ii ] = 0;
    }
    adcinfo->adcHoldTime = 0;
    adcinfo->adcHoldTimeMax = 0;
    adcinfo->adcHoldTimeEverMax = 0;
    adcinfo->adcHoldTimeEverMaxWhen = 0;
    adcinfo->adcHoldTimeMin = 0xffff;
    adcinfo->adcHoldTimeAvg = 0;
    adcinfo->adcWait = 0;
    adcinfo->chanHop = 0;

    return 0;
}


// ADC Read *****************************************************************
// Routine for reading data from ADC modules.
int
iop_adc_read( adcInfo_t* adcinfo, unsigned long long cpuClk[] )
{
    int           ii, jj, kk;
    volatile int* packedData;
    int           limit;
    int           mask;
    unsigned int  offset;
    int           num_outs;
    int           ioMemCntr = 0;
    int           card;
    int           chan;
    int           max_loops = UNDERSAMPLE;  //number of samples typically processed from an A2D per cycle.
    int           loops = max_loops;     //can be less than max_loops on the first cycle when time_shift parameter is used.
    int           iocycle = 0;
    int           first_chan_marker = 0;
    unsigned long long           adc_wait_limit = 0;
    double        val2dec = 0;
    static double iopdHistory[ ( MAX_ADC_MODULES * MAX_ADC_CHN_PER_MOD ) ]
                             [ MAX_HISTRY ];

    // On startup, may take up to 1.25 seconds before clocks start
    // while waiting for 1PPS sync marker, so wait longer for
    // ADC data to arriver.
    if ( adcinfo->startup_sync )
        adc_wait_limit = MAX_ADC_WAIT;
    else
        adc_wait_limit = MAX_ADC_WAIT_CARD_0;

    // Read ADC data
    for ( jj = 0; jj < cdsPciModules.adcCount; jj++ )
    {
        card = adcinfo->adcRdOrder[ jj ];



        /// - ---- ADC DATA RDY is detected when last channel in memory no
        /// longer contains the dummy variable written during initialization and
        /// reset after each read.
        packedData = cdsPciModules.pci_adc[ card ];

        /// Where last channel is is dependent on adcDummyDatanumber of channels a
        /// particular card type has.
        switch ( cdsPciModules.adcType[ card ] )
        {
        case GSC_18AI32SSC1M:
            if( first_adc_read && cdsPciModules.adcTimeShift[card] == UNDERSAMPLE )
            {
                // we'll re-use the first DMA result next IOP cycle.
                continue;
            }
            if(first_adc_read)
            {
                packedData +=
                    ( cdsPciModules.adcChannels[ card ] * (UNDERSAMPLE
                                - cdsPciModules.adcTimeShift[card]) - 1 );
            }
            else
            {
                packedData +=
                    ( cdsPciModules.adcChannels[ card ] * UNDERSAMPLE - 1 );
            }
            first_chan_marker = GSAF_FIRST_SAMPLE_MARK;
            break;
        case GSC_18AI64SSC:
            if( first_adc_read && cdsPciModules.adcTimeShift[card] == UNDERSAMPLE )
            {
                // we'll re-use the first DMA result next IOP cycle.
                continue;
            }
            if(first_adc_read)
            {
                packedData +=
                    ( cdsPciModules.adcChannels[ card ] *( UNDERSAMPLE
                      - cdsPciModules.adcTimeShift[card] )- 1 );
            }
            else
            {
                packedData +=
                    ( cdsPciModules.adcChannels[ card ] * UNDERSAMPLE - 1 );
            }
            first_chan_marker = GSA7_FIRST_SAMPLE_MARK;
            break;
        default:
            if(first_adc_read && cdsPciModules.adcTimeShift[card] == 1)
            {
                continue;
            }
            packedData += GSAI_64_OFFSET;
            first_chan_marker = GSAI_FIRST_SAMPLE_MARK;
            break;
        }

        // Capture CPU clock cycle for timing diags
        cpuClk[ CPU_TIME_RDY_ADC ] = rdtsc_ordered( );
        // Wait for ADC data to arrive via DMA
        do
        {
            /// - ---- Need to delay if not ready as constant banging of the
            /// input register will slow down the ADC DMA.
            cpuClk[ CPU_TIME_ADC_WAIT ] = rdtsc_ordered( );
            adcinfo->adcWait =
                ( cpuClk[ CPU_TIME_ADC_WAIT ] - cpuClk[ CPU_TIME_RDY_ADC ] ) /
                CPURATE;
        } while ( ( *packedData == DUMMY_ADC_VAL ) &&
                  ( adcinfo->adcWait < adc_wait_limit ) );


        /// - ---- Added ADC timing diagnostics to verify timing consistent and
        /// all rdy together.
        if ( jj == 0 )
        {
            adcinfo->adcRdTime[ card ] = ( cpuClk[ CPU_TIME_ADC_WAIT ] -
                                           cpuClk[ CPU_TIME_CYCLE_START ] ) /
                CPURATE;
#ifdef XMIT_DOLPHIN_TIME
            // Send time on Dolphin net if this is the time xmitter.
            if ( pLocalEpics->epicsOutput.disableRemoteIpc == 0) {
                pcieTimer->gps_time = timeSec;
                pcieTimer->cycle = cycleNum/UNDERSAMPLE;
                clflush_cache_range( (void*)&pcieTimer->gps_time, 16 );
            }
#endif
        }
        else
        {
            adcinfo->adcRdTime[ card ] = adcinfo->adcWait;
        }

        if ( adcinfo->adcRdTime[ card ] > adcinfo->adcRdTimeMax[ card ] )
            adcinfo->adcRdTimeMax[ card ] = adcinfo->adcRdTime[ card ];

        if ( ( jj == 0 ) &&
             ( adcinfo->adcRdTimeMax[ card ] >
               ( MAX_ADC_WAIT_CARD_0 * UNDERSAMPLE ) ) )
            adcinfo->adcRdTimeErr[ card ]++;

        if ( ( jj != 0 ) &&
             ( adcinfo->adcRdTimeMax[ card ] > MAX_ADC_WAIT_CARD_S ) )
            adcinfo->adcRdTimeErr[ card ]++;

        /// - --------- If data not ready in time, abort.
        /// Either the clock is missing or code is running too slow and ADC FIFO
        /// is overflowing.
        if ( adcinfo->adcWait >= adc_wait_limit )
        {
            pLocalEpics->epicsOutput.statAdc[ card ] &= ~( ADC_RD_TIME );
            fe_status_return = ADC_TO_ERROR;
            fe_status_return_subcode = card;
            return ( ADC_TO_ERROR );
        }


        if ( jj == 0 ) // Capture timing info
        {
            // Capture start of cycle time for cpu meter diagnostics
            cpuClk[ CPU_TIME_CYCLE_START ] = rdtsc_ordered( );
            /// \> If first cycle of a new second, capture time from IRIG-B
            /// module or LIGO PCIe timing receiver.
            if ( cycleNum == 0 )
            {
                // if SymCom type, just do write to lock current time and read
                // later This save a couple three microseconds here
                switch ( cdsPciModules.gpsType )
                {
                case SYMCOM_RCVR:
                    lockGpsTime( &cdsPciModules );
                    break;
                case TSYNC_RCVR:
                    /// - ---- Reading second info will lock the time register,
                    /// allowing nanoseconds to be read later (on next cycle).
                    /// Two step process used to save CPU time here, as each
                    /// read can take 2usec or more.
                    timeSec = getGpsSecTsync( &cdsPciModules );
                    break;
                case LIGO_RCVR:
                    // Read GPS second from LIGO Pcie timing card.
                    timeSec = lptc_get_gps_sec( &cdsPciModules );
                    break;
                default:
                    break;
                }
            }
        }

        /// \> Read adc data
        // Set pointer to first channel
        packedData = cdsPciModules.pci_adc[ card ];
        /// - ---- First, and only first, channel should have upper bit marker
        /// set. If not, have a channel hopping error.
        if ( !( *packedData & first_chan_marker ) )
        {
            adcinfo->chanHop = 1;
            adcinfo->adcChanErr[ card ] = 1;
            fe_status_return = CHAN_HOP_ERROR;
            fe_status_return_subcode = card;
            return CHAN_HOP_ERROR;
        }

        // Various ADC models have different number of channels/data bits
        if ( cdsPciModules.adcType[ card ] == GSC_16AI64SSA )
        {
            limit = OVERFLOW_LIMIT_16BIT;
            offset = GSAI_DATA_CODE_OFFSET;
            mask = GSAI_DATA_MASK;
        }
        else
        {
            limit = OVERFLOW_LIMIT_18BIT;
            offset = GSA7_DATA_CODE_OFFSET;
            mask = GSA7_DATA_MASK;
        }
        num_outs = cdsPciModules.adcChannels[ card ];

        // loops and max_loops are usually equal to UNDERSAMPLE, but on the first cycle
        // they loops may be less.
        max_loops = UNDERSAMPLE;
        if(first_adc_read)
        {
            loops = UNDERSAMPLE - cdsPciModules.adcTimeShift[card];
        }
        else
        {
            loops = UNDERSAMPLE;
        }

        // GSC_16AI64SSA can't support rate > 128KS/sec
        // Even if IOP rate is faster, read these ADCs at 65kHz.
        if ( cdsPciModules.adcType[ card ] == GSC_16AI64SSA && UNDERSAMPLE > 4 )
        {
            max_loops = 1;
            loops = 1;
        }

        /// - ---- Determine next ipc memory location to load ADC data
        ioMemCntr = ( (int)( cycleNum / ADC_MEMCPY_RATE ) % IO_MEMORY_SLOTS );
        iocycle = (int)( cycleNum / ADC_MEMCPY_RATE );


        /// - ----  Read adc data from PCI mapped memory into local variables
        for ( kk = 0; kk < loops; kk++ )
        {
            for ( chan = 0; chan < num_outs; chan++ )
            {
                // adcData is the integer representation of the ADC data
                adcinfo->adcData[ card ][ chan ] = ( *packedData & mask );
                adcinfo->adcData[ card ][ chan ] -= offset;
#ifdef DEC_TEST
                // This only used on test system for checking decimation filters
                // by providing an ADC signal via a GDS EXC signal
                if ( chan == 0 )
                {
                    adcinfo->adcData[ card ][ chan ] =
                        dspPtr[ 0 ]->data[ 0 ].exciteInput;
                }
#endif
                // dWord is the double representation of the ADC data
                // This is the value used by the rest of the code calculations.
                dWord[ card ][ chan ][ kk ] = adcinfo->adcData[ card ][ chan ];
                // If running with fast ADC at 512K and gsc16ai64 at 64K, then:
                // fill in the missing data by doing data copy
                if ( cdsPciModules.adcType[ card ] == GSC_16AI64SSA &&
                     UNDERSAMPLE > 4 )
                {
                    for ( ii = 1; ii < UNDERSAMPLE; ii++ )
                        dWord[ card ][ chan ][ ii ] =
                            adcinfo->adcData[ card ][ chan ];
                }
                /// - ----  Load ADC value into ipc shared memory buffer
                if ( max_loops == 8 )
                {
                    val2dec = adcinfo->adcData[ card ][ chan ];
                    ioMemData->iodata[ card ][ ioMemCntr ].data[ chan ] =
                        (int)iir_filter_biquad(
                            val2dec,
                            feCoeff8x,
                            3,
                            &iopdHistory[ chan + card * MAX_ADC_CHN_PER_MOD ]
                                        [ 0 ] );
                }
                else
                {
                    ioMemData->iodata[ card ][ ioMemCntr ].data[ chan ] =
                        adcinfo->adcData[ card ][ chan ];
                }
                /// - ---- Check for ADC overflows
                if ( ( adcinfo->adcData[ card ][ chan ] > limit ) ||
                     ( adcinfo->adcData[ card ][ chan ] < -limit ) )
                {
                    adcinfo->overflowAdc[ card ][ chan ]++;
                    pLocalEpics->epicsOutput.overflowAdcAcc[ card ][ chan ]++;
                    overflowAcc++;
                    adcinfo->adcOF[ card ] = 1;
                    odcStateWord |= ODC_ADC_OVF;
                }
                packedData++;
            }

            // For normal IOP ADC undersampling ie not a mixed fast 512K adc and
            // normal 64K adc
            if ( UNDERSAMPLE < 5 )
            {
                /// - ---- Write GPS time and cycle count as indicator to
                /// control app that adc data is ready
                ioMemData->gpsSecond = timeSec;
                ioMemData->iodata[ card ][ ioMemCntr ].timeSec = timeSec;
                ioMemData->iodata[ card ][ ioMemCntr ].cycle = iocycle;
                ioMemCntr++;
                iocycle++;
                iocycle %= IOP_IO_RATE;
            }
        }

        // For ADC undersampling when running with a fast ADC at 512K
        // to limit rate to user apps at 64K
        if ( UNDERSAMPLE > 4 )
        {
            /// - ---- Write GPS time and cycle count as indicator to control
            /// app that adc data is ready
            ioMemData->gpsSecond = timeSec;
            ioMemData->iodata[ card ][ ioMemCntr ].timeSec = timeSec;
            ioMemData->iodata[ card ][ ioMemCntr ].cycle = iocycle;
        }

        /// - ---- Clear out 1st channel from last ADC data read for test on
        /// next cycle
        /// - ---- First channel should never be zero ie should have channel
        /// marker bit
        packedData = cdsPciModules.pci_adc[ card ];
        *packedData = 0x0;


        // Enable the ADC Demand DMA for next read
        switch ( cdsPciModules.adcType[ card ] )
        {
        case GSC_18AI32SSC1M:
        case GSC_18AI64SSC:
            packedData +=
                ( cdsPciModules.adcChannels[ card ] * UNDERSAMPLE - 1 );
            *packedData = DUMMY_ADC_VAL;
            if ( first_adc_read )
            {
                plx9056_adc_dma_set_size(card,
                          cdsPciModules.adcChannels[ card ] * UNDERSAMPLE * 4 );
            }
            plx9056_adc_dma_start( card );
            break;
        default:
            packedData += GSAI_64_OFFSET;
            *packedData = DUMMY_ADC_VAL;
            plx9056_adc_dma_start( card );
            break;
        }

#ifdef DIAG_TEST
        // For DIAGS ONLY !!!!!!!!
        // This will change ADC DMA BYTE count
        // -- Greater than normal will result in channel hopping.
        // -- Less than normal will result in ADC timeout.
        // In both cases, real-time kernel code should exit with errors to dmesg
        if ( pLocalEpics->epicsInput.bumpAdcRd != 0 )
        {
            gsc16ai64DmaBump( card, pLocalEpics->epicsInput.bumpAdcRd );
            pLocalEpics->epicsInput.bumpAdcRd = 0;
        }
#endif
        adc_wait_limit = MAX_ADC_WAIT_CARD_0;
    }
    first_adc_read = 0;
    return 0;
}
