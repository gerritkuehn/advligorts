#include "drv/iop_dac_functions.h" 
#include "drv/gsc_adc_common.h"
#include "gsc_dac_common.h" //_dacPtr
#include "drv/gsc20ao8.h" //GSAO_20BIT_PRELOAD
#include "drv/gsc16ao16.h" //GSAO_16BIT_PRELOAD
#include "drv/gsc18ao8.h" //GSAO_18BIT_PRELOAD
#include "controller.h"
#include "drv/plx_9056.h"

#include "../fe/controllerIop.h"//dacWriteEnable

int
iop_dac_init( int errorPend[] )
{
    int ii, jj;
    int status;

    /// \> Zero out DAC outputs
    for ( ii = 0; ii < MAX_DAC_MODULES; ii++ )
    {
        errorPend[ ii ] = 0;
        for ( jj = 0; jj < 16; jj++ )
        {
            dacOut[ ii ][ jj ] = 0.0;
            dacOutUsed[ ii ][ jj ] = 0;
            dacOutBufSize[ ii ] = 0;
            // Zero out DAC channel map in the shared memory
            // to be used to check on control apps' channel allocation
            ioMemData->dacOutUsed[ ii ][ jj ] = 0;
        }
    }

    for ( jj = 0; jj < cdsPciModules.dacCount; jj++ )
    {
        pLocalEpics->epicsOutput.statDac[ jj ] = DAC_FOUND_BIT;
        status = cdsPciModules.dacAcr[ jj ] & DAC_CAL_PASS;
        if(status)  pLocalEpics->epicsOutput.statDac[ jj ] |= DAC_AUTOCAL_BIT;
        // Arm DAC DMA for full data size
        if ( cdsPciModules.dacType[ jj ] == GSC_16AO16 )
        {
            plx9056_dac_16_dma_setup( jj );
        }
        else
        {
            plx9056_dac_1820_dma_setup( jj );
        }
    }

    return 0;
}

void
iop_dac_preload( int card )
{
    int fc = 0;
    int lc = 0;
    int                         ii, jj;

     if ( card == GSAI_ALL_CARDS )
    {
        fc = 0;
        lc = cdsPciModules.dacCount;
    }
    else
    {
        fc = card;
        lc = card + 1;
    }

    for ( jj = fc; jj < lc; jj++ )
    {
        if ( cdsPciModules.dacType[ jj ] == GSC_18AO8 )
        {
            for ( ii = 0; ii < GSAO_18BIT_PRELOAD; ii++ )
                _dacPtr[ jj ]->OUTPUT_BUF = 0;
        }
        else if ( cdsPciModules.dacType[ jj ] == GSC_20AO8 )
        {
            for ( ii = 0; ii < GSAO_20BIT_PRELOAD; ii++ )
                _dacPtr[ jj ]->OUTPUT_BUF = 0;
        }
        else
        {
            for ( ii = 0; ii < GSAO_16BIT_PRELOAD; ii++ )
                _dacPtr[ jj ]->ODB = 0;
        }
    }
}

int
iop_dac_write(  )
{
    volatile unsigned int* pDacData;
    int           mm;
    int           limit;
    int           mask;
    int           num_outs;
    int           status = 0;
    int           card = 0;
    int           chan = 0;
    int           dac_out = 0;

    /// START OF IOP DAC WRITE ***************************************** \n
    /// \> If DAC FIFO error, always output zero to DAC modules. \n
    /// - -- Code will require restart to clear.
    // COMMENT OUT NEX LINE FOR TEST STAND w/bad DAC cards.
    /// \> Loop thru all DAC modules
    if ( dacWriteEnable > DAC_START_CYCLE )
    {
        for ( card = 0; card < cdsPciModules.dacCount; card++ )
        {
            /// - -- Point to DAC memory buffer
            pDacData = (volatile unsigned int*)( cdsPciModules.pci_dac[ card ] );
            /// - -- locate the proper DAC memory block
            mm = cdsPciModules.dacConfig[ card ];
            /// - -- Determine if memory block has been set with the correct
            /// cycle count by control app.
            if ( ioMemData->iodata[ mm ][ ioMemCntrDac ].cycle == ioClockDac )
            {
                dacEnable |= pBits[ card ];
            }
            else
            {
                dacEnable &= ~( pBits[ card ] );
                dacChanErr[ card ] += 1;
            }
            /// - -- Set overflow limits, data mask, and chan count based on DAC
            /// type
            limit = OVERFLOW_LIMIT_16BIT;
            mask = GSAO_16BIT_MASK;
            num_outs = GSAO_16BIT_CHAN_COUNT;
            if ( cdsPciModules.dacType[ card ] == GSC_18AO8 )
            {
                limit = OVERFLOW_LIMIT_18BIT; // 18 bit limit
                mask = GSAO_18BIT_MASK;
                num_outs = GSAO_18BIT_CHAN_COUNT;
            }
            if ( cdsPciModules.dacType[ card ] == GSC_20AO8 )
            {
                limit = OVERFLOW_LIMIT_20BIT; // 20 bit limit
                mask = GSAO_20BIT_MASK;
                num_outs = GSAO_20BIT_CHAN_COUNT;
            }
            /// - -- For each DAC channel
            for ( chan = 0; chan < num_outs; chan++ )
            {
                /// - ---- Read DAC output value from shared memory and reset
                /// memory to zero
                if ( ( !dacChanErr[ card ] ) && ( iopDacEnable ) )
                {
                    dac_out = ioMemData->iodata[ mm ][ ioMemCntrDac ] .data[ chan ];
                    /// - --------- Zero out data in case user app dies by next
                    /// cycle when two or more apps share same DAC module.
                    ioMemData->iodata[ mm ][ ioMemCntrDac ].data[ chan ] = 0;
                }
                else
                {
                    dac_out = 0;
                    status = 1;
                }
                /// - ----  Write out ADC duotone signal to first DAC, last
                /// channel, if DAC duotone is enabled.
                if ( ( dt_diag.dacDuoEnable ) && ( chan == ( num_outs - 1 ) ) &&
                     ( card == 0 ) )
                {
                    dac_out = adcinfo.adcData[ 0 ][ ADC_DUOTONE_CHAN ]
                        * cdsPciModules.dacDuoToneMultiplier[0] / cdsPciModules.adcDuoToneDivisor[0];
                }
// Code below is only for use in DAQ test system.
#ifdef DIAG_TEST
                if ( ( chan == 0 ) && ( card < 3 ) )
                {
                    if ( cycleNum < 800 )
                        dac_out = limit / 20;
                    else
                        dac_out = 0;
                }
#endif
                /// - ---- Check output values are within range of DAC \n
                /// - --------- If overflow, clip at DAC limits and report
                /// errors
                if ( dac_out > limit || dac_out < -limit )
                {
                    dacinfo.overflowDac[ card ][ chan ]++;
                    pLocalEpics->epicsOutput.overflowDacAcc[ card ][ chan ]++;
                    overflowAcc++;
                    dacinfo.dacOF[ card ] = 1;
                    odcStateWord |= ODC_DAC_OVF;
                    ;
                    if ( dac_out > limit )
                        dac_out = limit;
                    else
                        dac_out = -limit;
                }
                /// - ---- If DAQKILL tripped, set output to zero.
                if ( !iopDacEnable )
                    dac_out = 0;
                /// - ---- Load last values to EPICS channels for monitoring on
                /// GDS_TP screen.
                dacOutEpics[ card ][ chan ] = dac_out;

                /// - ---- Load DAC testpoints
                floatDacOut[ 16 * card + chan ] = dac_out;

                /// - ---- Write to DAC local memory area, for later xmit to DAC
                /// module
                *pDacData = (unsigned int)( dac_out & mask );
                pDacData++;
            }
            /// - -- Mark cycle count as having been used -1 \n
            /// - --------- Forces control apps to mark this cycle or will not
            /// be used again by Master
            ioMemData->iodata[ mm ][ ioMemCntrDac ].cycle = -1;
            /// - -- DMA Write data to DAC module
            if ( dacWriteEnable > DAC_START_CYCLE )
            {
                plx9056_dac_dma_start( card );
            }
        }
    }
#ifdef USE_ADC_CLOCK
    gscDacSoftClock( &cdsPciModules, GSAI_ALL_CARDS );
#endif
    /// \> Increment DAC memory block pointers for next cycle
    ioClockDac = ( ioClockDac + 1 ) % IOP_IO_RATE;
    ioMemCntrDac = ( ioMemCntrDac + 1 ) % IO_MEMORY_SLOTS;
    if ( dacWriteEnable < 10 )
        dacWriteEnable++;
    /// END OF IOP DAC WRITE *************************************************
    return status;
}

int
check_dac_buffers( int cardNum, int report_all_faults )
{
    // if report_all_faults not set, then will only set
    // dacTimingError on FIFO full.
    int                         out_buf_size = 0;
    int                         status = 0;

    if ( cdsPciModules.dacType[ cardNum ] == GSC_18AO8 )
    {
        out_buf_size = _dacPtr[ cardNum ]->OUT_BUF_SIZE;
        dacOutBufSize[ cardNum ] = out_buf_size;
        pLocalEpics->epicsOutput.buffDac[ cardNum ] = out_buf_size;
        if ( report_all_faults )
        {
            // if ( !dacTimingError )
            if ( dacTimingErrorPending[ cardNum ] < 5 )
            {
                if ( ( out_buf_size < 8 ) || ( out_buf_size > 24 ) )
                {
                    pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_BIT );
                    if ( dacTimingErrorPending[ cardNum ] > DAC_WD_TRIP_SET)
                        dacTimingError = 1;
                    dacTimingErrorPending[ cardNum ] ++;
                }
                else
                {
                    pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_BIT;
                    dacTimingErrorPending[ cardNum ] = 0;
                }
            }

            // Set/unset FIFO empty,hi qtr, full diags
            if ( out_buf_size < 8 )
                pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_EMPTY;
            else
                pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_EMPTY );
            if ( out_buf_size > 24 )
                pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_HI_QTR;
            else
                pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_HI_QTR );
            if ( out_buf_size > 320 )
            {
                pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_FULL;
                dacTimingError = 1;
            }
            else
            {
                pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_FULL );
            }
        }
        else
        {
            pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_EMPTY );
            pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_HI_QTR );
            // Check only for FIFO FULL
            if ( out_buf_size > 320 )
            {
                pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_FULL;
                pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_BIT );
                dacTimingError = 1;
            }
            else
            {
                pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_BIT;
            }
        }
    }

    if ( cdsPciModules.dacType[ cardNum ] == GSC_20AO8 )
    {
        out_buf_size = _dacPtr[ cardNum ]->OUT_BUF_SIZE;
        dacOutBufSize[ cardNum ] = out_buf_size;
        pLocalEpics->epicsOutput.buffDac[ cardNum ] = out_buf_size;
        if ( ( out_buf_size > 24 ) )
        {
            pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_BIT );
            if ( (dacTimingErrorPending[ cardNum ] > DAC_WD_TRIP_SET) && report_all_faults )
                dacTimingError = 1;
            else dacTimingErrorPending[ cardNum ] ++;
        }
        else
        {
            pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_BIT;
            dacTimingErrorPending[ cardNum ] = 0;
        }

        // Set/unset FIFO empty,hi qtr, full diags
        if ( out_buf_size > 24 )
            pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_HI_QTR;
        else
            pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_HI_QTR );
        if ( out_buf_size > 500 )
        {
            pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_FULL;
            dacTimingError = 1;
        }
        else
        {
            pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_FULL );
        }
    }
    if ( cdsPciModules.dacType[ cardNum ] == GSC_16AO16 )
    {
        status = gsc16ao16CheckDacBuffer( cardNum );
        dacOutBufSize[ cardNum ] = status;
        pLocalEpics->epicsOutput.buffDac[ cardNum ] = out_buf_size;
        if ( report_all_faults )
        {
            if ( !dacTimingError )
            {
                if ( status != 2 )
                {
                    pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_BIT );
                    if ( dacTimingErrorPending[ cardNum ] > DAC_WD_TRIP_SET)
                        dacTimingError = 1;
                    else dacTimingErrorPending[ cardNum ] ++;
                }
                else
                {
                    pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_BIT;
                    dacTimingErrorPending[ cardNum ] = 0;
                }
            }

            // Set/unset FIFO empty,hi qtr, full diags
            if ( status & 1 )
                pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_EMPTY;
            else
                pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_EMPTY );
            if ( status & 8 )
            {
                pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_FULL;
                dacTimingError = 1;
            }
            else
            {
                pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_FULL );
            }
            if ( status & 4 )
                pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_HI_QTR;
            else
                pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_HI_QTR );
        }
        else
        {
            pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_EMPTY );
            pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_HI_QTR );
            // Check only for FIFO FULL
            if ( status & 8 )
            {
                pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_BIT );
                pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_FULL;
                pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_BIT );
                dacTimingError = 1;
            }
            else
            {
                pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_BIT;
                pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_BIT;
                pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_FULL );
            }
        }
    }
    return 0;
}
