///     \file gsc18ao8.c
///     \brief File contains the initialization routine and various register
///     read/write
///<            operations for the General Standards 18bit, 8 channel DAC
///<            modules. \n
///< For board info, see
///<    <a
///<    href="http://www.generalstandards.com/view-products2.php?BD_family=18ao8">GSC
///<    18AO8 Manual</a>

#include "drv/gsc_dac_common.h"
#include "drv/gsc18ao8.h"
#include "ioremap_selection.h"
#include "drv/rts-logger.h"
#include "drv/map.h"
#include "drv/plx_9056.h"

#include <linux/delay.h> //udelay()


// *****************************************************************************
/// \brief Routine to initialize GSC 18AO8 DAC modules.
///     @param[in,out] *pHardware Pointer to global data structure for storing
///     I/O
///<            register mapping information.
///     @param[in] *dacdev PCI address information passed by the mapping code in
///     map.c
///     @return Status from board enable command.
// *****************************************************************************
int
gsc18ao8Init( CDS_HARDWARE* pHardware, struct pci_dev* dacdev )
{
    int devNum; /// @param devNum Index into CDS_HARDWARE struct for adding
                /// board info.
    char* _dac_add; /// @param *_dac_add DAC register address space
    static unsigned int pci_io_addr; /// @param pci_io_addr Bus address of PCI
                                     /// card I/O register.
    int pedStatus; /// @param pedStatus Status return from call to enable
                   /// device.
    volatile GSC_DAC_REG*
        dac18bitPtr; /// @param *dac18bitPtr Pointer to DAC control registers.
#ifdef DAC_AUTOCAL
    int timer = 0;
#endif

    /// Get index into CDS_HARDWARE struct based on total number of DAC cards
    /// found by mapping routine in map.c
    devNum = pHardware->dacCount;
    /// Enable the device, PCI required
    pedStatus = pci_enable_device( dacdev );
    /// Register module as Master capable, required for DMA
    pci_set_master( dacdev );
    /// Get the PLX chip PCI address, it is advertised at address 0
    pedStatus = pci_read_config_dword( dacdev, PCI_BASE_ADDRESS_0, &pci_io_addr );
    if(pedStatus != 0)
        return -1;
    RTSLOG_INFO( "pci0 = 0x%x\n", pci_io_addr );
    _dac_add = IOREMAP( (unsigned long)pci_io_addr, 0x200 );
    /// Set up a pointer to DMA registers on PLX chip
    dacDma[ devNum ] = (PLX_9056_DMA*)_dac_add;

    /// Get the DAC register address
    pedStatus = pci_read_config_dword( dacdev, PCI_BASE_ADDRESS_2, &pci_io_addr );
    if(pedStatus != 0)
        return -1;
    RTSLOG_INFO( "dac pci2 = 0x%x\n", pci_io_addr );
    _dac_add = IOREMAP( (unsigned long)pci_io_addr, 0x200 );
    RTSLOG_INFO( "DAC I/O address=0x%x  0x%lx\n", pci_io_addr, (long)_dac_add );

    dac18bitPtr = (GSC_DAC_REG*)_dac_add;
    _dacPtr[ devNum ] = (GSC_DAC_REG*)_dac_add;

    RTSLOG_INFO( "DAC BCR = 0x%x\n", dac18bitPtr->BCR );
    /// Reset the DAC board and wait for it to finish (3msec)

    dac18bitPtr->BCR |= GSAO_18BIT_RESET;

    do
    {
    } while ( ( dac18bitPtr->BCR & GSAO_18BIT_RESET ) != 0 );

    /// Enable 2s complement by clearing offset binary bit
    dac18bitPtr->BCR &= ~GSAO_18BIT_OFFSET_BINARY;
    dac18bitPtr->BCR |= GSAO_18BIT_SIMULT_OUT;
    RTSLOG_INFO( "DAC BCR after init = 0x%x\n", dac18bitPtr->BCR );
    RTSLOG_INFO( "DAC OUTPUT CONFIG = 0x%x\n", dac18bitPtr->OUTPUT_CONFIG );

    /// Enable 10 volt output range
    dac18bitPtr->OUTPUT_CONFIG |= GSAO_18BIT_10VOLT_RANGE;
    // Various bits setup
    // dac18bitPtr->OUTPUT_CONFIG |= GSAO_18BIT_ENABLE_EXT_CLOCK;
    // dac18bitPtr->OUTPUT_CONFIG |= GSAO_18BIT_EXT_CLOCK_SRC;
    // dac18bitPtr->OUTPUT_CONFIG |= GSAO_18BIT_EXT_TRIG_SRC;
    dac18bitPtr->OUTPUT_CONFIG |= GSAO_18BIT_DIFF_OUTS;

#ifdef DAC_AUTOCAL
    // Start Calibration
    dac18bitPtr->BCR |= GSAO_18BIT_AUTOCAL_SET;
    pHardware->dacAcr[ devNum ] = 0;
    do
    {
        udelay( 1000 );
        timer += 1;
    } while ( ( dac18bitPtr->BCR & GSAO_18BIT_AUTOCAL_SET ) != 0 );

    if ( dac18bitPtr->BCR & GSAO_18BIT_AUTOCAL_PASS )
    {
        RTSLOG_INFO( "GSC_18AO8 : devNum %d : Took %d ms : DAC AUTOCAL PASS \n", 
                     devNum, timer );
        pHardware->dacAcr[ devNum ] = DAC_CAL_PASS;
    } else {
        RTSLOG_ERROR( "GSC_18AO8 : devNum %d : Took %d ms : DAC AUTOCAL FAIL\n", 
                      devNum, timer );
    }
    RTSLOG_INFO( "DAC OUTPUT CONFIG after init = 0x%x with BCR = 0x%x\n",
            dac18bitPtr->OUTPUT_CONFIG,
            dac18bitPtr->BCR );
#else
    RTSLOG_INFO( "DAC OUTPUT CONFIG after init = 0x%x\n",
            dac18bitPtr->OUTPUT_CONFIG );
#endif

    pHardware->pci_dac[ devNum ] =
        (volatile int *)dma_alloc_coherent( &dacdev->dev, 0x200, &dac_dma_handle[ devNum ], GFP_ATOMIC );
    pHardware->dacAcr[ devNum ] |= ((int)( dac18bitPtr->ASY_CONFIG ) & DAC_ACR_MASK);
    pHardware->dacType[ devNum ] = GSC_18AO8;
    pHardware->dacDuoToneMultiplier[ devNum ] = 4;
    pHardware->dacCount++;
    pHardware->dacInstance[ devNum ] = pHardware->card_count[ GSC_18AO8 ];
    pHardware->card_count[ GSC_18AO8 ] ++;

    /// Call patch in map.c needed to properly write to native PCIe module
    /// version of 16AO16
    set_8111_prefetch( dacdev );
    return ( pedStatus );
}
