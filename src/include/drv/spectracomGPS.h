#ifndef LIGO_SPECTRACOM_GPS_H
#define LIGO_SPECTRACOM_GPS_H
// Spectracom GPS input card

#include "portableInline.h"
#include "drv/cdsHardware.h"
//#include "controller.h" //cdsPciModules

#include <linux/pci.h>

#define TSYNC_VID               0x1ad7
#define TSYNC_TID               0x8000
#define TSYNC_SEC               0x1
#define TSYNC_USEC              0x2
#define TSYNC_RCVR              0x2
typedef struct TSYNC_REGISTER{
        unsigned int SUPER_SEC_LOW;     // Host Super-Sec Low + Mid Low
        unsigned int SUPER_SEC_HIGH;    // Host Super-Sec Mid High + High
        unsigned int SUB_SEC;           // Sub-Second Low + High
        unsigned int BCD_SEC;           // Super Second binary time
        unsigned int BCD_SUB_SEC;       // Sub Second binary time
}TSYNC_REGISTER;

#ifdef __cplusplus
extern "C" {
#endif

//
// Function Prototypes
//
int spectracomGpsInit( CDS_HARDWARE* pHardware, struct pci_dev* gpsdev );
int spectracomGpsInitCheckSync( CDS_HARDWARE*   pHardware,
                                struct pci_dev* gpsdev,
                                int*            need_sync );


//
// Inline Functions
//

//***********************************************************************
/// \brief  Get current GPS time from TSYNC IRIG-B Rcvr
/// @param[out] *tsyncSec Pointer to register with seconds information.
/// @param[out] *tsyncUsec Pointer to register with microsecond information.
/// @return GPS Sync bit (0=No sync 1=Sync)
//***********************************************************************
LIGO_INLINE int
getGpsTimeTsync(CDS_HARDWARE* pHardware, unsigned int* tsyncSec, unsigned int* tsyncUsec )
{
    volatile TSYNC_REGISTER* timeRead;
    unsigned int    timeSec, timeNsec, sync;

    if (  pHardware->gps )
    {
        timeRead = (TSYNC_REGISTER*)pHardware->gps;
        timeSec = timeRead->BCD_SEC;
        timeSec += pHardware->gpsOffset;
        *tsyncSec = timeSec;
        timeNsec = timeRead->SUB_SEC;
        *tsyncUsec = ( ( timeNsec & 0xfffffff ) * 5 ) / 1000;
        sync = ( ( timeNsec >> 31 ) & 0x1 ) + 1;
        return ( sync );
    }
    return ( 0 );
}

//***********************************************************************
/// \brief Get current GPS seconds from TSYNC IRIG-B Rcvr
/// @param[in] pHardware Pointer to PCI hardware map
//***********************************************************************
LIGO_INLINE unsigned int
getGpsSecTsync( CDS_HARDWARE* pHardware )
{
    volatile TSYNC_REGISTER* timeRead;
    unsigned int    timeSec;

    if ( pHardware->gps )
    {
        timeRead = (TSYNC_REGISTER*)pHardware->gps;
        timeSec = timeRead->BCD_SEC;
        timeSec += pHardware->gpsOffset;
        return ( timeSec );
    }
    return ( 0 );
}

//***********************************************************************
/// \brief Get current GPS useconds from TSYNC IRIG-B Rcvr
/// @param[in] pHardware Pointer to PCI hardware map
/// @param[out] *tsyncUsec Pointer to register with microsecond information.
/// @return GPS Sync bit (0=No sync 1=Sync)
//***********************************************************************
LIGO_INLINE int
getGpsuSecTsync( CDS_HARDWARE* pHardware, unsigned int* tsyncUsec )
{
    volatile TSYNC_REGISTER* timeRead;
    unsigned int    timeNsec, sync;

    if (  pHardware->gps )
    {
        timeRead = (TSYNC_REGISTER*)pHardware->gps;
        timeNsec = timeRead->SUB_SEC;
        *tsyncUsec = ( ( timeNsec & 0xfffffff ) * 5 ) / 1000;
        sync = ( ( timeNsec >> 31 ) & 0x1 ) + 1;
        return ( sync );
    }
    return ( 0 );
}


#ifdef __cplusplus
}
#endif


#endif //LIGO_SPECTRACOM_GPS_H
