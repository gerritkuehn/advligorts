#ifndef LIGO_GSC16AO16_H
#define LIGO_GSC16AO16_H
/* GSA 16AO16 DAC Module Definitions ********************************************************* */

#include <linux/pci.h>

#ifdef __cplusplus
extern "C" {
#endif

int gsc16ao16Init( CDS_HARDWARE* , struct pci_dev* );
int gsc16ao16CheckDacBuffer( int );
void gsc16ao16ClearDacBuffer( int );

#ifdef __cplusplus
}
#endif


#define DAC_SS_ID               0x3120  /* Subsystem ID to find module on PCI bus       */
#define GSAO_RESET              0x8000
#define GSAO_OUT_RANGE_25       0x10000
#define GSAO_OUT_RANGE_05       0x20000
#define GSAO_OUT_RANGE_10       0x30000
#define GSAO_SIMULT_OUT         0x80
#define GSAO_2S_COMP            0x10
#define GSAO_EXTERN_CLK         0x10
#define GSAO_ENABLE_CLK         0x20
#define GSAO_SFT_TRIG           0x80
#define GSAO_CLR_BUFFER         0x800
#define GSAO_FIFO_16            0x1
#define GSAO_FIFO_32            0x2
#define GSAO_FIFO_64            0x3
#define GSAO_FIFO_128           0x4
#define GSAO_FIFO_256           0x5
#define GSAO_FIFO_512           6
#define GSAO_FIFO_1024          7
#define GSAO_FIFO_2048          8
#define GSAO_16BIT_PRELOAD      144     // Number of data points to preload DAC FIFO on startup 
#define GSAO_16BIT_MASK         0xffff
#define GSAO_16BIT_CHAN_COUNT   16
#define GSAO_ISOLATE_EXT_CLK    0x100000
#define GSAO_FIFO_SIZE      GSAO_FIFO_256
#define GSAO_BOR_SW_CLOCK      ( GSAO_SFT_TRIG | GSAO_ISOLATE_EXT_CLK | GSAO_EXTERN_CLK | GSAO_ENABLE_CLK | GSAO_FIFO_SIZE )

#endif //LIGO_GSC16AO16_H
