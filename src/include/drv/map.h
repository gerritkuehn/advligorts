#ifndef MAP_H_INCLUDED
#define MAP_H_INCLUDED

#include "drv/cdsHardware.h"

#include <linux/types.h> //dma_addr_t
#include <linux/pci.h> 

#ifdef __cplusplus
extern "C" {
#endif


void set_8111_prefetch( struct pci_dev* );
int find_card_slot( CDS_HARDWARE* pCds, int ctype, int cinstance );
int  mapPciModules( CDS_HARDWARE* ); /* Init routine to map adc/dac cards*/

#ifdef __cplusplus
}
#endif

// PCI Device variables
// volatile PLX_9056_INTCTRL* plxIcr; /* Ptr to interrupt cntrl reg on PLX chip */
extern dma_addr_t rfm_dma_handle[ MAX_DAC_MODULES ]; /* PCI add of RFM DMA memory */

#endif
