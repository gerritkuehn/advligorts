/// 	\file ligoPcieTiming64.c

#include "ligoPcieTiming.h"
#include "controller.h" //IOC_CLK_SLOW, MAX_UDELAY

#include <linux/delay.h> //udelay()


void
lptc_enable_all_slots( CDS_HARDWARE* pCds )
{
    unsigned int   regval;
    int            ii, jj;
    volatile LPTC_REGISTER* lptcPtr;

    for ( jj = 0; jj < pCds->card_count[ LPTC ]; jj++ )
    {
        lptcPtr = (LPTC_REGISTER*)pCds->lptc[ jj ];
        for ( ii = 0; ii < LPTC_IOC_SLOTS; ii++ )
        {
            switch ( pCds->ioc_config[ ii ] )
            {
            case GSC_18AI64SSC:
            case GSC_18AI32SSC1M:
#if MODEL_RATE_HZ >= 524288
                lptcPtr->slot_info[ ii ].config =
                    ( LPTC_SCR_ADC_SET | IOC_CLK_FAST );
#else
                lptcPtr->slot_info[ ii ].config =
                    ( LPTC_SCR_ADC_SET | IOC_CLK_SLOW );
#endif
                break;
            case GSC_16AI64SSA:
                lptcPtr->slot_info[ ii ].config =
                    ( LPTC_SCR_ADC_SET | IOC_CLK_SLOW );
                break;
            case GSC_16AO16:
            case GSC_18AO8:
            case GSC_20AO8:
                lptcPtr->slot_info[ ii ].config =
                    ( LPTC_SCR_DAC_SET | IOC_CLK_SLOW );
                break;
            default:
                lptcPtr->slot_info[ ii ].config =
                    ( LPTC_SCR_ADC_SET | IOC_CLK_SLOW );
                break;
            }
            if ( ii == 0 )
                lptcPtr->slot_info[ ii ].config |= LPTC_SCR_ADC_DT_ENABLE;
            udelay( MAX_UDELAY );
            regval = lptcPtr->slot_info[ ii ].config;
        }
    }
    udelay( MAX_UDELAY );
    udelay( MAX_UDELAY );
}

int
lptc_start_clock( CDS_HARDWARE* pCds )
{
    int            ii, jj;
    volatile LPTC_REGISTER* lptcPtr;

    if(pCds->card_count[ LPTC ] < 1) return -1;

    for ( jj = 0; jj < pCds->card_count[ LPTC ]; jj++ )
    {
        lptcPtr = (LPTC_REGISTER*)pCds->lptc[ jj ];
        lptcPtr->bp_config = LPTC_CMD_START_CLK_ALL;
        // Update lptc status to epics
        for ( ii = 0; ii < LPTC_IOC_SLOTS; ii++ )
            pLocalEpics->epicsOutput.lptMon[ ii ] =
                lptc_get_slot_status( &cdsPciModules, ii );
        for ( ii = 0; ii < LPTC_IOC_SLOTS; ii++ )
            pLocalEpics->epicsOutput.lptMon[ ( ii + LPTC_IOC_SLOTS ) ] =
                lptc_get_slot_config( &cdsPciModules, ii );
        pLocalEpics->epicsOutput.lpt_bp_config =
            lptc_get_bp_config( &cdsPciModules );
        pLocalEpics->epicsOutput.lpt_bp_status =
            lptc_get_bp_status( &cdsPciModules );
        pLocalEpics->epicsOutput.lpt_status =
            lptc_get_lptc_status( &cdsPciModules );
    }

    return lptcPtr->bp_status;
}

void
lptc_status_update( CDS_HARDWARE* pCds )
{
    int ii;

    // Update lptc status to epics
    for ( ii = 0; ii < LPTC_IOC_SLOTS; ii++ )
        pLocalEpics->epicsOutput.lptMon[ ii ] =
            lptc_get_slot_status( pCds, ii );
    for ( ii = 0; ii < LPTC_IOC_SLOTS; ii++ )
        pLocalEpics->epicsOutput.lptMon[ ( ii + LPTC_IOC_SLOTS ) ] =
            lptc_get_slot_config( &cdsPciModules, ii );
    pLocalEpics->epicsOutput.lpt_bp_config =
        lptc_get_bp_config( &cdsPciModules );
    pLocalEpics->epicsOutput.lpt_bp_status =
        lptc_get_bp_status( &cdsPciModules );
    pLocalEpics->epicsOutput.lpt_status =
        lptc_get_lptc_status( &cdsPciModules );
}

int
lptc_stop_clock( CDS_HARDWARE* pCds )
{
    int            jj;
    volatile LPTC_REGISTER* lptcPtr;

    if(pCds->card_count[ LPTC ] < 1) return -1;

    for ( jj = 0; jj < pCds->card_count[ LPTC ]; jj++ )
    {
        lptcPtr = (LPTC_REGISTER*)pCds->lptc[ jj ];
        lptcPtr->bp_config = LPTC_CMD_STOP_CLK_ALL;
        udelay( MAX_UDELAY );
        udelay( MAX_UDELAY );
    }
    return lptcPtr->bp_status;
}

void
lptc_dac_duotone( CDS_HARDWARE* pCds, int setting )
{
    volatile LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ 0 ];
    if ( setting )
    {
        lptcPtr->slot_info[ 0 ].config |= LPTC_SCR_DAC_DT_ENABLE;
        lptcPtr->slot_info[ 1 ].config |= LPTC_SCR_DAC_DT_ENABLE;
    }
    else
    {
        lptcPtr->slot_info[ 0 ].config &= ~( LPTC_SCR_DAC_DT_ENABLE );
        lptcPtr->slot_info[ 1 ].config &= ~( LPTC_SCR_DAC_DT_ENABLE );
    }
}

void
lptc_slot_clk_set( CDS_HARDWARE* pCds, int slot, int enable )
{
    int jj;

    for ( jj = 0; jj < pCds->card_count[ LPTC ]; jj++ )
    {
        volatile LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ 0 ];
        if ( enable )
            lptcPtr->slot_info[ slot ].config |= LPTC_SCR_CLK_ENABLE;
        else
            lptcPtr->slot_info[ slot ].config &= ~( LPTC_SCR_CLK_ENABLE );
    }
}

void
lptc_slot_clk_disable_all( CDS_HARDWARE* pCds )
{
    int slot = 0;
    int jj;

    for ( jj = 0; jj < pCds->card_count[ LPTC ]; jj++ )
    {
        volatile LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ jj ];
        for ( slot = 0; slot < LPTC_IOC_SLOTS; slot++ )
            lptcPtr->slot_info[ slot ].config &= ~( LPTC_SCR_CLK_ENABLE );
    }
}

void
lptc_slot_clk_enable_all( CDS_HARDWARE* pCds )
{
    int slot = 0;
    int jj;

    for ( jj = 0; jj < pCds->card_count[ LPTC ]; jj++ )
    {
        volatile LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ jj ];
        for ( slot = 0; slot < LPTC_IOC_SLOTS; slot++ )
            lptcPtr->slot_info[ slot ].config |= LPTC_SCR_CLK_ENABLE;
    }
}
