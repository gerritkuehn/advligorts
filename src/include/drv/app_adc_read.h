#ifndef LIGO_APP_ADC_READ_H 
#define LIGO_APP_ADC_READ_H

#include "portableInline.h"
#include "cds_types.h"
#include "controller.h" //cdsPciModules
#include "fm10Gen.h"
#include "util/macros.h" //COUNT_OF()

#include <asm/msr.h>

#ifdef __cplusplus
extern "C" {
#endif

//This function is forced static because the rdtsc_ordered() function is static
static inline int
app_adc_read( int ioMemCtr, int ioClk, adcInfo_t* adcinfo, unsigned long long cpuClk[], int* startGpsTime_ptr  )
{
    int limit; //Set below
    int mm;
    int card = 0;
    int chan = 0;

    /// \> Control model gets its adc data from MASTER via ipc shared memory\n
    /// \> For each ADC defined:
    for ( card = 0; card < cdsPciModules.adcCount; card++ )
    {

        // Various ADC models have different number of channels/data bits
        if ( cdsPciModules.adcType[ card ] == GSC_16AI64SSA )
            limit = OVERFLOW_LIMIT_16BIT;
        else
            limit = OVERFLOW_LIMIT_18BIT;


        mm = cdsPciModules.adcConfig[ card ];
        if( mm >= 0 ) 
        {
            cpuClk[ CPU_TIME_RDY_ADC ] = rdtsc_ordered( );
            cpuClk[ CPU_TIME_ADC_WAIT ] = cpuClk[ CPU_TIME_RDY_ADC ];
            adcinfo->adcWait = 0;

            /// - ---- Wait for proper timestamp in shared memory, indicating data
            /// ready.
            while ( ( ioMemData->iodata[ mm ][ ioMemCtr ].cycle != ioClk ) &&
                    ( adcinfo->adcWait < MAX_ADC_WAIT_CONTROL ) )
            {
                ndelay(1000);
                cpuClk[ CPU_TIME_ADC_WAIT ] = rdtsc_ordered( );
                adcinfo->adcWait =
                    ( cpuClk[ CPU_TIME_ADC_WAIT ] - cpuClk[ CPU_TIME_RDY_ADC ] ) /
                    CPURATE;
            }
            timeSec = ioMemData->iodata[ mm ][ ioMemCtr ].timeSec;
            if ( cycle_gps_time == 0 )
            {
                *startGpsTime_ptr = timeSec;
                pLocalEpics->epicsOutput.startgpstime = *startGpsTime_ptr;
            }
            cycle_gps_time = timeSec;

            /// - --------- If data not ready in time, set error, release DAC
            /// channel reservation and exit the code.
            if ( adcinfo->adcWait >= MAX_ADC_WAIT_CONTROL )
                return 1;

            for ( chan = 0; chan < MAX_ADC_CHN_PER_MOD; chan++ )
            {
                /// - ---- Read data from shared memory.
                adcinfo->adcData[ card ][ chan ] =
                    ioMemData->iodata[ mm ][ ioMemCtr ].data[ chan ];
    #ifdef FLIP_SIGNALS
                adcinfo->adcData[ card ][ chan ] *= -1;
    #endif

                //Set model ADC sample from IOP shared memory
                dWord[ card ][ chan ] = adcinfo->adcData[ card ][ chan ];

                if ( dWordUsed[ card ][ chan ] )
                {

                    #ifdef OVERSAMPLE //Defined when model rate is < ADC clock
                    /// - ---- Downsample ADC data from 64K to rate of user application
                    dWord[ card ][ chan ] =
                        iir_filter_biquad( dWord[ card ][ chan ],
                                           FE_OVERSAMPLE_COEFF,
                                           //Calculates the number of second order sections in filter definition
                                           //It takes the number of elements in the array - 1 (for gain) divided by 4
                                           (COUNT_OF(FE_OVERSAMPLE_COEFF)-1)/4,
                                           &dHistory[ chan + card * MAX_ADC_CHN_PER_MOD ][ 0 ] );
                    #endif


                    /// - ---- Check for ADC data over/under range
                    if ( ( adcinfo->adcData[ card ][ chan ] > limit ) ||
                         ( adcinfo->adcData[ card ][ chan ] < -limit ) )
                    {
                        adcinfo->overflowAdc[ card ][ chan ]++;
                        pLocalEpics->epicsOutput.overflowAdcAcc[ card ][ chan ]++;
                        overflowAcc++;
                        adcinfo->adcOF[ card ] = 1;
                        odcStateWord |= ODC_ADC_OVF;
                    }


                } //if (dWordUsed[ card ][ chan ])
            } //For all chans in ADCs
        }
    }
    return 0;
}

LIGO_INLINE int
app_adc_status_update( adcInfo_t* adcinfo )
{
    int status = 0;
    int card = 0;
    int chan = 0;
    int num_chans = 0;

    for ( card = 0; card < cdsPciModules.adcCount; card++ )
    {
        // SET/CLR Channel Hopping Error
        if ( adcinfo->adcChanErr[ card ] )
        {
            pLocalEpics->epicsOutput.statAdc[ card ] &= ~( ADC_CHAN_HOP );
            status |= FE_ERROR_ADC;
        }
        else
            pLocalEpics->epicsOutput.statAdc[ card ] |= ADC_CHAN_HOP;
        adcinfo->adcChanErr[ card ] = 0;
        // SET/CLR Overflow Error
        if ( adcinfo->adcOF[ card ] )
        {
            pLocalEpics->epicsOutput.statAdc[ card ] &= ~( ADC_OVERFLOW );
            status |= FE_ERROR_OVERFLOW;
        }
        else
            pLocalEpics->epicsOutput.statAdc[ card ] |= ADC_OVERFLOW;
        adcinfo->adcOF[ card ] = 0;
        if(cdsPciModules.adcType[ card ] == GSC_18AI32SSC1M) num_chans = 8;
        else num_chans = MAX_ADC_CHN_PER_MOD;
        for ( chan = 0; chan < num_chans; chan++ )
        {
            if ( pLocalEpics->epicsOutput.overflowAdcAcc[ card ][ chan ] >
                 OVERFLOW_CNTR_LIMIT )
            {
                pLocalEpics->epicsOutput.overflowAdcAcc[ card ][ chan ] = 0;
            }
            pLocalEpics->epicsOutput.overflowAdc[ card ][ chan ] =
                adcinfo->overflowAdc[ card ][ chan ];
            adcinfo->overflowAdc[ card ][ chan ] = 0;
        }
    }
    return status;
}

#ifdef __cplusplus
}
#endif


#endif //LIGO_APP_ADC_READ_H
