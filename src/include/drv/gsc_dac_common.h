#ifndef __GDS_DAC_COMMON_H__
#define __GDS_DAC_COMMON_H__

/// @file gsc_dac_common.h
/// @brief Provides DAC register map and common defs

#include "drv/cdsHardware.h" //CDS_HARDWARE

#include <linux/types.h>

#ifdef __cplusplus
extern "C" {
#endif


// Prototypes for routines found in gsc_dac_common.c
void gscEnableDacModule( CDS_HARDWARE* , int , int );
int gscDacFifoCheck( CDS_HARDWARE* pHardware, int , int );
void gscDacFifoClear( CDS_HARDWARE* , int );
void gscDacSoftClock( CDS_HARDWARE* pHardware, int card );

#ifdef __cplusplus
}
#endif


// Standard DAC module register map
typedef struct GSC_DAC_REG {
        u32 BCR;                   // 0x00 
        u32 CSR_DIO;               // 0x04  CSR for 16AO16 / digital io for 18/20bit
        u32 SAMPLE_RATE;           // 0x8   Only 16AO16
        u32 BOR;                   // 0x0C  Only 16AO16
        u32 ASSC;                  // 0x10  Only 16AO16
        u32 AC_VALS;               // 0x14  Selftest config on 18ao8 and 20ao8
        u32 ODB;                   // 0x18  Selftest dbuf on 18ao8 and 20ao8
        u32 AUX_SYNC_IO_CTRL;      // 0x1c 
        // Following only apply to 18ao8 and 20ao8
        u32 reserved2;
        u32 reserved3;
        u32 reserved4;
        u32 reserved5;
        u32 PRIMARY_STATUS;    /* 0x30 */
        u32 ASY_CONFIG;        /* 0x34 */
        u32 AUTOCAL_VALS;      /* 0x38 */
        u32 BUF_OUTPUT_OPS;    /* 0x3C */
        u32 OUT_BUF_THRESH;    /* 0x40 */
        u32 OUT_BUF_SIZE;      /* 0x44 RO */
        u32 OUTPUT_BUF;        /* 0x48 WO */
        u32 RATE_GEN_C;        /* 0x4C */
        u32 RATE_GEN_D;        /* 0x50 */
        u32 OUTPUT_CONFIG;     /* 0x54 */
} GSC_DAC_REG;

extern volatile GSC_DAC_REG *_dacPtr[MAX_DAC_MODULES];  /* Ptr to DAC registers */

#define DAC_FIFO_EMPTY_TEST      1
#define DAC_FIFO_NOT_EMPTY_TEST  0
#define DAC_FIFO_CLEAR      (1 << 11)
#define DAC_CLK_ENABLE              1
#define DAC_CLK_DISABLE             0
// Set lower 4 bits of DAC DIO register to outputs
#define DAC_DIO_L4B_OUTPUT         (1 << 7)

#endif // __GDS_DAC_COMMON_H__


