#ifndef LIGO_IOP_ADC_FUNCTIONS_H
#define LIGO_IOP_ADC_FUNCTIONS_H

#include "cds_types.h"

#ifdef __cplusplus
extern "C" {
#endif

int iop_adc_init( adcInfo_t* );
int iop_adc_read( adcInfo_t*, unsigned long long[] );

#ifdef __cplusplus
}
#endif



#endif //LIGO_IOP_ADC_FUNCTIONS_H
