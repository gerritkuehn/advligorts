#ifndef LIGO_PCIE_TIMING_CARD_HEADER_H
#define LIGO_PCIE_TIMING_CARD_HEADER_H
// LIGO PCIe Timing Card
//

// PCI identifiers
#define LPTC_VID	0x10ee
#define LPTC_TID	0xd8c6

// LPTC board id in firmware version
// can be read as lptc_get_sowftware_id() >> 4
// if not equal to this value, you don't have a well programmed FPGA
#define LPTC_BOARD_ID 0x2000337

#include "drv/cdsHardware.h"

#include <linux/types.h>
#include <linux/pci.h>

#ifdef __cplusplus
extern "C" {
#endif

// Prototypes for routines in ligoPcieTiming_core.c
int lptcInit( CDS_HARDWARE* , struct pci_dev* );
int lptc_get_gps_time( CDS_HARDWARE* , u32* , u32* );
unsigned int lptc_get_gps_usec( CDS_HARDWARE* );
unsigned int lptc_get_gps_sec( CDS_HARDWARE* );
int lptc_get_lptc_status( CDS_HARDWARE* );
int lptc_get_bp_status( CDS_HARDWARE* );
int lptc_get_bp_config( CDS_HARDWARE* );
int lptc_get_slot_phase( CDS_HARDWARE* pCds, int slot );


// slot functions in ligoPcieTiming_core.c
int lptc_get_slot_config( CDS_HARDWARE* , int );
int lptc_get_slot_status( CDS_HARDWARE* , int );

// diagnostics functions in ligoPcieTiming_core.c
int lptc_get_board_id( CDS_HARDWARE* pCds );
int lptc_get_board_sn( CDS_HARDWARE* pCds );
int lptc_get_software_id( CDS_HARDWARE* pCds );
int lptc_get_software_rev( CDS_HARDWARE* pCds );
int lptc_get_gps_sec_diag( CDS_HARDWARE* pCds );
int lptc_get_mod_address( CDS_HARDWARE* pCds );
int lptc_get_board_status( CDS_HARDWARE* pCds );
int lptc_get_board_config( CDS_HARDWARE* pCds );
int lptc_get_ocxo_controls( CDS_HARDWARE* pCds );
int lptc_get_ocxo_error( CDS_HARDWARE* pCds );
int lptc_get_uplink_1pps_delay( CDS_HARDWARE* pCds );
int lptc_get_external_1pps_delay( CDS_HARDWARE* pCds );
int lptc_get_gps_1pps_delay( CDS_HARDWARE* pCds );
int lptc_get_fanout_up_loss( CDS_HARDWARE* pCds );
int lptc_get_fanout_missing_delay_error( CDS_HARDWARE* pCds );
int lptc_get_leaps_and_error( CDS_HARDWARE* pCds );

// 'On Board Features' (LIGO-T2000406 3.9)
// in ligoPcieTimeing_core.c
int lptc_get_brd_synch_factors( CDS_HARDWARE* pCds );
int lptc_get_xadc_config( CDS_HARDWARE* pCds );
int lptc_get_board_and_powersupply_status( CDS_HARDWARE* pCds );
int lptc_get_xadc_status( CDS_HARDWARE* pCds );
//temperature from 0x0190 offset
int lptc_get_temp( CDS_HARDWARE* pCds );
//internal power values from 0x0190 and 0x0194.
void lptc_get_internal_pwr( CDS_HARDWARE* pCds, u32 *output );
// ouput should be a preallocated array the same size as
// LPTC_OBF_REGISTER:EPS
void lptc_get_external_pwr( CDS_HARDWARE* pCds, u32 *output);

// Prototypes for routine in ligoPcieTiming.c
void lptc_enable_all_slots( CDS_HARDWARE* );
int lptc_start_clock( CDS_HARDWARE* );
void lptc_status_update( CDS_HARDWARE* );
int lptc_stop_clock( CDS_HARDWARE* );
void lptc_dac_duotone( CDS_HARDWARE* , int );
void lptc_slot_clk_set( CDS_HARDWARE* , int, int );
void lptc_slot_clk_disable_all( CDS_HARDWARE* );
void lptc_slot_clk_enable_all( CDS_HARDWARE* );

#ifdef __cplusplus
}
#endif




#define LPTC_BP_SLOTS 10
#define LPTC_OBF_EPS_REGS 8


typedef struct SLOT_CONFIG {
	u32 config;
	u32 phase;
	u32 status;
	u32 reserved;
}SLOT_CONFIG;


typedef struct LPTC_REGISTER {
	u64 gps_time;		// 0x0000
	u32 status;		    // 0x0008
	u32 revision;		// 0x000c
	u32 bp_config;		// 0x0010
	u32 wd_reset;		// 0x0014
	u32 bp_status;		// 0x0018
	u32 reserved3;		// 0x001c
	SLOT_CONFIG slot_info[LPTC_BP_SLOTS];	// 0x0020
}LPTC_REGISTER;

typedef struct LPTC_OBF_REGISTER {
    u32 brd_config;	// 0x0180
	u32 xadc_config;	// 0x0184
	u32 bps_status;	// 0x0188
	u32 xadc_status;	// 0x018c
	u32 tips1;		// 0x0190
	u32 ips2;		// 0x0194
	u32 eps[LPTC_OBF_EPS_REGS];		// 0x0198
}LPTC_OBF_REGISTER;

// Data read from LPTC at 0x1000 offset
typedef struct LPTC_DIAG_INFO {
	u32 board_id;		// 0x1000
	u32 board_sn;		// 0x1004
	u32 sftware_id;	    // 0x1008
	u32 sftware_rev;	// 0x100c
	u32 gps_seconds;	// 0x1010
	u32 mod_address;	// 0x1014
	u32 board_status;	// 0x1018
	u32 board_config;	// 0x101c
        u32 ocxo_controls;      // 0x1020
        u32 ocxo_error;         // 0x1024
        u32 uplink_1pps_delay;  // 0x1028
        u32 external_1pps_delay; //0x102c
        u32 gps_1pps_delay;     //0x1030
        u32 fanout_up_loss;     //0x1034
        u32 fanout_missing_delay_error; //0x1038
        u32 leaps_and_error;    //0x103c
}LPTC_DIAG_INFO;

//complete lptc register space
typedef struct LPTC_REGISTER_SPACE {
    LPTC_REGISTER lptc_registers;   //ends at 0xc0
    u64 unused[0x18];   //another 0xc0 bytes gets to offset 0x180
    LPTC_OBF_REGISTER obf_registers; //starts at 0x180, ends at 0x1a4
    u32 unused2[0x392];  //another 0xe48 bytes gets to 0x1000.
    LPTC_DIAG_INFO diag_info;
} LPTC_REGISTER_SPACE;

// Slot configuration register bits
// Bit 0-7 = Frequency 
#define LPTC_SCR_CLK_ENABLE		    0x100       // Bit 8 : Enable clock signal
#define LPTC_SCR_CLK_INVERT		    0x200       // Bit 9 : Inverted clock signal
#define LPTC_SCR_CLK_1PPS           0x400       // Bit 10: Start clock at next second boundary
#define LPTC_SCR_IDLE_TRANS         0x800       // Bit 11: Start clock at next transition from idle
#define LPTC_SCR_IDLE_HIGH		    0x1000      // Bit 12: Full Idle clock line high
//                                             Bit 13-15: Reserved
#define LPTC_SCR_LVDS               0x10000     // Bit 16: Use LVDS clock lines
#define LPTC_SCR_ADC_DT_ENABLE		0x20000 // Bit 17: Enable ADC Duotone on last ADC chan
#define LPTC_SCR_DAC_DT_ENABLE		0x40000 // Bit 18: Enable DAC Duotone on last -1  ADC chan

 #define LPTC_USEC_CONVERT		0.00023283064365  // = 10^6 / 2^32

#define LPTC_SCR_ADC_SET		    (LPTC_SCR_CLK_ENABLE | LPTC_SCR_CLK_INVERT | LPTC_SCR_IDLE_HIGH)
#define LPTC_SCR_DAC_SET		    (LPTC_SCR_CLK_ENABLE | LPTC_SCR_IDLE_HIGH)

// LPTC Status register at 0x0008
#define LPTC_STATUS_OK 			    0x80000000  // Bit 31
#define LPTC_STATUS_ROOT_NODE 		0x40000000  // Bit 30
#define LPTC_STATUS_SUP_FANOUT 		0x20000000  // Bit 29
#define LPTC_STATUS_UPLINK_OK 		0x10000000  // Bit 28
#define LPTC_STATUS_UPLINK_LOSS 	0x8000000   // Bit 27
#define LPTC_STATUS_OCXO_LOCK	 	0x4000000   // Bit 26
#define LPTC_STATUS_GPS_LOCK	 	0x2000000   // Bit 25
#define LPTC_STATUS_VCXO_VOOR	 	0x1000000   // Bit 24
#define LPTC_STATUS_UTC_TIME	 	0x800000    // Bit 23
#define LPTC_STATUS_LSEC_DECODE	 	0x400000    // Bit 22
#define LPTC_STATUS_LSEC_SUB_PEND	0x200000    // Bit 21
#define LPTC_STATUS_LSEC_ADD_PEND	0x100000    // Bit 20
#define LPTC_STATUS_LEAP_SEC		0xff00

// LPTC Backplane status register at 0x0018
#define LPTC_BPS_BP_PRESENT		0x200
#define LPTC_BPS_BP_REV			0x18
#define LPTC_BPS_CLK_ACTIVE		0x1
#define LPTC_BPS_CLK_RUN		0x2
#define LPTC_BPS_WD_MON			0x4

#define LPTC_CMD_STOP_CLK_ALL		0x0
#define LPTC_CMD_START_CLK_ALL		0xc
#define LPTC_ENABLE_DAC_DUO		0x4000
// Slot Clock Speeds
#define LPTC_SCR_CLK_64K    0x10

#define LIGO_RCVR			0x3
#define LPTC_IOC_SLOTS      10
#define LPTC_SLOT_ENABLE      1
#define LPTC_SLOT_DISABLE      0

#endif /* LIGO_PCIE_TIMING_CARD_HEADER_H */
