#ifndef LIGO_IOP_DAC_FUNCTIONS_H
#define LIGO_IOP_DAC_FUNCTIONS_H

#ifdef __cplusplus
extern "C" {
#endif

int iop_dac_init( int[] );
void iop_dac_preload( int );
int iop_dac_write( void  );
int check_dac_buffers( int cardNum, int report_all_faults );

#ifdef __cplusplus
}
#endif


#endif //LIGO_IOP_DAC_FUNCTIONS_H
