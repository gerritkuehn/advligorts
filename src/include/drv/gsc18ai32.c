///	\file gsc18ai32.c
///	\brief File contains the initialization routine and various register
/// read/write
///<		operations for the General Standards 18bit, 32 channel ADC
///< modules. \n

#include "gsc_adc_common.h"
#include "gsc18ai32.h"
#include "ioremap_selection.h"
#include "drv/plx_9056.h"
#include "drv/rts-logger.h"

#include <linux/delay.h> //udelay()


// *****************************************************************************
/// \brief Routine to initialize GSC 18bit, 32 channel ADC modules
///     @param[in,out] *pHardware Pointer to global data structure for storing
///     I/O
///<            register mapping information.
///     @param[in] *adcdev PCI address information passed by the mapping code in
///     map.c
///	@return Status from board enable command.
// *****************************************************************************
int
gsc18ai32Init( CDS_HARDWARE* pHardware, struct pci_dev* adcdev )
{
    static unsigned int pci_io_addr; /// @param pci_io_addr Bus address of PCI
                                     /// card I/O register.
    int devNum; /// @param devNum Index into CDS_HARDWARE struct for adding
                /// board info.
    char* _adc_add; /// @param *_adc_add ADC register address space
    int   pedStatus; /// @param pedStatus Status return from call to enable
                   /// device.
    volatile GSA_ADC_REG* adc18Ptr;
    int                   autocal = 0;
    int                   chans = 2;

    /// Get index into CDS_HARDWARE struct based on total number of ADC cards
    /// found by mapping routine in map.c
    devNum = pHardware->adcCount;
    /// Enable the module.
    pedStatus = pci_enable_device( adcdev );
    /// Enable device to be DMA master.
    pci_set_master( adcdev );
    /// Get the PLX chip address
    pedStatus = pci_read_config_dword( adcdev, PCI_BASE_ADDRESS_0, &pci_io_addr );
    if(pedStatus != 0)
        return -1;
    RTSLOG_INFO( "pci0 = 0x%x\n", pci_io_addr );
    /// Map module DMA space directly to computer memory space.
    _adc_add = IOREMAP( (unsigned long)pci_io_addr, 0x200 );
    /// Map the module DMA control registers via PLX chip registers
    adcDma[ devNum ] = (PLX_9056_DMA*)_adc_add;

    /// Get the ADC register address
    pedStatus = pci_read_config_dword( adcdev, PCI_BASE_ADDRESS_2, &pci_io_addr );
    if(pedStatus != 0)
        return -1;
    RTSLOG_INFO( "pci2 = 0x%x\n", pci_io_addr );
    /// Map the module control register so local memory space.
    _adc_add = IOREMAP( (unsigned long)pci_io_addr, 0x200 );
    RTSLOG_INFO( "ADC 18 I/O address=0x%x  0x%lx\n", pci_io_addr, (long)_adc_add );
    /// Set global ptr to control register memory space.
    adc18Ptr = (GSA_ADC_REG*)_adc_add;
    _adcPtr[ devNum ] = (GSA_ADC_REG*)_adc_add;
    RTSLOG_INFO( "ADC  pointer init  at 0x%lx\n", (long)_adcPtr[ devNum ] );

    RTSLOG_INFO( "BCR = 0x%x\n", adc18Ptr->BCR );
    /// Reset the ADC board
    adc18Ptr->BCR |= GSAF_RESET;
    do
    {
    } while ( ( adc18Ptr->BCR & GSAF_RESET ) != 0 );

    /// Write in a sync word
    adc18Ptr->SMUW = 0x0000;
    adc18Ptr->SMLW = 0x0000;

    /// Set sample rate close to 16384Hz
    /// Unit runs with external clock, so this probably not necessary
    adc18Ptr->RAG =
        (unsigned int)( GSC18AI32_OSC_FREQ / ( UNDERSAMPLE * IOP_IO_RATE ) );
    RTSLOG_INFO( "RAG = %d \n", adc18Ptr->RAG );
    RTSLOG_INFO( "BCR = 0x%x\n", adc18Ptr->BCR );
    adc18Ptr->RAG &= ~( GSAF_SAMPLE_START ); //  0x10000

    /// Initiate board calibration
    adc18Ptr->BCR |= GSAF_AUTO_CAL;
    /// Wait for internal calibration to complete.
    do
    {
        autocal++;
        udelay( 100 );
    } while ( ( adc18Ptr->BCR & GSAF_AUTO_CAL ) != 0 ); // 0x2000
    if ( ( adc18Ptr->BCR & GSAF_AUTO_CAL_PASS ) == 0 )
    {
        RTSLOG_INFO( "GSC_18AI32SSC1M : devNum %d : Took %d ms : ADC AUTOCAL FAIL\n", 
                     devNum, autocal/10 );
        autocal = 0;
    }
    else
    {
        RTSLOG_INFO( "GSC_18AI32SSC1M : devNum %d : Took %d ms : ADC AUTOCAL PASS\n", 
                     devNum, autocal/10 );
        autocal = GSAF_AUTO_CAL_PASS;
    }
    // End AutoCal

    adc18Ptr->RAG |= GSAF_SAMPLE_START; // 0x10000
    adc18Ptr->IDBC = ( GSAI_CLEAR_BUFFER | GSAI_THRESHOLD | GSAF_18BIT_DATA );
    // Determine number of channels on board
    // Presently, LIGO has both 8 and 16 channel versions
    pHardware->adcConfig[ devNum ] = adc18Ptr->ASSC;
    chans = ( pHardware->adcConfig[ devNum ] >> 16 ) & 0x3;
    // Set SSC register according to channel count ie all avail channels armed
    switch ( chans )
    {
    case 0:
        pHardware->adcChannels[ devNum ] = 32;
        adc18Ptr->SSC =
            ( GSAF_32_CHANNEL ); // Sets 32 channels and external clock
        break;
    case 1:
        pHardware->adcChannels[ devNum ] = 16;
        adc18Ptr->SSC =
            ( GSAF_16_CHANNEL ); // Sets 16 channels and external clock
        break;
    case 2:
    default:
        pHardware->adcChannels[ devNum ] = 8;
        adc18Ptr->SSC =
            ( GSAF_8_CHANNEL ); // Sets 8 channels and external clock
        break;
    }
#ifdef USE_ADC_CLOCK
    // Only for Cymacs using ADC internal clock as clock source
    adc18Ptr->SSC |= GSAF_USE_RAG_CLK;
#endif
    RTSLOG_INFO( "SSC = 0x%x\n", adc18Ptr->SSC );
    RTSLOG_INFO( "IDBC = 0x%x\n", adc18Ptr->IDBC );
    /// Fill in CDS_HARDWARE structure with ADC information.
    pHardware->pci_adc[ devNum ] =
        (volatile int *)dma_alloc_coherent( &adcdev->dev, 0x2000, &adc_dma_handle[ devNum ], GFP_ATOMIC );
    pHardware->adcType[ devNum ] = GSC_18AI32SSC1M;
    pHardware->adcDuoToneDivisor[ devNum ] = 4;
    pHardware->adcInstance[ devNum ] = pHardware->card_count[ GSC_18AI32SSC1M ];
    pHardware->card_count[ GSC_18AI32SSC1M ] ++;
    pHardware->adcConfig[ devNum ] |= autocal;
    pHardware->adcCount++;
    /// Return board enable status.
    return ( pedStatus );
}
