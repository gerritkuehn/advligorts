#ifndef LIGO_DAC_INFO_IOC2_H
#define LIGO_DAC_INFO_IOC2_H

#include "portableInline.h"

#include "cds_types.h" //dacInfo_t
#include "controller.h" //cdsPciModules


#ifdef __cplusplus
extern "C" {
#endif


LIGO_INLINE int
dac_status_update( dacInfo_t* dacinfo )
{
    int ii = 0;
    int status = 0;
    int jj = 0;
    int kk = 0;
    int tmpval = 0;

    for ( jj = 0; jj < cdsPciModules.dacCount; jj++ )
    {
        kk = cdsPciModules.dacSlot[ jj ];

        tmpval = pLocalEpics->epicsOutput.statDac[ jj ];
        if ( cdsPciModules.dacType[ jj ] == GSC_16AO16 )
        {
            // 16AO16 cards don't use AIC WD, so set it to green
            pLocalEpics->epicsOutput.statSlot[ kk ] |= CARD_DAC_AIC;
        } else {
            if ( tmpval & DAC_WD_BIT )
                pLocalEpics->epicsOutput.statSlot[ kk ] |= CARD_DAC_AIC;
            else
                pLocalEpics->epicsOutput.statSlot[ kk ] &= ~( CARD_DAC_AIC );
        }
        if ( tmpval & DAC_FOUND_BIT )
            pLocalEpics->epicsOutput.statSlot[ kk ] |= CARD_MAPPED;
        else
            pLocalEpics->epicsOutput.statSlot[ kk ] &= ~( CARD_MAPPED );

        if ( tmpval & DAC_AUTOCAL_BIT )
            pLocalEpics->epicsOutput.statSlot[ kk ] |= CARD_AUTOCAL;

        if ( tmpval & DAC_TIMING_BIT )
            pLocalEpics->epicsOutput.statSlot[ kk ] |= CARD_DAC_WD;
        else
            pLocalEpics->epicsOutput.statSlot[ kk ] &= ~( CARD_DAC_WD );
        ;

        if ( tmpval & DAC_FIFO_BIT )
            pLocalEpics->epicsOutput.statSlot[ kk ] |= CARD_TIMING;
        else
            pLocalEpics->epicsOutput.statSlot[ kk ] &= ~( CARD_TIMING );
        ;

        if ( tmpval & DAC_FIFO_EMPTY )
            pLocalEpics->epicsOutput.statSlot[ kk ] |= CARD_DAC_EMPTY;
        else
            pLocalEpics->epicsOutput.statSlot[ kk ] &= ~( CARD_DAC_EMPTY );
        ;

        if ( tmpval & DAC_FIFO_HI_QTR )
            pLocalEpics->epicsOutput.statSlot[ kk ] |= CARD_DAC_HIQTR;
        else
            pLocalEpics->epicsOutput.statSlot[ kk ] &= ~( CARD_DAC_HIQTR );
        ;

        if ( tmpval & DAC_FIFO_FULL )
            pLocalEpics->epicsOutput.statSlot[ kk ] |= CARD_DAC_FULL;
        else
            pLocalEpics->epicsOutput.statSlot[ kk ] &= ~( CARD_DAC_FULL );
        ;

        if ( dacinfo->dacOF[ jj ] )
        {
            pLocalEpics->epicsOutput.statDac[ jj ] &= ~( DAC_OVERFLOW_BIT );
            pLocalEpics->epicsOutput.statSlot[ kk ] &= ~( CARD_OVERRANGE );
            status |= FE_ERROR_OVERFLOW;
        }
        else
        {
            pLocalEpics->epicsOutput.statDac[ jj ] |= DAC_OVERFLOW_BIT;
            pLocalEpics->epicsOutput.statSlot[ kk ] |= CARD_OVERRANGE;
        }
        dacinfo->dacOF[ jj ] = 0;
        if ( dacChanErr[ jj ] )
        {
            pLocalEpics->epicsOutput.statDac[ jj ] &= ~( DAC_TIMING_BIT );
        }
        else
        {
            pLocalEpics->epicsOutput.statDac[ jj ] |= DAC_TIMING_BIT;
        }
        dacChanErr[ jj ] = 0;
        for ( ii = 0; ii < MAX_DAC_CHN_PER_MOD; ii++ )
        {

            if ( pLocalEpics->epicsOutput.overflowDacAcc[ jj ][ ii ] >
                 OVERFLOW_CNTR_LIMIT )
            {
                pLocalEpics->epicsOutput.overflowDacAcc[ jj ][ ii ] = 0;
            }
            pLocalEpics->epicsOutput.overflowDac[ jj ][ ii ] =
                dacinfo->overflowDac[ jj ][ ii ];
            dacinfo->overflowDac[ jj ][ ii ] = 0;
        }
    }
    return status;
}

#ifdef __cplusplus
}
#endif


#endif //LIGO_DAC_INFO_IOC2_H
