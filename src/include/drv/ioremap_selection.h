/*
 * The ioremap call changes names at kernel 5.6.
 *
 * In 5.10 we use ioremap, in older versions we use ioremap_nocache.
 * At least on x86 the impelmentation of these functions is the same.
 */

#ifndef IOREMAP_SELECTION_H
#define IOREMAP_SELECTION_H

#include <linux/version.h> //KERNEL_VERSION

/* Keeping IOREMAP as a macro instead of a static inline function as
 * the kernel has a few similar but different definitions of the argument
 * types, and I will not wade into that here.
 */
#if LINUX_VERSION_CODE < KERNEL_VERSION(5, 6, 0)
#define IOREMAP ioremap_nocache
#else
#define IOREMAP ioremap
#endif

#endif /* IOREMAP_SELECTION_H */
