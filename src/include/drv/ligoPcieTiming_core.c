/// \file ligoPcieTiming_core.c
/// \brief core functions for the LIGO timing card, init and read time
#include "drv/ligoPcieTiming.h"
#include "drv/ioremap_selection.h"
#include "drv/rts-logger.h"

#include <linux/delay.h> //udelay()


/// Convert fractional part of 2^32 Hz LPTC timing counter to microseconds 
/// \param clock_ticks_2_32_hz fractional time as reported by LPTC
/// \return fractional time converted to microseconds
static inline uint32_t
lptc_usec_convert_int(uint32_t clock_ticks_2_32_hz)
{
    return ((uint64_t)clock_ticks_2_32_hz * 1000000) >> 32;
}

int
lptcInit( CDS_HARDWARE* pCds, struct pci_dev* lptcdev )
{
    static unsigned int pci_io_addr;
    int                 status = 0;
    char*               addr;
    volatile LPTC_REGISTER*      lptcPtr;
    uint32_t            usec, sec;
    unsigned int        regval;
    int                 card = 0;
    u64 gpstime;

    card = pCds->card_count[ LPTC ];
    status = pci_enable_device( lptcdev );
    RTSLOG_INFO( "Xilinx enabled status = %d\n", status );
    pci_read_config_dword( lptcdev, PCI_BASE_ADDRESS_0, &pci_io_addr );
    pci_io_addr &= 0xfffffff0;
    addr = (char*)IOREMAP( (unsigned long)pci_io_addr, 0x2000 );
    RTSLOG_INFO( "Xilinx mapped  = 0x%x   0x%p\n", pci_io_addr, addr );
    pCds->lptc[ card ] = (unsigned int*)addr;

    if ( card == 0 )
    {
        pCds->gps = (unsigned int*)addr;
        pCds->gpsType = LIGO_RCVR;
    }

    lptcPtr = (LPTC_REGISTER*)addr;

    gpstime = lptcPtr->gps_time;
    //usec = (gpstime & 0xffffffff) * LPTC_USEC_CONVERT;
    usec = lptc_usec_convert_int((uint32_t)(gpstime & 0xffffffff));
    sec = (gpstime >> 32) & 0xffffffff;
    RTSLOG_INFO( "Xilinx time1  = %u   %u\n", sec, usec );
    udelay(1000);
    gpstime = lptcPtr->gps_time;
    //usec = (gpstime & 0xffffffff) * LPTC_USEC_CONVERT;
    usec = lptc_usec_convert_int((int32_t)(gpstime & 0xffffffff));
    sec = (gpstime >> 32) & 0xffffffff;
    RTSLOG_INFO( "Xilinx time2  = %u   %u\n", sec, usec );

    RTSLOG_INFO( "Xilinx status  = 0x%x  \n", lptcPtr->status );
    RTSLOG_INFO( "Xilinx sw revision  = 0x%x  \n", lptcPtr->revision );
    RTSLOG_INFO( "Xilinx bp config  = 0x%x  \n", lptcPtr->bp_config );
    RTSLOG_INFO( "Xilinx bp status  = 0x%x  \n", lptcPtr->bp_status );

    regval = lptcPtr->status;
    if ( regval & LPTC_STATUS_OK )
        RTSLOG_INFO( "LPTC Status = OK\n" );
    else
        RTSLOG_ERROR( "LPTC Status = BAD \n" );
    if ( regval & LPTC_STATUS_UPLINK_OK )
        RTSLOG_INFO( "LPTC Uplink = OK\n" );
    else
        RTSLOG_ERROR( "LPTC Uplink = BAD \n" );
    RTSLOG_INFO( "LPTC Leap Seconds = %d\n",
            ( ( regval & LPTC_STATUS_LEAP_SEC ) >> 8 ) );
    RTSLOG_INFO( "LPTC Leap Seconds = 0x%x\n",
            ( ( regval & LPTC_STATUS_LEAP_SEC ) ) );

    lptcPtr->bp_config = LPTC_CMD_STOP_CLK_ALL;
    msleep( 10 );
    regval = lptcPtr->bp_status;
    if ( regval & LPTC_BPS_BP_PRESENT )
        RTSLOG_INFO( "LPTC backplane present = OK\n" );
    else
        RTSLOG_ERROR( "LPTC BACKPLANE IS NOT PRESENT \n" );
    pCds->card_count[ LPTC ]++;
    return 0;
}

int
lptc_get_gps_time( CDS_HARDWARE* pCds, uint32_t* sec, uint32_t* usec )
{
    volatile u64 regval;

    volatile LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ 0 ];
    regval = lptcPtr->gps_time;
    //*usec = (regval & 0xffffffff) * LPTC_USEC_CONVERT;
    *sec = (regval >> 32) & 0xffffffff;
    *usec = lptc_usec_convert_int((uint32_t)(regval & 0xffffffff));
    return 0;
}

unsigned int
lptc_get_gps_usec( CDS_HARDWARE* pCds )
{
    uint32_t timeval;
    volatile u64 regval;

    volatile LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ 0 ];
    regval = lptcPtr->gps_time;
    //timeval = (regval & 0xffffffff) * LPTC_USEC_CONVERT;
    timeval = lptc_usec_convert_int((uint32_t)(regval & 0xffffffff));
    return timeval;
}

unsigned int
lptc_get_gps_sec( CDS_HARDWARE* pCds )
{
    volatile u64 regval;
    unsigned int sec;

    volatile LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ 0 ];
    regval = lptcPtr->gps_time;
    sec = (regval >> 32) & 0xffffffff;
    return sec;
}


int
lptc_get_lptc_status( CDS_HARDWARE* pCds )
{
    volatile LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ 0 ];
    return lptcPtr->status;
}

int
lptc_get_bp_status( CDS_HARDWARE* pCds )
{
    volatile LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ 0 ];
    return lptcPtr->bp_status;
}

int
lptc_get_bp_config( CDS_HARDWARE* pCds )
{
    volatile LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ 0 ];
    return lptcPtr->bp_config;
}

int
lptc_get_slot_config( CDS_HARDWARE* pCds, int slot )
{
    volatile LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ 0 ];
    return ( lptcPtr->slot_info[ slot ].config & 0xffffff );
}

int
lptc_get_slot_status( CDS_HARDWARE* pCds, int slot )
{
    volatile LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ 0 ];
    return ( lptcPtr->slot_info[ slot ].status & 0xffffff );
}

int
lptc_get_slot_phase( CDS_HARDWARE* pCds, int slot )
{
    volatile LPTC_REGISTER* lptcPtr = (LPTC_REGISTER*)pCds->lptc[ 0 ];
    return ( lptcPtr->slot_info[ slot ].phase);
}

// diagnostics
int
lptc_get_board_id( CDS_HARDWARE* pCds )
{
    volatile LPTC_REGISTER_SPACE* lptcPtr = (LPTC_REGISTER_SPACE *)pCds->lptc[ 0 ];
    return lptcPtr->diag_info.board_id;
}

int
lptc_get_board_sn( CDS_HARDWARE* pCds )
{
    volatile LPTC_REGISTER_SPACE* lptcPtr = (LPTC_REGISTER_SPACE *)pCds->lptc[ 0 ];
    return lptcPtr->diag_info.board_sn;
}

int
lptc_get_software_id( CDS_HARDWARE* pCds )
{
    volatile LPTC_REGISTER_SPACE* lptcPtr = (LPTC_REGISTER_SPACE *)pCds->lptc[ 0 ];
    return lptcPtr->diag_info.sftware_id;
}

int
lptc_get_software_rev( CDS_HARDWARE* pCds )
{
    volatile LPTC_REGISTER_SPACE* lptcPtr = (LPTC_REGISTER_SPACE *)pCds->lptc[ 0 ];
    return lptcPtr->diag_info.sftware_rev;
}

// an alternate path to get GPS seconds out through diagnostics registers
int
lptc_get_gps_sec_diag( CDS_HARDWARE* pCds )
{
    volatile LPTC_REGISTER_SPACE* lptcPtr = (LPTC_REGISTER_SPACE *)pCds->lptc[ 0 ];
    return lptcPtr->diag_info.gps_seconds;
}

int
lptc_get_mod_address( CDS_HARDWARE* pCds )
{
    volatile LPTC_REGISTER_SPACE* lptcPtr = (LPTC_REGISTER_SPACE *)pCds->lptc[ 0 ];
    return lptcPtr->diag_info.mod_address;
}

//diagnostic alternates for backplane registers
int
lptc_get_board_status( CDS_HARDWARE* pCds )
{
    volatile LPTC_REGISTER_SPACE* lptcPtr = (LPTC_REGISTER_SPACE *)pCds->lptc[ 0 ];
    return lptcPtr->diag_info.board_status;
}

int
lptc_get_board_config( CDS_HARDWARE* pCds )
{
    volatile LPTC_REGISTER_SPACE* lptcPtr = (LPTC_REGISTER_SPACE *)pCds->lptc[ 0 ];
    return lptcPtr->diag_info.board_config;
}


int
lptc_get_ocxo_controls( CDS_HARDWARE* pCds )
{
    volatile LPTC_REGISTER_SPACE* lptcPtr = (LPTC_REGISTER_SPACE *)pCds->lptc[ 0 ];
    return lptcPtr->diag_info.ocxo_controls;
}

int
lptc_get_ocxo_error( CDS_HARDWARE* pCds )
{
    volatile LPTC_REGISTER_SPACE* lptcPtr = (LPTC_REGISTER_SPACE *)pCds->lptc[ 0 ];
    return lptcPtr->diag_info.ocxo_error;
}

int
lptc_get_uplink_1pps_delay( CDS_HARDWARE* pCds )
{
    volatile LPTC_REGISTER_SPACE* lptcPtr = (LPTC_REGISTER_SPACE *)pCds->lptc[ 0 ];
    return lptcPtr->diag_info.uplink_1pps_delay;
}

int
lptc_get_external_1pps_delay( CDS_HARDWARE* pCds )
{
    volatile LPTC_REGISTER_SPACE* lptcPtr = (LPTC_REGISTER_SPACE *)pCds->lptc[ 0 ];
    return lptcPtr->diag_info.external_1pps_delay;
}

int
lptc_get_gps_1pps_delay( CDS_HARDWARE* pCds )
{
    volatile LPTC_REGISTER_SPACE* lptcPtr = (LPTC_REGISTER_SPACE *)pCds->lptc[ 0 ];
    return lptcPtr->diag_info.gps_1pps_delay;
}

int
lptc_get_fanout_up_loss( CDS_HARDWARE* pCds )
{
    volatile LPTC_REGISTER_SPACE* lptcPtr = (LPTC_REGISTER_SPACE*)pCds->lptc[ 0 ];
    return lptcPtr->diag_info.external_1pps_delay;
}

int
lptc_get_fanout_missing_delay_error( CDS_HARDWARE* pCds )
{
    volatile LPTC_REGISTER_SPACE* lptcPtr = (LPTC_REGISTER_SPACE*)pCds->lptc[ 0 ];
    return lptcPtr->diag_info.fanout_missing_delay_error;
}

int
lptc_get_leaps_and_error( CDS_HARDWARE* pCds )
{
    volatile LPTC_REGISTER_SPACE* lptcPtr = (LPTC_REGISTER_SPACE*)pCds->lptc[ 0 ];
    return lptcPtr->diag_info.leaps_and_error;
}

// 'On Board Features' (LIGO-T2000406 3.9)
int
lptc_get_brd_synch_factors( CDS_HARDWARE* pCds )
{
    volatile LPTC_REGISTER_SPACE* lptcPtr = (LPTC_REGISTER_SPACE*)pCds->lptc[ 0 ];
    return lptcPtr->obf_registers.brd_config;
}

int
lptc_get_xadc_config( CDS_HARDWARE* pCds )
{
    volatile LPTC_REGISTER_SPACE* lptcPtr = (LPTC_REGISTER_SPACE*)pCds->lptc[ 0 ];
    return lptcPtr->obf_registers.xadc_config;
}

int
lptc_get_board_and_powersupply_status( CDS_HARDWARE* pCds )
{
    volatile LPTC_REGISTER_SPACE* lptcPtr = (LPTC_REGISTER_SPACE*)pCds->lptc[ 0 ];
    return lptcPtr->obf_registers.bps_status;
}

int
lptc_get_xadc_status( CDS_HARDWARE* pCds )
{
    volatile LPTC_REGISTER_SPACE* lptcPtr = (LPTC_REGISTER_SPACE*)pCds->lptc[ 0 ];
    return lptcPtr->obf_registers.xadc_status;
}

//temperature from 0x0190 offset
int
lptc_get_temp( CDS_HARDWARE* pCds )
{
    volatile LPTC_REGISTER_SPACE* lptcPtr = (LPTC_REGISTER_SPACE*)pCds->lptc[ 0 ];
    return lptcPtr->obf_registers.tips1 & 0xffff;
}

//internal power values from 0x0190 and 0x0194.
void
lptc_get_internal_pwr( CDS_HARDWARE* pCds, u32 *output )
{
    volatile LPTC_REGISTER_SPACE* lptcPtr = (LPTC_REGISTER_SPACE*)pCds->lptc[ 0 ];
    output[0] = lptcPtr->obf_registers.tips1 & 0xffff0000;
    output[1] = lptcPtr->obf_registers.ips2;
}

//output should be a preallocated array the same size as
// LPTC_OBF_REGISTER:EPS
void lptc_get_external_pwr( CDS_HARDWARE* pCds, u32 *output)
{
    int i;
    volatile LPTC_REGISTER_SPACE* lptcPtr = (LPTC_REGISTER_SPACE*)pCds->lptc[ 0 ];
    if(NULL != output)
    {
        for(i = 0; i < LPTC_OBF_EPS_REGS; ++i)
        {
            output[i] = lptcPtr->obf_registers.eps[i];
        }
    }
}

