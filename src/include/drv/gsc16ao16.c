///     \file gsc16ao16.c
///     \brief File contains the initialization routine and various register
///     read/write
///<            operations for the General Standards 16bit, 16 channel DAC
///<            modules. \n
///< For board info, see
///<    <a
///<    href="http://www.generalstandards.com/view-products2.php?BD_family=16ao16">GSC
///<    16AO16 Manual</a>

#include "gsc_dac_common.h"
#include "gsc16ao16.h"
#include "ioremap_selection.h"
#include "drv/plx_9056.h"
#include "drv/map.h"
#include "drv/rts-logger.h"


// *****************************************************************************
/// \brief Routine to initialize GSC 16AO16 DAC modules.
///     @param[in,out] *pHardware Pointer to global data structure for storing
///     I/O
///<            register mapping information.
///     @param[in] *dacdev PCI address information passed by the mapping code in
///     map.c
///     @return Status from board enable command.
// *****************************************************************************
int
gsc16ao16Init( CDS_HARDWARE* pHardware, struct pci_dev* dacdev )
{
    int devNum; /// @param devNum Index into CDS_HARDWARE struct for adding
                /// board info.
    char* _dac_add; /// @param *_dac_add DAC register address space
    static unsigned int pci_io_addr; /// @param pci_io_addr Bus address of PCI
                                     /// card I/O register.
    int pedStatus; /// @param pedStatus Status return from call to enable
                   /// device.

    /// Get index into CDS_HARDWARE struct based on total number of DAC cards
    /// found by mapping routine in map.c
    devNum = pHardware->dacCount;
    /// Enable the device, PCI required
    pedStatus = pci_enable_device( dacdev );
    /// Register module as Master capable, required for DMA
    pci_set_master( dacdev );
    /// Get the PLX chip PCI address, it is advertised at address 0
    pedStatus = pci_read_config_dword( dacdev, PCI_BASE_ADDRESS_0, &pci_io_addr );
    if(pedStatus != 0)
        return -1;
    RTSLOG_INFO( "pci0 = 0x%x\n", pci_io_addr );
    _dac_add = IOREMAP( (unsigned long)pci_io_addr, 0x200 );
    /// Set up a pointer to DMA registers on PLX chip
    dacDma[ devNum ] = (PLX_9056_DMA*)_dac_add;

    /// Get the DAC register address
    pedStatus = pci_read_config_dword( dacdev, PCI_BASE_ADDRESS_2, &pci_io_addr );
    if(pedStatus != 0)
        return -1;
    RTSLOG_INFO( "dac pci2 = 0x%x\n", pci_io_addr );
    _dac_add = IOREMAP( (unsigned long)pci_io_addr, 0x200 );
    RTSLOG_INFO( "DAC I/O address=0x%x  0x%lx\n", pci_io_addr, (long)_dac_add );
    _dacPtr[ devNum ] = (GSC_DAC_REG*)_dac_add;

    RTSLOG_INFO( "DAC BCR = 0x%x\n", _dacPtr[ devNum ]->BCR );
    /// Reset the DAC board and wait for it to finish (3msec)
    _dacPtr[ devNum ]->BCR |= GSAO_RESET;

    do
    {
    } while ( ( _dacPtr[ devNum ]->BCR & GSAO_RESET ) != 0 );

    /// Enable 2s complement by clearing offset binary bit
    _dacPtr[ devNum ]->BCR = ( GSAO_OUT_RANGE_10 | GSAO_SIMULT_OUT );
    RTSLOG_INFO( "DAC BCR after init = 0x%x\n", _dacPtr[ devNum ]->BCR );
    RTSLOG_INFO( "DAC CSR = 0x%x\n", _dacPtr[ devNum ]->CSR_DIO );
    RTSLOG_INFO( "DAC SRR = 0x%x\n", _dacPtr[ devNum ]->SAMPLE_RATE );

    /// Set DAC FIFO buffer size. Set to match DAC timing diagnostics.
    _dacPtr[ devNum ]->BOR = GSAO_FIFO_SIZE;
#ifndef USE_ADC_CLOCK
    _dacPtr[ devNum ]->BOR |= GSAO_EXTERN_CLK;
#endif
    RTSLOG_INFO( "DAC BOR = 0x%x\n", _dacPtr[ devNum ]->BOR );
    pHardware->pci_dac[ devNum ] =
        (volatile int *)dma_alloc_coherent( &dacdev->dev, 0x200, &dac_dma_handle[ devNum ], GFP_ATOMIC );
    pHardware->dacAcr[ devNum ] = (int)( _dacPtr[ devNum ]->ASSC );
    pHardware->dacType[ devNum ] = GSC_16AO16;
    pHardware->dacDuoToneMultiplier[ devNum ] = 1;
    pHardware->dacCount++;
    pHardware->dacInstance[ devNum ] =  pHardware->card_count[ GSC_16AO16 ];
    pHardware->card_count[ GSC_16AO16 ] ++;

    /// Call patch in map.c needed to properly write to native PCIe module
    /// version of 16AO16
    set_8111_prefetch( dacdev );
    return ( pedStatus );
}

//
// *****************************************************************************
/// \brief Diagnostic to check number of samples in DAC FIFO.
///     @param[in] numDac ID number of board to be accessed.
///	@return DAC FIFO size information.
// *****************************************************************************
int
gsc16ao16CheckDacBuffer( int numDac )
{
    int dataCount;
    dataCount = ( _dacPtr[ numDac ]->BOR >> 12 ) & 0xf;
    return ( dataCount );
}

// *****************************************************************************
/// \brief Clear DAC FIFO.
///     @param[in] numDac ID number of board to be accessed.
// *****************************************************************************
void
gsc16ao16ClearDacBuffer( int numDac )
{
    _dacPtr[ numDac ]->BOR |= GSAO_CLR_BUFFER;
}

