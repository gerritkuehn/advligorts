#ifndef LIGO_ACCESIIRO16_H
#define LIGO_ACCESIIRO16_H

#include "cdsHardware.h"

#include <linux/pci.h>

/* ACCESS PCI-IIRO-16 isolated digital input and output (16 channels) */
#define ACC_IIRO_TID16 0x0f09
#define ACC_IIRO_TID16_OLD 0x0f08

#define IIRO_DIO16_INPUT  0x1
#define IIRO_DIO16_OUTPUT 0x0

#ifdef __cplusplus
extern "C" {
#endif

int accesIiro16Init( CDS_HARDWARE* pHardware, struct pci_dev* diodev );
unsigned int accesIiro16ReadInputRegister( CDS_HARDWARE* pHardware, int modNum );
void accesIiro16WriteOutputRegister( CDS_HARDWARE* pHardware, int modNum, int data );

#ifdef __cplusplus
}
#endif


#endif //LIGO_ACCESIIRO16_H
