///     \file gsc20ao8.c
///     \brief File contains the initialization routine and various register
///     read/write
///<            operations for the General Standards 20bit, 8 channel DAC
///<            modules. \n
///< For board info, see
///<    <a
///<    href="http://www.generalstandards.com/view-products2.php?BD_family=18ao8">GSC
///<    18AO8 Manual</a>

#include "gsc_dac_common.h"
#include "gsc20ao8.h"
#include "ioremap_selection.h"
#include "drv/rts-logger.h"
#include "drv/plx_9056.h"
#include "drv/map.h"

#include <linux/delay.h> //udelay()


// *****************************************************************************
/// \brief Routine to initialize GSC 20AO8 DAC modules.
///     @param[in,out] *pHardware Pointer to global data structure for storing
///     I/O
///<            register mapping information.
///     @param[in] *dacdev PCI address information passed by the mapping code in
///     map.c
///     @return Status from board enable command.
// *****************************************************************************
int
gsc20ao8Init( CDS_HARDWARE* pHardware, struct pci_dev* dacdev )
{
    int devNum; /// @param devNum Index into CDS_HARDWARE struct for adding
                /// board info.
    char* _dac_add; /// @param *_dac_add DAC register address space
    static unsigned int pci_io_addr; /// @param pci_io_addr Bus address of PCI
                                     /// card I/O register.
    int pedStatus; /// @param pedStatus Status return from call to enable
                   /// device.
    volatile GSC_DAC_REG*
        dac20bitPtr; /// @param *dac20bitPtr Pointer to DAC control registers.
    int timer = 0;

    /// Get index into CDS_HARDWARE struct based on total number of DAC cards
    /// found by mapping routine in map.c
    devNum = pHardware->dacCount;
    /// Enable the device, PCI required
    pedStatus = pci_enable_device( dacdev );
    /// Register module as Master capable, required for DMA
    pci_set_master( dacdev );
    /// Get the PLX chip PCI address, it is advertised at address 0
    pedStatus = pci_read_config_dword( dacdev, PCI_BASE_ADDRESS_0, &pci_io_addr );
    if(pedStatus != 0)
        return -1;
    RTSLOG_INFO( "pci0 = 0x%x\n", pci_io_addr );
    _dac_add = IOREMAP( (unsigned long)pci_io_addr, 0x200 );
    /// Set up a pointer to DMA registers on PLX chip
    dacDma[ devNum ] = (PLX_9056_DMA*)_dac_add;

    /// Get the DAC register address
    pedStatus = pci_read_config_dword( dacdev, PCI_BASE_ADDRESS_2, &pci_io_addr );
    if(pedStatus != 0)
        return -1;
    // Send some info to dmesg
    RTSLOG_INFO( "dac pci2 = 0x%x\n", pci_io_addr );
    _dac_add = IOREMAP( (unsigned long)pci_io_addr, 0x200 );
    // Send some info to dmesg
    RTSLOG_INFO( "DAC I/O address=0x%x  0x%lx\n", pci_io_addr, (long)_dac_add );

    dac20bitPtr = (GSC_DAC_REG*)_dac_add;
    _dacPtr[ devNum ] = (GSC_DAC_REG*)_dac_add;

    // Send some info to dmesg
    RTSLOG_INFO( "DAC BCR = 0x%x\n", dac20bitPtr->BCR );
    /// Reset the DAC board and wait for it to finish (3msec)

    dac20bitPtr->BCR |= GSAO_20BIT_RESET;

    timer = 6000;
    do
    {
        udelay( 1000 );
        timer -= 1;
    } while ( ( dac20bitPtr->BCR & GSAO_20BIT_RESET ) != 0 && timer > 0 &&
              dac20bitPtr->PRIMARY_STATUS == 1 );

    // RTSLOG_INFO("DAC PSR after init = 0x%x and timer =
    // %d\n",dac20bitPtr->PRIMARY_STATUS,timer);

    /// Enable 2s complement by clearing offset binary bit
    dac20bitPtr->BCR &= ~GSAO_20BIT_OFFSET_BINARY;
    // Set simultaneous outputs
    dac20bitPtr->BCR |= GSAO_20BIT_SIMULT_OUT;
    // Send some info to dmesg
    // RTSLOG_INFO("DAC BCR after init = 0x%x\n",dac20bitPtr->BCR);
    // RTSLOG_INFO("DAC OUTPUT CONFIG = 0x%x\n",dac20bitPtr->OUTPUT_CONFIG);

    /// Enable 10 volt output range
    dac20bitPtr->OUTPUT_CONFIG |= GSAO_20BIT_10VOLT_RANGE;
    // Set differential outputs
    dac20bitPtr->OUTPUT_CONFIG |= GSAO_20BIT_DIFF_OUTS;
    // Enable outputs.
    dac20bitPtr->BCR |= GSAO_20BIT_OUTPUT_ENABLE;
    udelay( 1000 );
    // Set primary status to detect autocal
    RTSLOG_INFO( "DAC PSR = 0x%x\n", dac20bitPtr->PRIMARY_STATUS );
    dac20bitPtr->PRIMARY_STATUS = 2;
    udelay( 1000 );
    RTSLOG_INFO( "DAC PSR after reset = 0x%x\n", dac20bitPtr->PRIMARY_STATUS );

    // Start Calibration
    dac20bitPtr->BCR |= GSAO_20BIT_AUTOCAL_SET;
    // Wait for autocal to complete
    timer = 0;
    pHardware->dacAcr[ devNum ] = 0;
    do
    {
        udelay( 1000 );
        timer += 1;
    } while ( ( dac20bitPtr->BCR & GSAO_20BIT_AUTOCAL_SET ) != 0 );

    RTSLOG_INFO( "DAC after autocal PSR = 0x%x\n", dac20bitPtr->PRIMARY_STATUS );
    if ( dac20bitPtr->BCR & GSAO_20BIT_AUTOCAL_PASS )
    {
        RTSLOG_INFO( "GSC_20AO8 : devNum %d : Took %d ms : DAC AUTOCAL PASS\n", 
                     devNum, timer );
        pHardware->dacAcr[ devNum ] = DAC_CAL_PASS;
    } else {
        RTSLOG_ERROR( "GSC_20AO8 : devNum %d : Took %d ms : DAC AUTOCAL FAIL\n", 
                      devNum, timer );
    }
    RTSLOG_INFO( "DAC PSR = 0x%x\n", dac20bitPtr->PRIMARY_STATUS );

    // If 20bit DAC, need to enable outputs.
    dac20bitPtr->BCR |= GSAO_20BIT_OUTPUT_ENABLE;
    RTSLOG_INFO( "DAC OUTPUT CONFIG after init = 0x%x with BCR = 0x%x\n",
            dac20bitPtr->OUTPUT_CONFIG,
            dac20bitPtr->BCR );

    pHardware->pci_dac[ devNum ] =
        (volatile int *)dma_alloc_coherent( &dacdev->dev, 0x200, &dac_dma_handle[ devNum ], GFP_ATOMIC );
    pHardware->dacAcr[ devNum ] |= ((int)( dac20bitPtr->ASY_CONFIG )  & DAC_ACR_MASK);

    // Return the device type to main code.
    pHardware->dacType[ devNum ] = GSC_20AO8;
    pHardware->dacDuoToneMultiplier[ devNum ] = 8;
    pHardware->dacCount++;
    pHardware->dacInstance[ devNum ] = pHardware->card_count[ GSC_20AO8 ];
    pHardware->card_count[ GSC_20AO8 ] ++;

    /// Call patch in map.c needed to properly write to native PCIe module
    set_8111_prefetch( dacdev );
    return ( pedStatus );
}

