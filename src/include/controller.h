#ifndef LIGO_CONTROLLER_H
#define LIGO_CONTROLLER_H

#include "cds_types.h" //adcInfo_t, dacInfo_t, timing_diag_t
#include "fm10Gen_types.h" //FILT_MOD 
#include "daq_core_defs.h" //DAQ_DCU_SIZE
#include "filter_coeffs.h"

#include "../cds-shmem/cds-shmem.h"
#include "../shmem/shmem_all.h" //TESTPOINT_CFG, AWG_DATA
#include "fe_state_word.h"

extern adcInfo_t     adcinfo;
extern dacInfo_t     dacinfo;
extern timing_diag_t timeinfo;

/// Maintains present cycle count within a one second period.
/// This ticks at the model rate, which is faster than 65536 for fast models
extern int          cycleNum;
extern unsigned int odcStateWord;
/// Value of readback from DAC FIFO size registers; used in diags for FIFO
/// overflow/underflow.
extern int          out_buf_size; // test checking DAC buffer size
extern unsigned int cycle_gps_time; // Time at which ADCs triggered
extern unsigned int cycle_gps_event_time; // Time at which ADCs triggered
extern unsigned int cycle_gps_ns;
extern unsigned int cycle_gps_event_ns;
extern unsigned int gps_receiver_locked; // Lock/unlock flag for GPS time card
/// GPS time in GPS seconds
extern unsigned int     timeSec;
extern unsigned int     timeSecDiag;
extern unsigned int     ipcErrBits;
extern int              cardCountErr;
extern struct rmIpcStr* daqPtr;
extern int dacOF[ MAX_DAC_MODULES ]; /// @param dacOF[]  DAC overrange counters

extern char daqArea[ DAQ_DCU_SIZE ]; // Space allocation for daqLib buffers
extern int  cpuId;

// Initial diag reset flag
extern int initialDiagReset;

// Cache flushing mumbo jumbo suggested by Thomas Gleixner, it is probably
// useless Did not see any effect
extern char fp[ 64 * 1024 ];

#define MX_OK 3



#define ODC_ADC_OVF 0x1
#define ODC_DAC_OVF 0x2
#define ODC_EXC_SET 0x4

#define CPURATE ( cpu_khz / 1000 )
#define ONE_PPS_THRESH 1000
#define SYNC_SRC_NONE 0
#define SYNC_SRC_DOLPHIN 1
#define SYNC_SRC_1PPS 2
#define SYNC_SRC_TDS 4
#define SYNC_SRC_MASTER 8
#define SYNC_SRC_TIMER 16
#define SYNC_SRC_LTC 32
#define SYNC_SRC_INTERNAL 64

#define CPU_TIMER_CNT 10
#define CPU_TIME_CYCLE_START 0
#define CPU_TIME_CYCLE_END 1
#define CPU_TIME_USR_START 4
#define CPU_TIME_USR_END 5
#define CPU_TIME_RDY_ADC 8
#define CPU_TIME_ADC_WAIT 9

// fe_state defs
#define IO_CARD_MAP_ERROR   -9
#define IO_CONFIG_ERROR     -8
#define ADC_TO_ERROR        -7
#define DAC_INIT_ERROR      -6
#define BURT_RESTORE_ERROR  -5
#define CHAN_HOP_ERROR      -4
#define RUN_ON_TIMER        -3
#define DAQ_INIT_ERROR      -2
#define FILT_INIT_ERROR     -1
#define LOADING              0
#define FIND_MODULES         1
#define WAIT_BURT            2
#define LOCKING_CORE         3
#define INIT_ADC_MODS        4
#define INIT_DAC_MODS        5
#define INIT_SYNC            6
#define NORMAL_RUN           7
#define IOC_SLOT_CHK         8

//Constants that control if feCode() is being called for the
//first time (FE_CODE_INIT) or during a normal cyclic run (FE_CODE_PROCESS)
#define FE_CODE_INIT       1
#define FE_CODE_PROCESS    0


// microseconds per IOP cycle, *NOT* microseconds per fastest ADC.
#define USEC_PER_CYCLE (1000000 * UNDERSAMPLE / MODEL_RATE_HZ )
//Max time for user code time, before we trigger a timing error
#define CYCLE_TIME_ALRM (USEC_PER_CYCLE + 5)
//Max time between ADC reads (cycle-to-cycle), before we trigger a timing error
#define CYCLE_TIME_ALRM_HI (USEC_PER_CYCLE + 10)
//Min time tolerate between ADC reads (cycle-to-cycle), before we trigger a timing error
#define CYCLE_TIME_ALRM_LO (USEC_PER_CYCLE - 5)


//DAC Parameters
#if defined(IOP_MODEL)

#if MODEL_RATE_HZ < 32768
#error "An IOP model's rate must be over >=32768 Hz"
#endif

#define DAC_PRELOAD_CNT 0 //All IOPs have a DAC preload of 0

//ADC Parameters
//These values are passed in by the code generator, TODO: per-ADC sample rates
//16 bit ADCs clock rate in form 2^(IOC_CLK_SLOW) (default is 2^16)
#define IOC_CLK_SLOW    SLOW_ADC_SAMPLE_RATE_POW_2
//18 bit ADCs clock rate in form 2^(IOC_CLK_FAST) (default is IOP rate)
#define IOC_CLK_FAST    FAST_ADC_SAMPLE_RATE_POW_2


//Models have history behind their preload count, depended on cycle time
//these were probably chosen based on production models cycle times (why 4k is so big)
//TODO: Could modles pick their own count based on avg cycle time
#elif MODEL_RATE_HZ == 65536
#define DAC_PRELOAD_CNT 1
#elif  MODEL_RATE_HZ == 32768
#define DAC_PRELOAD_CNT 2
#elif  MODEL_RATE_HZ == 16384
#define DAC_PRELOAD_CNT 4
#elif  MODEL_RATE_HZ == 4096
#define DAC_PRELOAD_CNT 16
#elif  MODEL_RATE_HZ == 2048
#define DAC_PRELOAD_CNT 8
#elif  MODEL_RATE_HZ == 256
#define DAC_PRELOAD_CNT 32
#endif //if defined(IOP_MODEL)

//Do some checking to make sure DAC_PRELOAD_CNT was defined (as we have ifs above)
#if !defined(DAC_PRELOAD_CNT)
#if !defined(__KERNEL__)
#error "Userspace models faster than 65536 are not currently supported."
#else
#error "DAC_PRELOAD_CNT not defined, you may have a model rate whos support has not been added to this file"
#endif
#endif



//DAQ Parameters
#define DAQ_RATE (MODEL_RATE_HZ/16)
#define END_OF_DAQ_BLOCK (DAQ_RATE-1)
#define DAQ_CYCLE_CHANGE 120 





// DIAGNOSTIC RETURNS FROM FE
// epicsOutput.diagWord / FEC-<DCUID>_DIAG_WORD values
#define ADC_TIMEOUT_ERR    0x1
#define FE_ADC_HOLD_ERR    0x2
#define FE_PROC_TIME_ERR   0x8
#define TIME_ERR_IRIGB     0x10
#define TIME_ERR_1PPS      0x20
#define TIME_ERR_TDS       0x40


// DAC status bits to EPICS
#define DAC_FOUND_BIT      1
#define DAC_TIMING_BIT     2
#define DAC_OVERFLOW_BIT   4
#define DAC_FIFO_BIT       8
#define DAC_WD_BIT        16
#define DAC_FIFO_EMPTY    32
#define DAC_FIFO_HI_QTR   64
#define DAC_FIFO_FULL    128
#define DAC_AUTOCAL_BIT  256

//DAC WD Parameters
#define DAC_WD_TRIP_SET    5


#define MAX_IRIGB_SKEW  (int)( ((USEC_PER_CYCLE) * 1.6) + (TIME0_DELAY_US*2) )
#define MIN_IRIGB_SKEW (int)( ((USEC_PER_CYCLE) * 0.334) + (TIME0_DELAY_US*2) )
#define MAX_DT_DIAG_VAL 1
#define MIN_DT_DIAG_VAL -1

// HOUSEKEEPING CYCLE DEFS
// 1Hz Jobs triggered by cycleNum count in controller code
#define HKP_READ_SYMCOM_IRIGB 0
#define HKP_READ_TSYNC_IRIBB 1
#define HKP_READ_DIO 10
#define HKP_DT_CALC 16
#define HKP_DAC_DT_SWITCH 17
#define HKP_TIMING_UPDATES 18
#define HKP_DIAG_UPDATES 19
#define HKP_ADC_DAC_STAT_UPDATES 21
#define HKP_CHECK_EXIT 22
#define HKP_MATH_EXCEPTION_CHECK 23
#define HKP_RFM_CHK_CYCLE 300 // ONLY IOP
#define HKP_DAC_WD_CLK 400 // ONLY IOP
#define HKP_DAC_WD_CHK 500 // ONLY IOP
#define HKP_DAC_FIFO_CHK 600 // ONLY IOP
#define HKP_DAC_FIFO_CHK 600 // ONLY IOP
#define HKP_IOC_SLOT_STATUS 1000 // ONLY IOP
#define HKP_IOC_SLOT_CONFIG 1100 // ONLY IOP
#define HKP_IOC_BP_CONFIG 1200 // ONLY IOP
#define HKP_IOC_BP_STATUS 1205 // ONLY IOP
#define HKP_IOC_LPTC_STATUS 1210 // ONLY IOP

// 16Hz Jobs triggered by cycle_16Hz_reset in controller code
#define DAC_EPICS_UPDATES_16HZ 20


#define NUM_SYSTEMS 1
#define MAX_UDELAY 19999


extern char*          build_date ;

extern volatile char* _epics_shm; ///< Ptr to EPICS shared memory area
extern shmem_handle g_epics_shm_handle;

extern volatile TESTPOINT_CFG  *_tp_shm;
extern shmem_handle g_tp_shm_handle;

extern volatile AWG_DATA *_awg_shm;
extern shmem_handle g_awg_shm_handle;

extern volatile char*          _ipc_shm; ///< Ptr to inter-process communication area
extern shmem_handle g_ipc_shm_handle;

extern volatile char*          _daq_shm; ///< Ptr to frame builder comm shared mem area
extern shmem_handle g_daq_shm_handle;

extern volatile char*          _shmipc_shm; ///< Ptr to IOP I/O data to/from User app shared mem area
extern shmem_handle g_shmipc_shm_handle;

extern volatile char*          _io_shm; ///< Ptr to user space I/O area
extern shmem_handle g_io_shm_handle;

extern int            daq_fd; ///< File descriptor to share memory file

extern long                      daqBuffer; // Address for daq dual buffers in daqLib.c
extern CDS_HARDWARE              cdsPciModules; // Structure of PCI hardware addresses
extern volatile IO_MEM_DATA*     ioMemData;
extern volatile IO_MEM_DATA_IOP* ioMemDataIop;
extern volatile int              vmeDone ; // Code kill command
extern volatile int              fe_status_return; // fe code status return to module_exit
extern volatile int              fe_status_return_subcode; // fe code status return to module_exit
extern volatile int              stop_working_threads;

extern int     iop_rfm_valid; //From mbuf kernel module
extern unsigned int cpu_khz; //From linux kernel tsc.c

// Following define string names of above modules
// Used to print info to syslog
#define CDS_CARD_TYPES  16
extern char _cdscardtypename[ CDS_CARD_TYPES ][ 40 ];


/// Testpoints which are not part of filter modules
extern double* testpoint[ GDS_MAX_NFM_TP ];
#ifndef NO_DAQ
extern DAQ_RANGE daq; // Range settings for daqLib.c
extern int       numFb;
extern int       fbStat[ 2 ]; // Status of DAQ backend computer
/// Excitation points which are not part of filter modules
extern double xExc[ GDS_MAX_NFM_EXC ]; // GDS EXC not associated with filter modules
#endif
/// 1/16 sec cycle counters for DAQS
/// This counts 0-((MODEL_RATE_HZ / UNDERSAMPLE) - 1 ), at the MODEL_RATE_HZ tick rate.
/// So this is reset at a 16Hz rate, meaning if you trigger code on a specific cycle of 
/// this counter, it will run every 16 Hz
extern int cycle_16Hz_reset; // Internal cycle counter
/// DAQ cycle counter (0-15)
extern unsigned int daqCycle; // DAQS cycle counter

// Sharded memory discriptors
extern int wfd;
extern int ipc_fd;
extern volatile CDS_EPICS* pLocalEpics; // Local mem ptr to EPICS control data
extern volatile char*      pEpicsDaq; // Local mem ptr to EPICS daq data

// Filter module variables
/// Standard Filter Module Structure
extern FILT_MOD dsp[ NUM_SYSTEMS ]; // SFM structure.
/// Pointer to local memory SFM Structure
extern FILT_MOD* dspPtr[ NUM_SYSTEMS ]; // SFM structure pointer.
/// Pointer to SFM in shared memory.
extern FILT_MOD* pDsp[ NUM_SYSTEMS ]; // Ptr to SFM in shmem.
/// Pointer to filter coeffs local memory.
extern COEF dspCoeff[ NUM_SYSTEMS ]; // Local mem for SFM coeffs.
/// Pointer to filter coeffs shared memory.
extern VME_COEF* pCoeff[ NUM_SYSTEMS ]; // Ptr to SFM coeffs in shmem

// ADC Variables
/// Array of ADC values
#ifdef IOP_MODEL
extern double dWord[ MAX_ADC_MODULES ][ MAX_ADC_CHN_PER_MOD ][ 16 ]; // ADC read values
#else
extern double dWord[ MAX_ADC_MODULES ][ MAX_ADC_CHN_PER_MOD ]; // ADC read values
#endif
/// List of ADC channels used by this app. Used to determine if downsampling
/// required.
extern unsigned int dWordUsed[ MAX_ADC_MODULES ]
                             [ MAX_ADC_CHN_PER_MOD ]; // ADC chans used by app code

// DAC Variables
/// Enables writing of DAC values; Used with DACKILL parts..
extern int iopDacEnable; // Returned by feCode to allow writing values or zeros to DAC
                  // modules
extern int dacChanErr[ MAX_DAC_MODULES ];
#ifdef IOP_MODEL
extern int dacOutBufSize[ MAX_DAC_MODULES ];
#endif
/// Array of DAC output values.
extern double dacOut[ MAX_DAC_MODULES ][ MAX_DAC_CHN_PER_MOD ]; // DAC output values
/// DAC output values returned to EPICS
extern int dacOutEpics[ MAX_DAC_MODULES ]
                      [ MAX_DAC_CHN_PER_MOD ]; // DAC outputs reported back to EPICS
/// DAC channels used by an app.; determines up sampling required.
extern unsigned int dacOutUsed[ MAX_DAC_MODULES ]
                              [ MAX_DAC_CHN_PER_MOD ]; // DAC chans used by app code
/// Array of DAC overflow (overrange) counters.
extern int overflowDac[ MAX_DAC_MODULES ]
                      [ MAX_DAC_CHN_PER_MOD ]; // DAC overflow diagnostics
/// DAC outputs stored as floats, to be picked up as test points
extern double floatDacOut[ 160 ]; // DAC outputs stored as floats, to be picked up as
                           // test points

/// Counter for total ADC/DAC overflows
extern int overflowAcc; // Total ADC/DAC overflow counter

#ifndef IOP_MODEL
// Variables for Digital I/O board values
// DIO board I/O is handled in control (user) applications for timing reasons
// (longer I/O access times)
/// Read value from Acces I/O 24bit module
extern int dioInput[ MAX_DIO_MODULES ];
/// Write value to Acces I/O 24bit module
extern int dioOutput[ MAX_DIO_MODULES ];
/// Last value written to Acces I/O 24bit module
extern int dioOutputHold[ MAX_DIO_MODULES ];

extern int rioInputOutput[ MAX_DIO_MODULES ];
extern int rioOutput[ MAX_DIO_MODULES ];
extern int rioOutputHold[ MAX_DIO_MODULES ];

extern int rioInput1[ MAX_DIO_MODULES ];
extern int rioOutput1[ MAX_DIO_MODULES ];
extern int rioOutputHold1[ MAX_DIO_MODULES ];
extern int rioInputInput[ MAX_DIO_MODULES ];


// Contec 32 bit output modules
/// Read value from Contec 32bit I/O module
extern unsigned int CDO32Input[ MAX_DIO_MODULES ];
/// Write value to Contec 32bit I/O module
extern unsigned int CDO32Output[ MAX_DIO_MODULES ];


#endif

#ifdef OVERSAMPLE

// History buffers for oversampling filters
extern double dHistory[ ( MAX_ADC_MODULES * 32 ) ][ MAX_HISTRY ];
extern double dDacHistory[ ( MAX_DAC_MODULES * 16 ) ][ MAX_HISTRY ];

#else

#define OVERSAMPLE_TIMES 1

#endif //OVERSAMPLE

#endif //LIGO_CONTROLLER_H
