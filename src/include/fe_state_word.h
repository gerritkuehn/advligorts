#ifndef LIGO_FE_STATE_WORD
#define LIGO_FE_STATE_WORD
/// @file fe_state_word.h
/// @brief Contains prototype defs for EPICS state word diagnostic
//         used to signal errors to operator screens 


#ifdef __cplusplus
extern "C" {
#endif

// The following bits define the EPICS STATE_WORD
#define FE_ERROR_TIMING 0x2 // bit 1
#define FE_ERROR_ADC 0x4 // bit 2
#define FE_ERROR_DAC 0x8 // bit 3
#define FE_ERROR_DAQ 0x10 // bit 4
#define FE_ERROR_IPC 0x20 // bit 5
#define FE_ERROR_AWG 0x40 // bit 6
#define FE_ERROR_DAC_ENABLE 0x80 // bit 7
#define FE_ERROR_EXC_SET 0x100 // bit 8
#define FE_ERROR_OVERFLOW 0x200 // bit 9
#define FE_ERROR_CFC 0x400 // bit 10, used by and also defined in skeleton.st
#define FE_ERROR_FLT 0x800 //bit 11, Filter load error
#define FE_ERROR_FPU 0x1000 //bit 12, Floating point unit (FPU) error detected

#ifdef __cplusplus
}
#endif

#endif //LIGO_FE_STATE_WORD

