
#ifndef DAQ_CHECK_H
#define DAQ_CHECK_H

// called by the Model IOC process in skeleton.st to load the DAQ (INI) file.
// makes consistency checks with C structures that define some shared memories
// produces plan for copying data to the DAQ shared memory.
int loadDaqConfigFile( DAQ_INFO_BLOCK*, GDS_INFO_BLOCK*, char*, char*, char* );

#endif //DAQ_CHECK_H