#ifndef LIGO_TIMING_H
#define LIGO_TIMING_H

#include "util/fixed_width_types.h"

#if defined(__KERNEL__)

#include <asm/cpufeature.h>
#include <asm/msr.h> //rdtsc_ordered()
#include <asm/tsc.h> //tsc_khz

#define CYC2NS_SCALE (1000000ULL * 1>>27 / tsc_khz)

//
// We are making these functions statix inline bacause rdtsc_ordered()
// is a static function, and the compiler will complain if we aren't
//

/** @brief Returns a monotonic count of nanoseconds from an unspecified
 *         starting point.
 *
 *  This is the kernelspace implementation and uses the TSC register, which 
 *  counts the number of CPU cycles since it was laster reset. We multiply 
 *  by 1e6 first so we can keep the math fixed point, and factor in the CPU
 *  frequency with tsc_khz. 
 *
 *  TODO: How do we end up with ns?
 *
 *  @return uint64_t A number of ns
 */
static inline uint64_t getMonotonic_ns_utin64( void ) 
{
    return (rdtsc_ordered() * 1000000ULL) / tsc_khz ; 
}

/** @brief Fills the uint64_t * with the start time recorded
 *         when this function is called.
 *
 *  @param start_tsc [out] This is filled with the current time (tsc)
 *
 *  @return None
 */
static inline void timer_start( uint64_t * start_tsc)
{
    *start_tsc = rdtsc_ordered();
}

/** @brief Returns the time elapsed from when the start_tsc
 *         was last filled with timer_start()
 *
 *  @param start_tsc [in] The start time you are using to
 *                       calculate the elapsed time (tsc)
 *
 *  @return The time elapsed from when the start_tsc was
 *          filled to now, in ns
 */
static inline uint64_t timer_tock_ns( uint64_t * start_tsc)
{
    return ((rdtsc_ordered() - *start_tsc) * 1000000ULL ) / tsc_khz ;
}

/** @brief Returns the time elapsed from when the start_tsc
 *         was last filled with timer_start(), and also stores
 *         that result in the start_tsc parameter
 *
 *  @param start_ns [in/out] This is filled with duration (ns)
 *                           between the time stored in start_tsc when first
 *                           passed in and now.
 *
 *  @return Returns the elapsed time from that start time passed in and now
 */

static inline uint64_t timer_end_ns( uint64_t * start_tsc)
{
    *start_tsc = ((rdtsc_ordered() - *start_tsc) * 1000000ULL ) / tsc_khz ;
    return *start_tsc; //This is now the duration in ns
}


#else //

#include <time.h>

/** @brief Returns a monotonic count of nanoseconds from an unspecified
 *         starting point.
 *         
 *  This is the userspace implementation and simply uses the clock_gettime()
 *  function provided by the standard library.
 *
 *  @return uint64_t A number of ns
 */
static inline uint64_t getMonotonic_ns_utin64( void ) 
{
    struct timespec monotime;
    clock_gettime(CLOCK_MONOTONIC_RAW, &monotime);
    return (monotime.tv_sec * 1000000000ULL) + monotime.tv_nsec; 
}

#endif


#endif //LIGO_TIMING_H
