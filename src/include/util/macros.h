#ifndef LIGO_MACROS_HH
#define LIGO_MACROS_HH

#include "util/fixed_width_types.h"

//Returns the number of elements in an array, with a bit of error checking that will
//divide by 0 at compile time if there is an issue
#define COUNT_OF(x) ((sizeof(x)/sizeof(0[x])) / ((size_t)(!(sizeof(x) % sizeof(0[x])))))

#endif //LIGO_MACROS_HH
