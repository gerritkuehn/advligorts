//
// Created by jonathan.hanks on 4/2/22.
//
#include "catch.hpp"
#include "burt_file.hh"
#include <string>
#include <vector>

TEST_CASE( "Test encodeString" )
{
    struct TestCase
    {
        const char* input;
        const char* expected_output;
    };
    std::vector< TestCase > test_cases{
        { nullptr, "\\0" },
        { "", "\\0" },
        { "abc", "abc" },
        { "abc def", "\"abc def\"" },
        { "abc\"d\"ef", "\"abc\"d\"ef\"" },
        { "abc\"def", "\"abc\"def\"" },
        { "abc\ndef", "\"abc\ndef\"" },
        { "abc\tdef", "\"abc\tdef\"" },
    };
    for ( const auto& test_case : test_cases )
    {
        embedded::fixed_string< 18 > src( test_case.input );
        embedded::fixed_string< 20 > dest;
        BURT::encodeString( src, dest );
        REQUIRE( strcmp( test_case.expected_output, dest.c_str( ) ) == 0 );
    }
}
#include <iostream>
TEST_CASE( "Test parseLine" )
{
    struct TestCase
    {
        const char*                input;
        std::vector< std::string > expected_output;
    };
    std::vector< TestCase > test_cases{
        { "", {} },
        { "\\0", { "" } },
        { "abc", { "abc" } },
        { "abc def", { "abc", "def" } },
        { "abc\tdef", { "abc", "def" } },
        /* newline ends the parsing */
        { "abc\ndef", { "abc" } },
        { "0 1 2 3 4 5", { "0", "1", "2", "3", "4", "5" } },
        { "0 1 2 \\0 4 5", { "0", "1", "2", "", "4", "5" } },
        /* shouldn't include the quote in the output */
        { "\"0\" 1 2 3 4 5", { "0", "1", "2", "3", "4", "5" } },
        { "0 1 2 3 4 5 6", { "0", "1", "2", "3", "4", "5" } },
        { "0 1 2 3 4 5 6 7 8 9 10", { "0", "1", "2", "3", "4", "5" } },
        { "0 \" 1 2 3 4 5 \" 6 7 8 9 10",
          { "0", " 1 2 3 4 5 ", "6", "7", "8", "9" } },
        { "0 \" 1\t2\t3 4 5 \" 6 7 8 9 10",
          { "0", " 1\t2\t3 4 5 ", "6", "7", "8", "9" } },
        { "0 \" 1\t2\n\t3 4 5 \" 6 7 8 9 10", { "0", " 1\t2" } },
        { "012345678901234567890", { "01234567890123" } },
        { "012345678901234567890 012345678901234567890",
          { "01234567890123", "01234567890123" } },
        /* Make sure the quote is not in the output */
        { "\"012345678901234567890\" 012345678901234567890",
          { "01234567890123", "01234567890123" } },
        /* truncate, and do not overflow */
        { "012345678901234567890 \"abcdefghijklmnop\" ABC",
          { "01234567890123", "abcdefghijklmn", "ABC" } },
    };
    for ( const auto& test_case : test_cases )
    {
        embedded::fixed_size_vector< embedded::fixed_string< 15 >, 6 > out{ };

        std::cout << test_case.input << "\n";
        int wc = BURT::parseLine( test_case.input, out );
        REQUIRE( wc == test_case.expected_output.size( ) );
        REQUIRE( wc == out.size( ) );
        for ( auto i = 0; i < wc; ++i )
        {
            REQUIRE( out[ i ] == test_case.expected_output[ i ].c_str( ) );
        }
    }
}