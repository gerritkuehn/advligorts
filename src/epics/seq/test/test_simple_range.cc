//
// Created by jonathan.hanks on 11/17/20.
//
#include "catch.hpp"

#include "simple_range.hh"

#include <vector>

TEST_CASE("You can create a simple range from two iterators")
{
    using it_type = typename std::vector<int>::iterator;
    std::vector<int> backing_store{1,2,3,4,5,6,7,8,9,0};

    embedded::range<it_type> rng = embedded::make_range(backing_store.begin() + 3, backing_store.begin() + 3 + 3);
    REQUIRE( *rng.begin() == 4 );
    REQUIRE( *rng.end() == 7 );
}
