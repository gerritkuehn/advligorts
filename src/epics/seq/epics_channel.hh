//
// Created by jonathan.hanks on 9/7/21.
//
// Error codes returned by dbNameToAddr are defined in the
// modules/database/src/ioc/dbStatic/dbStaticLib.h file of the epics-base
// git repository. https://git.launchpad.net/epics-base/tree/

#ifndef DAQD_TRUNK_EPICS_CHANNEL_HH
#define DAQD_TRUNK_EPICS_CHANNEL_HH

#include <cstdint>
#include <type_traits>
#include <stdexcept>

#include "dbAccess.h"

#include "epics_channel_common.hh"

namespace epics
{
    namespace detail
    {
        template < PVType T >
        struct epics_lookup
        {
        };

        template <>
        struct epics_lookup<PVType::UInt16>
        {
            static const auto value = DBF_USHORT;
        };

        template <>
        struct epics_lookup<PVType::Int32>
        {
            static const auto value = DBF_LONG;
        };

        template <>
        struct epics_lookup<PVType::UInt32>
        {
            static const auto value = DBF_ULONG;
        };

        template <>
        struct epics_lookup<PVType::Float64>
        {
            static const auto value = DBF_DOUBLE;
        };

        template <>
        struct epics_lookup<PVType::String>
        {
            static const auto value = DBF_STRING;
        };
    }

    template<PVType DataType>
    class DBEntry {
        static constexpr const dbfType epicsType = detail::epics_lookup<DataType>::value;
    public:
        struct ValidateAddress {};

        using value_type = typename type_lookup<DataType>::type;

        explicit DBEntry(channel_name name)
        {
            dbNameToAddr(name.c_str(), &addr_);
        }

        DBEntry(channel_name name, ValidateAddress tag)
        {
            auto status = dbNameToAddr(name.c_str(), &addr_);
            if ( status )
            {
                throw std::runtime_error("Failure with PV lookup in the EPICS "
                                          "database. If this is the first PV "
                                          "this usually signifies an error "
                                          "with the .cmd passed to iocsh(), "
                                          "or a misconfigured EPICS "
                                          "environment");
            }
        }

        DBEntry(channel_name name, const char* val)
        {
            static_assert(epicsType == DBF_STRING, "Initialization by const char* must be on a string type");
            auto status = dbNameToAddr(name.c_str(), &addr_);
            if ( status )
            {
                throw std::runtime_error("dbNameToAddr(" + std::string(name.c_str()) +
                                         ") : Failed with the return code: " + std::to_string(status) );
            }
            dbPutField(&addr_, epicsType, reinterpret_cast<const void*>(val), 1);
        }


        DBEntry(channel_name name, const value_type& val)
        {
            auto status = dbNameToAddr(name.c_str(), &addr_);
            if ( status )
            {
                throw std::runtime_error("dbNameToAddr(" + std::string(name.c_str()) +
                                         ") : Failed with the return code: " + std::to_string(status) );
            }
            dbPutField(&addr_, epicsType, reinterpret_cast<const void*>(&val), 1);
        }

        void
        set(const value_type& val)
        {
            dbPutField(&addr_, epicsType, reinterpret_cast<const void*>(&val), 1);
        }

        void
        set(const char* val)
        {
            static_assert(epicsType == DBF_STRING, "Setting a const char* must be done on a string type");
            dbPutField(&addr_, epicsType, reinterpret_cast<const void*>(val), 1);
        }

        void
        get(value_type& val)
        {
            long ropts{0};
            long nvals{1};
            dbGetField(&addr_, epicsType, &val, &ropts, &nvals, nullptr);
        }
    private:


        dbAddr addr_{};
    };

}

#endif //DAQD_TRUNK_EPICS_CHANNEL_HH
