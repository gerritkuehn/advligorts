//
// Created by jonathan.hanks on 8/16/18.
//

#ifndef FIXED_SIZE_STRING_FIXED_SIZE_STRING_HH
#define FIXED_SIZE_STRING_FIXED_SIZE_STRING_HH

#include <algorithm>
#include <cstdarg>
#include <cstddef>
#include <cstdio>
#include <cstring>

namespace embedded
{

    /*!
     * @brief A fixed size string abstraction that knows its bounds and does not
     * overflow.
     * @tparam max_size
     * @note this class does not allocation and should not throw an exception
     * ever.
     */
    template < std::size_t max_size >
    class fixed_string
    {
        static_assert( max_size >= 1, "Fixed string capacity must be >= 1" );

    public:
        typedef std::size_t size_type;

        /**
         * @brief the buffer object is a way to get raw access to manipulate the
         * fixed_string.  It looses most protections of the fixed_string class,
         * but ensures that the buffer is null terminated when the buffer is
         * destroyed.
         */
        class buffer
        {
            friend class fixed_string;
            explicit buffer( char* buf ) : buf_{ buf }
            {
            }

        public:
            buffer( buffer&& other ) noexcept = default;
            ~buffer( )
            {
                if ( buf_ )
                {
                    buf_[ fixed_string< max_size >::last_index( ) ] = '\0';
                }
            }
            buffer( const buffer& other ) = delete;
            buffer& operator=( const buffer& other ) = delete;
            buffer& operator=( buffer&& other ) noexcept = default;

            char*
            data( ) noexcept
            {
                return buf_;
            }
            constexpr size_type
            capacity( ) const noexcept
            {
                return max_size;
            }

        private:
            char* buf_;
        };

        fixed_string( ) noexcept : data_( )
        {
            data_[ 0 ] = '\0';
        }
        fixed_string( const fixed_string& ) noexcept = default;

        template < std::size_t other_max_size >
        explicit fixed_string(
            const fixed_string< other_max_size >& other ) noexcept : data_( )
        {
            copy_in_data( other.c_str( ) );
        }
        explicit fixed_string( const char* in ) noexcept : data_( )
        {
            copy_in_data( in );
        }

        fixed_string& operator=( const fixed_string& other ) noexcept = default;

        template < std::size_t other_max_size >
        fixed_string&
        operator=( const fixed_string< other_max_size >& other ) noexcept
        {
            operator=( other.c_str( ) );
            return *this;
        }

        fixed_string&
        operator=( const char* in ) noexcept
        {
            copy_in_data( in );
            return *this;
        }

        fixed_string&
        operator+=( char in ) noexcept
        {
            if ( in && ( remaining( ) > 0 ) )
            {
                size_type cur_size = size( );
                data_[ cur_size ] = in;
                data_[ cur_size + 1 ] = 0;
            }
            return *this;
        }

        fixed_string&
        operator+=( const char* in ) noexcept
        {
            if ( in )
            {
                size_type cur_size = size( );
                size_type in_size = std::min( std::strlen( in ), remaining( ) );
                std::memcpy( data_ + cur_size, in, in_size );
                data_[ cur_size + in_size ] = 0;
            }
            return *this;
        }

        template < std::size_t in_max >
        fixed_string&
        operator+=( const fixed_string< in_max >& in ) noexcept
        {
            size_type in_size = std::min( in.size( ), remaining( ) );
            if ( in_size > 0 )
            {
                size_type cur_size = size( );
                std::memcpy( data_ + cur_size, in.c_str( ), in_size );
                data_[ cur_size + in_size ] = 0;
            }
            return *this;
        }

        explicit operator const char*( ) const noexcept
        {
            return c_str( );
        }

        void
        clear( ) noexcept
        {
            data_[ 0 ] = '\0';
        }

        size_type
        printf( const char* fmt, ... ) noexcept
        {
            if ( !fmt )
            {
                clear( );
                return 0;
            }
            va_list args;
            va_start( args, fmt );
            size_type results = std::vsnprintf( data_, capacity( ), fmt, args );
            va_end( args );
            return results;
        }

        bool
        operator!=( const fixed_string& other ) const noexcept
        {
            return !operator==( other );
        }

        bool
        operator!=( const char* str ) const noexcept
        {
            return !operator==( str );
        }

        template < std::size_t other_max_size >
        bool
        operator!=( const fixed_string< other_max_size >& other ) const noexcept
        {
            return !operator==( other );
        }

        bool
        operator==( const fixed_string& other ) const noexcept
        {
            return std::strcmp( c_str( ), other.c_str( ) ) == 0;
        }

        bool
        operator==( const char* str ) const noexcept
        {
            static const char* dummy = "";
            return std::strcmp( c_str( ), ( str ? str : dummy ) ) == 0;
        }

        template < std::size_t other_max_size >
        bool
        operator==( const fixed_string< other_max_size >& other ) const noexcept
        {
            return std::strcmp( c_str( ), other.c_str( ) ) == 0;
        }

        size_type
        remaining( ) const noexcept
        {
            return last_index( ) - size( );
        }

        constexpr size_type
        capacity( ) const noexcept
        {
            return max_size;
        }

        size_type
        size( ) const noexcept
        {
            return std::strlen( data_ );
        }

        bool
        empty( ) const noexcept
        {
            return size( ) == 0;
        }

        const char*
        c_str( ) const noexcept
        {
            return data_;
        }

        const char*
        data( ) const noexcept
        {
            return data_;
        }

        const char*
        begin( ) const noexcept
        {
            return data_;
        }

        const char*
        end( ) const noexcept
        {
            return data_ + size( );
        }

        void
        pop_back_n( size_type n ) noexcept
        {
            if ( n > 0 )
            {
                auto length = size( );
                auto remove = std::min( length, n );
                data_[ length - remove ] = '\0';
            }
        }

        buffer
        get_buffer( ) noexcept
        {
            return buffer( data_ );
        }

    private:
        void
        copy_in_data( const char* in ) noexcept
        {
            if ( in )
            {
                size_type in_size =
                    std::min( std::strlen( in ), last_index( ) );
                std::memcpy( data_, in, in_size );
                data_[ in_size ] = 0;
            }
            else
            {
                data_[ 0 ] = 0;
            }
        }

        static constexpr size_type
        last_index( ) noexcept
        {
            return max_size - 1;
        }
        char data_[ max_size ];
    };

} // namespace embedded

#endif // FIXED_SIZE_STRING_FIXED_SIZE_STRING_HH
