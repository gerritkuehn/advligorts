#!/usr/bin/env python3

import sys
import os
import argparse

#
#
# Example Usage
# db = EPICS_DB(db_file_path)
# db.print_enum_pvs()
# print (f"is_enum: {db.is_enum('X1:FEC-10_SDF_MON_ALL')}, default: {db.get_default_value('X1:FEC-10_SDF_MON_ALL')} ")
#
#
#


class EPICS_DB:
    def __init__(self, db_file_path):

        chan_to_enum = {}
        self.enum_pvs = {}
        self.bio_val_to_num = {'ZNAM': 0, 'ONAM': 1}
        self.mbmio_val_to_num = {'ZRST':0, 'ONST':1, 'TWST':2, 'THST':3, 'FRST':4, 'FVST':5, 'SXST':6,
                                 'SVST':7, 'EIST':8, 'NIST':9, 'TEST':10, 'ELST':11, 'TVST':12, 'TTST':13,
                                 'FTST':14, 'FFST':15}

        #These are the generated channel postfix values associated with filter modules, 
        #we don't include the individual filter stage names in this list
        self.filter_req_fields = {"_INMON":0x1, "_EXCMON":0x2, "_TRAMP":0x4, "_OFFSET":0x8, "_GAIN":0x10, "_LIMIT":0x20, "_OUTMON":0x40, 
                                  "_OUT16":0x80, "_OUTPUT":0x100, "_SW1":0x200, "_SW2":0x400, "_RSET":0x800, "_SW1R":0x1000, "_SW2R":0x2000, "_SW1S":0x4000, 
                                  "_SW2S":0x8000, "_SWSTAT":0x10000, "_SWREQ":0x20000, "_SWMASK":0x40000, "_SWSTR":0x80000}

        self.actual_filter_modules = {}
        possible_filters = {} #Temp storage while we read all channels

        with open(db_file_path) as file:
            lines = file.readlines()
            lines = [line.strip() for line in lines]

            last_chan_name = ""
            in_bracket = False
            line_num = 0

            for line in lines:
                line_num += 1
                if line.startswith("grecord("):
                    pv_type = line.split(',')[0][8:]
                    last_chan_name = line.split('"')[1]
                    chan_to_enum[last_chan_name] = { 'chan_type': pv_type, 'enumerated_values': [None] * 16 }
                    in_bracket = True

                    #Filter module detection
                    postfix = self.find_postfix(last_chan_name)
                    if postfix in self.filter_req_fields:
                        chan_prefix = last_chan_name[:-len(postfix)]
                        if not chan_prefix in possible_filters:
                            possible_filters[ chan_prefix ] = 0 #zero out new possible filter 
                        possible_filters[ chan_prefix ] |= self.filter_req_fields[postfix]


                elif line == "}":
                    in_bracket = False
                    if chan_to_enum[last_chan_name]['enumerated_values'][0] != None:
                        self.enum_pvs[last_chan_name] = chan_to_enum[last_chan_name]
                    last_chan_name = ""
                    pv_type = ""

                if in_bracket and line.startswith("field("):
                    field_type = line.split(",")[0][6:]

                    if pv_type == "bi" or pv_type == "bo":
                        if field_type in self.bio_val_to_num:
                            chan_to_enum[last_chan_name]['enumerated_values'][ self.bio_val_to_num[field_type] ] = line.split('"')[1]

                    elif pv_type == "mbbi" or pv_type == "mbbo":
                        if field_type in self.mbmio_val_to_num:
                            chan_to_enum[last_chan_name]['enumerated_values'][ self.mbmio_val_to_num[field_type] ] = line.split('"')[1]


            #After we are done looping over all vars, find the actual filter modules we have
            for filt_name, found_vars in possible_filters.items():
                if found_vars == 0xFFFFF: #All required vars are present, should be a filter module
                    self.actual_filter_modules[ filt_name ] = 1


    def is_enum(self, chan_name ):
        if chan_name in self.enum_pvs:
            return True
        return False

    def get_default_value(self, chan_name ):
        return self.enum_pvs[chan_name]['enumerated_values'][0]

    def get_all_enum_values(self, chan_name ):
        return self.enum_pvs[chan_name]['enumerated_values']

    def check_enum_value(self, chan_name, value):
        if value in self.enum_pvs[chan_name]['enumerated_values']:
            return True
        return False

    #This method checks a channel name and postfix of that channel, to make sure it ends in the expected postfix
    #and is a channel of an actual filter module
    def isFilterModuleChan(self, channel_name, postfix):
        if channel_name.endswith(postfix) and channel_name[:-len(postfix)] in self.actual_filter_modules:
            return True
        else: 
            return False

    def print_enum_pvs(self):
        for key, item in self.enum_pvs.items():
            print(f"chan: {key}, default: {item['enumerated_values']}")

    def find_postfix(self, channel_name):
        index = channel_name.rfind("_") #Find last '_'
        if index == -1 or index == len(channel_name)-1: #not found or nothing after  
            return None
        return channel_name[index:]



    


def convert_snap(db_file_path, snap_file_path, output_file):

    db = EPICS_DB(db_file_path)
    inBurtHeader = False
    new_snap_file_lines = ""


    with open(snap_file_path) as snap_file :
        lines = snap_file.readlines()
        
        for line in lines:

            if line.startswith('---') and inBurtHeader == True:
                inBurtHeader = False
                new_snap_file_lines += line
                continue

            if line.startswith('---'):
                inBurtHeader = True
                new_snap_file_lines += line
                continue

            if inBurtHeader == True:
                new_snap_file_lines += line
                continue

            split_line = line.split()
            chan_name = split_line[0]
            value = split_line[2]
            if db.is_enum(chan_name):

                if db.check_enum_value(chan_name, value) == False:

                    try:
                        if float(value) >= 0 and float(value) < len(db.get_all_enum_values(chan_name)):
                            print(f"Chan {chan_name}, value should be {db.get_all_enum_values(chan_name)[int(float(value))]}" )
                            new_line = chan_name + " " + split_line[1] + " " + db.get_all_enum_values(chan_name)[int(float(value))] + " " + split_line[3] +"\n"
                            line = new_line
                        else:
                            print("Error: No valid int -> enum mapping found for chan {chan_name}, val '{value}'")
                            exit(1)
                    except ValueError:
                        print("Error: Could not convert the PV snap value to a float, your .snap file may be malformed.")
                        exit(1)


            new_snap_file_lines += line

        if output_file != None:
            f = open(output_file, "w")
            f.write(new_snap_file_lines)
            f.close()


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Build a new safe.snap with numbers replaced with enumerated values')
    parser.add_argument('-epics_db_file', type=str, required=True, help='The filepath of the <model>.db file')
    parser.add_argument('-orig_safe_snap', type=str, required=True, help='The filepath of the safe.snal to convert')
    parser.add_argument('--new_snap_file', type=str, help='The filepath of new snap to write')

    args = parser.parse_args()

    convert_snap(args.epics_db_file, args.orig_safe_snap, args.new_snap_file)













