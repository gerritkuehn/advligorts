cmake_minimum_required(VERSION 3.0)

if(NOT MODEL_NAME)
    message(FATAL_ERROR "MODEL_NAME must be defined when generating the build files with CMake.")
endif()

project (${MODEL_NAME})

option(USE_STDLIB_MATH "This option uses the stdlib's math.h implementation instead of the inline assembly default" OFF)

# We want to use ${MODEL_NAME} for the library name, cmake will complain
# if we pass it in because the exe name matches, remove the "lib"
# prefix, add it back manually and cmake does not complain
set(CMAKE_STATIC_LIBRARY_PREFIX "")

set(LIB_NAME lib${MODEL_NAME})

# Include generated FLAGS, options and INC dirs.
include(${CMAKE_CURRENT_SOURCE_DIR}/UserspaceVars.cmake)

if(${FORCE_STATIC_INLINE})
    list(APPEND CFLAGS "-DLIGO_INLINE=\"static inline\"")
endif()

if(${USE_STDLIB_MATH}) 
    list(APPEND CFLAGS "-DUSE_STDLIB_MATH=1") 
endif()

if(${DEFAULT_TO_SHMEM_BUFFERS})
    list(APPEND CFLAGS "-DDEFAULT_TO_SHMEM_BUFFERS=1")
endif()

list(APPEND CFLAGS "-fvisibility=hidden")

# Turn the ; seperated list into space seperated list and add to CFLAGS
string(REPLACE ";" " " CFLAGS_SPACE_SEPERATED "${CFLAGS}")
set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${CFLAGS_SPACE_SEPERATED}" )

#
# Define all the source files we need that are common to IOP and APP builds
#
list(APPEND COMMON_SRC_LIST "${CMAKE_CURRENT_SOURCE_DIR}/${MODEL_NAME}_core.c"
                     "${CMAKE_CURRENT_SOURCE_DIR}/src/fe/print_io_info.c"
                     "${CMAKE_CURRENT_SOURCE_DIR}/src/include/controller.c"
                     "${CMAKE_CURRENT_SOURCE_DIR}/src/include/controllerko.c"
                     "${CMAKE_CURRENT_SOURCE_DIR}/src/drv/crc.c"
                     "${CMAKE_CURRENT_SOURCE_DIR}/src/include/drv/fm10Gen.c"
                     "${CMAKE_CURRENT_SOURCE_DIR}/src/include/drv/epicsXfer.c"
                     "${CMAKE_CURRENT_SOURCE_DIR}/src/include/drv/daqLib.c"
                     "${CMAKE_CURRENT_SOURCE_DIR}/src/include/filter_coeffs.c"
                     "${CMAKE_CURRENT_SOURCE_DIR}/src/fe/rcguserCommon.c"
                     "${CMAKE_CURRENT_SOURCE_DIR}/src/fe/commData3.c"
                     "${CMAKE_CURRENT_SOURCE_DIR}/src/cds-shmem/cds-shmem.c"
                     "${CMAKE_CURRENT_SOURCE_DIR}/src/fe/demod/Demodulation.c"
                     "${CMAKE_CURRENT_SOURCE_DIR}/src/fe/demod/Biquad.c"
                     "${CMAKE_CURRENT_SOURCE_DIR}/src/drv/rts-logger/userspace/usp-rts-logger.c")

if( ${CMAKE_SYSTEM_PROCESSOR} STREQUAL "x86_64")
    list(APPEND COMMON_SRC_LIST "${CMAKE_CURRENT_SOURCE_DIR}/src/fe/util/fenv.c")
endif()

if(${LIBRTS_BUILD})
    list(APPEND COMMON_SRC_LIST "${CMAKE_CURRENT_SOURCE_DIR}/src/drv/fmReadCoeff.c")
endif()


if(BUILD_USP_GPSCLOCK)
    list(APPEND COMMON_SRC_LIST "${CMAKE_CURRENT_SOURCE_DIR}/src/drv/gpsclock.c")
endif()


if(IOP_MODEL)
    list(APPEND MODEL_SRC_LIST "${CMAKE_CURRENT_SOURCE_DIR}/src/fe/controllerIopUser.c"
                         "${CMAKE_CURRENT_SOURCE_DIR}/src/fe/rcguserIop.c")
else() #Control Model
    list(APPEND MODEL_SRC_LIST "${CMAKE_CURRENT_SOURCE_DIR}/src/fe/controllerAppUser.c"
                         "${CMAKE_CURRENT_SOURCE_DIR}/src/fe/rcguser.c"
                         "${CMAKE_CURRENT_SOURCE_DIR}/src/fe/mapApp.c")
endif(IOP_MODEL)

if(DEFINED USE_DOLPHIN_TIMING OR DEFINED XMIT_DOLPHIN_TIME)

else()
# Looks like userspace controllerIopUser uses initAdcModules() which are inline
# https://git.ligo.org/cds/advligorts/-/blob/master/src/fe/controllerIopUser.c#L550
endif()

#
# Configure and build the library
list(APPEND LIB_SRC_LIST ${COMMON_SRC_LIST} "${CMAKE_CURRENT_SOURCE_DIR}/src/fe/mapApp.c")
add_library(${LIB_NAME} ${LIB_SRC_LIST})
target_include_directories(${LIB_NAME} PUBLIC ${INC_DIRS})

target_link_libraries(${LIB_NAME} PUBLIC -lrt)

if (${USE_STDLIB_MATH})
    target_link_libraries(${LIB_NAME} PUBLIC -lm)
endif()


#
# Configure and build the model. ${INC_DIRS} is defined by UserspaceVars.cmake
if(NOT MODEL_NO_EXECUTABLE)
    add_executable(${PROJECT_NAME} ${MODEL_SRC_LIST} )
    target_include_directories(${PROJECT_NAME} PRIVATE ${INC_DIRS})
    target_link_libraries(${PROJECT_NAME} PRIVATE ${LIB_NAME})
endif()


