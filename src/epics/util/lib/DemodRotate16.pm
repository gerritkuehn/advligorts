package CDS::DemodRotate16;
use Exporter;
@ISA = ('Exporter');

$frequencies = 16;

# 1 extra input for data
$num_ins = $frequencies + 1;

# I and Q outputs for each frequency
$num_outs = $frequencies * 2;

sub partType {
	return DemodRotate16;
}

# Print Epics communication structure into a header file
# Current part number is passed as first argument
sub printHeaderStruct {
	my ($i) = @_;
}

# Print Epics variable definitions
# Current part number is passed as first argument
sub printEpics {
        my ($i) = @_;
}

# Print variable declarations int front-end file
# Current part number is passed as first argument
sub printFrontEndVars  {
        my ($i) = @_;
        print ::OUT "double \L$::xpartName[$i]_freq\[$num_ins\];\n";
        print ::OUT "double \L$::xpartName[$i]_out\[$num_outs\];\n";
        print ::OUT "double \L$::xpartName[$i]_signal;\n";
        print ::OUT "avx_rotation_struct \L$::xpartName[$i]_hist;\n";
}

# Check inputs are connected
sub checkInputConnect {
        my ($i) = @_;
        return "";
}


# Return front end initialization code
# Argument 1 is the part number
# Returns calculated code string
sub frontEndInitCode {
        my ($i) = @_;
        return "avx_rotation_struct_init(&\L$::xpartName[$i]_hist, " . "FE_RATE);\n";
}

# Figure out part input code
# Argument 1 is the part number
# Argument 2 is the input number
# Returns calculated input code
sub fromExp {
        my ($i, $j) = @_;
        my $from = $::partInNum[$i][$j];
        my $fromPort = $::partInputPort[$i][$j];
        return "\L$::xpartName[$from]_out" . "\[" . $fromPort . "\]";
}

# Return front end code
# Argument 1 is the part number
# Returns calculated code string

sub frontEndCode {
	my ($i) = @_;

	my $name = "\L$::xpartName[$i]";
        my $calcExp = "// DemodRotation:  $::xpartName[$i]\n";
        for my $i (0..$frequencies) {
        	$calcExp .= $name . "_freq\[$i\] = $::fromExp[$i];\n";
        }

        my $sig_index = $num_ins - 1;
        $calcExp .= $name . "_signal = $::fromExp[$sig_index];\n";

	$calcExp .= "demodulation_decimation_rotation8_section3($name" . "_signal, $name" . "_freq, ";
	$calcExp .= $name . "_out, feCoeff8x, &$name" . "_hist, $frequencies);\n";
        return $calcExp;
}
