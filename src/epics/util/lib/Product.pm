package CDS::Product;
use Exporter;
@ISA = ('Exporter');

sub partType {
    return Product;
}

# Print Epics communication structure into a header file
# Current part number is passed as first argument
sub printHeaderStruct {
    die "printHeaderStruct called in part with EpicsVariable interface.";
}

# Print Epics variable definitions
# Current part number is passed as first argument
sub printEpics {
    die "printEpics called in part with EpicsVariable interface.";
}

# return an array of EpicsVariable objects
# this function should be idempotent: calling it over and over should give the same
# result and have no side effects.
sub getEpicsVariables {
    my ($i) = @_;

    @epics_vars = ();

    push @epics_vars, CDS::EpicsVariable::new("$::xpartName[$i]\_TRAMP", "$::xpartName[$i]\_TRAMP", "int", "ai", 1, 0, {"PREC" => 0});
    push @epics_vars, CDS::EpicsVariable::new("$::xpartName[$i]\_RMON", "$::xpartName[$i]\_RMON", "int", "ao", 0, 0, {"PREC" => 0});
    push @epics_vars, CDS::EpicsVariable::new("$::xpartName[$i]", "$::xpartName[$i]", "double", "ai", 1, 0, {"PREC" => 3});

    return @epics_vars;
}



# Print variable declarations int front-end file
# Current part number is passed as first argument
sub printFrontEndVars  {
    my ($i) = @_;
    print ::OUT "double \L$::xpartName[$i]\[8\];\n";
    print ::OUT "float $::xpartName[$i]\_CALC;\n";
}

# Check inputs are connected
sub checkInputConnect {
    my ($i) = @_;
    return "";
}

# Figure out part input code
# Argument 1 is the part number
# Argument 2 is the input number
# Returns calculated input code
sub fromExp {
    my ($i, $j) = @_;
    my $from = $::partInNum[$i][$j];
    my $fromPort = $::partInputPort[$i][$j];
    return "\L$::xpartName[$from]" . "\[" . $fromPort . "\]";
}

# Return front end initialization code
# Argument 1 is the part number
# Returns calculated code string
sub frontEndInitCode {
    my ($i) = @_;
    return "";
}


# Return front end code
# Argument 1 is the part number
# Returns calculated code string
sub frontEndCode {
    my ($i) = @_;
    my $calcExp = "// PRODUCT:  $::xpartName[$i]\n";
    $calcExp .= "pLocalEpics->$::systemName\.$::xpartName[$i]";
    $calcExp .= "_RMON = \n\tgainRamp(pLocalEpics->$::systemName\.$::xpartName[$i],";
    $calcExp .= "pLocalEpics->$::systemName\.$::xpartName[$i]";
    $calcExp .= "_TRAMP,";
    $calcExp .= "$::gainCnt\,\&$::xpartName[$i]\_CALC, FE_RATE);";
    $calcExp .= "\n\n";
    for (0 .. $::inCnt-1) {
        $calcExp .= "\L$::xpartName[$i]";
        $calcExp .= "\[";
        $calcExp .= $_;
        $calcExp .= "\] = ";
        $calcExp .= "$::xpartName[$i]";
        $calcExp .= "_CALC * ";
        $calcExp .= $::fromExp[$_];
        $calcExp .= ";\n";
    }
    $::gainCnt ++;
    return $calcExp;
}
