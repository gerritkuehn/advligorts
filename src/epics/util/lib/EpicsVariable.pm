package CDS::EpicsVariable;

@ISA = {'Exporter'};

# Represents a single epics variable needed by some model part
# Parts can return a list of these in getEpicsVariables
# instead of writing out to a file with printHeaderStruct and
# printEpics

#  create a new EpicsVariable object
#  args:
#  1 - EPICS name, without leading "X1:" where X1 is the IFO code
#  2 - C name
#  3 - C type, one of ["int", "uint", "double"]
#  4 - Epics type, one of ["ao", "ai", "bo", "bi"]
#  5 - Precision - epics variable PREC field
# returns an object reference with values
# epics_name
# c_name
# c_type
# epics_type
# precision

my @valid_ctypes = qw(int uint double);

my @valid_epics_types = qw(ao ai bo bi);

my @alarm_level_extensions = qw(HIHI HIGH LOW LOLO);

# the prefix of the epics line has to announce
# whether the variable is an INVARIABLE or an OUTVARIABLE
my %epics_class = (
    'ao' => 'OUTVARIABLE',
    'bo' => 'OUTVARIABLE',
    'ai' => 'INVARIABLE',
    'bi' => 'INVARIABLE',
);

sub new {
    my $epics_name = shift;
    my $c_name = shift;
    my $c_type = shift;
    my $epics_type = shift;

    # if true, create a _mask c variable for this epics variable
    my $create_mask = shift;

    # if true, add .HIGH, .HIHI, .LOW, .LOLO subvariables
    # to to epics and the C structure.
    my $add_alarm_levels = shift;

    my $fields = shift;

    $good_ctype = 0;
    for( @valid_ctypes) {
        if ($c_type eq $_) {
            $good_ctype = 1;
            last;
        }
    }
    die "EPICS variable '$epics_name': Invalid C type '$c_type'." unless $good_ctype;

    $good_epics_type = 0;
    for( @valid_epics_types) {
        if ($epics_type eq $_) {
            $good_epics_type = 1;
            last;
        }
    }

    die "EPICS variable '$epics_name': Invalid EPICS type '$epics_type'." unless $good_epics_type;

    my $self = {
        "epics_name"  => $epics_name,
        "c_name"      => $c_name,
        "c_type"      => $c_type,
        "epics_type" => $epics_type,
        "fields"   => $fields,
        "create_mask" => $create_mask,
        "add_alarm_levels" => $add_alarm_levels,
    };

    bless $self;

    return $self;
}


# return a C declarations for the variable
sub getCDeclaration {
    my $self = shift;

    if (ref($$self{epics_name}) eq 'ARRAY') {
        #die "@{ $$self{epics_name} } size: " . scalar @{ $$self{epics_name} };
        return "$$self{c_type} $$self{c_name}\[".scalar @{ $$self{epics_name} }."\];";
    }
    else {
        return "$$self{c_type} $$self{c_name};";
    }
}

# return a C mask declarations for the variable
sub getCMaskDeclaration {
    my $self = shift;

    if ($$self{create_mask}) {
        return "$$self{c_type} $$self{c_name}_mask;";
    }
    else {
        return "";
    }
}

# return C declaration for alarm add_alarm_levels
sub getAlarmLevelCDeclarations {
    my $self = shift;

    my @cdecls = ();

    if ($$self{add_alarm_levels})
    {
        push @cdecls, "// alarm levels for $$self{epics_name}";
        for (@alarm_level_extensions) {
            $ext = lc($_);
            push @cdecls, "$$self{c_type} $$self{c_name}_$ext;";
        }
    }
    else
    {
        # do nothing: no cdecls
    }
    return @cdecls;
}

# return a list of epics DB entries for the variable
sub getEpicsEntries {
    my $self = shift;

    $fields = $$self{fields};

    $field_str = "";
    for my $key (keys %$fields) {
        $value = $fields->{$key};
        $field_str .= " field($key,\"$value\")";
    }

    $class = $epics_class{$$self{epics_type}};

    if (($class eq "OUTVARIABLE") && $$self{add_alarm_levels}) {
        $class = "OUTALARM";
    }

    die "Epics var '$$self{epics_name}': couldn't find class for epics type '$$self{epics_type}'" unless $class;

    if (ref($$self{epics_name}) eq 'ARRAY') {
        my @ret_val = ();
        my $index = 0;
        foreach ( @{ $$self{epics_name} }  ) {
            push(@ret_val, "$class $_ $::systemName\.$$self{c_name}\[". $index ."\] $$self{c_type} $$self{epics_type} 0" . $field_str);
            $index += 1;
        }
        return @ret_val;
    }
    else {
        return ("$class $$self{epics_name} $::systemName\.$$self{c_name} $$self{c_type} $$self{epics_type} 0" . $field_str );
    }
}

# returns true if variable is an integer type
sub isInteger {
    my $self = shift;

    return ( $$self{c_type} eq 'int' || $$self{c_type} eq 'uint' );
}

# returns true if variable is an integer type
sub isDouble {
    my $self = shift;

    return ( $$self{c_type} eq 'double');
}
