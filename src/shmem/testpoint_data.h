//
// Created by erik.vonreis on 5/19/21.
//

#ifndef DAQD_TRUNK_TESTPOINT_DATA_H
#define DAQD_TRUNK_TESTPOINT_DATA_H

#include "shmem_header.h"
#include "daqmap.h"


typedef struct TESTPOINT_CFG
{
    SHMEM_HEADER _header;
    uint32_t excitations[DAQ_GDS_MAX_TP_NUM];
    uint32_t excitations_writeback[DAQ_GDS_MAX_TP_NUM];
    uint32_t testpoints[DAQ_GDS_MAX_TP_NUM];
    uint32_t testpoints_writeback[DAQ_GDS_MAX_TP_NUM];
} TESTPOINT_CFG;

#define TESTPOINT_CFG_ID (2)  // structure identification.  Should be unique to the structure.
#define TESTPOINT_CFG_VERSION (1)

#define TESTPOINT_CFG_INIT(AD_PTR) SHMEM_SET_VERSION(AD_PTR, TESTPOINT_CFG_ID, TESTPOINT_CFG_VERSION)

#endif // DAQD_TRUNK_TESTPOINT_DATA_H
