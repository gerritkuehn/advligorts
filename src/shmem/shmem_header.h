//
// Created by erik.vonreis on 4/27/21.
//

#ifndef DAQD_TRUNK_SHMEM_HEADER_H
#define DAQD_TRUNK_SHMEM_HEADER_H

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif

// header to suppport version control for shared memory structures.
typedef struct SHMEM_HEADER
{
    uint32_t magic_bytes;  // used to verify structure is using this header
    uint32_t struct_id;  //unique id that identifies the structure type (1 = AWG_DATA, 2 = ...
    uint32_t version;  // should be 1 or more
    uint32_t _reserved;    //size should be aligned to 16-byte boundary, at least.
} SHMEM_HEADER;

#define SHMEM_HEADER_MAGIC_BYTES (0xf000beef)

/**
 * Pass a pointer to the header, and a version number.
 * Sets version number and magic bytes.
 */
#define SHMEM_SET_VERSION(HEADER_PTR, ID, VERSION)  { volatile SHMEM_HEADER *_h_p_ = (volatile SHMEM_HEADER *)HEADER_PTR; \
        _h_p_->magic_bytes = SHMEM_HEADER_MAGIC_BYTES;                                  \
        _h_p_->struct_id = ID;                                                                                \
        _h_p_->version = VERSION; }

/**
 * Get the version number of a shared memory structure
 * Returns 0 if the structure does not start with a properly intialized
 * SHMEM_HEADER
 */
#define SHMEM_GET_VERSION(HDR_PTR)  ( (((volatile SHMEM_HEADER *)HDR_PTR)->magic_bytes == SHMEM_HEADER_MAGIC_BYTES) \
                                      ? ((volatile SHMEM_HEADER *)HDR_PTR)->version : 0 )

#define SHMEM_GET_ID(HDR_PTR)  ( (((volatile SHMEM_HEADER *)HDR_PTR)->magic_bytes == SHMEM_HEADER_MAGIC_BYTES) \
                                      ? ((volatile SHMEM_HEADER *)HDR_PTR)->struct_id : 0 )

#endif // DAQD_TRUNK_SHMEM_HEADER_H
