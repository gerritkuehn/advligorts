/// @file verify_slots.c
/// @brief Software used to verify PCIe cards match up to
///< backplane slots in IOC Gen 2.

#include "../fe/verify_slots.h"
#include "controller.h"//IO_CONFIG_ERROR, pLocalEpics, etc
#include "drv/gsc_adc_common.h" //GSAI_ALL_CARDS
#include "drv/gsc_dac_common.h" //DAC_FIFO_EMPTY_TEST
#include "drv/ligoPcieTiming.h" //LPTC_SLOT_ENABLE
#include "drv/rts-logger.h"

#if defined( RUN_WO_IO_MODULES ) || defined( USE_DOLPHIN_TIMING )
#include "drv/no_ioc_dac_preload.h"
#else
#include "drv/iop_dac_functions.h"
#endif

#include <linux/delay.h> //udelay()

//***********************************************************************
// Sub: verify_cards2slots()
// This routine sequences thru all ADC/DAC modules and
// returns error if card does not match up with bp slot.
//***********************************************************************
int
verify_cards2slots( CDS_HARDWARE* pCds )
{
    int status;
    int err_return = 0;
    int ii;

    // Check all ADC cards
    for ( ii = 0; ii < pCds->adcCount; ii++ )
    {
        status = adc_cards2slots( pCds, ii );
        if ( status == IO_CONFIG_ERROR )
            err_return = IO_CONFIG_ERROR;
    }
    // Check all DAC cards
    for ( ii = 0; ii < pCds->dacCount; ii++ )
    {
        status = dac_cards2slots( pCds, ii );
        if ( status == IO_CONFIG_ERROR )
            err_return = IO_CONFIG_ERROR;
    }
    gscEnableDacModule( pCds, GSAI_ALL_CARDS, 0 );
    return err_return;
}

//***********************************************************************
// Sub: dac_cards2slots()
// Routine to check if specified DAC card matches up to
// assigned bp slot.
// To perform this check: 
// -  DAC card FIFO is loaded with some values
// -  Corresponding BP slot clock is enabled.
// -  Check that DAC FIFO has been emptied.
//***********************************************************************
int
dac_cards2slots( CDS_HARDWARE* pCds, int card )
{
    int status;
    int max_wait = 20;
    int timer = 0;
    int err_return = 0;
    int cardslot = pCds->dacSlot[ card ];

    // Disable all the individual slot clocks
    lptc_slot_clk_disable_all( pCds );

    // Verify FIFO empty
    status = gscDacFifoCheck( pCds, card, DAC_FIFO_EMPTY_TEST );
    if ( status != 0 )
        err_return = IO_CONFIG_ERROR;

    // Preload DAC FIFO, such that FIFO != empty
    iop_dac_preload( card );
    udelay( MAX_UDELAY ); // 20msec

    // Verify FIFO Not empty
    status = gscDacFifoCheck( pCds, card, DAC_FIFO_NOT_EMPTY_TEST );
    if ( status != 0 )
        err_return = IO_CONFIG_ERROR;

    // Enable a single DAC module for clocking
    gscEnableDacModule( pCds, card, DAC_CLK_ENABLE );

    // Enable the backplane slot clock for this DAC
    lptc_slot_clk_set( pCds, pCds->dacSlot[ card ], LPTC_SLOT_ENABLE );
    udelay( 100 );

    // Enable the backplane global clock
    lptc_start_clock( pCds );

    // Now wait for clocks to start and check when DAC FIFO is empty
    timer = 0;
    do
    {
        msleep(100);
        lptc_status_update( pCds );
        pLocalEpics->epicsOutput.epicsSync =
            ( pLocalEpics->epicsOutput.epicsSync + 1 ) % 16;
        timer++;
        status = gscDacFifoCheck( pCds, card, DAC_FIFO_EMPTY_TEST );
    } while ( ( timer < max_wait ) && ( status != 0 ) );
    // If DAC FIFO !empty, then fault
    if ( timer >= max_wait )
    {
        err_return = IO_CONFIG_ERROR;
        // Clear the DAC module mapped bit in DAC status word.
        pLocalEpics->epicsOutput.statDac[ card ] &= ~( DAC_FOUND_BIT );
        RTSLOG_ERROR("dac_cards2slots() Timed out while waiting for clocked DAC FIFO. card: %d\n", card);
    }
    // Clear bp global clock enable
    lptc_stop_clock( pCds );
    // Disable all bp slot clocks
    lptc_slot_clk_disable_all( pCds );
    // Clear external clk enable
    gscEnableDacModule( pCds, card, DAC_CLK_DISABLE );
    // Clear the DAC FIFO
    gscDacFifoClear( pCds, card );
    // If card/slot correct, send slot number to EPICS
    if ( err_return != IO_CONFIG_ERROR )
        pLocalEpics->epicsOutput.pcieSlotNum[ cardslot ] = cardslot + 1;
    else 
        pLocalEpics->epicsOutput.pcieSlotNum[ cardslot ] =  0;
    // Return test result
    return err_return;
}

//***********************************************************************
// Sub: adc_cards2slots()
// Routine to check if specified ADC card matches up to
// assigned bp slot.
// To perform this check: 
// -  Corresponding BP slot clock is enabled.
// -  Check that ADC FIFO fills with data.
//***********************************************************************
int
adc_cards2slots( CDS_HARDWARE* pCds, int card )
{
    int           status = 0;
    int           max_wait = 20;
    int           timer = 0;
    int           adcbufsize = 0;
    int           cardslot = pCds->adcSlot[ card ];

    timer = 0;
    // Disable all the individual slot clocks
    lptc_slot_clk_disable_all( pCds );
    // Clear all ADC external clk enables
    gscDisableAdcClk( pCds, GSAI_ALL_CARDS );
    // Enable a single ADC module for clocking
    // Don't want DMA mode, as FIFO size not available
    gscEnableAdcModule( pCds, card, GSAI_NO_DMA );
    // Enable the backplane slot clock for this ADC
    lptc_slot_clk_set( pCds, pCds->adcSlot[ card ], LPTC_SLOT_ENABLE );
    udelay( 100 );
    // Enable the backplane global clock
    lptc_start_clock( pCds );
    // Now wait for clocks to start and check when ADC has data
    do
    {
        msleep(100);
        lptc_status_update( pCds );
        // Trigger EPICS to do updates during test
        pLocalEpics->epicsOutput.epicsSync =
            ( pLocalEpics->epicsOutput.epicsSync + 1 ) % 16;
        timer++;
        // Get the ADC FIFO data size
        adcbufsize = gscCheckAdcBuffer( card );
    } while ( ( timer < max_wait ) && ( adcbufsize < 100 ) );
    // If !adcbufsize then fault
    if ( timer >= max_wait )
    {
        status = IO_CONFIG_ERROR;
        pLocalEpics->epicsOutput.statAdc[ card ] &= ~( ADC_MAPPED );
        RTSLOG_ERROR("adc_cards2slots() Timed out while waiting for clocked ADC FIFO. card: %d\n", card);
    }
    // Disable the bp global clock
    lptc_stop_clock( pCds );
    // Disable all the individual slot clocks
    lptc_slot_clk_disable_all( pCds );
    // Clear all ADC external clk enables
    gscDisableAdcClk( pCds, GSAI_ALL_CARDS );
    // If card/slot correct, send slot number to EPICS
    if ( status == 0 )
        pLocalEpics->epicsOutput.pcieSlotNum[ cardslot ] = cardslot + 1;

    return status;
}
