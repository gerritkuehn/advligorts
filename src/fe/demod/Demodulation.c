#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif


#include "Demodulation.h"
#include "Biquad.h"
#include "drv/rts-logger.h"
#include "util/inlineMath.h" //M_PI, lfabs()
#include "util/lstring.h" //memcpy()
#include "util/inlineMath.h"

#ifdef __KERNEL__

// defining _MM_MALLOC_H_INCLUDED here blocks header
// files included in immintrin.h that require malloc
// and can't be used when building kernel modules
#define _MM_MALLOC_H_INCLUDED

#endif // __KERNEL__


#if USE_SSE3 > 0 || USE_AVX2 > 0 || USE_AVX512 > 0
#include <immintrin.h>
#endif

/* M_SQRT1_2
 ************************************************************************/
#ifndef M_SQRT1_2
#define M_SQRT1_2  0.707106781186547524401
#endif

/* Cosine of 11.25 deg
 ************************************************************************/
#define M_c11 0.980785280403230449126

/* Sine of 11.25 deg
 ************************************************************************/
#define M_s11 0.195090322016128267848

/* Cosine of 22.5 deg
 ************************************************************************/
#define M_c22 0.923879532511286756128

/* Sine of 22.5 deg
 ************************************************************************/
#define M_s22 0.382683432365089771728

/* Cosine of 33.75 deg
 ************************************************************************/
#define M_c33 0.831469612302545237079

/* Sine of 33.75 deg
 ************************************************************************/
#define M_s33 0.555570233019602224743

/* Cosine/Sine of 45 deg
 ************************************************************************/
#define M_c45 M_SQRT1_2


/* cordic
 ************************************************************************/
inline static
void cordic_step(double* rotation, const double* anglexy)
{
	const double tempc = anglexy[0] * rotation[0] - anglexy[1] * rotation[1];
	rotation[1] = anglexy[1] * rotation[0] + anglexy[0] * rotation[1];
	rotation[0] = tempc;
}

/* avx_stride_struct_init
 ************************************************************************/
void avx_stride_struct_init(avx_stride_struct* self, double rate)
{
	if (!self) return;
	memset(self, 0, sizeof(avx_stride_struct));
	if (rate > 0.0)	self->two_pi_over_rate = 2 * M_PI / rate;
	self->rotation[0] = 1.0;
	self->rotation[1] = 0.0;
	self->anglexy[0] = 1.0;
	self->anglexy[1] = 0.0;
}

/* avx_stride_struct_set_frequency
 ************************************************************************/
void avx_stride_struct_set_frequency(avx_stride_struct* self, double freq)
{
	self->freqhist = freq;
        sincos(freq * self->two_pi_over_rate, self->anglexy + 1, self->anglexy + 0);
}

/* avx_stride_struct_cordic
 ************************************************************************/
void avx_stride_struct_cordic(avx_stride_struct* self)
{
	cordic_step(self->rotation, self->anglexy);
}


/* avx_rotation_struct_init
 ************************************************************************/
void avx_rotation_struct_init(avx_rotation_struct* init, double rate)
{
	memset(init, 0, sizeof(avx_rotation_struct));
	if (rate > 0.0) init->two_pi_over_rate = 2 * M_PI / rate;
        size_t i;
	for (i = 0; i < 2 * MAX_STRIDE; i += 2)
	{
		init->rotation[i + 0] = 1.0;
		init->rotation[i + 1] = 0.0;
		init->anglexy[i + 0] = 1.0;
		init->anglexy[i + 1] = 0.0;
	}
}

/* avx_rotation_struct_set_frequency
 ************************************************************************/
void avx_rotation_struct_set_frequency(avx_rotation_struct* self, double freq, unsigned int idx)
{
	if (idx >= MAX_STRIDE) return;
	self->freqhist[idx] = freq;
	sincos(freq * self->two_pi_over_rate, self->anglexy + 2 * idx + 1, self->anglexy + 2 * idx + 0);
        RTSLOG_DEBUG("sin=%d cos=%d\n", (int)(1000000.0*self->anglexy[2*idx+1]), (int)(1000000.0*self->anglexy[2*idx]));
}


/* demodulation_decimation_stride8_section3
 ************************************************************************/
void demodulation_decimation_stride8_section3_std(const double* inp, double freq, 
	double* out, const double* coeff, avx_stride_struct* hist, size_t stride)
{
#define stride8_mm64 8
#define stride_mm64_max 32
	// number of sections are 3
#define sections3_intrinsic 3

	if ((stride < stride8_mm64) || (stride % stride8_mm64 != 0) || (stride > stride_mm64_max)) return;

	// Check if new frequency

        if (freq != hist->freqhist) {
                avx_stride_struct_set_frequency(hist, freq);
        }

	double x[2 * stride_mm64_max];
	// loop over stride
	double c = hist->rotation[0];
	double s = hist->rotation[1];
        size_t i;
	for (i = 0; i < stride; ++i)
	{
		x[2 * i + 0] = c * inp[i];
		x[2 * i + 1] = s * inp[i];
        }
	biquad_stride2_std(x, out, coeff, hist->hist, sections3_intrinsic, 2 * stride, 1, 0);

	avx_stride_struct_cordic(hist);
}

/* demodulation_decimation_rotation8_section3_std
 ************************************************************************/
void demodulation_decimation_rotation8_section3_std(double inp, const double* freq, 
	double* out, const double* coeff, avx_rotation_struct* hist, size_t stride)
{
#define stride8_mm64 8
#define stride_mm64_max 32
	// number of sections are 3
#define sections3_intrinsic 3
	if ((stride < stride8_mm64) || (stride % stride8_mm64 != 0) || (stride > stride_mm64_max)) return;

	double x[2 * stride_mm64_max];
	// loop over stride
        size_t i;
	for (i = 0; i < stride; ++i)
	{
                // update frequency if it's changed by a significant amount
                if (freq[i] != hist->freqhist[i])
                {
                        RTSLOG_DEBUG("setting frequencies for id %d\n", i);
                        avx_rotation_struct_set_frequency(hist, freq[i], i);
                }

                ++hist->idx;
		x[2 * i + 0] = hist->rotation[2 * i + 0] * inp;
		x[2 * i + 1] = hist->rotation[2 * i + 1] * inp;
		cordic_step(hist->rotation + 2 * i, hist->anglexy + 2 * i);

        }
	biquad_stride2_std(x, out, coeff, hist->hist, sections3_intrinsic, 2 * stride, 1, 0);
}


#if USE_SSE3
/* demodulation_decimation_stride8_section3_sse3
 ************************************************************************/
void demodulation_decimation_stride8_section3_sse3(const double* inp, double freq,
	double* out, const double* coeff, avx_stride_struct* hist, size_t stride)
{
#define stride_mm128 2
#define stride_mm128_q(i) (	 (i == 0) ? 0 * stride_mm128: \
							 (i == 1) ? 1 * stride_mm128 : \
							 (i == 2) ? 2 * stride_mm128 : \
							 (i == 3) ? 3 * stride_mm128 : \
							 (i == 4) ? 4 * stride_mm128 : \
							 (i == 5) ? 5 * stride_mm128 : \
							 (i == 6) ? 6 * stride_mm128 : \
									    7 * stride_mm128)
#define stride8_mm128 8
#define stride_mm128_max 32



	if ((stride < stride8_mm128) || (stride % stride8_mm128 != 0) || (stride > stride_mm128_max)) return;


	// Check if new frequency
        if (freq != hist->freqhist) {
                avx_stride_struct_set_frequency(hist, freq);
        }

	// Load rotation
	__m128d rotation = *(__m128d*)hist->rotation;
	alignas(16) double x[2 * stride_mm128_max];

	// loop over stride
        size_t i = 0;
	for (i = 0; i <=  stride - stride8_mm128 ; i = i + stride8_mm128)
	{
		_mm_store_pd(x + 2 * i + stride_mm128_q(0), _mm_mul_pd(rotation, _mm_set1_pd(inp[i + 0])));
		_mm_store_pd(x + 2 * i + stride_mm128_q(1), _mm_mul_pd(rotation, _mm_set1_pd(inp[i + 1])));
		_mm_store_pd(x + 2 * i + stride_mm128_q(2), _mm_mul_pd(rotation, _mm_set1_pd(inp[i + 2])));
		_mm_store_pd(x + 2 * i + stride_mm128_q(3), _mm_mul_pd(rotation, _mm_set1_pd(inp[i + 3])));
		_mm_store_pd(x + 2 * i + stride_mm128_q(4), _mm_mul_pd(rotation, _mm_set1_pd(inp[i + 4])));
		_mm_store_pd(x + 2 * i + stride_mm128_q(5), _mm_mul_pd(rotation, _mm_set1_pd(inp[i + 5])));
		_mm_store_pd(x + 2 * i + stride_mm128_q(6), _mm_mul_pd(rotation, _mm_set1_pd(inp[i + 6])));
		_mm_store_pd(x + 2 * i + stride_mm128_q(7), _mm_mul_pd(rotation, _mm_set1_pd(inp[i + 7])));
	}
#if	defined(__AVX512F__) && defined(__AVX512CD__) && defined(__AVX512DQ__) && defined(__AVX512BW__)
	biquad_stride8_section3_sse3(x, out, coeff, hist->hist, 2 * stride, 1, 0);
#else
	biquad_stride2_section3_sse3(x, out, coeff, hist->hist, 2 * stride, 1, 0);
#endif
	avx_stride_struct_cordic(hist);


}

/* demodulation_decimation_rotation8_section3_sse3
 ************************************************************************/
void demodulation_decimation_rotation8_section3_sse3(double inp, const double* freq,
	double* out, const double* coeff, avx_rotation_struct* hist, size_t stride)
{
#define stride_mm128 2
#define stride_mm128_q(i) (	 (i == 0) ? 0 * stride_mm128: \
							 (i == 1) ? 1 * stride_mm128 : \
							 (i == 2) ? 2 * stride_mm128 : \
							 (i == 3) ? 3 * stride_mm128 : \
							 (i == 4) ? 4 * stride_mm128 : \
							 (i == 5) ? 5 * stride_mm128 : \
							 (i == 6) ? 6 * stride_mm128 : \
									    7 * stride_mm128)
#define stride8_mm128 8
#define stride16_mm128 16
#define stride_mm128_max 32

	if ((stride < stride8_mm128) || (stride % stride8_mm128 != 0) || (stride > stride_mm128_max)) return;

        // loop over stride to check for a frequency change
        // recalculate cordic angle if necessary
        // but do only one every 16th cycle
        for (int chan=0; chan < stride; ++chan) {
                if (freq[chan] != hist->freqhist[chan])
                {
                        avx_rotation_struct_set_frequency(hist, freq[chan], chan);
                }
        }
	// Load input 
	__m128d input = _mm_set1_pd(inp);
	alignas(16) double x[2 * stride_mm128_max];

	// loop over stride: rotate input and apply cordic
        size_t i;
	for (i = 0; i <= 2 * stride - stride8_mm128; i = i + stride8_mm128)
	{
		const __m128d r1 = _mm_load_pd(hist->rotation + i + stride_mm128_q(0));
		const __m128d r2 = _mm_load_pd(hist->rotation + i + stride_mm128_q(1));
		const __m128d r3 = _mm_load_pd(hist->rotation + i + stride_mm128_q(2));
		const __m128d r4 = _mm_load_pd(hist->rotation + i + stride_mm128_q(3));
		const __m128d cor1 = _mm_load_pd(hist->anglexy + i + stride_mm128_q(0));
		const __m128d cor2 = _mm_load_pd(hist->anglexy + i + stride_mm128_q(1));
		const __m128d cor3 = _mm_load_pd(hist->anglexy + i + stride_mm128_q(2));
		const __m128d cor4 = _mm_load_pd(hist->anglexy + i + stride_mm128_q(3));

		_mm_store_pd(x + i + stride_mm128_q(0), _mm_mul_pd(input, r1));
		_mm_store_pd(x + i + stride_mm128_q(1), _mm_mul_pd(input, r2));
		_mm_store_pd(x + i + stride_mm128_q(2), _mm_mul_pd(input, r3));
		_mm_store_pd(x + i + stride_mm128_q(3), _mm_mul_pd(input, r4));

		__m128d temp1 = _mm_mul_pd(_mm_unpackhi_pd(r1, r1), _mm_shuffle_pd(cor1, cor1, 0x01));
		__m128d temp2 = _mm_mul_pd(_mm_unpackhi_pd(r2, r2), _mm_shuffle_pd(cor2, cor2, 0x01));
		__m128d temp3 = _mm_mul_pd(_mm_unpackhi_pd(r3, r3), _mm_shuffle_pd(cor3, cor3, 0x01));
		__m128d temp4 = _mm_mul_pd(_mm_unpackhi_pd(r4, r4), _mm_shuffle_pd(cor4, cor4, 0x01));
		temp1 = _mm_fmaddsub_pd(_mm_unpacklo_pd(r1, r1), cor1, temp1);
		temp2 = _mm_fmaddsub_pd(_mm_unpacklo_pd(r2, r2), cor2, temp2);
		temp3 = _mm_fmaddsub_pd(_mm_unpacklo_pd(r3, r3), cor3, temp3);
		temp4 = _mm_fmaddsub_pd(_mm_unpacklo_pd(r4, r4), cor4, temp4);
		_mm_store_pd(hist->rotation + i + stride_mm128_q(0), temp1);
		_mm_store_pd(hist->rotation + i + stride_mm128_q(1), temp2);
		_mm_store_pd(hist->rotation + i + stride_mm128_q(2), temp3);
		_mm_store_pd(hist->rotation + i + stride_mm128_q(3), temp4);
	}
	// filter
#if	defined(__AVX512F__) && defined(__AVX512CD__) && defined(__AVX512DQ__) && defined(__AVX512BW__)
	biquad_stride8_section3_sse3(x, out, coeff, hist->hist, 2 * stride, 1, 0);
#else
	biquad_stride2_section3_sse3(x, out, coeff, hist->hist, 2 * stride, 1, 0);
#endif
}
#endif //USE_SSE3


#if USE_AVX2
/* demodulation_decimation_avx2_stride8_section3
 ************************************************************************/

void demodulation_decimation_stride8_section3_avx2(const double* inp, double freq, 
	double* out, const double* coeff, avx_stride_struct* hist, size_t stride)
{
#define stride_mm256 4
#define stride_mm256_q(i) (	 (i == 0) ? 0 * stride_mm256: \
									 (i == 1) ? 1 * stride_mm256 : \
									 (i == 2) ? 2 * stride_mm256 : \
									 (i == 3) ? 3 * stride_mm256 : \
									 (i == 4) ? 4 * stride_mm256 : \
									 (i == 5) ? 5 * stride_mm256 : \
									 (i == 6) ? 6 * stride_mm256 : \
									 7 * stride_mm256)
#define stride8_mm256 8
#define stride_mm256_max 32

	if ((stride < stride8_mm256) || (stride % stride8_mm256 != 0) || (stride > stride_mm256_max)) return;

	// Check if new frequency


        if (freq != hist->freqhist) {
                avx_stride_struct_set_frequency(hist, freq);
        }


	// Load rotation
	__m256d rotation = _mm256_broadcast_pd((__m128d*)hist->rotation);
	alignas(32) double x[2 * stride_mm256_max];

	// loop over stride
        size_t i;
	for (i = 0; i <= stride - stride8_mm256; i = i + stride8_mm256)
	{
		__m256d i1 = _mm256_loadu_pd(inp + i + stride_mm256_q(0));
		__m256d i2 = _mm256_loadu_pd(inp + i + stride_mm256_q(1));
		_mm256_store_pd(x + 2 * i + stride_mm256_q(0), _mm256_mul_pd(rotation, _mm256_permute4x64_pd(i1, 0x50)));
		_mm256_store_pd(x + 2 * i + stride_mm256_q(1), _mm256_mul_pd(rotation, _mm256_permute4x64_pd(i1, 0xFA)));
		_mm256_store_pd(x + 2 * i + stride_mm256_q(2), _mm256_mul_pd(rotation, _mm256_permute4x64_pd(i2, 0x50)));
		_mm256_store_pd(x + 2 * i + stride_mm256_q(3), _mm256_mul_pd(rotation, _mm256_permute4x64_pd(i2, 0xFA)));
	}
#if	defined(__AVX512F__) && defined(__AVX512CD__) && defined(__AVX512DQ__) && defined(__AVX512BW__)
	biquad_stride16_section3_avx2(x, out, coeff, hist->hist, 2 * stride, 1, 0);
#else
	biquad_stride4_section3_avx2(x, out, coeff, hist->hist, 2 * stride, 1, 0);
#endif
	avx_stride_struct_cordic(hist);
}

/* demodulation_decimation_rotation8_section3_avx2
 ************************************************************************/
void demodulation_decimation_rotation8_section3_avx2(double inp, const double* freq, 
	double* out, const double* coeff, avx_rotation_struct* hist, size_t stride)
{
#define stride_mm256 4
#define stride_mm256_q(i) (	 (i == 0) ? 0 * stride_mm256: \
									 (i == 1) ? 1 * stride_mm256 : \
									 (i == 2) ? 2 * stride_mm256 : \
									 (i == 3) ? 3 * stride_mm256 : \
									 (i == 4) ? 4 * stride_mm256 : \
									 (i == 5) ? 5 * stride_mm256 : \
									 (i == 6) ? 6 * stride_mm256 : \
									 7 * stride_mm256)
#define stride8_mm256 8
#define stride16_mm256 16
#define stride_mm256_max 32

	if ((stride < stride8_mm256) || (stride % stride8_mm256 != 0) || (stride > stride_mm256_max)) return;

	// loop over stride to check for a frequency change
	// recalculate cordic angle if necessary
	// but do only one every 16th cycle
	for (int chan=0; chan < stride; ++chan) {
		if (freq[chan] != hist->freqhist[chan])
		{
			avx_rotation_struct_set_frequency(hist, freq[chan], chan);
		}
	}
	// Load input 
	__m256d input = _mm256_set1_pd(inp);
	alignas(32) double x[2 * stride_mm256_max];

	// loop over stride: rotate input and apply cordic
        size_t i;
	for (i = 0; i <= 2 * stride - stride8_mm256; i = i + stride8_mm256)
	{
		const __m256d r1 = _mm256_load_pd(hist->rotation + i + stride_mm256_q(0));
		const __m256d r2 = _mm256_load_pd(hist->rotation + i + stride_mm256_q(1));
		const __m256d cor1 = _mm256_load_pd(hist->anglexy + i + stride_mm256_q(0));
		const __m256d cor2 = _mm256_load_pd(hist->anglexy + i + stride_mm256_q(1));

		_mm256_store_pd(x + i + stride_mm256_q(0), _mm256_mul_pd(input, r1));
		_mm256_store_pd(x + i + stride_mm256_q(1), _mm256_mul_pd(input, r2));

		__m256d temp1 = _mm256_mul_pd(_mm256_permute_pd(r1, 0x0F), _mm256_permute_pd(cor1, 0x05));
		__m256d temp2 = _mm256_mul_pd(_mm256_permute_pd(r2, 0x0F), _mm256_permute_pd(cor2, 0x05));
		temp1 = _mm256_fmaddsub_pd(_mm256_permute_pd(r1, 0x00), cor1, temp1);
		temp2 = _mm256_fmaddsub_pd(_mm256_permute_pd(r2, 0x00), cor2, temp2);
		_mm256_store_pd(hist->rotation + i + stride_mm256_q(0), temp1);
		_mm256_store_pd(hist->rotation + i + stride_mm256_q(1), temp2);
	}
	// filter
#if	defined(__AVX512F__) && defined(__AVX512CD__) && defined(__AVX512DQ__) && defined(__AVX512BW__)
	biquad_stride16_section3_avx2(x, out, coeff, hist->hist, 2 * stride, 1, 0);
#else
	biquad_stride4_section3_avx2(x, out, coeff, hist->hist, 2 * stride, 1, 0);
#endif
}
#endif // USE_AVX2


#if USE_AVX512
/* demodulation_decimation_stride8_section3_avx512
 ************************************************************************/

void demodulation_decimation_stride8_section3_avx512(const double* inp, double freq, 
	double* out, const double* coeff, avx_stride_struct* hist, size_t stride)
{
#define stride_mm512 8
#define stride_mm512_q(i) (	 (i == 0) ? 0 * stride_mm512: \
									 (i == 1) ? 1 * stride_mm512 : \
									 (i == 2) ? 2 * stride_mm512 : \
									 (i == 3) ? 3 * stride_mm512 : \
									 (i == 4) ? 4 * stride_mm512 : \
									 (i == 5) ? 5 * stride_mm512 : \
									 (i == 6) ? 6 * stride_mm512 : \
									 7 * stride_mm512)
#define stride8_mm512 8
#define stride_mm512_max 32

	if ((stride < stride8_mm512) || (stride % stride8_mm512 != 0) || (stride > stride_mm512_max)) return;

        if (freq != hist->freqhist) {
                avx_stride_struct_set_frequency(hist, freq);
        }

	// Load rotation
	__m512d rotation = _mm512_broadcast_f64x2(*(__m128d*)hist->rotation);// _mm512_broadcast_f64x4(_mm256_broadcast_f64x2(*(__m128d*)hist->rotation));
	alignas(64) double x[2 * stride_mm512_max];

	// loop over stride
        size_t i;
	for (i = 0; i <= stride - stride8_mm512; i = i + stride8_mm512)
	{
		_mm512_store_pd(x + 2 * i + stride_mm512_q(0), _mm512_mul_pd(_mm512_setr_pd(inp[i + 0], inp[i + 0], inp[i + 1], inp[i + 1], inp[i + 2], inp[i + 2], inp[i + 3], inp[i + 3]), rotation));
		_mm512_store_pd(x + 2 * i + stride_mm512_q(1), _mm512_mul_pd(_mm512_setr_pd(inp[i + 4], inp[i + 4], inp[i + 5], inp[i + 5], inp[i + 6], inp[i + 6], inp[i + 7], inp[i + 7]), rotation));
	}
	biquad_stride16_section3_avx512(x, out, coeff, hist->hist, 2 * stride, 1, 0);
	avx_stride_struct_cordic(hist);
}

/* demodulation_decimation_rotation8_section3_avx512
 ************************************************************************/
void demodulation_decimation_rotation8_section3_avx512(double inp, const double* freq, 
	double* out, const double* coeff, avx_rotation_struct* hist, size_t stride)
{
#define stride_mm512 8
#define stride_mm512_q(i) (	 (i == 0) ? 0 * stride_mm512: \
									 (i == 1) ? 1 * stride_mm512 : \
									 (i == 2) ? 2 * stride_mm512 : \
									 (i == 3) ? 3 * stride_mm512 : \
									 (i == 4) ? 4 * stride_mm512 : \
									 (i == 5) ? 5 * stride_mm512 : \
									 (i == 6) ? 6 * stride_mm512 : \
									 7 * stride_mm512)
#define stride8_mm512 8
#define stride_mm512_max 32

	if ((stride < stride8_mm512) || (stride % stride8_mm512 != 0) || (stride > stride_mm512_max)) return;

        // loop over stride to check for a frequency change
        // recalculate cordic angle if necessary
        // but do only one every 16th cycle
        for (int chan=0; chan < stride; ++chan) {
                if (freq[chan] != hist->freqhist[chan])
                {
                        avx_rotation_struct_set_frequency(hist, freq[chan], chan);
                }
        }

	// Load input 
	__m512d input = _mm512_set1_pd(inp);
	alignas(64) double x[2 * stride_mm512_max];

	// loop over stride: rotate input and apply cordic
        size_t i;
	for (i = 0; i <= 2 * stride - stride8_mm512; i = i + stride8_mm512)
	{
		const __m512d r1 = _mm512_load_pd(hist->rotation + i);
		const __m512d cor1 = _mm512_load_pd(hist->anglexy + i);

		// apply rotation
		_mm512_store_pd(x + i, _mm512_mul_pd(input, r1));

		// cordic
		__m512d temp1 = _mm512_mul_pd(_mm512_permute_pd(r1, 0xFF), _mm512_permute_pd(cor1, 0x55));
		temp1 = _mm512_fmaddsub_pd(_mm512_permute_pd(r1, 0x00), cor1, temp1);
		_mm512_store_pd(hist->rotation + i, temp1);
	}
	// filter
	biquad_stride16_section3_avx512(x, out, coeff, hist->hist, 2 * stride, 1, 0);
}
#endif // USE_AVX512

void demodulation_decimation_stride8_section3(const double* inp, double freq,
                                          double* out, const double* coeff, avx_stride_struct* hist, size_t stride)
{
#if USE_AVX512
#define VEC_MESSAGE   "Using AVX512 demodulation"
    demodulation_decimation_stride8_section3_avx512(inp, freq, out, coeff, hist, stride);
#elif USE_AVX2
#define VEC_MESSAGE "Using AVX2 demodulation"
    demodulation_decimation_stride8_section3_avx2(inp, freq, out, coeff, hist, stride);
#elif USE_SSE3
#define VEC_MESSAGE "Using SSE3 demodulation"
    demodulation_decimation_stride8_section3_sse3(inp, freq, out, coeff, hist, stride);
#else
#define VEC_MESSAGE "Using non-vectorized demodulation"
    demodulation_decimation_stride8_section3_std(inp, freq, out, coeff, hist, stride);
#endif
}


void demodulation_decimation_rotation8_section3(double inp, const double* freq,
                                            double* out, const double* coeff, avx_rotation_struct* hist, size_t stride)
{
#if USE_AVX512
    demodulation_decimation_rotation8_section3_avx512(inp, freq, out, coeff, hist, stride);
#elif USE_AVX2
    demodulation_decimation_rotation8_section3_avx2(inp, freq, out, coeff, hist, stride);
#elif USE_SSE3
    demodulation_decimation_rotation8_section3_sse3(inp, freq, out, coeff, hist, stride);
#else
    demodulation_decimation_rotation8_section3_std(inp, freq, out, coeff, hist, stride);
#endif
}


/* demodulation_decimation_band1_section3_std
 ************************************************************************/
void demodulation_decimation_band1_section3(double inp,
                                            double* out, const double* coeff, double* hist, int step)
{
    double x[2];
    switch ((unsigned int)step & 0x1) {
    case 0:
                x[0] = inp;  x[1] = inp;
                break;
    case 1:
                x[0] = inp;  x[1] = -inp;
                break;
    }
    biquad_stride2_std(x, out, coeff, hist, sections3_intrinsic, 2, 1, 0);
}

/* demodulation_decimation_band2_section3_std
 ************************************************************************/
void demodulation_decimation_band2_section3(double inp,
                                            double* out, const double* coeff, double* hist, int step)
{
    double x[4];
    switch ((unsigned int)step & 0x3) {
    case 0:
                x[0] = inp;  x[1] = inp;
                x[2] = inp;  x[3] = 0.0;
                break;
    case 1:
                x[0] = inp;  x[1] = -inp;
                x[2] = 0.0;  x[3] = inp;
                break;
    case 2:
                x[0] = inp;  x[1] = inp;
                x[2] = -inp;  x[3] = 0.0;
                break;
    case 3:
                x[0] = inp;  x[1] = -inp;
                x[2] = 0.0;  x[3] = -inp;
                break;
    }
    biquad_stride4_section3(x, out, coeff, hist, 4, 1, 0);
}

/* demodulation_decimation_band4_section3_std
 ************************************************************************/
void demodulation_decimation_band4_section3(double inp,
                                            double* out, const double* coeff, double* hist, int step)
{
    double x[8];
    double c45;

    c45 = inp * M_c45;
    switch ((unsigned int)step & 0x7) {
    case 0:
                x[0] = inp;  x[1] = inp;
                x[2] = inp;  x[3] = 0.0;
                x[4] = inp;  x[5] = 0.0;
                x[6] = inp;  x[7] = 0.0;
                break;
    case 1:
                x[0] = inp;  x[1] = -inp;
                x[2] = c45;  x[3] = c45;
                x[4] = 0.0;  x[5] = inp;
                x[6] = -c45;  x[7] = c45;
                break;
    case 2:
                x[0] = inp;  x[1] = inp;
                x[2] = 0.0;  x[3] = inp;
                x[4] = -inp;  x[5] = 0.0;
                x[6] = 0.0;  x[7] = -inp;
                break;
    case 3:
                x[0] = inp;  x[1] = -inp;
                x[2] = -c45;  x[3] = c45;
                x[4] = 0.0;  x[5] = -inp;
                x[6] = c45;  x[7] = c45;
                break;
    case 4:
                x[0] = inp;  x[1] = inp;
                x[2] = -inp;  x[3] = 0.0;
                x[4] = inp;  x[5] = 0.0;
                x[6] = -inp;  x[7] = 0.0;
                break;
    case 5:
                x[0] = inp;  x[1] = -inp;
                x[2] = -c45;  x[3] = -c45;
                x[4] = 0.0;  x[5] = inp;
                x[6] = c45;  x[7] = -c45;
                break;
    case 6:
                x[0] = inp;  x[1] = inp;
                x[2] = 0.0;  x[3] = -inp;
                x[4] = -inp;  x[5] = 0.0;
                x[6] = 0.0;  x[7] = inp;
                break;
    case 7:
                x[0] = inp;  x[1] = -inp;
                x[2] = c45;  x[3] = -c45;
                x[4] = 0.0;  x[5] = -inp;
                x[6] = -c45;  x[7] = -c45;
                break;
    }
    biquad_stride4_section3(x, out, coeff, hist, 8, 1, 0);
}

/* demodulation_decimation_band8_section3_std
 ************************************************************************/
void demodulation_decimation_band8_section3(double inp,
                                            double* out, const double* coeff, double* hist, int step)
{
    double x[16];
    double c22;
    double s22;
    double c45;

    c22 = inp * M_c22;
    s22 = inp * M_s22;
    c45 = inp * M_c45;
    switch ((unsigned int)step & 0xf) {
    case 0:
                x[0] = inp;  x[1] = inp;
                x[2] = inp;  x[3] = 0.0;
                x[4] = inp;  x[5] = 0.0;
                x[6] = inp;  x[7] = 0.0;
                x[8] = inp;  x[9] = 0.0;
                x[10] = inp;  x[11] = 0.0;
                x[12] = inp;  x[13] = 0.0;
                x[14] = inp;  x[15] = 0.0;
                break;
    case 1:
                x[0] = inp;  x[1] = -inp;
                x[2] = c22;  x[3] = s22;
                x[4] = c45;  x[5] = c45;
                x[6] = s22;  x[7] = c22;
                x[8] = 0.0;  x[9] = inp;
                x[10] = -s22;  x[11] = c22;
                x[12] = -c45;  x[13] = c45;
                x[14] = -c22;  x[15] = s22;
                break;
    case 2:
                x[0] = inp;  x[1] = inp;
                x[2] = c45;  x[3] = c45;
                x[4] = 0.0;  x[5] = inp;
                x[6] = -c45;  x[7] = c45;
                x[8] = -inp;  x[9] = 0.0;
                x[10] = -c45;  x[11] = -c45;
                x[12] = 0.0;  x[13] = -inp;
                x[14] = c45;  x[15] = -c45;
                break;
    case 3:
                x[0] = inp;  x[1] = -inp;
                x[2] = s22;  x[3] = c22;
                x[4] = -c45;  x[5] = c45;
                x[6] = -c22;  x[7] = -s22;
                x[8] = 0.0;  x[9] = -inp;
                x[10] = c22;  x[11] = -s22;
                x[12] = c45;  x[13] = c45;
                x[14] = -s22;  x[15] = c22;
                break;
    case 4:
                x[0] = inp;  x[1] = inp;
                x[2] = 0.0;  x[3] = inp;
                x[4] = -inp;  x[5] = 0.0;
                x[6] = 0.0;  x[7] = -inp;
                x[8] = inp;  x[9] = 0.0;
                x[10] = 0.0;  x[11] = inp;
                x[12] = -inp;  x[13] = 0.0;
                x[14] = 0.0;  x[15] = -inp;
                break;
    case 5:
                x[0] = inp;  x[1] = -inp;
                x[2] = -s22;  x[3] = c22;
                x[4] = -c45;  x[5] = -c45;
                x[6] = c22;  x[7] = -s22;
                x[8] = 0.0;  x[9] = inp;
                x[10] = -c22;  x[11] = -s22;
                x[12] = c45;  x[13] = -c45;
                x[14] = s22;  x[15] = c22;
                break;
    case 6:
                x[0] = inp;  x[1] = inp;
                x[2] = -c45;  x[3] = c45;
                x[4] = 0.0;  x[5] = -inp;
                x[6] = c45;  x[7] = c45;
                x[8] = -inp;  x[9] = 0.0;
                x[10] = c45;  x[11] = -c45;
                x[12] = 0.0;  x[13] = inp;
                x[14] = -c45;  x[15] = -c45;
                break;
    case 7:
                x[0] = inp;  x[1] = -inp;
                x[2] = -c22;  x[3] = s22;
                x[4] = c45;  x[5] = -c45;
                x[6] = -s22;  x[7] = c22;
                x[8] = 0.0;  x[9] = -inp;
                x[10] = s22;  x[11] = c22;
                x[12] = -c45;  x[13] = -c45;
                x[14] = c22;  x[15] = s22;
                break;
    case 8:
                x[0] = inp;  x[1] = inp;
                x[2] = -inp;  x[3] = 0.0;
                x[4] = inp;  x[5] = 0.0;
                x[6] = -inp;  x[7] = 0.0;
                x[8] = inp;  x[9] = 0.0;
                x[10] = -inp;  x[11] = 0.0;
                x[12] = inp;  x[13] = 0.0;
                x[14] = -inp;  x[15] = 0.0;
                break;
    case 9:
                x[0] = inp;  x[1] = -inp;
                x[2] = -c22;  x[3] = -s22;
                x[4] = c45;  x[5] = c45;
                x[6] = -s22;  x[7] = -c22;
                x[8] = 0.0;  x[9] = inp;
                x[10] = s22;  x[11] = -c22;
                x[12] = -c45;  x[13] = c45;
                x[14] = c22;  x[15] = -s22;
                break;
    case 10:
                x[0] = inp;  x[1] = inp;
                x[2] = -c45;  x[3] = -c45;
                x[4] = 0.0;  x[5] = inp;
                x[6] = c45;  x[7] = -c45;
                x[8] = -inp;  x[9] = 0.0;
                x[10] = c45;  x[11] = c45;
                x[12] = 0.0;  x[13] = -inp;
                x[14] = -c45;  x[15] = c45;
                break;
    case 11:
                x[0] = inp;  x[1] = -inp;
                x[2] = -s22;  x[3] = -c22;
                x[4] = -c45;  x[5] = c45;
                x[6] = c22;  x[7] = s22;
                x[8] = 0.0;  x[9] = -inp;
                x[10] = -c22;  x[11] = s22;
                x[12] = c45;  x[13] = c45;
                x[14] = s22;  x[15] = -c22;
                break;
    case 12:
                x[0] = inp;  x[1] = inp;
                x[2] = 0.0;  x[3] = -inp;
                x[4] = -inp;  x[5] = 0.0;
                x[6] = 0.0;  x[7] = inp;
                x[8] = inp;  x[9] = 0.0;
                x[10] = 0.0;  x[11] = -inp;
                x[12] = -inp;  x[13] = 0.0;
                x[14] = 0.0;  x[15] = inp;
                break;
    case 13:
                x[0] = inp;  x[1] = -inp;
                x[2] = s22;  x[3] = -c22;
                x[4] = -c45;  x[5] = -c45;
                x[6] = -c22;  x[7] = s22;
                x[8] = 0.0;  x[9] = inp;
                x[10] = c22;  x[11] = s22;
                x[12] = c45;  x[13] = -c45;
                x[14] = -s22;  x[15] = -c22;
                break;
    case 14:
                x[0] = inp;  x[1] = inp;
                x[2] = c45;  x[3] = -c45;
                x[4] = 0.0;  x[5] = -inp;
                x[6] = -c45;  x[7] = -c45;
                x[8] = -inp;  x[9] = 0.0;
                x[10] = -c45;  x[11] = c45;
                x[12] = 0.0;  x[13] = inp;
                x[14] = c45;  x[15] = c45;
                break;
    case 15:
                x[0] = inp;  x[1] = -inp;
                x[2] = c22;  x[3] = -s22;
                x[4] = c45;  x[5] = -c45;
                x[6] = s22;  x[7] = -c22;
                x[8] = 0.0;  x[9] = -inp;
                x[10] = -s22;  x[11] = -c22;
                x[12] = -c45;  x[13] = -c45;
                x[14] = -c22;  x[15] = -s22;
                break;
    }
    biquad_stride16_section3(x, out, coeff, hist, 16, 1, 0);
}


/* demodulation_decimation_band16_section3_std
 ************************************************************************/
void demodulation_decimation_band16_section3(double inp,
                                             double* out, const double* coeff, double* hist, int step)
{
    double x[32];
    double c11;
    double s11;
    double c22;
    double s22;
    double c33;
    double s33;
    double c45;

    c11 = inp * M_c11;
    s11 = inp * M_s11;
    c22 = inp * M_c22;
    s22 = inp * M_s22;
    c33 = inp * M_c33;
    s33 = inp * M_s33;
    c45 = inp * M_c45;
    switch ((unsigned int)step & 0x1f) {
    case 0:
                x[0] = inp;  x[1] = inp;
                x[2] = inp;  x[3] = 0.0;
                x[4] = inp;  x[5] = 0.0;
                x[6] = inp;  x[7] = 0.0;
                x[8] = inp;  x[9] = 0.0;
                x[10] = inp;  x[11] = 0.0;
                x[12] = inp;  x[13] = 0.0;
                x[14] = inp;  x[15] = 0.0;
                x[16] = inp;  x[17] = 0.0;
                x[18] = inp;  x[19] = 0.0;
                x[20] = inp;  x[21] = 0.0;
                x[22] = inp;  x[23] = 0.0;
                x[24] = inp;  x[25] = 0.0;
                x[26] = inp;  x[27] = 0.0;
                x[28] = inp;  x[29] = 0.0;
                x[30] = inp;  x[31] = 0.0;
                break;
    case 1:
                x[0] = inp;  x[1] = -inp;
                x[2] = c11;  x[3] = s11;
                x[4] = c22;  x[5] = s22;
                x[6] = c33;  x[7] = s33;
                x[8] = c45;  x[9] = c45;
                x[10] = s33;  x[11] = c33;
                x[12] = s22;  x[13] = c22;
                x[14] = s11;  x[15] = c11;
                x[16] = 0.0;  x[17] = inp;
                x[18] = -s11;  x[19] = c11;
                x[20] = -s22;  x[21] = c22;
                x[22] = -s33;  x[23] = c33;
                x[24] = -c45;  x[25] = c45;
                x[26] = -c33;  x[27] = s33;
                x[28] = -c22;  x[29] = s22;
                x[30] = -c11;  x[31] = s11;
                break;
    case 2:
                x[0] = inp;  x[1] = inp;
                x[2] = c22;  x[3] = s22;
                x[4] = c45;  x[5] = c45;
                x[6] = s22;  x[7] = c22;
                x[8] = 0.0;  x[9] = inp;
                x[10] = -s22;  x[11] = c22;
                x[12] = -c45;  x[13] = c45;
                x[14] = -c22;  x[15] = s22;
                x[16] = -inp;  x[17] = 0.0;
                x[18] = -c22;  x[19] = -s22;
                x[20] = -c45;  x[21] = -c45;
                x[22] = -s22;  x[23] = -c22;
                x[24] = 0.0;  x[25] = -inp;
                x[26] = s22;  x[27] = -c22;
                x[28] = c45;  x[29] = -c45;
                x[30] = c22;  x[31] = -s22;
                break;
    case 3:
                x[0] = inp;  x[1] = -inp;
                x[2] = c33;  x[3] = s33;
                x[4] = s22;  x[5] = c22;
                x[6] = -s11;  x[7] = c11;
                x[8] = -c45;  x[9] = c45;
                x[10] = -c11;  x[11] = s11;
                x[12] = -c22;  x[13] = -s22;
                x[14] = -s33;  x[15] = -c33;
                x[16] = 0.0;  x[17] = -inp;
                x[18] = s33;  x[19] = -c33;
                x[20] = c22;  x[21] = -s22;
                x[22] = c11;  x[23] = s11;
                x[24] = c45;  x[25] = c45;
                x[26] = s11;  x[27] = c11;
                x[28] = -s22;  x[29] = c22;
                x[30] = -c33;  x[31] = s33;
                break;
    case 4:
                x[0] = inp;  x[1] = inp;
                x[2] = c45;  x[3] = c45;
                x[4] = 0.0;  x[5] = inp;
                x[6] = -c45;  x[7] = c45;
                x[8] = -inp;  x[9] = 0.0;
                x[10] = -c45;  x[11] = -c45;
                x[12] = 0.0;  x[13] = -inp;
                x[14] = c45;  x[15] = -c45;
                x[16] = inp;  x[17] = 0.0;
                x[18] = c45;  x[19] = c45;
                x[20] = 0.0;  x[21] = inp;
                x[22] = -c45;  x[23] = c45;
                x[24] = -inp;  x[25] = 0.0;
                x[26] = -c45;  x[27] = -c45;
                x[28] = 0.0;  x[29] = -inp;
                x[30] = c45;  x[31] = -c45;
                break;
    case 5:
                x[0] = inp;  x[1] = -inp;
                x[2] = s33;  x[3] = c33;
                x[4] = -s22;  x[5] = c22;
                x[6] = -c11;  x[7] = s11;
                x[8] = -c45;  x[9] = -c45;
                x[10] = s11;  x[11] = -c11;
                x[12] = c22;  x[13] = -s22;
                x[14] = c33;  x[15] = s33;
                x[16] = 0.0;  x[17] = inp;
                x[18] = -c33;  x[19] = s33;
                x[20] = -c22;  x[21] = -s22;
                x[22] = -s11;  x[23] = -c11;
                x[24] = c45;  x[25] = -c45;
                x[26] = c11;  x[27] = s11;
                x[28] = s22;  x[29] = c22;
                x[30] = -s33;  x[31] = c33;
                break;
    case 6:
                x[0] = inp;  x[1] = inp;
                x[2] = s22;  x[3] = c22;
                x[4] = -c45;  x[5] = c45;
                x[6] = -c22;  x[7] = -s22;
                x[8] = 0.0;  x[9] = -inp;
                x[10] = c22;  x[11] = -s22;
                x[12] = c45;  x[13] = c45;
                x[14] = -s22;  x[15] = c22;
                x[16] = -inp;  x[17] = 0.0;
                x[18] = -s22;  x[19] = -c22;
                x[20] = c45;  x[21] = -c45;
                x[22] = c22;  x[23] = s22;
                x[24] = 0.0;  x[25] = inp;
                x[26] = -c22;  x[27] = s22;
                x[28] = -c45;  x[29] = -c45;
                x[30] = s22;  x[31] = -c22;
                break;
    case 7:
                x[0] = inp;  x[1] = -inp;
                x[2] = s11;  x[3] = c11;
                x[4] = -c22;  x[5] = s22;
                x[6] = -s33;  x[7] = -c33;
                x[8] = c45;  x[9] = -c45;
                x[10] = c33;  x[11] = s33;
                x[12] = -s22;  x[13] = c22;
                x[14] = -c11;  x[15] = -s11;
                x[16] = 0.0;  x[17] = -inp;
                x[18] = c11;  x[19] = -s11;
                x[20] = s22;  x[21] = c22;
                x[22] = -c33;  x[23] = s33;
                x[24] = -c45;  x[25] = -c45;
                x[26] = s33;  x[27] = -c33;
                x[28] = c22;  x[29] = s22;
                x[30] = -s11;  x[31] = c11;
                break;
    case 8:
                x[0] = inp;  x[1] = inp;
                x[2] = 0.0;  x[3] = inp;
                x[4] = -inp;  x[5] = 0.0;
                x[6] = 0.0;  x[7] = -inp;
                x[8] = inp;  x[9] = 0.0;
                x[10] = 0.0;  x[11] = inp;
                x[12] = -inp;  x[13] = 0.0;
                x[14] = 0.0;  x[15] = -inp;
                x[16] = inp;  x[17] = 0.0;
                x[18] = 0.0;  x[19] = inp;
                x[20] = -inp;  x[21] = 0.0;
                x[22] = 0.0;  x[23] = -inp;
                x[24] = inp;  x[25] = 0.0;
                x[26] = 0.0;  x[27] = inp;
                x[28] = -inp;  x[29] = 0.0;
                x[30] = 0.0;  x[31] = -inp;
                break;
    case 9:
                x[0] = inp;  x[1] = -inp;
                x[2] = -s11;  x[3] = c11;
                x[4] = -c22;  x[5] = -s22;
                x[6] = s33;  x[7] = -c33;
                x[8] = c45;  x[9] = c45;
                x[10] = -c33;  x[11] = s33;
                x[12] = -s22;  x[13] = -c22;
                x[14] = c11;  x[15] = -s11;
                x[16] = 0.0;  x[17] = inp;
                x[18] = -c11;  x[19] = -s11;
                x[20] = s22;  x[21] = -c22;
                x[22] = c33;  x[23] = s33;
                x[24] = -c45;  x[25] = c45;
                x[26] = -s33;  x[27] = -c33;
                x[28] = c22;  x[29] = -s22;
                x[30] = s11;  x[31] = c11;
                break;
    case 10:
                x[0] = inp;  x[1] = inp;
                x[2] = -s22;  x[3] = c22;
                x[4] = -c45;  x[5] = -c45;
                x[6] = c22;  x[7] = -s22;
                x[8] = 0.0;  x[9] = inp;
                x[10] = -c22;  x[11] = -s22;
                x[12] = c45;  x[13] = -c45;
                x[14] = s22;  x[15] = c22;
                x[16] = -inp;  x[17] = 0.0;
                x[18] = s22;  x[19] = -c22;
                x[20] = c45;  x[21] = c45;
                x[22] = -c22;  x[23] = s22;
                x[24] = 0.0;  x[25] = -inp;
                x[26] = c22;  x[27] = s22;
                x[28] = -c45;  x[29] = c45;
                x[30] = -s22;  x[31] = -c22;
                break;
    case 11:
                x[0] = inp;  x[1] = -inp;
                x[2] = -s33;  x[3] = c33;
                x[4] = -s22;  x[5] = -c22;
                x[6] = c11;  x[7] = s11;
                x[8] = -c45;  x[9] = c45;
                x[10] = -s11;  x[11] = -c11;
                x[12] = c22;  x[13] = s22;
                x[14] = -c33;  x[15] = s33;
                x[16] = 0.0;  x[17] = -inp;
                x[18] = c33;  x[19] = s33;
                x[20] = -c22;  x[21] = s22;
                x[22] = s11;  x[23] = -c11;
                x[24] = c45;  x[25] = c45;
                x[26] = -c11;  x[27] = s11;
                x[28] = s22;  x[29] = -c22;
                x[30] = s33;  x[31] = c33;
                break;
    case 12:
                x[0] = inp;  x[1] = inp;
                x[2] = -c45;  x[3] = c45;
                x[4] = 0.0;  x[5] = -inp;
                x[6] = c45;  x[7] = c45;
                x[8] = -inp;  x[9] = 0.0;
                x[10] = c45;  x[11] = -c45;
                x[12] = 0.0;  x[13] = inp;
                x[14] = -c45;  x[15] = -c45;
                x[16] = inp;  x[17] = 0.0;
                x[18] = -c45;  x[19] = c45;
                x[20] = 0.0;  x[21] = -inp;
                x[22] = c45;  x[23] = c45;
                x[24] = -inp;  x[25] = 0.0;
                x[26] = c45;  x[27] = -c45;
                x[28] = 0.0;  x[29] = inp;
                x[30] = -c45;  x[31] = -c45;
                break;
    case 13:
                x[0] = inp;  x[1] = -inp;
                x[2] = -c33;  x[3] = s33;
                x[4] = s22;  x[5] = -c22;
                x[6] = s11;  x[7] = c11;
                x[8] = -c45;  x[9] = -c45;
                x[10] = c11;  x[11] = s11;
                x[12] = -c22;  x[13] = s22;
                x[14] = s33;  x[15] = -c33;
                x[16] = 0.0;  x[17] = inp;
                x[18] = -s33;  x[19] = -c33;
                x[20] = c22;  x[21] = s22;
                x[22] = -c11;  x[23] = s11;
                x[24] = c45;  x[25] = -c45;
                x[26] = -s11;  x[27] = c11;
                x[28] = -s22;  x[29] = -c22;
                x[30] = c33;  x[31] = s33;
                break;
    case 14:
                x[0] = inp;  x[1] = inp;
                x[2] = -c22;  x[3] = s22;
                x[4] = c45;  x[5] = -c45;
                x[6] = -s22;  x[7] = c22;
                x[8] = 0.0;  x[9] = -inp;
                x[10] = s22;  x[11] = c22;
                x[12] = -c45;  x[13] = -c45;
                x[14] = c22;  x[15] = s22;
                x[16] = -inp;  x[17] = 0.0;
                x[18] = c22;  x[19] = -s22;
                x[20] = -c45;  x[21] = c45;
                x[22] = s22;  x[23] = -c22;
                x[24] = 0.0;  x[25] = inp;
                x[26] = -s22;  x[27] = -c22;
                x[28] = c45;  x[29] = c45;
                x[30] = -c22;  x[31] = -s22;
                break;
    case 15:
                x[0] = inp;  x[1] = -inp;
                x[2] = -c11;  x[3] = s11;
                x[4] = c22;  x[5] = -s22;
                x[6] = -c33;  x[7] = s33;
                x[8] = c45;  x[9] = -c45;
                x[10] = -s33;  x[11] = c33;
                x[12] = s22;  x[13] = -c22;
                x[14] = -s11;  x[15] = c11;
                x[16] = 0.0;  x[17] = -inp;
                x[18] = s11;  x[19] = c11;
                x[20] = -s22;  x[21] = -c22;
                x[22] = s33;  x[23] = c33;
                x[24] = -c45;  x[25] = -c45;
                x[26] = c33;  x[27] = s33;
                x[28] = -c22;  x[29] = -s22;
                x[30] = c11;  x[31] = s11;
                break;
    case 16:
                x[0] = inp;  x[1] = inp;
                x[2] = -inp;  x[3] = 0.0;
                x[4] = inp;  x[5] = 0.0;
                x[6] = -inp;  x[7] = 0.0;
                x[8] = inp;  x[9] = 0.0;
                x[10] = -inp;  x[11] = 0.0;
                x[12] = inp;  x[13] = 0.0;
                x[14] = -inp;  x[15] = 0.0;
                x[16] = inp;  x[17] = 0.0;
                x[18] = -inp;  x[19] = 0.0;
                x[20] = inp;  x[21] = 0.0;
                x[22] = -inp;  x[23] = 0.0;
                x[24] = inp;  x[25] = 0.0;
                x[26] = -inp;  x[27] = 0.0;
                x[28] = inp;  x[29] = 0.0;
                x[30] = -inp;  x[31] = 0.0;
                break;
    case 17:
                x[0] = inp;  x[1] = -inp;
                x[2] = -c11;  x[3] = -s11;
                x[4] = c22;  x[5] = s22;
                x[6] = -c33;  x[7] = -s33;
                x[8] = c45;  x[9] = c45;
                x[10] = -s33;  x[11] = -c33;
                x[12] = s22;  x[13] = c22;
                x[14] = -s11;  x[15] = -c11;
                x[16] = 0.0;  x[17] = inp;
                x[18] = s11;  x[19] = -c11;
                x[20] = -s22;  x[21] = c22;
                x[22] = s33;  x[23] = -c33;
                x[24] = -c45;  x[25] = c45;
                x[26] = c33;  x[27] = -s33;
                x[28] = -c22;  x[29] = s22;
                x[30] = c11;  x[31] = -s11;
                break;
    case 18:
                x[0] = inp;  x[1] = inp;
                x[2] = -c22;  x[3] = -s22;
                x[4] = c45;  x[5] = c45;
                x[6] = -s22;  x[7] = -c22;
                x[8] = 0.0;  x[9] = inp;
                x[10] = s22;  x[11] = -c22;
                x[12] = -c45;  x[13] = c45;
                x[14] = c22;  x[15] = -s22;
                x[16] = -inp;  x[17] = 0.0;
                x[18] = c22;  x[19] = s22;
                x[20] = -c45;  x[21] = -c45;
                x[22] = s22;  x[23] = c22;
                x[24] = 0.0;  x[25] = -inp;
                x[26] = -s22;  x[27] = c22;
                x[28] = c45;  x[29] = -c45;
                x[30] = -c22;  x[31] = s22;
                break;
    case 19:
                x[0] = inp;  x[1] = -inp;
                x[2] = -c33;  x[3] = -s33;
                x[4] = s22;  x[5] = c22;
                x[6] = s11;  x[7] = -c11;
                x[8] = -c45;  x[9] = c45;
                x[10] = c11;  x[11] = -s11;
                x[12] = -c22;  x[13] = -s22;
                x[14] = s33;  x[15] = c33;
                x[16] = 0.0;  x[17] = -inp;
                x[18] = -s33;  x[19] = c33;
                x[20] = c22;  x[21] = -s22;
                x[22] = -c11;  x[23] = -s11;
                x[24] = c45;  x[25] = c45;
                x[26] = -s11;  x[27] = -c11;
                x[28] = -s22;  x[29] = c22;
                x[30] = c33;  x[31] = -s33;
                break;
    case 20:
                x[0] = inp;  x[1] = inp;
                x[2] = -c45;  x[3] = -c45;
                x[4] = 0.0;  x[5] = inp;
                x[6] = c45;  x[7] = -c45;
                x[8] = -inp;  x[9] = 0.0;
                x[10] = c45;  x[11] = c45;
                x[12] = 0.0;  x[13] = -inp;
                x[14] = -c45;  x[15] = c45;
                x[16] = inp;  x[17] = 0.0;
                x[18] = -c45;  x[19] = -c45;
                x[20] = 0.0;  x[21] = inp;
                x[22] = c45;  x[23] = -c45;
                x[24] = -inp;  x[25] = 0.0;
                x[26] = c45;  x[27] = c45;
                x[28] = 0.0;  x[29] = -inp;
                x[30] = -c45;  x[31] = c45;
                break;
    case 21:
                x[0] = inp;  x[1] = -inp;
                x[2] = -s33;  x[3] = -c33;
                x[4] = -s22;  x[5] = c22;
                x[6] = c11;  x[7] = -s11;
                x[8] = -c45;  x[9] = -c45;
                x[10] = -s11;  x[11] = c11;
                x[12] = c22;  x[13] = -s22;
                x[14] = -c33;  x[15] = -s33;
                x[16] = 0.0;  x[17] = inp;
                x[18] = c33;  x[19] = -s33;
                x[20] = -c22;  x[21] = -s22;
                x[22] = s11;  x[23] = c11;
                x[24] = c45;  x[25] = -c45;
                x[26] = -c11;  x[27] = -s11;
                x[28] = s22;  x[29] = c22;
                x[30] = s33;  x[31] = -c33;
                break;
    case 22:
                x[0] = inp;  x[1] = inp;
                x[2] = -s22;  x[3] = -c22;
                x[4] = -c45;  x[5] = c45;
                x[6] = c22;  x[7] = s22;
                x[8] = 0.0;  x[9] = -inp;
                x[10] = -c22;  x[11] = s22;
                x[12] = c45;  x[13] = c45;
                x[14] = s22;  x[15] = -c22;
                x[16] = -inp;  x[17] = 0.0;
                x[18] = s22;  x[19] = c22;
                x[20] = c45;  x[21] = -c45;
                x[22] = -c22;  x[23] = -s22;
                x[24] = 0.0;  x[25] = inp;
                x[26] = c22;  x[27] = -s22;
                x[28] = -c45;  x[29] = -c45;
                x[30] = -s22;  x[31] = c22;
                break;
    case 23:
                x[0] = inp;  x[1] = -inp;
                x[2] = -s11;  x[3] = -c11;
                x[4] = -c22;  x[5] = s22;
                x[6] = s33;  x[7] = c33;
                x[8] = c45;  x[9] = -c45;
                x[10] = -c33;  x[11] = -s33;
                x[12] = -s22;  x[13] = c22;
                x[14] = c11;  x[15] = s11;
                x[16] = 0.0;  x[17] = -inp;
                x[18] = -c11;  x[19] = s11;
                x[20] = s22;  x[21] = c22;
                x[22] = c33;  x[23] = -s33;
                x[24] = -c45;  x[25] = -c45;
                x[26] = -s33;  x[27] = c33;
                x[28] = c22;  x[29] = s22;
                x[30] = s11;  x[31] = -c11;
                break;
    case 24:
                x[0] = inp;  x[1] = inp;
                x[2] = 0.0;  x[3] = -inp;
                x[4] = -inp;  x[5] = 0.0;
                x[6] = 0.0;  x[7] = inp;
                x[8] = inp;  x[9] = 0.0;
                x[10] = 0.0;  x[11] = -inp;
                x[12] = -inp;  x[13] = 0.0;
                x[14] = 0.0;  x[15] = inp;
                x[16] = inp;  x[17] = 0.0;
                x[18] = 0.0;  x[19] = -inp;
                x[20] = -inp;  x[21] = 0.0;
                x[22] = 0.0;  x[23] = inp;
                x[24] = inp;  x[25] = 0.0;
                x[26] = 0.0;  x[27] = -inp;
                x[28] = -inp;  x[29] = 0.0;
                x[30] = 0.0;  x[31] = inp;
                break;
    case 25:
                x[0] = inp;  x[1] = -inp;
                x[2] = s11;  x[3] = -c11;
                x[4] = -c22;  x[5] = -s22;
                x[6] = -s33;  x[7] = c33;
                x[8] = c45;  x[9] = c45;
                x[10] = c33;  x[11] = -s33;
                x[12] = -s22;  x[13] = -c22;
                x[14] = -c11;  x[15] = s11;
                x[16] = 0.0;  x[17] = inp;
                x[18] = c11;  x[19] = s11;
                x[20] = s22;  x[21] = -c22;
                x[22] = -c33;  x[23] = -s33;
                x[24] = -c45;  x[25] = c45;
                x[26] = s33;  x[27] = c33;
                x[28] = c22;  x[29] = -s22;
                x[30] = -s11;  x[31] = -c11;
                break;
    case 26:
                x[0] = inp;  x[1] = inp;
                x[2] = s22;  x[3] = -c22;
                x[4] = -c45;  x[5] = -c45;
                x[6] = -c22;  x[7] = s22;
                x[8] = 0.0;  x[9] = inp;
                x[10] = c22;  x[11] = s22;
                x[12] = c45;  x[13] = -c45;
                x[14] = -s22;  x[15] = -c22;
                x[16] = -inp;  x[17] = 0.0;
                x[18] = -s22;  x[19] = c22;
                x[20] = c45;  x[21] = c45;
                x[22] = c22;  x[23] = -s22;
                x[24] = 0.0;  x[25] = -inp;
                x[26] = -c22;  x[27] = -s22;
                x[28] = -c45;  x[29] = c45;
                x[30] = s22;  x[31] = c22;
                break;
    case 27:
                x[0] = inp;  x[1] = -inp;
                x[2] = s33;  x[3] = -c33;
                x[4] = -s22;  x[5] = -c22;
                x[6] = -c11;  x[7] = -s11;
                x[8] = -c45;  x[9] = c45;
                x[10] = s11;  x[11] = c11;
                x[12] = c22;  x[13] = s22;
                x[14] = c33;  x[15] = -s33;
                x[16] = 0.0;  x[17] = -inp;
                x[18] = -c33;  x[19] = -s33;
                x[20] = -c22;  x[21] = s22;
                x[22] = -s11;  x[23] = c11;
                x[24] = c45;  x[25] = c45;
                x[26] = c11;  x[27] = -s11;
                x[28] = s22;  x[29] = -c22;
                x[30] = -s33;  x[31] = -c33;
                break;
    case 28:
                x[0] = inp;  x[1] = inp;
                x[2] = c45;  x[3] = -c45;
                x[4] = 0.0;  x[5] = -inp;
                x[6] = -c45;  x[7] = -c45;
                x[8] = -inp;  x[9] = 0.0;
                x[10] = -c45;  x[11] = c45;
                x[12] = 0.0;  x[13] = inp;
                x[14] = c45;  x[15] = c45;
                x[16] = inp;  x[17] = 0.0;
                x[18] = c45;  x[19] = -c45;
                x[20] = 0.0;  x[21] = -inp;
                x[22] = -c45;  x[23] = -c45;
                x[24] = -inp;  x[25] = 0.0;
                x[26] = -c45;  x[27] = c45;
                x[28] = 0.0;  x[29] = inp;
                x[30] = c45;  x[31] = c45;
                break;
    case 29:
                x[0] = inp;  x[1] = -inp;
                x[2] = c33;  x[3] = -s33;
                x[4] = s22;  x[5] = -c22;
                x[6] = -s11;  x[7] = -c11;
                x[8] = -c45;  x[9] = -c45;
                x[10] = -c11;  x[11] = -s11;
                x[12] = -c22;  x[13] = s22;
                x[14] = -s33;  x[15] = c33;
                x[16] = 0.0;  x[17] = inp;
                x[18] = s33;  x[19] = c33;
                x[20] = c22;  x[21] = s22;
                x[22] = c11;  x[23] = -s11;
                x[24] = c45;  x[25] = -c45;
                x[26] = s11;  x[27] = -c11;
                x[28] = -s22;  x[29] = -c22;
                x[30] = -c33;  x[31] = -s33;
                break;
    case 30:
                x[0] = inp;  x[1] = inp;
                x[2] = c22;  x[3] = -s22;
                x[4] = c45;  x[5] = -c45;
                x[6] = s22;  x[7] = -c22;
                x[8] = 0.0;  x[9] = -inp;
                x[10] = -s22;  x[11] = -c22;
                x[12] = -c45;  x[13] = -c45;
                x[14] = -c22;  x[15] = -s22;
                x[16] = -inp;  x[17] = 0.0;
                x[18] = -c22;  x[19] = s22;
                x[20] = -c45;  x[21] = c45;
                x[22] = -s22;  x[23] = c22;
                x[24] = 0.0;  x[25] = inp;
                x[26] = s22;  x[27] = c22;
                x[28] = c45;  x[29] = c45;
                x[30] = c22;  x[31] = s22;
                break;
    case 31:
                x[0] = inp;  x[1] = -inp;
                x[2] = c11;  x[3] = -s11;
                x[4] = c22;  x[5] = -s22;
                x[6] = c33;  x[7] = -s33;
                x[8] = c45;  x[9] = -c45;
                x[10] = s33;  x[11] = -c33;
                x[12] = s22;  x[13] = -c22;
                x[14] = s11;  x[15] = -c11;
                x[16] = 0.0;  x[17] = -inp;
                x[18] = -s11;  x[19] = -c11;
                x[20] = -s22;  x[21] = -c22;
                x[22] = -s33;  x[23] = -c33;
                x[24] = -c45;  x[25] = -c45;
                x[26] = -c33;  x[27] = -s33;
                x[28] = -c22;  x[29] = -s22;
                x[30] = -c11;  x[31] = -s11;
                break;
    }
    biquad_stride16_section3(x, out, coeff, hist, 32, 1, 0);
}


int demodulation_init(void)
{
    RTSLOG_INFO("%s\n", VEC_MESSAGE);
    return 0;
}
