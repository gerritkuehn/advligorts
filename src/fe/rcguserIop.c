///	@file rcguserIop.c
///	@brief File contains startup routines for IOP running in user space.

#define _GNU_SOURCE
#include "../drv/rts-cpu-isolator/usp-allocate.h" 

#include "rcguserCommon.h"
#include "controllerko.h" //tdsControl, tdsCount, etc
#include "controller.h" //daqArea
#include "feComms.h" //RFM_FE_COMMS
#include "drv/rts-logger.h"
#include "util/macros.h" //COUNT_OF()
#include "../fe/print_io_info.h"


#include <unistd.h>
#include <ctype.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/file.h>
#include <stdio.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>



// These externs and "16" need to go to a header file (mbuf.h)
extern int   fe_start_iop_user( );


void
usage( )
{
    fprintf( stderr, "Usage: rcgUser -m system_name \n" );
    fprintf( stderr, "-h - help\n" );
}

/// Startup function for initialization of kernel module.
int
main( int argc, char** argv )
{
    int  status;
    int  ii, jj, kk, mm; /// @param ii,jj,kk default loop counters
    char fname[ 128 ]; /// @param fname[128] Name of shared mem area to allocate
                       /// for DAQ data
    int cards; /// @param cards Number of PCIe cards found on bus
    int do32Cnt = 0; /// @param do32Cnt Total number of Contec 32 bit DIO cards
                 /// found by control model.
    int doIIRO16Cnt = 0; /// @param doIIRO16Cnt Total number of Acces I/O 16 bit
                     /// relay cards found by control model.
    int doIIRO8Cnt = 0; /// @param doIIRO8Cnt Total number of Acces I/O 8 bit relay
                    /// cards found by control model.
    int ret; /// @param ret Return value from various Malloc calls to allocate
             /// memory.
    int   cnt;
    char* sysname;
    char  shm_name[ 64 ];
    int   c;
    char* modelname;
    int allocated_cpu = -1;




    kk = 0;

    jj = 0;

    allocated_cpu = rts_cpu_isolator_alloc_core(allocated_cpu);
    if ( allocated_cpu == -1 )
    {
        RTSLOG_WARN("There was an error getting a free isolated core. "
                    "We are going to continue execution and expect isolation is set by some other method.\n");
    }
    else
    {
        RTSLOG_INFO("Going to isolate onto CPU : %d\n", allocated_cpu);

        //Lock our thread to the allocated core
        cpu_set_t my_set;
        CPU_ZERO(&my_set);
        CPU_SET(allocated_cpu, &my_set);
        if ( sched_setaffinity(0, sizeof(cpu_set_t), &my_set) != 0)
        {
            RTSLOG_ERROR("There was an error locking to the allocated core\n");
            return 1;
        }
    }


    attach_shared_memory(SYSTEM_NAME_STRING_LOWER);
    pLocalEpics = (CDS_EPICS*)&( (RFM_FE_COMMS*)_epics_shm )->epicsSpace;

    // Find and initialize all PCI I/O modules
    // ******************************************************* Following I/O
    // card info is from feCode
    cards = COUNT_OF(cards_used);
    RTSLOG_INFO( "configured to use %d cards\n", cards );
    cdsPciModules.cards = cards;
    cdsPciModules.cards_used = cards_used;
    // return -1;
    RTSLOG_INFO( "Initializing PCI Modules for IOP\n" );
    for ( jj = 0; jj < cards; jj++ )
        RTSLOG_INFO(
            "Card %d type = %d\n", jj, cdsPciModules.cards_used[ jj ].type );
    cdsPciModules.adcCount = 0;
    cdsPciModules.dacCount = 0;
    cdsPciModules.dioCount = 0;
    cdsPciModules.doCount = 0;
    cdsPciModules.rfmCount = 0;
    cdsPciModules.dolphinCount = 0;

    // If running as a control process, I/O card information is via ipc shared
    // memory
    RTSLOG_INFO( "%d PCI cards found\n", cards );
    status = 0;

    ioMemData->totalCards = cards;

    // Have to search thru all cards and find desired instance for application
    // Master will map ADC cards first, then DAC and finally DIO
    for ( jj = 0; jj < cards; jj++ )
    {
        /*
        RTSLOG_DEBUG("Model %d = %d, type = %d, instance = %d, dacCnt = %d \n",
                ii,ioMemData->model[ii],
                cdsPciModules.cards_used[jj].type,
                cdsPciModules.cards_used[jj].instance,
                dacCnt);
                */
        switch ( cdsPciModules.cards_used[ jj ].type )
        {
        case GSC_16AI64SSA:
            kk = cdsPciModules.adcCount;
            cdsPciModules.adcType[ kk ] = GSC_16AI64SSA;
            cdsPciModules.adcCount++;
            status++;
            break;
        case GSC_16AO16:
            kk = cdsPciModules.dacCount;
            cdsPciModules.dacType[ kk ] = GSC_16AO16;
            cdsPciModules.dacCount++;
            status++;
            break;
        case GSC_18AO8:
            kk = cdsPciModules.dacCount;
            cdsPciModules.dacType[ kk ] = GSC_18AO8;
            cdsPciModules.dacCount++;
            status++;
            break;
        case GSC_20AO8:
            kk = cdsPciModules.dacCount;
            cdsPciModules.dacType[ kk ] = GSC_20AO8;
            cdsPciModules.dacCount++;
            status++;
            break;
        case CON_6464DIO:
            kk = cdsPciModules.doCount;
            cdsPciModules.doType[ kk ] = CON_6464DIO;
            cdsPciModules.doCount++;
            cdsPciModules.card_count[ CON_6464DIO ] ++;
            status += 2;
            break;
        case CON_32DO:
            kk = cdsPciModules.doCount;
            cdsPciModules.doType[ kk ] = CON_32DO;
            cdsPciModules.doCount++;
            cdsPciModules.card_count[ CON_32DO ] ++;
            cdsPciModules.doInstance[ kk ] = do32Cnt;
            status++;
            break;
        case ACS_16DIO:
            kk = cdsPciModules.doCount;
            cdsPciModules.doCount++;
            cdsPciModules.card_count[ ACS_16DIO ] ++;
            cdsPciModules.doInstance[ kk ] = doIIRO16Cnt;
            status++;
            break;
        case ACS_8DIO:
            kk = cdsPciModules.doCount;
            cdsPciModules.doCount++;
            cdsPciModules.card_count[ ACS_8DIO ] ++;
            cdsPciModules.doInstance[ kk ] = doIIRO8Cnt;
            status++;
            break;
        default:
            break;
        }
    }
    // If no ADC cards were found, then control models cannot run
    if ( !cdsPciModules.adcCount )
    {
        RTSLOG_ERROR( "No ADC cards found - exiting\n" );
        return -1;
    }
    // This did not quite work for some reason
    // Need to find a way to handle skipped DAC cards in controllers
    // cdsPciModules.dacCount = ioMemData->dacCount;

    RTSLOG_INFO( "%d PCI cards found \n", status );
    if ( status < cards )
    {
        RTSLOG_ERROR( "Did not find correct number of cards! Expected %d "
                "and Found %d\n",
                cards,
                status );
        cardCountErr = 1;
    }

    // Print out all the I/O information
    RTSLOG_RAW( "******************************************************************"
            "*********\n" );
    // Master send module counds to control models via ipc shm
    ioMemData->totalCards = status;
    ioMemData->adcCount = cdsPciModules.adcCount;
    ioMemData->dacCount = cdsPciModules.dacCount;
    ioMemData->bioCount = cdsPciModules.doCount;
    // kk will act as ioMem location counter for mapping modules
    kk = cdsPciModules.adcCount;
    RTSLOG_INFO( "%d ADC cards found\n", cdsPciModules.adcCount );
    for ( ii = 0; ii < cdsPciModules.adcCount; ii++ )
    {
        // MASTER maps ADC modules first in ipc shm for control models
        ioMemData->model[ ii ] = cdsPciModules.adcType[ ii ];
        ioMemData->ipc[ ii ] = ii; 
        // ioData memory buffer for virtual I/O cards
        ioMemDataIop->model[ ii ] = cdsPciModules.adcType[ ii ];
        ioMemDataIop->ipc[ ii ] = ii; 
        // ioData fill virtual ADC mem with dummy data (65536 x 32 chans)
        for ( jj = 0; jj < 65536; jj++ )
        {
            for ( mm = 0; mm < 32; mm++ )
            {
                ioMemDataIop->iodata[ ii ][ jj ].data[ mm ] = jj - 32767;
            }
        }
        if ( cdsPciModules.adcType[ ii ] == GSC_16AI64SSA )
        {
            RTSLOG_INFO( "\tADC %d is a GSC_16AI64SSA module\n", ii );
            if ( ( cdsPciModules.adcConfig[ ii ] & 0x10000 ) > 0 )
                jj = 32;
            else
                jj = 64;
            RTSLOG_INFO( "\t\tChannels = %d \n", jj );
            RTSLOG_INFO( "\t\tFirmware Rev = %d \n\n",
                    ( cdsPciModules.adcConfig[ ii ] & 0xfff ) );
        }
    }
    RTSLOG_RAW( "******************************************************************"
            "*********\n" );
    RTSLOG_INFO( "%d DAC cards found\n", cdsPciModules.dacCount );
    for ( ii = 0; ii < cdsPciModules.dacCount; ii++ )
    {
        if ( cdsPciModules.dacType[ ii ] == GSC_18AO8 )
        {
            RTSLOG_INFO( "\tDAC %d is a GSC_18AO8 module\n", ii );
        }
        if ( cdsPciModules.dacType[ ii ] == GSC_16AO16 )
        {
            RTSLOG_INFO( "\tDAC %d is a GSC_16AO16 module\n", ii );
            if ( ( cdsPciModules.dacConfig[ ii ] & 0x10000 ) == 0x10000 )
                jj = 8;
            if ( ( cdsPciModules.dacConfig[ ii ] & 0x20000 ) == 0x20000 )
                jj = 12;
            if ( ( cdsPciModules.dacConfig[ ii ] & 0x30000 ) == 0x30000 )
                jj = 16;
            RTSLOG_INFO( "\t\tChannels = %d \n", jj );
            if ( ( cdsPciModules.dacConfig[ ii ] & 0xC0000 ) == 0x0000 )
            {
                RTSLOG_INFO( "\t\tFilters = None\n" );
            }
            if ( ( cdsPciModules.dacConfig[ ii ] & 0xC0000 ) == 0x40000 )
            {
                RTSLOG_INFO( "\t\tFilters = 10kHz\n" );
            }
            if ( ( cdsPciModules.dacConfig[ ii ] & 0xC0000 ) == 0x80000 )
            {
                RTSLOG_INFO( "\t\tFilters = 100kHz\n" );
            }
            if ( ( cdsPciModules.dacConfig[ ii ] & 0x100000 ) == 0x100000 )
            {
                RTSLOG_INFO( "\t\tOutput Type = Differential\n" );
            }
            RTSLOG_INFO( "\t\tFirmware Rev = %d \n\n",
                    ( cdsPciModules.dacConfig[ ii ] & 0xfff ) );
        }
        // Pass DAC info to control processes
        ioMemData->model[ kk ] = cdsPciModules.dacType[ ii ];
        ioMemData->ipc[ kk ] = kk;
        // Following used by IOP to point to ipc memory for inputting DAC
        // data from control models
        cdsPciModules.dacConfig[ ii ] = kk;
        RTSLOG_INFO( "MASTER DAC SLOT %d %d\n", ii, cdsPciModules.dacConfig[ ii ] );
        kk++;
    }
    RTSLOG_RAW( "******************************************************************"
            "*********\n" );
    RTSLOG_INFO( "%d DIO cards found\n", cdsPciModules.dioCount );
    RTSLOG_RAW( "******************************************************************"
            "*********\n" );
    RTSLOG_INFO( "%d IIRO-8 Isolated DIO cards found\n",
            cdsPciModules.card_count[ ACS_8DIO ] );
    RTSLOG_RAW( "******************************************************************"
            "*********\n" );
    RTSLOG_INFO( "%d IIRO-16 Isolated DIO cards found\n",
            cdsPciModules.card_count[ ACS_16DIO ] );
    RTSLOG_RAW( "******************************************************************"
            "*********\n" );
    RTSLOG_INFO( "%d Contec 32ch PCIe DO cards found\n", cdsPciModules.card_count[ CON_32DO ] );
    RTSLOG_INFO( "%d Contec PCIe DIO1616 cards found\n",
            cdsPciModules.card_count[ CON_1616DIO ] );
    RTSLOG_INFO( "%d Contec PCIe DIO6464 cards found\n",
            cdsPciModules.card_count[ CON_6464DIO ] );
    RTSLOG_INFO( "%d DO cards found\n", cdsPciModules.doCount );
    // IOP sends DIO module information to control models
    // Note that for DIO, control modules will perform the I/O directly and
    // therefore need to know the PCIe address of these modules.
    ioMemData->bioCount = cdsPciModules.doCount;
    for ( ii = 0; ii < cdsPciModules.doCount; ii++ )
    {
        // MASTER needs to find Contec 1616 I/O card to control timing receiver.
        if ( cdsPciModules.doType[ ii ] == CON_1616DIO )
        {
            tdsControl[ tdsCount ] = ii;
            RTSLOG_INFO( "TDS controller %d is at %d\n", tdsCount, ii );
            tdsCount++;
        }
        ioMemData->model[ kk ] = cdsPciModules.doType[ ii ];
        // Unlike ADC and DAC, where a memory buffer number is passed, a PCIe
        // address is passed for DIO cards.
        ioMemData->ipc[ kk ] = kk;
        kk++;
    }
    RTSLOG_INFO( "Total of %d I/O modules found and mapped\n", kk );
    RTSLOG_RAW( "******************************************************************"
            "*********\n" );
    print_io_info(SYSTEM_NAME_STRING_LOWER, &cdsPciModules, 1);
    // Following section maps Reflected Memory, both VMIC hardware style and
    // Dolphin PCIe network style. Control units will perform I/O transactions
    // with RFM directly ie IOP does not do RFM I/O. Master unit only maps
    // the RFM I/O space and passes pointers to control models.

    ioMemData->rfmCount = 9;
#ifdef DOLPHIN_TEST
    ioMemData->dolphinCount = cdsPciModules.dolphinCount;
    ioMemData->dolphinPcieReadPtr = cdsPciModules.dolphinPcieReadPtr;
    ioMemData->dolphinPcieWritePtr = cdsPciModules.dolphinPcieWritePtr;

#else
    // Clear Dolphin pointers so the control app sees NULLs
    ioMemData->dolphinCount = 0;
    ioMemData->dolphinPcieReadPtr = 0;
    ioMemData->dolphinPcieWritePtr = 0;
#endif
    ioMemData->stop_dolphin_ipcs = 0;

    // Initialize buffer for daqLib.c code
    RTSLOG_INFO( "Initializing space for daqLib buffers\n" );
    daqBuffer = (long)&daqArea[ 0 ];

    // Wait for SDF restore
    for ( cnt = 0; cnt < 10 && pLocalEpics->epicsInput.burtRestore == 0; cnt++ )
    {
        RTSLOG_INFO( "Epics burt restore is %d\n",
                pLocalEpics->epicsInput.burtRestore );
        usleep( 1000000 );
    }
    if ( cnt == 10 )
    {
        return -1;
    }

    pLocalEpics->epicsInput.vmeReset = 0;
    fe_start_iop_user( );

    return 0;
}
