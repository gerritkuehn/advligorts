#ifndef LIGO_TIMING_COMMON_H
#define LIGO_TIMING_COMMON_H

#include "portableInline.h"
#include "cds_types.h"

//***********************************************************************
/// \brief Calculate ADC/DAC duotone offset for diagnostics. \n
///< Code should only run on IOP
//***********************************************************************
#ifdef IOP_MODEL

/***
 * Calculate cross over point of duotime signal
 *
 * count - number of points in data
 * meanVal - mean value of data
 * data - duotone array around start of second as measured by adc
 *
 * returns cross over point of duotone signal in usec before start of second.
 */


// dtrescale = 2/3*(pi * f)^2 with f = 960.5 / 1E6
const float dtrescale = 6.07020313578E-6F;

LIGO_INLINE float
duotone_rescale(float val, float slope)
{
    float c = val / slope;
    return val * (1.0F - dtrescale * c * c);
}

LIGO_INLINE float
duotime(int count, int step, float meanVal, float *data)
{
    float est_slope;
    float x, y, sumX, sumY, sumXX, sumXY;
    int   ii;
    float xInc;
    float offset, slope;
    float den;

    xInc = 1e6f / (float)IOP_IO_RATE;
    x = - step * xInc;
    sumX = 0;
    sumY = 0;
    sumXX = 0;
    sumXY = 0;

    if ((count <= 0) || (data[0] * data[count - 1] >= 0.0F))
    {
        return (-1000.0f);
    }
    est_slope = (data[count - 1] - data[0]) / ((float)count * xInc);
    for (ii = 0; ii < count; ++ii)
    {
        y = duotone_rescale (data[ii] - meanVal, est_slope);
        sumX += x;
        sumY += y;
        sumXX += x * x;
        sumXY += x * y;
        x += xInc;
    }
    den = ((float)count * sumXX - sumX * sumX);
    if (den == 0.0f)
    {
        return (-1000.0f);
    }
    offset = (-sumX * sumXY + sumXX * sumY) / den;
    slope = (-sumX * sumY + (float)count * sumXY) / den;
    if (slope == 0.0f)
    {
        return (-1000.0f);
    }
    return -offset / slope;
}

LIGO_INLINE void
initializeDuotoneDiags( duotone_diag_t* dt_diag )
{
    int ii;
    for ( ii = 0; ii < IOP_IO_RATE; ii++ )
    {
        dt_diag->adc[ ii ] = 0;
        dt_diag->dac[ ii ] = 0;
    }
    dt_diag->totalAdc = 0.0;
    dt_diag->totalDac = 0.0;
    dt_diag->meanAdc = 0.0;
    dt_diag->meanDac = 0.0;
    dt_diag->dacDuoEnable = 0.0;
}
#endif

LIGO_INLINE void
initializeTimingDiags( timing_diag_t* timeinfo )
{
    timeinfo->cpuTimeEverMax = 0;
    timeinfo->cpuTimeEverMaxWhen = 0;
    timeinfo->startGpsTime = 0;
    timeinfo->usrTimeMax = 0;
    timeinfo->cycleTimeSec = 0;
    timeinfo->cycleTimeSecLast = 0;
    timeinfo->longestCycle = 0;
    timeinfo->timeHoldWhenHold = 0;
    timeinfo->usrTime = 0;
    timeinfo->cycleTime = 0;
}

LIGO_INLINE void
sendTimingDiags2Epics( volatile CDS_EPICS* pLocalEpics,
                       timing_diag_t*      timeinfo,
                       adcInfo_t*          adcinfo )
{
    pLocalEpics->epicsOutput.cpuMeterAvg =  (timeinfo->cycleTimeAvg * UNDERSAMPLE / MODEL_RATE_HZ);
    pLocalEpics->epicsOutput.cpuMeter = timeinfo->cycleTimeSec;
    pLocalEpics->epicsOutput.cpuMeterMax = timeinfo->cycleTimeMax;
    pLocalEpics->epicsOutput.cpuLongCycle = timeinfo->longestCycle;
    pLocalEpics->epicsOutput.userTime = timeinfo->usrTimeMax;
    timeinfo->cycleTimeSecLast = timeinfo->cycleTimeSec;
    timeinfo->cycleTimeSec = 0;
    timeinfo->timeHoldWhenHold = timeinfo->longestCycle;
    timeinfo->usrTimeMax = 0;
    timeinfo->cycleTimeAvg = 0;
    timeinfo->longestCycle = 0;

    pLocalEpics->epicsOutput.adcWaitTime =
        adcinfo->adcHoldTimeAvg / ( MODEL_RATE_HZ / UNDERSAMPLE );
    pLocalEpics->epicsOutput.adcWaitMin = adcinfo->adcHoldTimeMin;
    pLocalEpics->epicsOutput.adcWaitMax = adcinfo->adcHoldTimeMax;

    adcinfo->adcHoldTimeAvgPerSec = adcinfo->adcHoldTimeAvg / MODEL_RATE_HZ;
    adcinfo->adcHoldTimeMax = 0;
    adcinfo->adcHoldTimeMin = 0xffff;
    adcinfo->adcHoldTimeAvg = 0;
}

LIGO_INLINE void
captureEocTiming( int            cycle,
                  unsigned int   cycle_gps,
                  timing_diag_t* timeinfo,
                  adcInfo_t*     adcinfo )
{

    // Hold the max cycle time over the last 1 second
    if ( timeinfo->cycleTime > timeinfo->cycleTimeSec )
    {
        timeinfo->cycleTimeSec = timeinfo->cycleTime;
        timeinfo->longestCycle = cycle;
    }
    timeinfo->cycleTimeAvg += timeinfo->cycleTime;
    // Hold the max cycle time since last diag reset
    if ( timeinfo->cycleTime > timeinfo->cycleTimeMax )
        timeinfo->cycleTimeMax = timeinfo->cycleTime;
    // Avoid calculating the max hold time for the first few seconds
    if ( ( timeinfo->startGpsTime + 3 ) < cycle_gps )
    {
        if ( adcinfo->adcHoldTime > adcinfo->adcHoldTimeMax )
            adcinfo->adcHoldTimeMax = adcinfo->adcHoldTime;
        if ( adcinfo->adcHoldTime < adcinfo->adcHoldTimeMin )
            adcinfo->adcHoldTimeMin = adcinfo->adcHoldTime;
        adcinfo->adcHoldTimeAvg += adcinfo->adcHoldTime;
        if ( adcinfo->adcHoldTimeMax > adcinfo->adcHoldTimeEverMax )
        {
            adcinfo->adcHoldTimeEverMax = adcinfo->adcHoldTimeMax;
            adcinfo->adcHoldTimeEverMaxWhen = cycle_gps;
        }
        if ( timeinfo->cycleTimeMax > timeinfo->cpuTimeEverMax )
        {
            timeinfo->cpuTimeEverMax = timeinfo->cycleTimeMax;
            timeinfo->cpuTimeEverMaxWhen = cycle_gps;
        }
    }
}


#endif
