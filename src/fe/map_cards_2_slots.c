#include "../fe/map_cards_2_slots.h"

#include "controller.h"
#include "drv/plx_9056.h" //PLX_VID
#include "drv/gsc16ai64.h" //ADC_SS_ID
#include "drv/gsc18ai32.h" //ADC_18AI32_SS_ID
#include "drv/gsc16ao16.h" //DAC_SS_ID
#include "drv/gsc18ai64.h" //ADC_18AI64_SS_ID
#include "drv/gsc18ao8.h" //DAC_18BIT_SS_ID
#include "drv/gsc20ao8.h" //DAC_20BIT_SS_ID

#include <linux/types.h> //NULL
#include <linux/pci.h>

int
map_cards_2_slots (CDS_HARDWARE* pCds )
{
    struct pci_dev* dacdev;
    int    modCount = 0;
    int fast_adc_cnt = 0;
    int adc_750_cnt = 0;
    int adc_cnt = 0;
    int dac_cnt = 0;
    int dac_18bit_cnt = 0;
    int dac_20bit_cnt = 0;
    int i;


    dacdev = NULL;

    // Clear out IOC slot information
    for ( i = 0; i < 10; i++ )
    {
        pCds->ioc_config[ i ] = DUMMY_CARD;
        pCds->ioc_instance[ i ] = 0;
        pLocalEpics->epicsOutput.pcieInfo[ i ] =
            pCds->ioc_config[ i ] + ( pCds->ioc_instance [ i ] * 32 );
        pLocalEpics->epicsOutput.statSlot[ i ] = 0;
    }

    // Determine card order on backplane by slot
    // This is needed for IOC slot status info and proper setup of
    // IOC Gen2 backplane clocks/speeds/etc.
    while ( ( dacdev = pci_get_device( PLX_VID, PLX_TID, dacdev ) ) )
    {
        // Check if it is an ADC module
        if ( ( dacdev->subsystem_device == ADC_SS_ID ) &&
             ( dacdev->subsystem_vendor == PLX_VID ) )
        {
            pCds->ioc_config[ modCount ] = GSC_16AI64SSA;
            pCds->ioc_instance[ modCount ] = adc_cnt;
            pLocalEpics->epicsOutput.pcieInfo[ modCount ] =
               pCds->ioc_config[ modCount ] + ( pCds->ioc_instance [ modCount ] * 32 );
            adc_cnt ++;
            modCount++;
            pCds->ioc_cards = modCount;
        }
        // Check if it is a 1M ADC module
        if ( ( dacdev->subsystem_device == ADC_18AI32_SS_ID ) &&
             ( dacdev->subsystem_vendor == PLX_VID ) )
        {
            pCds->ioc_config[ modCount ] = GSC_18AI32SSC1M;
            pCds->ioc_instance[ modCount ] = fast_adc_cnt;
            pLocalEpics->epicsOutput.pcieInfo[ modCount ] =
               pCds->ioc_config[ modCount ] + ( pCds->ioc_instance [ modCount ] * 32 );
            fast_adc_cnt++;
            modCount++;
            pCds->ioc_cards = modCount;
        }
        // Check if it is a 750K ADC module
        if ( ( dacdev->subsystem_device == ADC_18AI64_SS_ID ) &&
             ( dacdev->subsystem_vendor == PLX_VID ) )
        {
            pCds->ioc_config[ modCount ] = GSC_18AI64SSC;
            pCds->ioc_instance[ modCount ] = adc_750_cnt;
            pLocalEpics->epicsOutput.pcieInfo[ modCount ] =
               pCds->ioc_config[ modCount ] + ( pCds->ioc_instance [ modCount ] * 32 );
            adc_750_cnt++;
            modCount++;
            pCds->ioc_cards = modCount;
        }
        // Check if it is a DAC16 module
        if ( ( dacdev->subsystem_device == DAC_SS_ID ) &&
             ( dacdev->subsystem_vendor == PLX_VID ) )
        {
            pCds->ioc_config[ modCount ] = GSC_16AO16;
            pCds->ioc_instance[ modCount ] = dac_cnt;
            pLocalEpics->epicsOutput.pcieInfo[ modCount ] =
               pCds->ioc_config[ modCount ] + ( pCds->ioc_instance [ modCount ] * 32 );
            dac_cnt++;
            modCount++;
            pCds->ioc_cards = modCount;
        }
        // Check if it is a DAC18 module
        if ( ( dacdev->subsystem_device == DAC_18BIT_SS_ID ) &&
             ( dacdev->subsystem_vendor == PLX_VID ) )
        {
            pCds->ioc_config[ modCount ] = GSC_18AO8;
            pCds->ioc_instance[ modCount ] = dac_18bit_cnt;
            pLocalEpics->epicsOutput.pcieInfo[ modCount ] =
               pCds->ioc_config[ modCount ] + ( pCds->ioc_instance [ modCount ] * 32 );
            dac_18bit_cnt++;
            modCount++;
            pCds->ioc_cards = modCount;
        }
        // Check if it is a DAC20 module
        if ( ( dacdev->subsystem_device == DAC_20BIT_SS_ID ) &&
             ( dacdev->subsystem_vendor == PLX_VID ) )
        {
            pCds->ioc_config[ modCount ] = GSC_20AO8;
            pCds->ioc_instance[ modCount ] = dac_20bit_cnt;
            pLocalEpics->epicsOutput.pcieInfo[ modCount ] =
               pCds->ioc_config[ modCount ] + ( pCds->ioc_instance [ modCount ] * 32 );
            dac_20bit_cnt++;
            modCount++;
            pCds->ioc_cards = modCount;
        }
    }
    return modCount;
}
