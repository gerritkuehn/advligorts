/* Clear given exceptions in current floating-point environment.
   Copyright (C) 2001-2023 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <https://www.gnu.org/licenses/>.  */

//
// This is ported from glibc and works in kernel space for LIGO real time modules
//

#include "util/ligo_fenv.h"
#include "util/fpu_control.h"

//Global to keep track of errors we have already reported
static unsigned g_error_cache = 0;

//
// Helper function for detecing new exceptions
//
int check_fpu_exceptions( void )
{

    unsigned excepts = fetestexcept(FE_ALL_EXCEPT);

    if ((FE_INVALID & excepts) != 0 && (FE_INVALID & g_error_cache) == 0 )
    {
        RTSLOG_ERROR("FE_ERROR_FPU - FPU detected an invalid math operation.\n");
        g_error_cache |= FE_INVALID;
    }

    if ((FE_DIVBYZERO & excepts) != 0 && (FE_DIVBYZERO & g_error_cache) == 0)
    {
        RTSLOG_ERROR("FE_ERROR_FPU - FPU detected an division by 0 math operation.\n");
        g_error_cache |= FE_DIVBYZERO;
    }

    if ((FE_OVERFLOW & excepts) != 0 && (FE_OVERFLOW & g_error_cache) == 0)
    {
        RTSLOG_ERROR("FE_ERROR_FPU - FPU detected an overflow.\n");
        g_error_cache |= FE_OVERFLOW;
    }

    /*
    if ((FE_UNDERFLOW & excepts) != 0 && (FE_UNDERFLOW & g_error_cache ) == 0)
    {
        RTSLOG_ERROR("FPU detected an underflow.\n");
        g_error_cache |= FE_UNDERFLOW;
    }*/

    /*
    if ((FE_INEXACT & excepts) != 0)
    {
    }*/

    if (excepts & (FE_INVALID | FE_DIVBYZERO | FE_OVERFLOW)) //Anything set
    {
        return FE_ERROR_FPU;
    }
    return 0;
}

void clear_fpu_exceptions(void)
{
    feclearexcept(FE_ALL_EXCEPT); //Clear all FPU exceptions
    g_error_cache = 0;
}

int
feclearexcept (int excepts)
{
  fenv_t temp;
  unsigned int mxcsr;

  /* Mask out unsupported bits/exceptions.  */
  excepts &= FE_ALL_EXCEPT;

  /* Bah, we have to clear selected exceptions.  Since there is no
     `fldsw' instruction we have to do it the hard way.  */
  __asm__ ("fnstenv %0" : "=m" (*&temp));

  /* Clear the relevant bits.  */
  temp.__status_word &= excepts ^ FE_ALL_EXCEPT;

  /* Put the new data in effect.  */
  __asm__ ("fldenv %0" : : "m" (*&temp));

  /* And the same procedure for SSE.  */
  __asm__ ("stmxcsr %0" : "=m" (*&mxcsr));

  /* Clear the relevant bits.  */
  mxcsr &= ~excepts;

  /* And put them into effect.  */
  __asm__ ("ldmxcsr %0" : : "m" (*&mxcsr));

  /* Success.  */
  return 0;
}

int
fedisableexcept (int excepts)
{
  unsigned short int new_exc, old_exc;
  unsigned int new;

  excepts &= FE_ALL_EXCEPT;

  /* Get the current control word of the x87 FPU.  */
  __asm__ ("fstcw %0" : "=m" (*&new_exc));

  old_exc = (~new_exc) & FE_ALL_EXCEPT;

  new_exc |= excepts;
  __asm__ ("fldcw %0" : : "m" (*&new_exc));

  /* And now the same for the SSE MXCSR register.  */
  __asm__ ("stmxcsr %0" : "=m" (*&new));

  /* The SSE exception masks are shifted by 7 bits.  */
  new |= excepts << 7;
  __asm__ ("ldmxcsr %0" : : "m" (*&new));

  return old_exc;
}

int
feenableexcept (int excepts)
{
  unsigned short int new_exc, old_exc;
  unsigned int new;

  excepts &= FE_ALL_EXCEPT;

  /* Get the current control word of the x87 FPU.  */
  __asm__ ("fstcw %0" : "=m" (*&new_exc));

  old_exc = (~new_exc) & FE_ALL_EXCEPT;

  new_exc &= ~excepts;
  __asm__ ("fldcw %0" : : "m" (*&new_exc));

  /* And now the same for the SSE MXCSR register.  */
  __asm__ ("stmxcsr %0" : "=m" (*&new));

  /* The SSE exception masks are shifted by 7 bits.  */
  new &= ~(excepts << 7);
  __asm__ ("ldmxcsr %0" : : "m" (*&new));

  return old_exc;
}

int
__fegetenv (fenv_t *envp)
{
  __asm__ ("fnstenv %0\n"
           /* fnstenv changes the exception mask, so load back the
              stored environment.  */
           "fldenv %0\n"
           "stmxcsr %1" : "=m" (*envp), "=m" (envp->__mxcsr));

  /* Success.  */
  return 0;
}

int
fegetexcept (void)
{
  unsigned short int exc;

  /* Get the current control word.  */
  __asm__ ("fstcw %0" : "=m" (*&exc));

  return (~exc) & FE_ALL_EXCEPT;
}


int
fegetmode (femode_t *modep)
{
  _FPU_GETCW (modep->__control_word);
  __asm__ ("stmxcsr %0" : "=m" (modep->__mxcsr));
  return 0;
}

    
int
__fegetround (void)
{
  int cw;
  /* We only check the x87 FPU unit.  The SSE unit should be the same
     - and if it's not the same there's no way to signal it.  */

  __asm__ ("fnstcw %0" : "=m" (*&cw));

  return cw & 0xc00;
}

int
__feholdexcept (fenv_t *envp)
{
  unsigned int mxcsr;

  /* Store the environment.  Recall that fnstenv has a side effect of
     masking all exceptions.  Then clear all exceptions.  */
  __asm__ ("fnstenv %0\n\t"
           "stmxcsr %1\n\t"
           "fnclex"
           : "=m" (*envp), "=m" (envp->__mxcsr));

  /* Set the SSE MXCSR register.  */
  mxcsr = (envp->__mxcsr | 0x1f80) & ~0x3f;
  __asm__ ("ldmxcsr %0" : : "m" (*&mxcsr));

  return 0;
}

    
    

int
fesetexcept (int excepts)
{
  unsigned int mxcsr;

  __asm__ ("stmxcsr %0" : "=m" (*&mxcsr));
  mxcsr |= excepts & FE_ALL_EXCEPT;
  __asm__ ("ldmxcsr %0" : : "m" (*&mxcsr));

  return 0;
}


int
__fesetround (int round)
{
  unsigned short int cw;
  int mxcsr;

  if ((round & ~0xc00) != 0)
    /* ROUND is no valid rounding mode.  */
    return 1;

  /* First set the x87 FPU.  */
  asm ("fnstcw %0" : "=m" (*&cw));
  cw &= ~0xc00;
  cw |= round;
  asm ("fldcw %0" : : "m" (*&cw));

  /* And now the MSCSR register for SSE, the precision is at different bit
     positions in the different units, we need to shift it 3 bits.  */
  asm ("stmxcsr %0" : "=m" (*&mxcsr));
  mxcsr &= ~ 0x6000;
  mxcsr |= round << 3;
  asm ("ldmxcsr %0" : : "m" (*&mxcsr));

  return 0;
}


int
fegetexceptflag (fexcept_t *flagp, int excepts)
{
  fexcept_t temp;
  unsigned int mxscr;

  /* Get the current exceptions for the x87 FPU and SSE unit.  */
  __asm__ ("fnstsw %0\n"
           "stmxcsr %1" : "=m" (*&temp), "=m" (*&mxscr));

  *flagp = (temp | mxscr) & FE_ALL_EXCEPT & excepts;

  /* Success.  */
  return 0;
}

int
__feraiseexcept (int excepts)
{
  /* Raise exceptions represented by EXPECTS.  But we must raise only
     one signal at a time.  It is important that if the overflow/underflow
     exception and the inexact exception are given at the same time,
     the overflow/underflow exception follows the inexact exception.  */

  /* First: invalid exception.  */
  if ((FE_INVALID & excepts) != 0)
    {
      /* One example of an invalid operation is 0.0 / 0.0.  */
      float f = 0.0;

      __asm__ __volatile__ ("divss %0, %0 " : : "x" (f));
      (void) &f;
    }

  /* Next: division by zero.  */
  if ((FE_DIVBYZERO & excepts) != 0)
    {
      float f = 1.0;
      float g = 0.0;

      __asm__ __volatile__ ("divss %1, %0" : : "x" (f), "x" (g));
      (void) &f;
    }

  /* Next: overflow.  */
  if ((FE_OVERFLOW & excepts) != 0)
    {
      /* XXX: Is it ok to only set the x87 FPU?  */
      /* There is no way to raise only the overflow flag.  Do it the
         hard way.  */
      fenv_t temp;

      /* Bah, we have to clear selected exceptions.  Since there is no
         `fldsw' instruction we have to do it the hard way.  */
      __asm__ __volatile__ ("fnstenv %0" : "=m" (*&temp));

      /* Set the relevant bits.  */
      temp.__status_word |= FE_OVERFLOW;

      /* Put the new data in effect.  */
      __asm__ __volatile__ ("fldenv %0" : : "m" (*&temp));

      /* And raise the exception.  */
      __asm__ __volatile__ ("fwait");
    }

  /* Next: underflow.  */
  if ((FE_UNDERFLOW & excepts) != 0)
    {
      /* XXX: Is it ok to only set the x87 FPU?  */
      /* There is no way to raise only the underflow flag.  Do it the
         hard way.  */
      fenv_t temp;

      /* Bah, we have to clear selected exceptions.  Since there is no
         `fldsw' instruction we have to do it the hard way.  */
      __asm__ __volatile__ ("fnstenv %0" : "=m" (*&temp));

      /* Set the relevant bits.  */
      temp.__status_word |= FE_UNDERFLOW;

      /* Put the new data in effect.  */
      __asm__ __volatile__ ("fldenv %0" : : "m" (*&temp));

      /* And raise the exception.  */
      __asm__ __volatile__ ("fwait");
    }

  /* Last: inexact.  */
  if ((FE_INEXACT & excepts) != 0)
    {
      /* XXX: Is it ok to only set the x87 FPU?  */
      /* There is no way to raise only the inexact flag.  Do it the
         hard way.  */
      fenv_t temp;

      /* Bah, we have to clear selected exceptions.  Since there is no
         `fldsw' instruction we have to do it the hard way.  */
      __asm__ __volatile__ ("fnstenv %0" : "=m" (*&temp));

      /* Set the relevant bits.  */
      temp.__status_word |= FE_INEXACT;

      /* Put the new data in effect.  */
      __asm__ __volatile__ ("fldenv %0" : : "m" (*&temp));

      /* And raise the exception.  */
      __asm__ __volatile__ ("fwait");
    }

  /* Success.  */
  return 0;
}

int
fesetexceptflag (const fexcept_t *flagp, int excepts)
{
  fenv_t temp;
  unsigned int mxcsr;

  /* XXX: Do we really need to set both the exception in both units?
     Shouldn't it be enough to set only the SSE unit?  */

  /* Get the current x87 FPU environment.  We have to do this since we
     cannot separately set the status word.  */
  __asm__ ("fnstenv %0" : "=m" (*&temp));

  temp.__status_word &= ~(excepts & FE_ALL_EXCEPT);
  temp.__status_word |= *flagp & excepts & FE_ALL_EXCEPT;

  /* Store the new status word (along with the rest of the environment.
     Possibly new exceptions are set but they won't get executed unless
     the next floating-point instruction.  */
  __asm__ ("fldenv %0" : : "m" (*&temp));

  /* And now the same for SSE.  */
  __asm__ ("stmxcsr %0" : "=m" (*&mxcsr));

  mxcsr &= ~(excepts & FE_ALL_EXCEPT);
  mxcsr |= *flagp & excepts & FE_ALL_EXCEPT;

  __asm__ ("ldmxcsr %0" : : "m" (*&mxcsr));

  /* Success.  */
  return 0;
}

int
fetestexcept (int excepts)
{
  int temp;
  unsigned int mxscr;

  /* Get current exceptions.  */
  __asm__ ("fnstsw %0\n"
           "stmxcsr %1" : "=m" (*&temp), "=m" (*&mxscr));

  return (temp | mxscr) & excepts & FE_ALL_EXCEPT;
}

