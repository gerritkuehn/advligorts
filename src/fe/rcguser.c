///	@file rcguser.c
///	@brief File contains startup routines for running user app in user
/// space.

// TODO:
//  - Release DAC channels
//  - Verify Dolphin operation

#define _GNU_SOURCE //sched_setaffinity and CPU_* macros
#include "../drv/rts-cpu-isolator/usp-allocate.h"

#include "rcguserCommon.h"
#include "mapApp.h"
#include "controller.h"
#include "feComms.h"
#include "print_io_info.h"
#include "drv/rts-logger.h"
#include "util/macros.h" //COUNT_OF()


#include <unistd.h>
#include <ctype.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/file.h>
#include <stdio.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <stdbool.h>


// These externs and "16" need to go to a header file (mbuf.h)
extern int   fe_start_app_user( );

/// Startup function for initialization of user space module.
int
main( int argc, char** argv )
{
    int  status = 0, mainReturn = 0;
    int  ii, jj, kk; /// @param ii,jj,kk default loop counters
    char fname[ 128 ]; /// @param fname[128] Name of shared mem area to allocate
                       /// for DAQ data
    int cards; /// @param cards Number of PCIe cards found on bus
    int adcCnt =
        0; /// @param adcCnt Number of ADC cards found by control model.
    int dacCnt = 0; /// @param dacCnt Number of 16bit DAC cards found by control
                    /// model.
    int dac18Cnt = 0; /// @param dac18Cnt Number of 18bit DAC cards found by
                      /// control model.
    int dac20Cnt = 0; /// @param dac20Cnt Number of 20bit DAC cards found by
                      /// control model.
    int doCnt = 0; /// @param doCnt Total number of digital I/O cards found by
                   /// control model.
    int do32Cnt = 0; /// @param do32Cnt Total number of Contec 32 bit DIO cards
                     /// found by control model.
    int doIIRO16Cnt = 0; /// @param doIIRO16Cnt Total number of Acces I/O 16 bit
                         /// relay cards found by control model.
    int doIIRO8Cnt = 0; /// @param doIIRO8Cnt Total number of Acces I/O 8 bit
                        /// relay cards found by control model.
    int cdo64Cnt = 0; /// @param cdo64Cnt Total number of Contec 6464 DIO card
                      /// 32bit output sections mapped by control model.
    int cdi64Cnt = 0; /// @param cdo64Cnt Total number of Contec 6464 DIO card
                      /// 32bit input sections mapped by control model.
    int ret; /// @param ret Return value from various Malloc calls to allocate
             /// memory.
    int cnt;

    int allocated_cpu = -1;

    allocated_cpu = rts_cpu_isolator_alloc_core(allocated_cpu);
    if ( allocated_cpu == -1 )
    {
        RTSLOG_WARN("There was an error getting a free isolated core. "
                    "We are going to continue execution and expect isolation is set by some other method.\n");
    }
    else
    {
        RTSLOG_INFO("Going to isolate onto CPU : %d\n", allocated_cpu);

        //Lock our thread to the allocated core
        cpu_set_t my_set;
        CPU_ZERO(&my_set);
        CPU_SET(allocated_cpu, &my_set);
        if ( sched_setaffinity(0, sizeof(cpu_set_t), &my_set) != 0)
        {
            RTSLOG_ERROR("There was an error locking to the allocated core\n");
            return 1;
        }
    }


    // Connect and allocate mbuf memory spaces
    attach_shared_memory( SYSTEM_NAME_STRING_LOWER );
    // Set pointer to EPICS shared memory
    pLocalEpics = (CDS_EPICS*)&( (RFM_FE_COMMS*)_epics_shm )->epicsSpace;

    // Find and initialize all PCI I/O modules
    // ******************************************************* Following I/O
    // card info is from feCode
    cards = COUNT_OF(cards_used);
    RTSLOG_INFO( "configured to use %d cards\n", cards );
    cdsPciModules.cards = cards;
    cdsPciModules.cards_used = cards_used;
    RTSLOG_INFO( "Initializing PCI Modules\n" );
    cdsPciModules.adcCount = 0;
    cdsPciModules.dacCount = 0;
    cdsPciModules.dioCount = 0;
    cdsPciModules.doCount = 0;
    cdsPciModules.rfmCount = 0;
    cdsPciModules.dolphinCount = 0;

    // If running as a control process, I/O card information is via ipc shared
    // memory
    RTSLOG_INFO( "%d PCI cards found\n", ioMemData->totalCards );

    initmap( &cdsPciModules );
    /// Call PCI initialization routine in map.c file.
    status = mapPciModules( &cdsPciModules );

    // If no ADC cards were found, then control cannot run
    if ( !cdsPciModules.adcCount )
    {
        RTSLOG_ERROR( "No ADC cards found - exiting\n" );
        mainReturn = -1;
        goto cleanup;
    }
    RTSLOG_INFO( "%d PCI cards found \n", status );
    if ( status < cards )
    {
        RTSLOG_ERROR( "Did not find correct number of cards! Expected %d "
                "and Found %d\n",
                cards,
                status );
        cardCountErr = 1;
    }

    // Control app gets RFM module count from MASTER.
    cdsPciModules.rfmCount = ioMemData->rfmCount;
    cdsPciModules.dolphinCount = ioMemData->dolphinCount;
#if 0
    if(cdsPciModules.dolphinCount)
    {
        dolphin_init(&cdsPciModules);
    }
#endif
    // Print out all the I/O information
    print_io_info(SYSTEM_NAME_STRING_LOWER, &cdsPciModules, 0 );

    // Initialize buffer for daqLib.c code
    RTSLOG_INFO( "Initializing space for daqLib buffers\n" );
    daqBuffer = (long)&daqArea[ 0 ];

    for ( cnt = 0; cnt < 10 && pLocalEpics->epicsInput.burtRestore == 0; cnt++ )
    {
        RTSLOG_INFO( "Epics burt restore is %d\n",
                pLocalEpics->epicsInput.burtRestore );
        usleep( 1000000 );
    }
    if ( cnt == 10 )
    {
        mainReturn = -1;
        goto cleanup;
    }

    pLocalEpics->epicsInput.vmeReset = 0;
    // Setup signal handler to catch Control C
    signal( SIGINT, intHandler );
    signal( SIGTERM, intHandler );
    sleep( 1 );

    // Start the control loop software
    fe_start_app_user( );

    cleanup:
    detach_shared_memory();

    rts_cpu_isolator_free_core();

    return mainReturn;

}
