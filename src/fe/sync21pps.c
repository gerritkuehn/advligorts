
#include "../fe/sync21pps.h"

#include "controller.h"
#include "drv/gsc_adc_common.h" //GSAI_ALL_CARDS
#include "drv/gsc_dac_common.h" //DAC_CLK_ENABLE
#include "../fe/timing_kernel.h" //current_nanosecond
#include "drv/plx_9056.h"
#include "drv/iop_adc_functions.h"
#include "drv/rts-logger.h"
#include "util/timing.h"



void print_sync2pps_error(SYNC2PPS_ERROR_T error_no)
{
    switch (error_no)
    {
        case SYNC2PPS_ERROR_OK:
        break; 
        case SYNC2PPS_ERROR_SYSTIME:
            RTSLOG_ERROR("The real time code could not sync up with the ADC within the allowed number of attempts.\n");
        break;
        case SYNC2PPS_ERROR_ADC_TO:
            RTSLOG_ERROR("The ADC read timed out\n");
        break;
        case SYNC2PPS_ERROR_CHAN_HOP:
            RTSLOG_ERROR("The ADC read detected a channel hop error.\n");
        break;
        default:
            RTSLOG_ERROR("Unknown error number: %d\n", error_no);
        break;
    }
}




// Code to synch FE startup with 1PPS signal on last channel
// of first ADC module.
// Clocks are already running, so can't enable all ADC cards
// at once as some may trigger on one clock and one or more
// on the next clock, leaving them out of sync with each other.
// So idea is to enable one ADC to find the clock and then
// enable all cards in time frame before next clock.

int
sync2pps_signal( one_pps_sync_t* p1pps, adcInfo_t* padcinfo, unsigned long long cpuClock[] )
{
    int           status;
    int           pps_wait = 0;
    int           sync = SYNC_SRC_1PPS;
    unsigned long nanotime = 100000;
    int ii;
    int jj = 0;
    int tc = 1;
    uint64_t start_tsc, total_ns=0;

    
    // Have to enable all of the DAC cards here.
    // Takes too long to run this after ADC startup.
    // This precludes DAC FIFO preload.
    gscEnableDacModule( &cdsPciModules, GSAI_ALL_CARDS, DAC_CLK_ENABLE );

    // Following will startup 1 ADC at a time.
    for(ii=0;ii<(cdsPciModules.adcCount - 1);ii++)
    {
        for(jj=0;jj<tc;jj++)
            gscEnableAdcModule( &cdsPciModules, jj, GSAI_DMA_MODE );

        // Wait for DMA done to indicate an ADC clock just occurred
        for(jj=0;jj<tc;jj++)
        {
            status = plx9056_wait_dma_done( jj, 1, 100 );
            if (status != 0)
                RTSLOG_ERROR("sync2pps_signal() - plx9056_wait_dma_done() timed out jj: %d\n", jj);
        }
        tc ++;
    }

    // Found a clock so now enable all cards
    gscEnableAdcModule( &cdsPciModules, GSAI_ALL_CARDS, GSAI_DMA_MODE );

    // Now search for the 1PPS sync pulse
    // on last channel of 1st ADC
    timer_start( &start_tsc);
    do
    {
        status = iop_adc_read( padcinfo, cpuClock );
        p1pps->value = dWord[ ADC_ONEPPS_BRD ][ ADC_DUOTONE_CHAN ][ 0 ];
        pps_wait++;

        total_ns += timer_tock_ns(&start_tsc);
        timer_start( &start_tsc );

    } while ( status == 0 && p1pps->value < ONE_PPS_THRESH && pps_wait < p1pps->time );

    RTSLOG_INFO("It took an avg %llu ns to read the next ADC value while looking for the PPS,"
                " estimated clock rate of %llu Hz. Num samples: %d\n",
                total_ns/pps_wait, 
                (uint64_t)(1.0/((total_ns/pps_wait)/1000000000.0)),
                pps_wait);

    if (status != 0) return status; //If the ADC read failed, we can't continue

#ifdef TEST_NOSYNC
    pps_wait = p1pps->time + 1;
#endif
    // If 1PPS signal not found, assume the ADC is being clocked (external/internal) and try 
    // to sync up with the ADC read period
    if ( pps_wait >= p1pps->time )
    {
        RTSLOG_INFO("Failed to find 1 pps signal on channel %d of card %d\n, defaulting to SYNC_SRC_NONE. \n",
                    ADC_DUOTONE_CHAN, ADC_ONEPPS_BRD);

        // Sync21PPS failed, so default to no sync
        sync = SYNC_SRC_NONE;
        pps_wait = 0;
        // Now search for the start of 1sec from internal clock
        do
        {
            status = iop_adc_read( padcinfo, cpuClock );
            nanotime = current_nanosecond( );
            pps_wait++;
        } while ( status == 0 && nanotime < 50000 && pps_wait < p1pps->time );

        if (status != 0) return status; //If the ADC read failed, we can't continue

        if ( pps_wait >= p1pps->time )
        {
            RTSLOG_ERROR("Could not sync to the ADC data within the given number of attempts (%d), last nanotime: %lu\n",
                         p1pps->time, nanotime);
            return SYNC2PPS_ERROR_SYSTIME;
        }
    }
    return sync;
}

void
sync21pps_check( one_pps_sync_t* p1pps, adcInfo_t* padcinfo, int hk_cycle )
{
    p1pps->value = adcinfo.adcData[ ADC_ONEPPS_BRD ][ ADC_DUOTONE_CHAN ];
    if ( ( p1pps->value > ONE_PPS_THRESH ) && ( p1pps->signalHi == 0 ) )
    {
        p1pps->time = hk_cycle;
        p1pps->signalHi = 1;
    }
    if ( p1pps->value < ONE_PPS_THRESH )
        p1pps->signalHi = 0;
    return;
}
