#ifndef LIGO_SYNC21PPS_H
#define LIGO_SYNC21PPS_H

#include "cds_types.h"
#include "controller.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum 
{
    SYNC2PPS_ERROR_OK = 0,
    SYNC2PPS_ERROR_SYSTIME = -1,
    SYNC2PPS_ERROR_CHAN_HOP = CHAN_HOP_ERROR,  //-4
    SYNC2PPS_ERROR_ADC_TO = ADC_TO_ERROR, //-7
} SYNC2PPS_ERROR_T;

void print_sync2pps_error(SYNC2PPS_ERROR_T error_no);

SYNC2PPS_ERROR_T sync2pps_signal( one_pps_sync_t* p1pps, adcInfo_t* padcinfo, unsigned long long cpuClock[] );

void sync21pps_check( one_pps_sync_t* p1pps, adcInfo_t* padcinfo, int hk_cycle );


#ifdef __cplusplus
}
#endif


#endif // LIGO_SYNC21PPS_H
