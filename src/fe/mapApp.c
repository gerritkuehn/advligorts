/// \file mapApp.c
/// \brief This file contains the software to find PCIe card mapping from IOP in
/// mbuf..

#include "mapApp.h"
#include "controller.h"

#include "drv/rts-logger.h"


int
mapPciModules( CDS_HARDWARE* pCds )
{
    int status = 0;
    int ii, jj, kk; /// @param ii,jj,kk default loop counters

    RTSLOG_INFO("IOP clock %u\n", ioMemData->mem_data_rate_hz );

    if(ioMemData->struct_version != IO_MEM_DATA_VERSION)
    {
        RTSLOG_ERROR("The ioMemData->struct_version is %d (from the IOP), but we expect %d.\n"
                     "This model needs to be rebuilt with the same version as the IOP.", 
                     ioMemData->struct_version,
                     IO_MEM_DATA_VERSION);
        return -1;
    }

    // Have to search thru all cards and find desired instance for application
    // IOP will map ADC cards first, then DAC and finally DIO
    for ( jj = 0; jj < pCds->cards; jj++ )
    {
        for ( ii = 0; ii < ioMemData->totalCards; ii++ )
        {
            switch ( ioMemData->model[ ii ] )
            {
            case GSC_16AI64SSA:
                if ( ( pCds->cards_used[ jj ].type == GSC_16AI64SSA ) &&
                     ( pCds->cards_used[ jj ].instance ==
                       ioMemData->card[ ii ] ) )
                {
                    kk = pCds->adcMap[ jj ];
                    pCds->adcConfig[ kk ] = ioMemData->ipc[ ii ];
                    pCds->card_count[ GSC_16AI64SSA ] ++;
                    status++;
                }
                break;
            case GSC_18AI32SSC1M:
                if ( ( pCds->cards_used[ jj ].type == GSC_18AI32SSC1M ) &&
                     ( pCds->cards_used[ jj ].instance ==
                       ioMemData->card[ ii ] ) )
                {
                    kk = pCds->adcMap[ jj ];
                    pCds->adcConfig[ kk ] = ioMemData->ipc[ ii ];
                    pCds->card_count[ GSC_18AI32SSC1M ] ++;
                    status++;
                }
                break;
            case GSC_18AI64SSC:
                if ( ( pCds->cards_used[ jj ].type == GSC_18AI64SSC ) &&
                     ( pCds->cards_used[ jj ].instance ==
                       ioMemData->card[ ii ] ) )
                {
                    kk = pCds->adcMap[ jj ];
                    pCds->adcConfig[ kk ] = ioMemData->ipc[ ii ];
                    pCds->card_count[ GSC_18AI64SSC ] ++;
                    status++;
                }
                break;
            case GSC_16AO16:
                if ( ( pCds->cards_used[ jj ].type == GSC_16AO16 ) &&
                     ( pCds->cards_used[ jj ].instance ==
                       ioMemData->card[ ii ] ) )
                {
                    kk = pCds->dacMap[ jj ];
                    pCds->dacConfig[ kk ] = ioMemData->ipc[ ii ];
                    pCds->pci_dac[ kk ] = (volatile int *)( ioMemData->iodata[ ii ] );
                    pCds->card_count[ GSC_16AO16 ] ++;
                    status++;
                }
                break;
            case GSC_18AO8:
                if ( ( pCds->cards_used[ jj ].type == GSC_18AO8 ) &&
                     ( pCds->cards_used[ jj ].instance ==
                       ioMemData->card[ ii ] ) )
                {
                    kk = pCds->dacMap[ jj ];
                    pCds->dacConfig[ kk ] = ioMemData->ipc[ ii ];
                    pCds->pci_dac[ kk ] = (volatile int *)( ioMemData->iodata[ ii ] );
                    pCds->card_count[ GSC_18AO8 ] ++;
                    status++;
                }
                break;
            case GSC_20AO8:
                if ( pCds->cards_used[ jj ].type == GSC_20AO8 &&
                     ( pCds->cards_used[ jj ].instance ==
                       ioMemData->card[ ii ] ) )
                {
                    kk = pCds->dacMap[ jj ];
                    pCds->dacConfig[ kk ] = ioMemData->ipc[ ii ];
                    pCds->pci_dac[ kk ] = (volatile int *)( ioMemData->iodata[ ii ] );
                    pCds->card_count[ GSC_20AO8 ] ++;
                    status++;
                }
                break;

            case CON_6464DIO:
                if ( ( pCds->cards_used[ jj ].type == CON_6464DIO ) &&
                     ( pCds->cards_used[ jj ].instance == ioMemData->card[ ii ] ) )
                {
                    kk = pCds->doCount;
                    pCds->doType[ kk ] = ioMemData->model[ ii ];
                    pCds->pci_do[ kk ] = ioMemData->ipc[ ii ];
                    pCds->doCount++;
                    pCds->pci_do[ kk ] = ioMemData->ipc[ ii ];
                    pCds->doInstance[ kk ] = pCds->card_count[ CON_6464DIO ];
                    pCds->card_count[ CON_6464DIO ] += 2;
                    status += 2;
                }
                if ( ( pCds->cards_used[ jj ].type == CDO64 ) &&
                     ( pCds->cards_used[ jj ].instance == ioMemData->card[ ii ] ) )
                {
                    kk = pCds->doCount;
                    pCds->doType[ kk ] = CDO64;
                    pCds->pci_do[ kk ] = ioMemData->ipc[ ii ];
                    pCds->doInstance[ kk ] =  kk;
                    pCds->doCount++;
                    pCds->card_count[ CDO64 ] ++;
                    status++;
                }
                if ( ( pCds->cards_used[ jj ].type == CDI64 ) &&
                     ( pCds->cards_used[ jj ].instance == ioMemData->card[ ii ] ) )
                {
                    kk = pCds->doCount;
                    pCds->doType[ kk ] = CDI64;
                    pCds->pci_do[ kk ] = ioMemData->ipc[ ii ];
                    pCds->doInstance[ kk ] = kk;
                    pCds->doCount++;
                    pCds->card_count[ CDI64 ] ++;
                    status++;
                }
                break;
            case CON_32DO:
                if ( ( pCds->cards_used[ jj ].type == CON_32DO ) &&
                     ( pCds->cards_used[ jj ].instance == ioMemData->card[ ii ] ) )
                {
                    kk = pCds->doCount;
                    pCds->doType[ kk ] = ioMemData->model[ ii ];
                    pCds->pci_do[ kk ] = ioMemData->ipc[ ii ];
                    pCds->doCount++;
                    pCds->doInstance[ kk ] = pCds->card_count[ CON_32DO ];
                    pCds->card_count[ CON_32DO ] ++;
                    status++;
                }
                break;
            case ACS_16DIO:
                if ( ( pCds->cards_used[ jj ].type == ACS_16DIO ) &&
                     ( pCds->cards_used[ jj ].instance == ioMemData->card[ ii ] ) )
                {
                    kk = pCds->doCount;
                    pCds->doType[ kk ] = ioMemData->model[ ii ];
                    pCds->pci_do[ kk ] = ioMemData->ipc[ ii ];
                    pCds->doCount++;
                    pCds->doInstance[ kk ] = pCds->card_count[ ACS_16DIO ];
                    pCds->card_count[ ACS_16DIO ] ++;
                    status++;
                }
                break;
            case ACS_8DIO:
                if ( ( pCds->cards_used[ jj ].type == ACS_8DIO ) &&
                     ( pCds->cards_used[ jj ].instance == ioMemData->card[ ii ] ) )
                {
                    kk = pCds->doCount;
                    pCds->doType[ kk ] = ioMemData->model[ ii ];
                    pCds->pci_do[ kk ] = ioMemData->ipc[ ii ];
                    pCds->doCount++;
                    pCds->doInstance[ kk ] = pCds->card_count[ ACS_8DIO ];
                    pCds->card_count[ ACS_8DIO ] ++;
                    status++;
                }
                break;
            default:
                break;
            }
        }
    }

    // Dolphin PCIe network style. Control units will perform I/O transactions
    // with RFM directly ie MASTER does not do RFM I/O. Master unit only maps
    // the RFM I/O space and passes pointers to control models.

    // Control app gets RFM module count from MASTER.
    cdsPciModules.rfmCount = ioMemData->rfmCount;
    // dolphinCount is number of segments
    cdsPciModules.dolphinCount = ioMemData->dolphinCount;
    // dolphin read/write 0 is for local PCIe network traffic
    cdsPciModules.dolphinPcieReadPtr = ioMemData->dolphinPcieReadPtr;
    cdsPciModules.dolphinPcieWritePtr = ioMemData->dolphinPcieWritePtr;
    // dolphin read/write 1 is for long range PCIe (RFM) traffic
    cdsPciModules.dolphinRfmReadPtr = ioMemData->dolphinRfmReadPtr;
    cdsPciModules.dolphinRfmWritePtr = ioMemData->dolphinRfmWritePtr;
    cdsPciModules.stop_dolphin_ipcs = 0;
    for ( ii = 0; ii < cdsPciModules.rfmCount; ii++ )
    {
        cdsPciModules.pci_rfm[ ii ] = ioMemData->pci_rfm[ ii ];
        cdsPciModules.pci_rfm_dma[ ii ] = ioMemData->pci_rfm_dma[ ii ];
    }
    // User APP does not access IRIG-B cards
    cdsPciModules.gps = 0;
    cdsPciModules.gpsType = 0;

    return status;
}

void initmap(CDS_HARDWARE* pCds)
{
    int i;
    int dac_cnt = 0;
    int adc_cnt = 0;
    pCds->adcCount = 0;
    pCds->dacCount = 0;
    pCds->dioCount = 0;
    pCds->doCount = 0;

 for ( i = 0; i < pCds->cards; i++ )
    {
     pCds->adcMap[ i ] = 0;
     pCds->dacMap[ i ] = 0;
    }

 for ( i = 0; i < pCds->cards; i++ )
    {
        if ( pCds->cards_used[ i ].type == GSC_18AO8 )
        {
            pCds->dacType[ dac_cnt ] = GSC_18AO8;
            pCds->dacInstance[ dac_cnt ] =  pCds->cards_used[ i ].instance;
            pCds->dacConfig[ dac_cnt ] = 0;
            pCds->dacMap[ i ] = dac_cnt;
            pCds->dacCount++;
            dac_cnt++;
        }
        if ( pCds->cards_used[ i ].type == GSC_16AO16 )
        {
            pCds->dacType[ dac_cnt ] = GSC_16AO16;
            pCds->dacInstance[ dac_cnt ] =  pCds->cards_used[ i ].instance;
            pCds->dacConfig[ dac_cnt ] = 0;
            pCds->dacMap[ i ] = dac_cnt;
            pCds->dacCount++;
            dac_cnt++;
        }
        if ( pCds->cards_used[ i ].type == GSC_20AO8 )
        {
            pCds->dacType[ dac_cnt ] = GSC_20AO8;
            pCds->dacInstance[ dac_cnt ] =  pCds->cards_used[ i ].instance;
            pCds->dacConfig[ dac_cnt ] = 0;
            pCds->dacMap[ i ] = dac_cnt;
            pCds->dacCount++;
            dac_cnt++;
        }
        if ( pCds->cards_used[ i ].type == GSC_16AI64SSA )
        {
            pCds->adcType[ adc_cnt ] = GSC_16AI64SSA;
            pCds->adcInstance[ adc_cnt ] =  pCds->cards_used[ i ].instance;
            pCds->adcConfig[ adc_cnt ] = -1;
            pCds->adcMap[ i ] = adc_cnt;
            pCds->adcCount++;
            adc_cnt++;
        }
        if ( pCds->cards_used[ i ].type == GSC_18AI32SSC1M )
        {
            pCds->adcType[ adc_cnt ] = GSC_18AI32SSC1M;
            pCds->adcInstance[ adc_cnt ] =  pCds->cards_used[ i ].instance;
            pCds->adcConfig[ adc_cnt ] = -1;
            pCds->adcMap[ i ] = adc_cnt;
            pCds->adcCount++;
            adc_cnt++;
        }
        if ( pCds->cards_used[ i ].type == GSC_18AI64SSC )
        {
            pCds->adcType[ adc_cnt ] = GSC_18AI64SSC;
            pCds->adcInstance[ adc_cnt ] =  pCds->cards_used[ i ].instance;
            pCds->adcConfig[ adc_cnt ] = -1;
            pCds->adcMap[ i ] = adc_cnt;
            pCds->adcCount++;
            adc_cnt++;
        }
    }

}

