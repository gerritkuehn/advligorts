/// @file print_io_info.c
/// @brief File contains routine to print IO info on code startup.

#include "print_io_info.h"
#include "controller.h" //_epics_shm, _tp_shm, _awg_shm, _ipc_shm

#ifndef USER_SPACE
#include "../fe/timing_kernel.h" //current_time_fe
#endif

#include "drv/rts-logger.h"


void
print_io_info(const char* model_name, CDS_HARDWARE* cdsp, int iopmodel )
{
    int ii, jj, kk;
    int channels = 0;
    jj = 0;

#ifndef USER_SPACE
    RTSLOG_INFO( "startup time is %ld\n",
                 current_time_fe( ) );
    RTSLOG_INFO( "cpu clock %u\n", cpu_khz );
#endif
    RTSLOG_INFO( "EPICSM at %p\n",
                  _epics_shm );
    RTSLOG_INFO( "TPSM at %p\n",
                 _tp_shm );
    RTSLOG_INFO( "AWGSM at %p\n",
                 _awg_shm );
    RTSLOG_INFO( "IPC    at %p\n",
                 _ipc_shm );
    RTSLOG_INFO( "IOMEM  at 0x%lx size 0x%lx\n",
            ( (unsigned long)_ipc_shm + 0x4000 ),
            sizeof( IO_MEM_DATA ) );
    RTSLOG_INFO( "DAQSM at %p\n",
            _daq_shm );
    RTSLOG_INFO( "configured to use %d cards\n",
            cdsp->cards );
    kk = 0;
    RTSLOG_RAW( "******************************************************************"
            "*********\n" );
    RTSLOG_INFO( "%d ADC cards found\n",
            cdsp->adcCount );
    for ( ii = 0; ii < cdsp->adcCount; ii++ )
    {
        kk++;
        RTSLOG_INFO( "\tADC %d is a %s module\n",
                ii,
                _cdscardtypename[ cdsp->adcType[ ii ] ] );
        if ( iopmodel )
        {
            RTSLOG_INFO( "\t\tChannels = %d \n",
                    cdsp->adcChannels[ ii ] );
            RTSLOG_INFO( "\t\tFirmware Rev = %d \n\n",
                    ( cdsp->adcConfig[ ii ] & 0xfff ) );
        }
        else
        {
            RTSLOG_INFO( "\tMemory at block %d\n",
                    cdsp->adcConfig[ ii ] );
        }
    }
    RTSLOG_RAW( "******************************************************************"
            "*********\n" );
    RTSLOG_INFO( "%d DAC cards found\n",
            cdsp->dacCount );
    for ( ii = 0; ii < cdsp->dacCount; ii++ )
    {
        kk++;
        RTSLOG_INFO( "\tDAC %d is a %s module\n",
                ii,
                _cdscardtypename[ cdsp->dacType[ ii ] ] );
        RTSLOG_INFO( "\tCard number is %d\n",
                cdsp->dacInstance[ ii ] );
        RTSLOG_INFO( "\tMemory at block %d\n",
                cdsp->dacConfig[ ii ] );
        if ( iopmodel )
        {
            if ( cdsp->dacType[ ii ] == GSC_16AO16 )
                channels = 16;
            else
                channels = 8;
            RTSLOG_INFO( "\tChannels = %d \n",
                    channels );
            RTSLOG_INFO( "\tFirmware Rev = %d \n\n",
                    ( cdsp->dacAcr[ ii ] & 0xfff ) );
        }
    }
    kk += cdsp->doCount;
    RTSLOG_RAW( "******************************************************************"
            "*********\n" );
    RTSLOG_INFO( "%d DIO cards found\n",
            cdsp->dioCount );
    RTSLOG_RAW( "******************************************************************"
            "*********\n" );
    RTSLOG_INFO( "%d IIRO-8 Isolated DIO cards found\n",
            cdsp->card_count[ ACS_8DIO ] );
    RTSLOG_RAW( "******************************************************************"
            "*********\n" );
    RTSLOG_INFO( "%d IIRO-16 Isolated DIO cards found\n",
            cdsp->card_count[ ACS_16DIO ] );
    RTSLOG_RAW( "******************************************************************"
            "*********\n" );
    RTSLOG_INFO( "%d Contec 32ch PCIe DO cards found\n",
            cdsp->card_count[ CON_32DO ] );
    RTSLOG_INFO( "%d Contec PCIe DIO1616 cards found\n",
            cdsp->card_count[ CON_1616DIO ] );
    pLocalEpics->epicsOutput.bioMon[ 2 ] = cdsp->card_count[ CON_1616DIO ];
    RTSLOG_INFO( "%d Contec PCIe DIO6464 cards found\n",
            cdsp->card_count[ CON_6464DIO ] );
    pLocalEpics->epicsOutput.bioMon[ 3 ] = cdsp->card_count[ CON_6464DIO ];
    RTSLOG_INFO( "%d Contec PCIe CDO64 cards found\n",
            cdsp->card_count[ CDO64 ] );
    pLocalEpics->epicsOutput.bioMon[ 1 ] = cdsp->card_count[ CDO64 ];
    RTSLOG_INFO( "%d Contec PCIe CDI64 cards found\n",
            cdsp->card_count[ CDI64 ] );
    pLocalEpics->epicsOutput.bioMon[ 0 ] = cdsp->card_count[ CDI64 ];
    RTSLOG_INFO( "%d DO cards found\n", cdsp->doCount );
    RTSLOG_INFO( "Total of %d I/O modules found and mapped\n", kk );
    RTSLOG_RAW( "******************************************************************"
            "*********\n" );
    RTSLOG_INFO( "%d RFM cards found\n",
            cdsp->rfmCount );
    for ( ii = 0; ii < cdsp->rfmCount; ii++ )
    {
        RTSLOG_INFO( "\tRFM %d is a VMIC_%x module with Node ID %d\n",
                ii,
                cdsp->rfmType[ ii ],
                cdsp->rfmConfig[ ii ] );
        RTSLOG_INFO( "address is 0x%lx\n", cdsp->pci_rfm[ ii ] );
    }
    RTSLOG_RAW( "******************************************************************"
            "*********\n" );
    if ( cdsp->gps )
    {
        RTSLOG_INFO( "IRIG-B card found %d\n",
                cdsp->gpsType );
        RTSLOG_RAW( "**************************************************************"
                "*************\n" );
    }

    if (cdsp->dolphinCount)
    {
        RTSLOG_INFO( "\tDolphin found, count %d\n", cdsp->dolphinCount);
        RTSLOG_INFO( "PCIE Read address is %p\n", cdsp->dolphinPcieReadPtr );
        RTSLOG_INFO( "PCIE Write address is %p\n", cdsp->dolphinPcieWritePtr );
        RTSLOG_INFO( "RFM Read address is %p\n", cdsp->dolphinRfmReadPtr );
        RTSLOG_INFO( "RFM Write address is %p\n", cdsp->dolphinRfmWritePtr );
    }

}



