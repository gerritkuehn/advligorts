///	@file moduleLoad.c
///	@brief File contains startup routines for real-time IOP and App code.

#include "moduleLoadCommon.h"

#include "controller.h" //daqArea
#include "rts-cpu-isolator/rts-cpu-isolator.h"
#include "verify_card_count.h"
#include "print_io_info.h"
#include "util/timing.h"
#include "util/kernel/exit_signaling.h"
#include "drv/map.h" //mapPciModules()
#include "drv/ligoPcieTiming.h"
#include "drv/rts-logger.h"
#include "util/macros.h" //COUNT_OF()
#include "../fe/verify_slots.h"
#include "../fe/mapApp.h" //initmap()
#include "../fe/dolphin.h"


#include <linux/uaccess.h>
#include <asm/uaccess.h>
#include <linux/ctype.h>
#include <linux/spinlock_types.h>
#include <linux/delay.h> //msleep
#include <linux/module.h>
#include <linux/init.h>

#include "Demodulation.h"


extern void  fe_start_controller( void );

//
// File function prototypes
//
static void rt_fe_cleanup( void );
static int rt_fe_init( void );

//
// Signaling variables for proper module shutdown logic
atomic_t g_atom_should_exit = ATOMIC_INIT(0);
atomic_t g_atom_has_exited = ATOMIC_INIT(0);
int g_core_used = -1;

//
// This symbol is used to enforce the IOP model
// is first loaded, before any app models
#ifdef IOP_MODEL
int need_to_load_IOP_first;
EXPORT_SYMBOL( need_to_load_IOP_first );
#else
extern int need_to_load_IOP_first;
#endif


#ifdef NO_CPU_SHUTDOWN

static struct task_struct* sthread;

/// @breif Wrapper function so kthread_create()
///        can have a function with the expected
///        prototype in the NO_CPU_SHUTDOWN case.
///
static int  fe_start_controller_kthread( void * arg )
{
    (void)arg;
    fe_start_controller();
    return 0;
}
#endif //NO_CPU_SHUTDOWN


// Linux Module init: Code starting point
// ****************************************************************
/// Startup function for initialization of kernel module.
static int __init rt_fe_init( void )
{
    int jj, kk; /// @param ii,jj,kk default loop counters
    int cards; /// @param cards Number of PCIe cards found on bus
    int ret; /// @param ret Return value from various Malloc calls to allocate
             /// memory.
    int        cnt;
    int        cardsfound = 0;
#if (defined(IOP_MODEL) && !defined(USE_DOLPHIN_TIMING) && !defined(TEST_1PPS) ) || defined (REQUIRE_IO_CNT)
    int        io_config_stat = 0;
#endif
    int        io_count_stat = 0;
    int model_cards[MAX_IO_MODULES];
    ISOLATOR_ERROR isolator_ret;
    char error_msg[ISOLATOR_ERROR_MSG_ALLOC_LEN];

    kk = 0;

    demodulation_init();


#ifdef DOLPHIN_TEST
    {
        /// Initialize the Dolphin interface
        int status = init_dolphin();
        if ( status != 0 )
        {
            RTSLOG_ERROR("Dolphin Network initialization failed; status = %d\n",
                    status );
            rt_fe_cleanup( );
            return -6;
        }
    }
#endif

    jj = 0;
#ifdef CONTROL_MODEL
    need_to_load_IOP_first = 0;
#endif

    ret = attach_shared_memory( );
    if ( ret < 0 )
    {
        RTSLOG_ERROR( "mbuf_allocate_area failed; ret = %d\n",
                ret );
        rt_fe_cleanup( );
        return ret;
    }

    /// Find and initialize all PCIe I/O modules
    // Following I/O card info is from feCode
    pLocalEpics->epicsOutput.fe_status = FIND_MODULES;
    cards = COUNT_OF(cards_used);
    cdsPciModules.cards = cards;
    cdsPciModules.cards_used = cards_used;

    initialize_card_counts( &cdsPciModules, model_cards );

#ifdef CONTROL_MODEL
    initmap( &cdsPciModules );
#endif
    /// Call PCI initialization routine in map.c file.
    cardsfound = mapPciModules( &cdsPciModules );

    if ( cardsfound < 0 )
    {
        pLocalEpics->epicsOutput.fe_status = IO_CARD_MAP_ERROR;
        rt_fe_cleanup( );
        return -5;
    }

    // If no ADC cards were found, then cannot run
    if ( !cdsPciModules.adcCount )
    {
        RTSLOG_ERROR( "No ADC cards found - exiting\n" );
        rt_fe_cleanup( );
        return -5;
    }

    // Verify all IO cards specified in model have been found on bus
    io_count_stat = verify_card_count( &cdsPciModules, (int*) &model_cards, SYSTEM_NAME_STRING_LOWER );

#ifdef IOP_MODEL
#ifndef USE_DOLPHIN_TIMING
#ifndef TEST_1PPS
    // If IOP and LIGO PCIe timing card present,
    // verify that PCIe cards line up with proper
    // backplane slot.
    if ( cdsPciModules.card_count[ LPTC ] == 1 )
    {
        // Set status to slot checking
        pLocalEpics->epicsOutput.fe_status = IOC_SLOT_CHK;
        // Card map to bp slot verification
        lptc_enable_all_slots( &cdsPciModules );
        // Call routine to check PCIe card map to backplane slot
        io_config_stat = verify_cards2slots( &cdsPciModules );
        if ( io_config_stat == IO_CONFIG_ERROR )
        {
            pLocalEpics->epicsOutput.fe_status = IO_CONFIG_ERROR;
        }
    }
#endif
#endif
#endif

#ifdef IOP_MODEL
    /// Wirte PCIe card info to mbuf for use by userapp models
    send_io_info_to_mbuf( cardsfound, &cdsPciModules );
#endif

    // Initialize buffer for daqLib.c code
    daqBuffer = (long)&daqArea[ 0 ];

    // wait to ensure EPICS is running before proceeding
    pLocalEpics->epicsOutput.fe_status = WAIT_BURT;
    msleep( 5000 );
    RTSLOG_INFO( "Waiting for EPICS BURT Restore = %d\n",
            pLocalEpics->epicsInput.burtRestore );
    /// Ensure EPICS running else exit
    for ( cnt = 0; cnt < 10 && pLocalEpics->epicsInput.burtRestore == 0; cnt++ )
    {
        msleep( 1000 );
    }
    if ( cnt == 10 )
    {
        RTSLOG_ERROR( "EPICS BURT restore not set - exiting\n" );
        pLocalEpics->epicsOutput.fe_status = BURT_RESTORE_ERROR;
        // Cleanup
        rt_fe_cleanup( );
        return -6;
    }

    // Print out all the I/O information
    // Following routine is in moduleLoadCommon.c
#ifdef IOP_MODEL
    print_io_info(SYSTEM_NAME_STRING_LOWER, &cdsPciModules, 1 );
#else
    print_io_info(SYSTEM_NAME_STRING_LOWER, &cdsPciModules, 0 );
#endif
#ifdef REQUIRE_IO_CNT
    RTSLOG_INFO( "IO stat = %d\n", io_config_stat );
    if ( io_config_stat != 0 || io_count_stat != 0)
    {
#ifdef DOLPHIN_TEST
        finish_dolphin( );
#endif
        pLocalEpics->epicsOutput.fe_status = IO_CONFIG_ERROR;
        RTSLOG_ERROR( "Model exiting because of %s\n", 
                      (io_count_stat != 0) ? "Incorrect card count." : "PCI map to backplane slot clocking/verification failure." );
        rt_fe_cleanup( );
        return -5;
    }
#endif

    RTSLOG_INFO("model rate (Hz) = %d\n", MODEL_RATE_HZ);
    RTSLOG_INFO("clock_div = %d\n", UNDERSAMPLE);
    RTSLOG_INFO("usec_per_cycle = %d\n", USEC_PER_CYCLE);

    pLocalEpics->epicsInput.vmeReset = 0;
    udelay( 2000 );

    /// Start the controller thread
#ifdef NO_CPU_SHUTDOWN
    sthread = kthread_create(
        fe_start_controller_kthread, 0, "fe_start_controller_kthread/%d");
    if ( IS_ERR( sthread ) )
    {
        RTSLOG_ERROR( "Failed to kthread_create()\n" );
        rt_fe_cleanup( );
        return -1;
    }
    //kthread_bind( sthread, 1 );
    wake_up_process( sthread );
#endif

#ifndef NO_CPU_SHUTDOWN
    pLocalEpics->epicsOutput.fe_status = LOCKING_CORE;
    RTSLOG_INFO( "Locking any available CPU core for real time function\n");

    isolator_ret = rts_isolator_run( fe_start_controller, -1, &g_core_used);
    if( isolator_ret != ISOLATOR_OK)
    {
        isolator_lookup_error_msg(isolator_ret, error_msg);
        RTSLOG_ERROR(": rts-cpu-isolator : %s\n", error_msg);
        rt_fe_cleanup( );
        return -1;
    }


#endif
    return 0;
}

void wait_for_module_exit(void)
{
    ISOLATOR_ERROR ret;
    char error_msg[ISOLATOR_ERROR_MSG_ALLOC_LEN];
    uint64_t stop_sig_time_tsc;

    if (g_core_used == -1)
    {
        //Isolation not performed yet, just return
        return;
    }

    timer_start( &stop_sig_time_tsc );
        
    // Wait for the module to signal that it has exited
    while (atomic_read(&g_atom_has_exited) == 0)
    {
        msleep( 1 );
    }

    RTSLOG_INFO("It took %lld ms for the RT code to exit.\n", timer_tock_ns(&stop_sig_time_tsc)/1000000);

    ret = rts_isolator_cleanup( g_core_used );
    if( ret != ISOLATOR_OK)
    {
        isolator_lookup_error_msg(ret, error_msg);
        RTSLOG_ERROR(": rts-cpu-isolator : There was an error when calling rts_isolator_cleanup(): %s\n", error_msg);
        return;
    }


}

// Kernel module cleanup function.
// We can't mark this as "__exit" because it can be called from rt_fe_init() on error
static void  rt_fe_cleanup( void )
{

    // Signal the model to stop
    atomic_set(&g_atom_should_exit, 1);

#ifdef NO_CPU_SHUTDOWN
    kthread_stop( sthread );
    msleep( 1000 );
#else
    wait_for_module_exit();
#endif

#ifdef DOLPHIN_TEST
    /// Cleanup Dolphin card connections
    finish_dolphin( );
#endif

    // Print out any error messages from FE code on exit
    print_exit_messages( fe_status_return, fe_status_return_subcode, SYSTEM_NAME_STRING_LOWER );
    detach_shared_memory( );
}

module_init( rt_fe_init );
module_exit( rt_fe_cleanup );

MODULE_DESCRIPTION( "Control system" );
MODULE_AUTHOR( "LIGO" );
MODULE_LICENSE( "Dual BSD/GPL" );
