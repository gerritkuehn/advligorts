/// @file commData3.c
///	@brief This is the generic software for communicating realtime data
/// between CDS applications.
///	@detail This software supports data communication via: \n
///		1) Shared memory, between two processes on the same computer \n
///		2) Dolphinics Reflected Memory over a local PCIe network.
///     3) Dolphinics Reflected Memory over a bridged PCIe network. (RFM)
/// @author R.Bork, A.Ivanov
/// @copyright Copyright (C) 2014 LIGO Project
/// California Institute of Technology	
///	Massachusetts Institute of Technology
///
/// @license This program is free software: you can redistribute it and/or
/// modify it under the terms of the GNU General Public License as published 
/// by the Free Software Foundation, version 3 of the License. This program
/// is distributed in the hope that it will be useful, but WITHOUT ANY
/// WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
/// for more details.
#include "commData3.h"
#include "controller.h" //cdsPciModules, _shmipc_shm
#include "drv/rts-logger.h"

#ifdef __KERNEL__
#include <asm/cacheflush.h>
#else
#include <stdio.h>
#include <stddef.h>
#endif


static int localCycleTo65K;

///	This function is called from the user application to initialize
/// communications structures 	and pointers.
///	@param[in] connects = total number of IPC connections in the application
///	@param[in] rate = Sample rate of the calling application eg 2048
///	@param[in,out] ipcInfo[] = Stucture to hold information about each IPC
void commData3Init(
    int connects, // total number of IPC connections in the application
    int model_rate_hz,
    int send_rate, // Sample rate of the calling application eg 2048, 16384, etc.
    CDS_IPC_INFO ipcInfo[] // IPC information structure
)
{
    int           ii;
    //Set ipcMemOffset to default value, will re-set below
    unsigned long ipcMemOffset = IPC_PCIE_BASE_OFFSET + RFM0_OFFSET;

    //For now all cycle numbers passed are in terms of a 65K or less
    //so we don't need to support a divisor
    if (model_rate_hz >= IPC_MAX_RATE)
    {
        localCycleTo65K = 1;
    }
    else
    {
        localCycleTo65K = IPC_MAX_RATE / model_rate_hz;
    }

    for ( ii = 0; ii < connects; ii++ )
    {
        // Set the sendCycle field, used by all send/rcv to determine IPC data
        // block to write/read
        //      All cycle counts sent as part of data sync word are based on max
        //      supported rate of 65536 cycles/sec, regardless of sender/receiver 
        //      native cycle rate.
        ipcInfo[ ii ].sendCycle = IPC_MAX_RATE / send_rate;

        // Sender always sends data at his native rate, or a configured lower ipc_rate. 
        // It is the responsiblity of the reciever to sync to this rate, 
        // regardless of the rate of the receiver application.
        if ( ipcInfo[ ii ].mode == IRCV ) // RCVR
        {

            if ( ipcInfo[ ii ].sendRate >= model_rate_hz )
            {
                ipcInfo[ ii ].rcvCycle65k = 1;
            }
            else //Model is faster than IPC
            {
                ipcInfo[ ii ].rcvCycle65k = IPC_MAX_RATE / ipcInfo[ ii ].sendRate;
            }
            RTSLOG_DEBUG("New Recever : sendRate: %d, rcvCycle65k: %d, localCycleTo65K: %d\n",
                         ipcInfo[ ii ].sendRate,
                         ipcInfo[ii].rcvCycle65k,
                         localCycleTo65K);
        }
        // Clear the data point
        ipcInfo[ ii ].data = 0.0;
        ipcInfo[ ii ].pIpcDataRead[ 0 ] = NULL;
        ipcInfo[ ii ].pIpcDataWrite[ 0 ] = NULL;

        // Save pointers to the IPC communications memory locations.
        if ( ipcInfo[ ii ].netType == IRFM0 )
            ipcMemOffset = IPC_PCIE_BASE_OFFSET + RFM0_OFFSET;
        else if ( ipcInfo[ ii ].netType == IRFM1 )
            ipcMemOffset = IPC_PCIE_BASE_OFFSET + RFM1_OFFSET;

        if ( ( ipcInfo[ ii ].netType == IRFM0 ||
               ipcInfo[ ii ].netType == IRFM1 ) &&
             ( ipcInfo[ ii ].mode == ISND ) &&
             ( cdsPciModules.dolphinRfmWritePtr ) )
        {
            ipcInfo[ ii ].pIpcDataWrite[ 0 ] =
                (CDS_IPC_COMMS*)( 
                    (volatile char*)( cdsPciModules.dolphinRfmWritePtr ) + ipcMemOffset 
                );
        }
        if ( ( ipcInfo[ ii ].netType == IRFM0 ||
               ipcInfo[ ii ].netType == IRFM1 ) &&
             ( ipcInfo[ ii ].mode == IRCV ) &&
             ( cdsPciModules.dolphinRfmReadPtr ) )
        {
            ipcInfo[ ii ].pIpcDataRead[ 0 ] =
                (CDS_IPC_COMMS*)( 
                    (volatile char*)( cdsPciModules.dolphinRfmReadPtr ) + ipcMemOffset 
                );
        }
        if ( ipcInfo[ ii ].netType ==
             ISHME ) // Computer shared memory ******************************
        {
            if ( ipcInfo[ ii ].mode == ISND )
                ipcInfo[ ii ].pIpcDataWrite[ 0 ] =
                    (CDS_IPC_COMMS*)( _shmipc_shm + IPC_BASE_OFFSET );
            else
                ipcInfo[ ii ].pIpcDataRead[ 0 ] =
                    (CDS_IPC_COMMS*)( _shmipc_shm + IPC_BASE_OFFSET );
        }
        // PCIe communications requires one pointer for sending data and a
        // second one for receiving data.
        if ( ( ipcInfo[ ii ].netType == IPCIE ) &&
             ( ipcInfo[ ii ].mode == IRCV ) &&
             ( cdsPciModules.dolphinPcieReadPtr ) )
        {
            ipcInfo[ ii ].pIpcDataRead[ 0 ] =
                (CDS_IPC_COMMS*)( 
                    (volatile char*)( cdsPciModules.dolphinPcieReadPtr ) + IPC_PCIE_BASE_OFFSET 
                );
        }
        if ( ( ipcInfo[ ii ].netType == IPCIE ) &&
             ( ipcInfo[ ii ].mode == ISND ) &&
             ( cdsPciModules.dolphinPcieWritePtr ) )
        {
            ipcInfo[ ii ].pIpcDataWrite[ 0 ] =
                (CDS_IPC_COMMS*)( 
                    (volatile char*)( cdsPciModules.dolphinPcieWritePtr ) + IPC_PCIE_BASE_OFFSET 
                );

        }
#if 0
	// Following for diags, if desired. Otherwise, leave out as it fills dmesg
	if(ipcInfo[ii].mode == ISND && ipcInfo[ii].netType != ISHME) {
        RTSLOG_DEBUG("IPC Number = %d\n",ipcInfo[ii].ipcNum);
        RTSLOG_DEBUG("IPC Name = %s\n",ipcInfo[ii].name);
        RTSLOG_DEBUG("Sender Model Name = %s\n",ipcInfo[ii].senderModelName);
        RTSLOG_DEBUG("RCV Rate  = %d\n",ipcInfo[ii].rcvRate);
        RTSLOG_DEBUG("Send Computer Number  = %d\n",ipcInfo[ii].sendNode);
        RTSLOG_DEBUG("Send address  = %lx\n",(unsigned long)&ipcInfo[ii].pIpcDataWrite[0]->dBlock[0][ipcInfo[ii].ipcNum].data);
	}
#endif
    }
}

// *************************************************************************************************
/// This function is called once on FE code init and returns the number of
/// IPC senders and receivers so they can be reported in a data channel
/// @param[in]  totalIPCs The total number of IPC in the model
/// @param[in]  ipcInfo[] The struct containing all IPC info for the model
/// @param[out] numSenders The number of IPCs sent by this model
/// @param[out] numReceiver The number of IPC receivers in this model
void commData3GetIpcNums(int totalIPCs, CDS_IPC_INFO ipcInfo[], int * numSenders, int * numReceiver)
{
    int i;

    *numSenders = 0;
    *numReceiver = 0;

    for ( i = 0; i < totalIPCs; ++i )
    {
        if ( ipcInfo[ i ].mode == ISND ) 
        {
            ++(*numSenders);
        }
        if ( ipcInfo[ i ].mode == IRCV ) 
        {
            ++(*numReceiver);
        }
    }
}


// *************************************************************************************************
///	@brief This function is called from the user application to send data via IPC
/// connections.
///
/// @detail This routine sends out all IPC data marked as a send (SND) channel in the IPC
///         INFO list. Data is sent at the native rate of the calling model. Data
///         sent is of type double, with timestamp and 65536 cycle count combined into
///         long. 
///	@param[in] connects = total number of IPC connections in the application
///	@param[in,out] ipcInfo[] = Structure to hold information about each IPC
///	@param[in] timeSec = Present GPS time in GPS seconds
///	@param[in] cycle = Present IPC cycle of the user application making this
/// call.
void commData3Send(
    int          connects, // Total number of IPC connections in the application
    CDS_IPC_INFO ipcInfo[], // IPC information structure
    int          timeSec, // Present GPS Second
    int          cycle ) // Application IPC write cycle count (0 to IPC_RATE)
{
    unsigned long
        syncWord; ///	\param syncWord Combined GPS timestamp and cycle counter
    int ipcIndex; ///	\param ipcIndex Pointer to next IPC data buffer
    int dataCycle; ///	\param dataCycle Cycle counter 0-65535
    int ii = 0; ///	\param ii Loop counter
    int chan; ///	\param chan Local ipc number
    int sendBlock; ///	\param sendBlock Data block data is to be sent to
    int lastPcie = -1;
    int dolphinEnabled = 1;

#ifdef RFM_DELAY
    // Need to write ahead one extra block
    int mycycle = ( cycle + RFM_DELAY );
    sendBlock = ( ( mycycle + 1 ) * ( IPC_MAX_RATE / IPC_RATE ) ) % IPC_BLOCKS;
    dataCycle = ( ( mycycle + 1 ) * ipcInfo[ 0 ].sendCycle ) % IPC_MAX_RATE;
    if ( dataCycle == 0 || dataCycle == ipcInfo[ 0 ].sendCycle )
        syncWord = timeSec + 1;
    else
        syncWord = timeSec;
    syncWord = ( syncWord << 32 ) + dataCycle;
#else


    sendBlock = ( ( cycle + 1 ) * ( IPC_MAX_RATE / IPC_RATE ) ) % IPC_BLOCKS;
    // Calculate the SYNC word to be sent with all data.
    // Determine the cycle count to be sent with the data
    dataCycle = ( ( cycle + 1 ) * ipcInfo[ 0 ].sendCycle ) % IPC_MAX_RATE;
    // Since this is write ahead, need to increment the GPS second if
    // writing to the first block of a new second.
    if ( dataCycle == 0 )
        syncWord = timeSec + 1;
    else
        syncWord = timeSec;
    // Combine GPS seconds and cycle counter into long word.
    syncWord = ( syncWord << 32 ) + dataCycle;
#endif

    //If the IOP model has signaled us to not send remote IPCsn
    if (ioMemData->stop_dolphin_ipcs != 0)
        dolphinEnabled = 0;


    // Want to send out RFM signals first to allow maximum time for data
    // delivery.
    for ( ii = 0; dolphinEnabled && ii < connects; ii++ )
    {
        // If IPC Sender on RFM Network:
        // RFM network has highest latency, so want to get these signals out
        // first.
        if ( ( ipcInfo[ ii ].mode == ISND ) &&
             ( ( ipcInfo[ ii ].netType == IRFM0 ) ||
               ( ipcInfo[ ii ].netType == IRFM1 ) ) )
        {
            chan = ipcInfo[ ii ].ipcNum;
            // Determine next block to write in IPC_BLOCKS block buffer
            ipcIndex = ipcInfo[ ii ].ipcNum;
            // Don't write to PCI RFM if network error detected by IOP
            if ( ipcInfo[ ii ].pIpcDataWrite[ 0 ] != NULL )
            {
                // Write Data
                ipcInfo[ ii ]
                    .pIpcDataWrite[ 0 ]
                    ->dBlock[ sendBlock ][ ipcIndex ]
                    .data = ipcInfo[ ii ].data;
                // Write timestamp/cycle counter word
                ipcInfo[ ii ]
                    .pIpcDataWrite[ 0 ]
                    ->dBlock[ sendBlock ][ ipcIndex ]
                    .timestamp = syncWord;
                lastPcie = ii;
            }
        }
    }

#ifdef __KERNEL__
    // Flush out the last PCIe transmission
    if ( lastPcie >= 0 )
    {
        clflush_cache_range(
            (void*)&( ipcInfo[ lastPcie ]
                          .pIpcDataWrite[ 0 ]
                          ->dBlock[ sendBlock ][ ipcInfo[ lastPcie ].ipcNum ]
                          .data ),
            16 );
    }
    lastPcie = -1;
#endif

//If RFM_DELAY is defined we messed up sendBlock and dataCycle above so we
//recalculate them for the below non-rfm IPCs
#ifdef RFM_DELAY
    // We don't want to delay SHMEM or PCIe writes, so calc block as usual,
    // so need to recalc send block and syncWord.
    sendBlock = ( ( cycle + 1 ) * ( IPC_MAX_RATE / IPC_RATE ) ) % IPC_BLOCKS;
    // Calculate the SYNC word to be sent with all data.
    // Determine the cycle count to be sent with the data
    dataCycle = ( ( cycle + 1 ) * ipcInfo[ 0 ].sendCycle ) % IPC_MAX_RATE;
    // Since this is write ahead, need to increment the GPS second if
    // writing to the first block of a new second.
    if ( dataCycle == 0 )
        syncWord = timeSec + 1;
    else
        syncWord = timeSec;
    // Combine GPS seconds and cycle counter into long word.
    syncWord = ( syncWord << 32 ) + dataCycle;
#endif

    for ( ii = 0; ii < connects; ii++ )
    {
        // If IPC Sender on PCIE Network or via Shared Memory:
        if ( ( ipcInfo[ ii ].mode == ISND ) &&
             ( ( ipcInfo[ ii ].netType == ISHME ) ||
               ( dolphinEnabled && ipcInfo[ ii ].netType == IPCIE ) ) ) //Don't send dolphin
        {
            chan = ipcInfo[ ii ].ipcNum;
            // Determine next block to write in IPC_BLOCKS block buffer
            ipcIndex = ipcInfo[ ii ].ipcNum;
            // Don't write to PCI RFM if network error detected by IOP
            if ( ipcInfo[ ii ].pIpcDataWrite[ 0 ] != NULL )
            {
                // Write Data
                ipcInfo[ ii ]
                    .pIpcDataWrite[ 0 ]
                    ->dBlock[ sendBlock ][ ipcIndex ]
                    .data = ipcInfo[ ii ].data;
                // Write timestamp/cycle counter word
                ipcInfo[ ii ]
                    .pIpcDataWrite[ 0 ]
                    ->dBlock[ sendBlock ][ ipcIndex ]
                    .timestamp = syncWord;
                if ( ipcInfo[ ii ].netType == IPCIE )
                {
                    lastPcie = ii;
                }
            }
        }
    }

#ifdef __KERNEL__
    // Flush out the last PCIe transmission
    if ( lastPcie >= 0 )
    {
        clflush_cache_range(
            (void*)&( ipcInfo[ lastPcie ]
                          .pIpcDataWrite[ 0 ]
                          ->dBlock[ sendBlock ][ ipcInfo[ lastPcie ].ipcNum ]
                          .data ),
            16 );
    }
#endif
}

// *************************************************************************************************
///	This function is called from the user application to receive data via
/// IPC connections. It receives all IPC data marked as a read (RCV) channel
/// in the IPC INFO list.
///
///	@param[in] connects = total number of IPC connections in the application
///	@param[in,out] ipcInfo[] = Stucture to hold information about each IPC
///	@param[in] timeSec = Present GPS time in GPS seconds
///	@param[in] cycle = Present cycle of the user application making this
/// call.
///
/// @return The number of IPCs that had at least one error during the read
///
int commData3Receive(
    int          connects, // Total number of IPC connections in the application
    CDS_IPC_INFO ipcInfo[], // IPC information structure
    int          timeSec, // Present GPS Second
    int          cycle ) // Application cycle count (0 to FE_CODE_RATE)

{
    unsigned long syncWord; // Combined GPS timestamp and cycle counter word
                            // received with data
    unsigned long mySyncWord; // Local version of syncWord for comparison and
                              // error detection
    int ipcIndex; // Pointer to next IPC data buffer
    int cycle65k; // All data sent with 64K cycle count; need to convert local
                  // app cycle count to match
    int    ii;
    int    rcvBlock; // Which of the IPC_BLOCKS IPC data blocks to read from
    double tmp; // Temp location for data for checking NaN
    int numInError=0;


    // Create local 65K cycle count
    cycle65k = (  cycle * localCycleTo65K );
    // Calculate the block where the next data point is at
    rcvBlock = (cycle65k) % IPC_BLOCKS;


    for ( ii = 0; ii < connects; ii++ )
    {
        if ( ipcInfo[ ii ].mode == IRCV ) // Zero = Rcv and One = Send
        {
            if ( (cycle65k % ipcInfo[ ii ].rcvCycle65k ) == 0 ) // Time to rcv
            {

                if ( ipcInfo[ ii ].pIpcDataRead[ 0 ] != NULL )
                {

                    ipcIndex = ipcInfo[ ii ].ipcNum;
                    // Read GPS time/cycle count
                    tmp = ipcInfo[ ii ]
                              .pIpcDataRead[ 0 ]
                              ->dBlock[ rcvBlock ][ ipcIndex ]
                              .data;
                    syncWord = ipcInfo[ ii ]
                                   .pIpcDataRead[ 0 ]
                                   ->dBlock[ rcvBlock ][ ipcIndex ]
                                   .timestamp;
                    mySyncWord = timeSec;
                    // Create local GPS time/cycle word for comparison to ipc
                    mySyncWord = ( mySyncWord << 32 ) + cycle65k;
                    // If IPC syncword = local syncword, data is good
                    if ( syncWord == mySyncWord )
                    {
                        ipcInfo[ ii ].data = tmp;
                        // If IPC syncword != local syncword, data is BAD
                        // Set error and leave value same as last good receive
                    }
                    else
                    {
                        ipcInfo[ ii ].errFlag++;
                    }
                }
                else
                {
                    ipcInfo[ ii ].errFlag++;
                }
            }
            if (ipcInfo[ ii ].errFlag != 0)
                ++numInError;
        }
    }

    // On cycle 0, set error flags to send back to EPICS
    if ( cycle == 0 )
    {
        ipcErrBits = ipcErrBits & 0xf0;
        for ( ii = 0; ii < connects; ii++ )
        {
            if ( ipcInfo[ ii ].mode == IRCV ) // Zero = Rcv and One = Send
            {
                ipcInfo[ ii ].errTotal = ipcInfo[ ii ].errFlag;
                if ( ipcInfo[ ii ].errFlag )
                {
                    ipcErrBits |= 1 << ( ipcInfo[ ii ].netType );
                    ipcErrBits |= 16 << ( ipcInfo[ ii ].netType );
                }
                ipcInfo[ ii ].errFlag = 0;
            }
        }
    } //if ( cycle == 0 )

    return numInError;
}

