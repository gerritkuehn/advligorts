#ifndef LIGO_CONTROLLER_IOP_H
#define LIGO_CONTROLLER_IOP_H

#include "controller.h"
#include "drv/cdsHardware.h"
#include "cds_types.h" 

#include "commData3.h" //TIMING_SIGNAL

#if defined( XMIT_DOLPHIN_TIME ) || defined( USE_DOLPHIN_TIMING )
extern volatile TIMING_SIGNAL* pcieTimer;
#endif

extern int ioClockDac;
extern int ioMemCntrDac;

extern int dacEnable;
extern int dacWriteEnable;

extern int dacTimingErrorPending[ MAX_DAC_MODULES ];

extern int dacTimingError;

extern int pBits[ 9 ];

extern duotone_diag_t dt_diag; 


#endif //LIGO_CONTROLLER_IOP_H
