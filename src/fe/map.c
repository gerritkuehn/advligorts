///	\file map.c
///	\brief This file contains the software to find PCIe devices on the bus.
//
/// The header for this file is at include/drv/map.h

#include "drv/map.h"



#include "drv/cdsHardware.h"
#include "drv/plx_9056.h"
#include "commData3.h"
#include "../drv/mbuf/mbuf_kernel.h"
#include "map_cards_2_slots.h"
#include "drv/rts-logger.h"

// Include driver code for all supported I/O cards
#include "drv/gsc16ai64.h"
#include "drv/gsc18ai32.h"
#include "drv/gsc18ai64.h"
#include "drv/gsc_adc_common.h"
#include "drv/gsc16ao16.h"
#include "drv/gsc18ao8.h"
#include "drv/gsc20ao8.h"
#include "drv/gsc_dac_common.h"
#include "drv/accesIIRO8.h"
#include "drv/accesIIRO16.h"
#include "drv/accesDio24.h"
#include "drv/contec6464.h"
#include "drv/contec1616.h"
#include "drv/contec32o.h"
#include "drv/vmic5565.h"
#include "drv/symmetricomGps.h"
#include "drv/spectracomGPS.h"
#include "drv/ligoPcieTiming.h"
#include "drv/spectracomGPS.h" //spectracomGpsInit()
#include "controller.h"

#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/pci.h>
#include <asm/io.h>
#include <asm/uaccess.h>
#include <asm/delay.h>


//
// Global Data
//
dma_addr_t rfm_dma_handle[ MAX_DAC_MODULES ];
//TODO: only vmic5565.c uses this, might be able to make parameter

//
// Local functions
//
#if defined( RUN_WO_IO_MODULES ) || defined( USE_DOLPHIN_TIMING ) || !defined(IOP_MODEL)
static int mapPciModulesVirtual( CDS_HARDWARE* pCds );
#else
static int mapPciModulesRealCards( CDS_HARDWARE* pCds );
#endif


// *****************************************************************************
/// \brief Patch to properly handle PEX PCIe chip for newer (PCIe) General
/// Standards
///< DAC modules ie those that are integrated PCIe boards vs. earlier versions
///< built with carrier boards. \n This is extracted from code provided by GSC..
// *****************************************************************************
void
set_8111_prefetch( struct pci_dev* dacdev )
{
    struct pci_dev* dev = dacdev->bus->self;

    RTSLOG_INFO( "set_8111_prefetch: subsys=0x%x; vendor=0x%x\n",
            dev->device,
            dev->vendor );
    if ( ( dev->device == 0x8111 ) && ( dev->vendor == PLX_VID ) )
    {
        unsigned int reg;
        // Handle PEX 8111 setup, enable prefetch, set pref size to 64
        // These numbers come from reverse engineering the GSC pxe8111 driver
        // and using their prefetch program to enable the prefetch and set pref
        // size to 64
        pci_write_config_dword( dev, 132, 72 );
        pci_read_config_dword( dev, 136, &reg );
        pci_write_config_dword( dev, 136, reg );
        pci_write_config_dword( dev, 132, 72 );
        pci_read_config_dword( dev, 136, &reg );
        pci_write_config_dword( dev, 136, reg | 1 );
        pci_write_config_dword( dev, 132, 12 );
        pci_read_config_dword( dev, 136, &reg );
        pci_write_config_dword( dev, 136, reg | 0x8000000 );
    }
}

// *****************************************************************************
/// Routine to find backplane slot for ADC/DAC cards.
// *****************************************************************************
int 
find_card_slot( CDS_HARDWARE* pCds, int ctype, int cinstance )
{
    int ii;
    for (ii=0;ii<pCds->ioc_cards;ii++)
    {
        if(pCds->ioc_config[ ii ] == ctype && pCds->ioc_instance[ ii ] == cinstance)
            return ii;
    }
    return -1;
}


int
mapPciModules( CDS_HARDWARE* pCds )
{
#if defined( RUN_WO_IO_MODULES ) || defined( USE_DOLPHIN_TIMING ) || !defined(IOP_MODEL)
    return mapPciModulesVirtual(pCds);
#else
    return mapPciModulesRealCards(pCds);
#endif
}


// *****************************************************************************
/// Routine to setup shared memory for virtual IO
// *****************************************************************************
#if defined( RUN_WO_IO_MODULES ) || defined( USE_DOLPHIN_TIMING ) || !defined(IOP_MODEL)
static int
mapPciModulesVirtual( CDS_HARDWARE* pCds )
{
    static struct pci_dev* dacdev;
    int                    status;
    int                    i,ii;
    int                    ret;
    int                    modCount = 0;
//This does not build if CONTROL_MODEL is defined because the use of adc_cnt
//is not blocked out below
//#ifndef CONTROL_MODEL
    int            adc_cnt = 0;
//#endif
    int            dac_cnt = 0;
    char           fname[ 128 ];
    volatile unsigned char* _device_shm;
    volatile int*           data;
    char err_msg[MBUF_ERROR_MSG_ALLOC_LEN];


    dacdev = NULL;
    status = 0;

    // Initialize CDS_HARDWARE Structure
    pCds->adcCount = 0;
    pCds->dacCount = 0;
    pCds->dioCount = 0;
    pCds->doCount = 0;

    for ( i = 0; i < pCds->cards; i++ )
    {
        if ( pCds->cards_used[ i ].type == GSC_18AO8 )
        {
            sprintf( fname, "%s_%d", "IO_DEV_", i );
            ret = mbuf_allocate_area( fname, 8 * 4 * 65536, (volatile void **)&_device_shm );
            if ( ret != MBUF_KERNEL_CODE_OK )
            {
                mbuf_lookup_error_msg(ret, err_msg);
                RTSLOG_ERROR( "mbuf_allocate_area() failed, error msg: %s\n", err_msg );
                return -1;
            }
            pCds->pci_dac[ dac_cnt ] = (volatile int *)_device_shm;
            pCds->dacType[ dac_cnt ] = GSC_18AO8;
            pCds->dacInstance[ dac_cnt ] =  pCds->card_count[ GSC_18AO8 ];
            pCds->dacCount++;
            pCds->card_count[ GSC_18AO8 ] ++;
            dac_cnt++;
        }
        if ( pCds->cards_used[ i ].type == GSC_16AO16 )
        {
            sprintf( fname, "%s_%d", "IO_DEV_", i );
            ret = mbuf_allocate_area( fname, 16 * 4 * 65536, (volatile void **)&_device_shm );
            if ( ret != MBUF_KERNEL_CODE_OK )
            {
                mbuf_lookup_error_msg(ret, err_msg);
                RTSLOG_ERROR( "mbuf_allocate_area() failed, error msg: %s\n", err_msg );
                return -1;
            }
            pCds->pci_dac[ dac_cnt ] = (volatile int *)_device_shm;
            pCds->dacType[ dac_cnt ] = GSC_16AO16;
            pCds->dacInstance[ dac_cnt ] =  pCds->card_count[ GSC_16AO16 ];
            pCds->dacCount++;
            pCds->card_count[ GSC_16AO16 ] ++;
            dac_cnt++;
        }
        if ( pCds->cards_used[ i ].type == GSC_20AO8 )
        {
            sprintf( fname, "%s_%d", "IO_DEV_", i );
            ret = mbuf_allocate_area( fname, 8 * 4 * 65536, (volatile void **)&_device_shm );
            if ( ret != MBUF_KERNEL_CODE_OK )
            {
                mbuf_lookup_error_msg(ret, err_msg);
                RTSLOG_ERROR( "mbuf_allocate_area() failed, error msg: %s\n", err_msg );
                return -1;
            }
            pCds->pci_dac[ dac_cnt ] = (volatile int *)_device_shm;
            pCds->dacType[ dac_cnt ] = GSC_20AO8;
            pCds->dacInstance[ dac_cnt ] =  pCds->card_count[ GSC_20AO8 ];
            pCds->dacCount++;
            dac_cnt++;
            pCds->card_count[ GSC_20AO8 ] ++;
        }
        if ( pCds->cards_used[ i ].type == GSC_16AI64SSA )
        {
            sprintf( fname, "%s_%d", "IO_DEV_", i );
            ret = mbuf_allocate_area( fname, 32 * 4 * 128, (volatile void **)&_device_shm );
            if ( ret != MBUF_KERNEL_CODE_OK )
            {
                mbuf_lookup_error_msg(ret, err_msg);
                RTSLOG_ERROR( "mbuf_allocate_area() failed, error msg: %s\n", err_msg );
                return -1;
            }
            pCds->pci_adc[ adc_cnt ] = (volatile int *)_device_shm;
            pCds->adcType[ adc_cnt ] = GSC_16AI64SSA;
            pCds->adcInstance[ adc_cnt ] =  pCds->card_count[ GSC_16AI64SSA ];
            pCds->card_count[ GSC_16AI64SSA ] ++;
            pCds->adcChannels[ adc_cnt ] = 32;
            pCds->adcCount++;
            data = (volatile int *) pCds->pci_adc[ adc_cnt ];
            for(ii=0;ii<64;ii++) {
                *data = ii;
                data ++;
            }
            adc_cnt++;
        }
        modCount++;
    }
    return ( modCount );
}


#else

// *****************************************************************************
/// Routine to find PCI modules and call the appropriate driver initialization
/// software.
// *****************************************************************************
static int
mapPciModulesRealCards( CDS_HARDWARE* pCds )
{
    static struct pci_dev* dacdev;
    int                    status;
    int                    i;
    int                    modCount = 0;
    int adc_750_cnt = 0;
    int adc_cnt = 0;
    int dac_cnt = 0;
    int dac_18bit_cnt = 0;
    int dac_20bit_cnt = 0;
    int            use_it;
    int            bo_cnt = 0;
    int            fast_adc_cnt = 0;

    dacdev = NULL;
    status = 0;

    // initialize dac duotone multipliers/divisors in case
    // we don't have one or the other of DAC or ADC cards
    pCds->adcDuoToneDivisor[ 0 ] = 1;
    pCds->dacDuoToneMultiplier[ 0 ] = 1;

   modCount = map_cards_2_slots ( &cdsPciModules ) ;

    // Map and Initialize ADC and DAC modules
    // This section map cards by model definition order
    for ( i = 0; i < pCds->cards; i++ )
    {
        adc_cnt = 0;
        fast_adc_cnt = 0;
        adc_750_cnt = 0;
        dac_cnt = 0;
        dac_18bit_cnt = 0;
        dac_20bit_cnt = 0;
        // Search system for any module with PLX-9056 and PLX id
        while ( ( dacdev = pci_get_device( PLX_VID, PLX_TID, dacdev ) ) )
        {
            // Check if it is an ADC module
            if ( ( dacdev->subsystem_device == ADC_SS_ID ) &&
                 ( dacdev->subsystem_vendor == PLX_VID ) )
            {
                if ( pCds->cards_used[ i ].instance == adc_cnt &&
                     pCds->cards_used[ i ].type == GSC_16AI64SSA )
                {
                    pCds->adcSlot[ pCds->adcCount] = find_card_slot( pCds, GSC_16AI64SSA, adc_cnt );
                    pCds->adcTimeShift[ pCds->adcCount ] = pCds->cards_used[i].time_shift;
                    pCds->adcRateSPS[ pCds->adcCount ] = pCds->cards_used[i].rate_sps;
                    status = gsc16ai64Init( pCds, dacdev );
                    RTSLOG_INFO( "adc card on bus %x; device %x status %d time shift %d\n",
                            dacdev->bus->number,
                            PCI_SLOT( dacdev->devfn ),
                            status,
                            pCds->adcTimeShift[ pCds->adcCount - 1 ]);
                    if(status != 0)
                    {
                        RTSLOG_ERROR( "Map fault GSC16AI64 ADC number %d slot %d\n",
                            adc_cnt,pCds->adcSlot[ pCds->adcCount]);
                        return IO_CARD_MAP_ERROR;
                    } 
                }
                adc_cnt++;
            }
            // Check if it is a 1M ADC module
            if ( ( dacdev->subsystem_device == ADC_18AI32_SS_ID ) &&
                 ( dacdev->subsystem_vendor == PLX_VID ) )
            {
                if ( pCds->cards_used[ i ].instance == fast_adc_cnt &&
                     pCds->cards_used[ i ].type == GSC_18AI32SSC1M )
                {
                    pCds->adcSlot[ pCds->adcCount] = find_card_slot( pCds, GSC_18AI32SSC1M, fast_adc_cnt );
                    pCds->adcTimeShift[ pCds->adcCount ] = pCds->cards_used[i].time_shift;
                    pCds->adcRateSPS[ pCds->adcCount ] = pCds->cards_used[i].rate_sps;
                    status = gsc18ai32Init( pCds, dacdev );
                    RTSLOG_INFO( "fast adc card on bus %x; device %x; time shift %d\n",
                            dacdev->bus->number,
                            PCI_SLOT( dacdev->devfn ),
                            pCds->adcTimeShift[ pCds->adcCount - 1 ]);
                    if(status != 0)
                    {
                        RTSLOG_ERROR( "Map fault GSC18AI32 ADC number %d slot %d\n",
                            fast_adc_cnt,pCds->adcSlot[ pCds->adcCount]);
                        return IO_CARD_MAP_ERROR;
                    } 
                }
                fast_adc_cnt++;
            }

            // Check if it is a 750KHz ADC module
            if ( ( dacdev->subsystem_device == ADC_18AI64_SS_ID ) &&
                 ( dacdev->subsystem_vendor == PLX_VID ) )
            {
                if ( pCds->cards_used[ i ].instance == adc_750_cnt &&
                     pCds->cards_used[ i ].type == GSC_18AI64SSC )
                {
                    pCds->adcSlot[ pCds->adcCount] = find_card_slot( pCds, GSC_18AI64SSC, adc_750_cnt );
                    pCds->adcTimeShift[ pCds->adcCount ] = pCds->cards_used[i].time_shift;
                    pCds->adcRateSPS[ pCds->adcCount ] = pCds->cards_used[i].rate_sps;
                    status = gsc18ai64Init( pCds, dacdev );
                    RTSLOG_INFO( "750KHz adc card on bus %x; device %x; time shift %d\n",
                            dacdev->bus->number,
                            PCI_SLOT( dacdev->devfn ),
                            pCds->adcTimeShift[ pCds->adcCount - 1 ]);
                    if(status != 0)
                    {
                        RTSLOG_ERROR( "Map fault GSC18AI64 ADC number %d slot %d\n",
                            adc_750_cnt,pCds->adcSlot[ pCds->adcCount]);
                        return IO_CARD_MAP_ERROR;
                    } 
                }
                adc_750_cnt++;
            }


            // Search for DAC16 cards
            // Search system for any module with PLX-9056 and PLX id
            // Check if it is a DAC16 module
            if ( ( dacdev->subsystem_device == DAC_SS_ID ) &&
                 ( dacdev->subsystem_vendor == PLX_VID ) )
            {
                if ( pCds->cards_used[ i ].instance == dac_cnt &&
                     pCds->cards_used[ i ].type == GSC_16AO16 )
                {
                    pCds->dacSlot[ pCds->dacCount] = find_card_slot( pCds, GSC_16AO16, dac_cnt );
                    status = gsc16ao16Init( pCds, dacdev );
                    RTSLOG_INFO( "16 bit dac card on bus %x; device %x status %d\n",
                            dacdev->bus->number,
                            PCI_SLOT( dacdev->devfn ),
                            status );
                    if(status != 0)
                    {
                        RTSLOG_ERROR( "Map fault GSC16AO16 DAC number %d slot %d\n",
                            dac_cnt,pCds->dacSlot[ pCds->dacCount]);
                        return IO_CARD_MAP_ERROR;
                    } 
                }
                dac_cnt++;
            }

            // Search system for any module with PLX-9056 and PLX id
            // Check if it is a DAC16 module
            if ( ( dacdev->subsystem_device == DAC_18BIT_SS_ID ) &&
                 ( dacdev->subsystem_vendor == PLX_VID ) )
            {
                if ( pCds->cards_used[ i ].instance == dac_18bit_cnt &&
                     pCds->cards_used[ i ].type == GSC_18AO8 )
                {
                    pCds->dacSlot[ pCds->dacCount] = find_card_slot( pCds, GSC_18AO8, dac_18bit_cnt );
                    status = gsc18ao8Init( pCds, dacdev );
                    RTSLOG_INFO( "18-bit dac card on bus %x; device %x status %d\n",
                            dacdev->bus->number,
                            PCI_SLOT( dacdev->devfn ),
                            status );
                    if(status != 0)
                    {
                        RTSLOG_ERROR( "Map fault GSC18AO8 DAC number %d slot %d\n",
                            dac_18bit_cnt,pCds->dacSlot[ pCds->dacCount]);
                        return IO_CARD_MAP_ERROR;
                    }
                }
                dac_18bit_cnt++;
            }

            // Check if it is a DAC20 module
            if ( ( dacdev->subsystem_device == DAC_20BIT_SS_ID ) &&
                 ( dacdev->subsystem_vendor == PLX_VID ) )
            {
                if ( pCds->cards_used[ i ].instance == dac_20bit_cnt &&
                     pCds->cards_used[ i ].type == GSC_20AO8 )
                {
                    pCds->dacSlot[ pCds->dacCount] = find_card_slot( pCds, GSC_20AO8, dac_20bit_cnt );
                    status = gsc20ao8Init( pCds, dacdev );
                    RTSLOG_INFO( "20-bit dac card on bus %x; device %x status %d\n",
                            dacdev->bus->number,
                            PCI_SLOT( dacdev->devfn ),
                            status );
                    if(status != 0)
                    {
                        RTSLOG_ERROR( "Map fault GSC18AO8 DAC number %d slot %d\n",
                            dac_20bit_cnt,pCds->dacSlot[ pCds->dacCount]);
                        return IO_CARD_MAP_ERROR;
                    } 
                }
                dac_20bit_cnt++;
            }
        } // end of while
    } // end of pci_cards useddrv/map.h

    dacdev = NULL;
    status = 0;
    bo_cnt = 0;
    // Search for ACCESS PCI-DIO  modules
    while ( ( dacdev = pci_get_device( ACC_VID, ACC_TID, dacdev ) ) )
    {
        use_it = 0;
        if ( pCds->cards )
        {
            use_it = 0;
            /* See if ought to use this one or not */
            for ( i = 0; i < pCds->cards; i++ )
            {
                if ( pCds->cards_used[ i ].type == ACS_24DIO &&
                     pCds->cards_used[ i ].instance == bo_cnt )
                {
                    use_it = 1;
                    break;
                }
            }
        }
        if ( use_it )
        {
            RTSLOG_INFO( "Access 24 BIO card on bus %x; device %x vendor 0x%x\n",
                    dacdev->bus->number,
                    PCI_SLOT( dacdev->devfn ),
                    dacdev->device );
            status = accesDio24Init( pCds, dacdev );
            modCount++;
        }
        bo_cnt++;
    }

    dacdev = NULL;
    status = 0;
    bo_cnt = 0;
    // Search for ACCESS PCI-IIRO-8 isolated I/O modules
    while ( ( dacdev = pci_get_device( ACC_VID, PCI_ANY_ID, dacdev ) ) )
    {
        if ( dacdev->device != ACC_IIRO_TID &&
             dacdev->device != ACC_IIRO_TID_OLD )
            continue;
        use_it = 0;
        if ( pCds->cards )
        {
            use_it = 0;
            /* See if ought to use this one or not */
            for ( i = 0; i < pCds->cards; i++ )
            {
                if ( pCds->cards_used[ i ].type == ACS_8DIO &&
                     pCds->cards_used[ i ].instance == bo_cnt )
                {
                    use_it = 1;
                    break;
                }
            }
        }
        if ( use_it )
        {
            RTSLOG_INFO( "Access 8 BIO card on bus %x; device %x vendor 0x%x\n",
                    dacdev->bus->number,
                    PCI_SLOT( dacdev->devfn ),
                    dacdev->device );
            status = accesIiro8Init( pCds, dacdev );
            modCount++;
        }
        bo_cnt++;
    }


    dacdev = NULL;
    status = 0;
    bo_cnt = 0;
    // Search for ACCESS PCI-IIRO-16 isolated I/O modules
    while ( ( dacdev = pci_get_device( ACC_VID, PCI_ANY_ID, dacdev ) ) )
    {
        if ( dacdev->device != ACC_IIRO_TID16 &&
             dacdev->device != ACC_IIRO_TID16_OLD )
            continue;
        use_it = 0;
        if ( pCds->cards )
        {
            use_it = 0;
            /* See if ought to use this one or not */
            for ( i = 0; i < pCds->cards; i++ )
            {
                if ( pCds->cards_used[ i ].type == ACS_16DIO &&
                     pCds->cards_used[ i ].instance == bo_cnt )
                {
                    use_it = 1;
                    break;
                }
            }
        }
        if ( use_it )
        {
            RTSLOG_INFO( "Access BIO-16 card on bus %x; device %x\n",
                    dacdev->bus->number,
                    PCI_SLOT( dacdev->devfn ) );
            status = accesIiro16Init( pCds, dacdev );
            modCount++;
        }
        bo_cnt++;
    }

    dacdev = NULL;
    status = 0;
    bo_cnt = 0;

    // Search for Contec C_DIO_6464L_PE isolated I/O modules
    while ( ( dacdev = pci_get_device( CONTEC_VID, C_DIO_6464L_PE, dacdev ) ) )
    {
        use_it = 0;
        if ( pCds->cards )
        {
            use_it = 0;
            /* See if ought to use this one or not */
            for ( i = 0; i < pCds->cards; i++ )
            {
                if ( pCds->cards_used[ i ].type == CON_6464DIO &&
                     ( pCds->cards_used[ i ].instance * 2 ) == bo_cnt )
                {
                    use_it = 1;
                    break;
                }
            }
        }
        if ( use_it )
        {
            RTSLOG_INFO( "Contec 6464 DIO card on bus %x; device %x\n",
                    dacdev->bus->number,
                    PCI_SLOT( dacdev->devfn ) );
            status = contec6464Init( pCds, dacdev );
            modCount++;
            modCount++;
        }
        bo_cnt++;
        bo_cnt++;
    }

    dacdev = NULL;
    status = 0;
    bo_cnt = 0;

    // Search for Contec C_DIO_1616L_PE isolated I/O modules
    // Support for this card is strictly limited in use to
    // setting LIGO timing card (non-PCIe version) settings
    while ( ( dacdev = pci_get_device( CONTEC_VID, C_DIO_1616L_PE, dacdev ) ) )
    {
        RTSLOG_INFO( "Contec 1616 DIO card on bus %x; device %x\n",
                dacdev->bus->number,
                PCI_SLOT( dacdev->devfn ) );
        status = contec1616Init( pCds, dacdev );
        modCount++;
        bo_cnt++;
    }

    dacdev = NULL;
    status = 0;
    bo_cnt = 0;

    // Search for Contec C_DO_32L_PE isolated I/O modules
    while ( ( dacdev = pci_get_device( CONTEC_VID, C_DO_32L_PE, dacdev ) ) )
    {
        use_it = 0;
        if ( pCds->cards )
        {
            use_it = 0;
            /* See if ought to use this one or not */
            for ( i = 0; i < pCds->cards; i++ )
            {
                if ( pCds->cards_used[ i ].type == CON_32DO &&
                     pCds->cards_used[ i ].instance == bo_cnt )
                {
                    use_it = 1;
                    break;
                }
            }
        }
        if ( use_it )
        {
            RTSLOG_INFO( "Contec BO card on bus %x; device %x\n",
                    dacdev->bus->number,
                    PCI_SLOT( dacdev->devfn ) );
            status = contec32OutInit( pCds, dacdev );
            modCount++;
        }
        bo_cnt++;
    }

    dacdev = NULL;
    status = 0;

    for ( i = 0; i < MAX_RFM_MODULES; i++ )
    {
        pCds->pci_rfm[ i ] = 0;
    }

    dacdev = NULL;
    status = 0;
    pCds->gps = 0;
    pCds->gpsType = 0;
    dacdev = NULL;
    status = 0;
    // Look for TSYNC GPS board
    if ( ( dacdev = pci_get_device( TSYNC_VID, TSYNC_TID, dacdev ) ) )
    {
        RTSLOG_INFO( "TSYNC GPS card on bus %x; device %x\n",
                dacdev->bus->number,
                PCI_SLOT( dacdev->devfn ) );
        status = spectracomGpsInit( pCds, dacdev );
    }
    // Look for a LIGO PCIe Timing Card
    dacdev = NULL;
    status = 0;
    bo_cnt = 0;
    while ( ( dacdev = pci_get_device( 0x10ee, PCI_ANY_ID, dacdev ) ) )
    {
	if(dacdev != NULL) {
        RTSLOG_INFO( "Xilinx card on bus %x; device %x vendor 0x%x\n",
                    dacdev->bus->number,
                    PCI_SLOT( dacdev->devfn ),
                    dacdev->device );
        status = lptcInit( pCds, dacdev );
	}
    }


    return ( modCount );
}

#endif
