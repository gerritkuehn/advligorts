/// @file print_io_info.h
/// @brief File contains routine to print IO info on code startup.
#ifndef LIGO_PRINT_IO_INFO_H
#define LIGO_PRINT_IO_INFO_H

#include "drv/cdsHardware.h"

#ifdef __cplusplus
extern "C" {
#endif

void print_io_info(const char* model_name, CDS_HARDWARE* cdsp, int iopmodel );

#ifdef __cplusplus
}
#endif



#endif //LIGO_PRINT_IO_INFO_H
