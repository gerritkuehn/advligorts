#ifndef LIGO_DOLPHIN_H
#define LIGO_DOLPHIN_H

#ifdef __cplusplus
extern "C" {
#endif

//We have two segments per model, one for "PCIE" IPCs,
//these are IPCs to/from models on a single 
//dolphin netowrk (CS, EX, EY). The second segment is 
//for "RFM" IPCs, these are for IPCs to/from models 
//on diffrent networks EX <-> CS <-> EY and are 
//trasfered by the cdsrfm model/FE
#define NUM_DOLPHIN_SEGMENTS_PER_MODEL 2

int init_dolphin( void );
void finish_dolphin( void );


#ifdef __cplusplus
}
#endif


#endif //LIGO_DOLPHIN_H
