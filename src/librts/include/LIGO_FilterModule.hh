#ifndef LIGO_FILTER_MODULE_HH
#define LIGO_FILTER_MODULE_HH


#include <memory>
#include <string>
#include <vector>
#include <optional>

namespace rts
{

    class LIGO_FilterModule
    {
        public:

            virtual ~LIGO_FilterModule() = default;

            //
            //Info methods
            //

            /**
             * @brief Returns the filter module's index
             *
             * @return The filter module's index, used as an identifier 
             *         and as an index in global filter arrays.
             */
            int get_index() const;

            /**
             * @brief Checks if the filter module is a FIR filter 
             *
             * @param stageIndexToRead The index of the filter stage
             *        to check.
             *
             * NOTE: This will only work after filters are loaded, 
             *       because the read logic uses the number of coefficients
             *       to determine the type of filter. 
             *
             * @return True if this module is a FIR filter, False otherwise
             *         
             */
            bool isFIR( int stageIndexToRead );

            //
            //Module level Control
            //
            LIGO_FilterModule& enable_input();
            LIGO_FilterModule& disable_input();
            LIGO_FilterModule& enable_output();
            LIGO_FilterModule& disable_output();
            LIGO_FilterModule& enable_offset(double offset);
            LIGO_FilterModule& disable_offset();
            LIGO_FilterModule& enable_limiter(double absLimit);
            LIGO_FilterModule& disable_limiter();
            LIGO_FilterModule& enable_hold();
            LIGO_FilterModule& disable_hold();
            LIGO_FilterModule& set_gain(double gain);
            LIGO_FilterModule& set_ramp_time_s(double time_s);
            int get_opSwitchE();
            LIGO_FilterModule& set_opSwitchE(int sw);
            int get_section_ramp(int section_index);
            LIGO_FilterModule& set_section_ramp(int section_index, int ramp);
            int get_section_timeout(int section_index);
            LIGO_FilterModule& set_section_timeout(int section_index, int timeout);
            
            //Stage level control
            LIGO_FilterModule& enable_stages( const std::vector<int> & indices); 
            LIGO_FilterModule& disable_stages( const std::vector<int> & indices);

            /*
            TODO: ALlow setting and unsetting of stage types
            LIGO_FilterModule& setStageTypeZeroHistory();
            LIGO_FilterModule& setStageTypeImmediately();
            */

        private:

            LIGO_FilterModule(int module_index) : _module_index(module_index) {};
            int _module_index;

            friend class Model;

            const static unsigned _stage_input_bits[];
            const static unsigned _stage_output_bits[];

    };

}

#endif //LIGO_FILTER_MODULE_HH
