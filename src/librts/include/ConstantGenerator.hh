#ifndef LIGO_CONSTANT_GENERATOR_HH
#define LIGO_CONSTANT_GENERATOR_HH

#include "Generator.hh"

#include <vector>
#include <stdexcept>

namespace rts
{
    class ConstantGenerator : public Generator
    {
        public:
            ConstantGenerator(double value)
                : _value(value)
            {
            }
            virtual double get_next_sample()
            {
                return _value;
            }
            virtual void reset() {}
            virtual ~ConstantGenerator() {};
        private:
            double _value;

    };
}




#endif //LIGO_CONSTANT_GENERATOR_HH
