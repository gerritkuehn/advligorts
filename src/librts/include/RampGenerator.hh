#ifndef LIGO_RAMP_GENERATOR_HH
#define LIGO_RAMP_GENERATOR_HH

#include "Generator.hh"

#include <vector>
#include <stdexcept>

namespace rts
{
    class RampGenerator : public Generator
    {
        public:
            RampGenerator(double start_val, double step)
                : _start_val(start_val), _step(step), _cur_val(start_val)
            {
            }
            virtual double get_next_sample()
            {
                double hold = _cur_val;
                _cur_val += _step;
                return hold;
            }
            virtual void reset()
            {
                _cur_val = _start_val;
            }
            virtual ~RampGenerator() {};
        private:
            double _start_val;
            double _step;
            double _cur_val;

    };
}




#endif //LIGO_CANNED_SIGNAL_GENERATOR_HH
