project (fcn-test)

add_executable(${PROJECT_NAME} ${CMAKE_CURRENT_SOURCE_DIR}/main.cpp)

target_include_directories(${PROJECT_NAME}
    PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}/../../include
    ${CMAKE_CURRENT_SOURCE_DIR}/../include/
    )

if (USE_OLD_CATCH_HEADER_ONLY)
    target_compile_options(${PROJECT_NAME} PRIVATE "-DUSE_OLD_CATCH_HEADER_ONLY=1")
else()
    target_link_libraries(${PROJECT_NAME} PRIVATE Catch2::Catch2WithMain)
endif()

target_link_libraries(${PROJECT_NAME}
    PRIVATE
    ${RTS_LIB_NAME}
    awgstandalone
    )

add_test(NAME ${PROJECT_NAME}
        COMMAND ${PROJECT_NAME}
        WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}")
