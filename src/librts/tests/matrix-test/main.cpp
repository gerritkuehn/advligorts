#include <cfenv>
#include <cmath>

#include "Model.hh"

#include <iostream>
#include <cmath>

#include "Catch_Multiversion.hpp"


#define TEST_SUBSYSTEM "MATRIX_TEST_"


TEST_CASE ("mux matrix",  "[general]" )
{
    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE( model_ptr != nullptr);


    std::feclearexcept(FE_ALL_EXCEPT);
    REQUIRE(std::fetestexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW) == 0);

    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "MUXMATRIX_IN_1", 1) );
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "MUXMATRIX_IN_2", 1) );
    model_ptr->record_model_var(TEST_SUBSYSTEM "MUXMATRIX_OUT_1", model_ptr->get_model_rate_Hz());
    model_ptr->record_model_var(TEST_SUBSYSTEM "MUXMATRIX_OUT_2", model_ptr->get_model_rate_Hz());

     REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "MUXMATRIX_1_1", 1));
     REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "MUXMATRIX_1_2", 1));
     REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "MUXMATRIX_2_1", 1));
     REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "MUXMATRIX_2_2", 1));

    model_ptr->run_model(100);

    std::optional< std::vector<double> > out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "MUXMATRIX_OUT_1");
    REQUIRE(out);
    REQUIRE(out.value().front() == Approx(2.0));
    REQUIRE(out.value().back() == Approx(2.0)); 

    out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "MUXMATRIX_OUT_2");
    REQUIRE(out);
    REQUIRE(out.value().front() == Approx(2.0));
    REQUIRE(out.value().back() == Approx(2.0));


    REQUIRE(std::fetestexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW) == 0);
}


TEST_CASE ("filter matrix",  "[general]" )
{
    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE( model_ptr != nullptr);

    std::feclearexcept(FE_ALL_EXCEPT);

    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "FILTMUXMATRIX_IN_1", 1) );
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "FILTMUXMATRIX_IN_2", 1) );
    model_ptr->record_model_var(TEST_SUBSYSTEM "FILTMUXMATRIX_OUT_1", model_ptr->get_model_rate_Hz());
    model_ptr->record_model_var(TEST_SUBSYSTEM "FILTMUXMATRIX_OUT_2", model_ptr->get_model_rate_Hz());

    std::optional<rts::LIGO_FilterModule> filter0_0 = model_ptr->get_filter_module_by_name(TEST_SUBSYSTEM "FILTMUXMATRIX_1_1");
    REQUIRE( filter0_0 );
    filter0_0.value().enable_input().enable_output().set_gain(1).set_ramp_time_s(0);

    std::optional<rts::LIGO_FilterModule> filter0_1 = model_ptr->get_filter_module_by_name(TEST_SUBSYSTEM "FILTMUXMATRIX_1_2");
    REQUIRE( filter0_1 );
    filter0_1.value().enable_input().enable_output().set_gain(1).set_ramp_time_s(0);

    std::optional<rts::LIGO_FilterModule> filter1_0 = model_ptr->get_filter_module_by_name(TEST_SUBSYSTEM "FILTMUXMATRIX_2_1");
    REQUIRE( filter1_0 );
    filter1_0.value().enable_input().enable_output().set_gain(1).set_ramp_time_s(0);

    std::optional<rts::LIGO_FilterModule> filter1_1 = model_ptr->get_filter_module_by_name(TEST_SUBSYSTEM "FILTMUXMATRIX_2_2");
    REQUIRE( filter1_1 );
    filter1_1.value().enable_input().enable_output().set_gain(1).set_ramp_time_s(0);
    
    model_ptr->run_model(100);
    std::optional< std::vector<double> > out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "FILTMUXMATRIX_OUT_1");
    REQUIRE(out);
    REQUIRE(out.value().front() == Approx(2.0));
    REQUIRE(out.value().back() == Approx(2.0));

    out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "FILTMUXMATRIX_OUT_2");
    REQUIRE(out);
    REQUIRE(out.value().front() == Approx(2.0));
    REQUIRE(out.value().back() == Approx(2.0));




    REQUIRE(std::fetestexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW) == 0);

}


TEST_CASE( "ramp mux matrix tests", "[general]" )
{
    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE( model_ptr != nullptr);

    std::feclearexcept(FE_ALL_EXCEPT);

    model_ptr->run_model( model_ptr->get_model_rate_Hz() * 1);

    REQUIRE(std::fetestexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW) == 0);


    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX_IN_1", 10) );
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX_IN_2", 5) );

    //REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX_TRAMP", 1));
    //REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX_LOAD_MATRIX", 1));

    model_ptr->record_model_var(TEST_SUBSYSTEM "RAMPMUXMATRIX_OUT_1", model_ptr->get_model_rate_Hz());
    model_ptr->record_model_var(TEST_SUBSYSTEM "RAMPMUXMATRIX_OUT_2", model_ptr->get_model_rate_Hz());

    model_ptr->run_model( model_ptr->get_model_rate_Hz() * 1);

    //
    std::optional< std::vector<double> > out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "RAMPMUXMATRIX_OUT_1");
    REQUIRE(out);
    REQUIRE(out.value().back() == 0.0); //Expect 0 because we have not loaded mult matrix

    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX_SETTING_1_1", 1));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX_SETTING_1_2", 1));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX_SETTING_2_1", 1));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX_SETTING_2_2", 1));

    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX_TRAMP", 5));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX_LOAD_MATRIX", 1));
    model_ptr->run_model( model_ptr->get_model_rate_Hz() * 1);

    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX_TRAMP", 0.0));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX_LOAD_MATRIX", 1));
    model_ptr->run_model( model_ptr->get_model_rate_Hz() * 1);

    out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "RAMPMUXMATRIX_OUT_1");
    REQUIRE(out);
    REQUIRE(out.value().back() != 0.0);


    REQUIRE(std::fetestexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW) == 0);

}


TEST_CASE( "ramp mux matrix 1 tests", "[general]" )
{
    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    REQUIRE( model_ptr != nullptr);

    std::feclearexcept(FE_ALL_EXCEPT);

    model_ptr->run_model( model_ptr->get_model_rate_Hz() * 1);

    REQUIRE(std::fetestexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW) == 0);



    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_IN_1", 10) );
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_IN_2", 5) );
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_IN_3", 1) );


    model_ptr->record_model_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_OUT_1", model_ptr->get_model_rate_Hz());
    model_ptr->record_model_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_OUT_2", model_ptr->get_model_rate_Hz());

    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_1_1", 1));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_1_2", 1));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_1_3", 1));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_2_1", 1));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_2_2", 1));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_2_3", 1));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_3_1", 1));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_3_2", 1));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_3_3", 1));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_4_1", 1));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_4_2", 1));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_4_3", 1));

    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_TRAMP", 1));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_LOAD_MATRIX", 1));


    model_ptr->run_model( model_ptr->get_model_rate_Hz() * 1);

    //
    std::optional< std::vector<double> > out = model_ptr->get_recorded_var<double>(TEST_SUBSYSTEM "RAMPMUXMATRIX1_OUT_1");
    REQUIRE(out);
    REQUIRE(out.value().back() != 0.0);


    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_TRAMP", 5));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_LOAD_MATRIX", 1));
    model_ptr->run_model( model_ptr->get_model_rate_Hz() * 10);

    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_1_1", 1000));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_1_2", 1000));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_1_3", 1000));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_2_2", 1000));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_2_3", 1000));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_3_1", 1000));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_3_2", 1000));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_3_3", 1000));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_4_1", 1000));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_4_2", 1000));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_4_3", 1000));


    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_TRAMP", 0.0));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_LOAD_MATRIX", 1));
    model_ptr->run_model( model_ptr->get_model_rate_Hz() * 1);


    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_1_1", 1));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_1_2", 1));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_1_3", 1));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_2_2", 1));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_2_3", 1));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_3_1", 1));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_3_2", 1));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_3_3", 1));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_4_1", 1));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_4_2", 1));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_SETTING_4_3", 1));



    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_TRAMP", 5));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_LOAD_MATRIX", 1));
    model_ptr->run_model( model_ptr->get_model_rate_Hz() * 1);


    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_TRAMP", 0));
    REQUIRE( model_ptr->set_var(TEST_SUBSYSTEM "RAMPMUXMATRIX1_LOAD_MATRIX", 1));

    model_ptr->run_model( model_ptr->get_model_rate_Hz() * 6);



    REQUIRE(std::fetestexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW) == 0);

}




