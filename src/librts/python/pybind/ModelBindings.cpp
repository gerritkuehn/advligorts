#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/numpy.h>

#include "Model.hh"
#include "LIGO_FilterModule.hh"

//Generators
#include "CannedSignalGenerator.hh"
#include "ConstantGenerator.hh"
#include "GaussianNoiseGenerator.hh"
#include "RampGenerator.hh"
#include "UniformRealGenerator.hh"

//AWG
#include "awgstandalone.h"

namespace py = pybind11;

using namespace rts;

#ifdef PYBIND11_OVERRIDE_PURE
// 
// This is a required trampoline class that allows us to define
// Generator classes in python and then have the underlying c++
// runtime use them 
// 
class PyGenerator : public rts::Generator {
    public:
        /* Inherit the constructors */
        using Generator::Generator;

        /* Trampoline (need one for each virtual function) */
        double get_next_sample() override {
            PYBIND11_OVERRIDE_PURE(
                double, /* Return type */
                rts::Generator,      /* Parent class */
                get_next_sample,          /* Name of function in C++ (must match Python name) */
            );
        }

        void reset() override {
            PYBIND11_OVERRIDE_PURE(
                void, /* Return type */
                rts::Generator,      /* Parent class */
                reset,          /* Name of function in C++ (must match Python name) */
            );
        }
};
#endif


PYBIND11_MODULE(PYBIND_MODULE_NAME, m) { //PYBIND_MODULE_NAME is defined by the build scripts

    m.def("create_instance", &rts::Model::create_instance, "Constructs the instance of the model", py::arg("log_level") = 1);

    py::class_<rts::Model>(m, "Model", py::module_local())
        .def("create_instance", &rts::Model::create_instance)
        .def("get_model_rate_Hz", &rts::Model::get_model_rate_Hz)
        .def("set_log_level", &rts::Model::set_log_level)
        .def("get_cycle_num", &rts::Model::get_cycle_num)
        .def("get_gps_time", &rts::Model::get_gps_time)
        .def("set_gps_time", &rts::Model::set_gps_time)
        .def("get_num_filter_modules", &rts::Model::get_num_filter_modules)
        .def("get_all_input_names", &rts::Model::get_all_input_names)
        .def("get_all_model_var_names", &rts::Model::get_all_model_var_names)
        .def("set_var", static_cast<bool (rts::Model::*)(const std::string &, double)>(&rts::Model::set_var) )
        .def("set_var", static_cast<bool (rts::Model::*)(const std::string &, float)>(&rts::Model::set_var) )
        .def("set_var", static_cast<bool (rts::Model::*)(const std::string &, int)>(&rts::Model::set_var) )
        .def("set_var_momentary", &rts::Model::set_var_momentary)
        .def("get_var", &rts::Model::get_var<double>) //Default get_var() is double
        .def("get_var_int", &rts::Model::get_var<int>)
        .def("get_var_double", &rts::Model::get_var<double>)
        .def("get_var_float", &rts::Model::get_var<float>)
        .def("get_var_byte", 
            [](const rts::Model &s, const std::string key) {
            std::optional<char> hold = s.get_var<char>(key);
            if ( !hold ) throw py::key_error();
            return py::bytes(&hold.value(), 1); 
        })
        .def("record_model_var", &rts::Model::record_model_var)
        .def("stop_recording_model_var", &rts::Model::stop_recording_model_var)
        .def("get_recorded_var", &rts::Model::get_recorded_var<double>)
        .def("get_recorded_var_double", &rts::Model::get_recorded_var<double>)
        .def("get_recorded_var_float", &rts::Model::get_recorded_var<float>)
        .def("get_recorded_var_int", &rts::Model::get_recorded_var<int>)
        .def("get_recorded_var_bytes", 
            [](const rts::Model &s, const std::string key) {
            std::optional<char> hold = s.get_var<char>(key);
            if ( !hold ) throw py::key_error();
            return py::bytes(&hold.value(), 1); 
        })

        .def("multicycle_record_control", &rts::Model::multicycle_record_control)
        .def("load_snap_file", &rts::Model::load_snap_file)
        .def("get_all_filter_names", &rts::Model::get_all_filter_names)
        .def("get_filter_module_by_name", &rts::Model::get_filter_module_by_name)
        .def("get_filter_module_by_id", &rts::Model::get_filter_module_by_id)
        .def("reset_filter_module", &rts::Model::reset_filter_module)
        .def("read_biquad_coefficients", &rts::Model::read_biquad_coefficients)
        .def("load_biquad_coefficients", &rts::Model::load_biquad_coefficients)
        .def("load_scipy_sos_coef", &rts::Model::load_scipy_sos_coef)
        .def("load_from_filter_file", &rts::Model::load_from_filter_file)
        .def("get_filter_switch_type", &rts::Model::get_filter_switch_type)
        .def("set_filter_switch_type", &rts::Model::set_filter_switch_type)

        .def("set_adc_channel_generator", &rts::Model::set_adc_channel_generator)
        .def("set_excitation_point_generator", &rts::Model::set_excitation_point_generator)
        .def("get_awg_slot", &rts::Model::get_awg_slot)
        .def("free_awg_slot", &rts::Model::free_awg_slot)
        .def("set_ipc_receiver_generator", &rts::Model::set_ipc_receiver_generator)
        .def("record_dac_output", &rts::Model::record_dac_output)
        .def("get_dac_output_by_id", &rts::Model::get_dac_output_by_id, py::arg("card"), py::arg("chan"))
        .def("run_model", &rts::Model::run_model)
        .def("__getitem__",
             [](const rts::Model &s, const std::string name) {
                 std::optional<double> res = s.get_var<double>(name);
                 if( ! res ) throw py::key_error();
                 return res.value();
             })
        .def("__setitem__",
             [](rts::Model &s, const std::string name, double value) {
                 if ( !s.set_var(name, value) ) throw py::key_error();
             })
        ;



    py::class_<rts::LIGO_FilterModule>(m, "LIGO_FilterModule", py::module_local())
        .def("get_index", &rts::LIGO_FilterModule::get_index)
        .def("isFIR", &rts::LIGO_FilterModule::isFIR)
        .def("enable_input", &rts::LIGO_FilterModule::enable_input)
        .def("disable_input", &rts::LIGO_FilterModule::disable_input)
        .def("enable_output", &rts::LIGO_FilterModule::enable_output)
        .def("disable_output", &rts::LIGO_FilterModule::disable_output)
        .def("enable_offset", &rts::LIGO_FilterModule::enable_offset)
        .def("disable_offset", &rts::LIGO_FilterModule::disable_offset)
        .def("enable_limiter", &rts::LIGO_FilterModule::enable_limiter)
        .def("disable_limiter", &rts::LIGO_FilterModule::disable_limiter)
        .def("enable_hold", &rts::LIGO_FilterModule::enable_hold)
        .def("disable_hold", &rts::LIGO_FilterModule::disable_hold)
        .def("set_gain", &rts::LIGO_FilterModule::set_gain)
        .def("set_ramp_time_s", &rts::LIGO_FilterModule::set_ramp_time_s)
        .def("get_opSwitchE", &rts::LIGO_FilterModule::get_opSwitchE)
        .def("set_opSwitchE", &rts::LIGO_FilterModule::set_opSwitchE)
        .def("get_section_ramp", &rts::LIGO_FilterModule::get_section_ramp)
        .def("set_section_ramp", &rts::LIGO_FilterModule::set_section_ramp)
        .def("get_section_timeout", &rts::LIGO_FilterModule::get_section_timeout)
        .def("set_section_timeout", &rts::LIGO_FilterModule::set_section_timeout)
        .def("enable_stages", &rts::LIGO_FilterModule::enable_stages)
        .def("disable_stages", &rts::LIGO_FilterModule::disable_stages)
        ;

#ifdef PYBIND11_OVERRIDE_PURE
    py::class_<rts::Generator, PyGenerator, std::shared_ptr<Generator> >(m, "Generator", py::module_local())
    .def(py::init<>())
    .def("get_next_sample", &rts::Generator::get_next_sample)
    .def("reset", &rts::Generator::reset);
#endif

    py::class_<rts::CannedSignalGenerator, rts::Generator, std::shared_ptr<rts::CannedSignalGenerator> >(m, "CannedSignalGenerator", py::module_local())
        .def(py::init<const std::vector<double>&, bool, uint64_t, double, bool>(), "constructor", py::arg("canned_data"), py::arg("repeat") = true, py::arg("wait_cycles") = 0, py::arg("gain") = 1.0, py::arg("hold_zero") = false)
        .def("get_next_sample", &rts::CannedSignalGenerator::get_next_sample)
        .def("reset", &rts::CannedSignalGenerator::reset)
        .def("set_gain", &rts::CannedSignalGenerator::set_gain)
        .def("append_data", &rts::CannedSignalGenerator::append_data);

    py::class_<rts::ConstantGenerator, rts::Generator, std::shared_ptr<rts::ConstantGenerator> >(m, "ConstantGenerator", py::module_local())
        .def(py::init<double>(), "constructor", py::arg("value"))
        .def("get_next_sample", &rts::ConstantGenerator::get_next_sample)
        .def("reset", &rts::ConstantGenerator::reset);

    py::class_<rts::GaussianNoiseGenerator, rts::Generator, std::shared_ptr<rts::GaussianNoiseGenerator> >(m, "GaussianNoiseGenerator", py::module_local())
        .def(py::init<double, double>(), "constructor", py::arg("mean"), py::arg("stdDev"))
        .def("get_next_sample", &rts::GaussianNoiseGenerator::get_next_sample)
        .def("reset", &rts::GaussianNoiseGenerator::reset);

    py::class_<rts::RampGenerator, rts::Generator, std::shared_ptr<rts::RampGenerator> >(m, "RampGenerator", py::module_local())
        .def(py::init<double, double>(), "constructor", py::arg("startVal"), py::arg("step"))
        .def("get_next_sample", &rts::RampGenerator::get_next_sample)
        .def("reset", &rts::RampGenerator::reset);

    py::class_<rts::UniformRealGenerator, rts::Generator, std::shared_ptr<rts::UniformRealGenerator> >(m, "UniformRealGenerator", py::module_local())
        .def(py::init<double, double>(), "constructor", py::arg("startVal"), py::arg("step"))
        .def("get_next_sample", &rts::UniformRealGenerator::get_next_sample)
        .def("reset", &rts::UniformRealGenerator::reset);


    py::enum_<AWG_WaveType>(m, "AWG_WaveType", py::module_local())
        .value("awgNone", awgNone)
        .value("awgSine", awgSine)
        .value("awgSquare", awgSquare)
        .value("awgRamp", awgRamp)
        .value("awgTriangle", awgTriangle)
        .value("awgImpulse", awgImpulse)
        .value("awgConst", awgConst)
        .value("awgNoiseN", awgNoiseN)
        .value("awgNoiseU", awgNoiseU)
        .value("awgArb", awgArb)
        .value("awgStream", awgStream)
        .value("awgWave11", awgWave11)
        .export_values();

    py::class_<AWG_Component>(m, "AWG_Component", py::module_local())
        .def(pybind11::init<>())
        .def_readwrite("wtype", &AWG_Component::wtype)
        .def_property("par", [](AWG_Component &c) -> pybind11::array {
                auto dtype = pybind11::dtype(pybind11::format_descriptor<double>::format());
                auto base = pybind11::array(dtype, {4}, {sizeof(double)});
                return pybind11::array(dtype, {4}, {sizeof(double)}, c.par, base);
            }, [](AWG_Component& c) {})
        .def_readwrite("start", &AWG_Component::start)
        .def_readwrite("duration", &AWG_Component::duration)
        .def_readwrite("restart", &AWG_Component::restart)
        .def_readwrite("ramptype", &AWG_Component::ramptype)
        .def_property("ramptime", [](AWG_Component &c) -> pybind11::array {
                auto dtype = pybind11::dtype(pybind11::format_descriptor<unsigned long long>::format());
                auto base = pybind11::array(dtype, {2}, {sizeof(unsigned long long)});
                return pybind11::array(dtype, {2}, {sizeof(unsigned long long)}, c.ramptime, base);
            }, [](AWG_Component& c) {})
        .def_property("ramppar", [](AWG_Component &c) -> pybind11::array {
                auto dtype = pybind11::dtype(pybind11::format_descriptor<double>::format());
                auto base = pybind11::array(dtype, {4}, {sizeof(double)});
                return pybind11::array(dtype, {4}, {sizeof(double)}, c.ramppar, base);
            }, [](AWG_Component& c) {});

    m.def("addWaveformAWG", [](int slot, py::list l) -> int {
            auto v = l.cast<std::vector<AWG_Component>>();
            return addWaveformAWG(slot, v.data(), l.size());
        }, "Adds waveforms to an AWG", py::arg("slot"), py::arg("comp"));

    m.def("stopWaveformAWG", &stopWaveformAWG, "Stops a waveform in an AWG", py::arg("slot"), py::arg("terminate"), py::arg("arg"));

    m.def("resetAWG", &resetAWG, "Flushes local buffer and deconfigures AWG", py::arg("slot"));

    m.def("setGainAWG", &setGainAWG, "Set overall gain of an AWG", py::arg("slot"), py::arg("gain"), py::arg("ramptime"));

    m.def("setFilterAWG", [](int slot, py::list l) -> int {
            auto v = l.cast<std::vector<double>>();
            return setFilterAWG(slot, v.data(), l.size());
        }, "Set filter of an AWG", py::arg("slot"), py::arg("coeff"));

}

