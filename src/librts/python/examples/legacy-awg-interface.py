#!/usr/bin/env python3
import x1unittest_pybind as model #Built by rtslib
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as sig


mod = model.create_instance()

slot = mod.get_awg_slot("TEST_EXCITATION")
c = model.AWG_Component()
c.wtype = c.wtype.awgSine
c.start = (mod.get_gps_time()+1)*int(1e9)
c.duration = -1
c.restart = -1
c.par[0] = 1
c.par[1] = 1.1
model.addWaveformAWG(slot, [c,])
model.addWaveformAWG(slot, [c,]) #Add second sin(), apm will be 2


slot2 = mod.get_awg_slot("TEST_EXCITATION_B")
c2 = model.AWG_Component()
c2.wtype = c.wtype.awgRamp
c2.start = (mod.get_gps_time()+1)*int(1e9)
c2.duration = -1
c2.restart = -1
c2.par[0] = 1
c2.par[1] = 1.1
model.addWaveformAWG(slot2, [c2,])




# Set up variable recording
mod.record_model_var("EXCITATION_EPICS_OUT", mod.get_model_rate_Hz()) 
mod.record_model_var("EXCITATION_EPICS_OUT_B", mod.get_model_rate_Hz())


mod.run_model( int(mod.get_model_rate_Hz() * 3) ) 

# Trend DAC card 0 chan 0 over the simulation
var_output = mod.get_recorded_var("EXCITATION_EPICS_OUT")
var_b_output = mod.get_recorded_var("EXCITATION_EPICS_OUT_B")


p1 = plt.figure(1)
plt.plot(var_output)
plt.title("Legacy AWG Interface EXCITATION_EPICS_OUT")

p2 = plt.figure(2)
plt.plot(var_b_output)
plt.title("Legacy AWG Interface EXCITATION_EPICS_OUT_B")




plt.show()

