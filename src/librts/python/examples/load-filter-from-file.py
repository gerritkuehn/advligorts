#!/usr/bin/env python3
import sys
import x1firtst_pybind as model #Built by rtslib
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as sig


if len(sys.argv) < 2:
    print("Error: Missing arguments, must pass the IIR filter pathname, and (optionally) the FIR filter pathname.")
    print(f"Usage: {sys.argv[0]} <IIR FILTER PATH (.txt)> [FIR FILTER PATH (.fir)]")
    exit(1)


mod = model.create_instance()

if len(sys.argv) > 2:
    fir_pathname = sys.argv[2]
else:
    fir_pathname = ""

mod.load_from_filter_file(sys.argv[1], fir_pathname)

filt1 = mod.get_filter_module_by_name("TAGSYSTEM_SENSCOR_X_FIR").enable_input().enable_output().set_gain(1).set_ramp_time_s(0.0).enable_stages( [1, 2] )

print(f"isFIR(): {filt1.isFIR(1)}")

output_var = "FIR_FM0_OUT" 

# Recored model vars
mod.record_model_var(output_var, mod.get_model_rate_Hz()) 


mod.run_model( mod.get_model_rate_Hz() ) 

#exit(0)

fm0_output = mod.get_recorded_var(output_var);
p1 = plt.figure(0)
plt.plot(fm0_output)
plt.title(output_var)

p2 =  plt.figure(2)
plt.plot(10*np.log(np.abs(np.fft.rfft(np.divide(fm0_output,len(fm0_output))))))
plt.title("Mag of Real Side FFT")


plt.show()

