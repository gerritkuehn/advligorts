#!/usr/bin/env python3
import x1unittest_pybind as model #Built by rtslib
import matplotlib.pyplot as plt
import numpy as np

TEST_SUBSYSTEM = "MATRIX_TEST_"

mod = model.create_instance( 1 )


mod.set_var(TEST_SUBSYSTEM + "RAMPMUXMATRIX1_IN_1", 10)
mod.set_var(TEST_SUBSYSTEM + "RAMPMUXMATRIX1_IN_2", 20)
mod.set_var(TEST_SUBSYSTEM + "RAMPMUXMATRIX1_IN_3", 30)


mod.set_var(TEST_SUBSYSTEM + "RAMPMUXMATRIX1_SETTING_1_1", 1)
mod.set_var(TEST_SUBSYSTEM + "RAMPMUXMATRIX1_SETTING_1_2", 2)
mod.set_var(TEST_SUBSYSTEM + "RAMPMUXMATRIX1_SETTING_1_3", 3)
mod.set_var(TEST_SUBSYSTEM + "RAMPMUXMATRIX1_SETTING_2_1", 4)
mod.set_var(TEST_SUBSYSTEM + "RAMPMUXMATRIX1_SETTING_2_2", 5)
mod.set_var(TEST_SUBSYSTEM + "RAMPMUXMATRIX1_SETTING_2_3", 6)
mod.set_var(TEST_SUBSYSTEM + "RAMPMUXMATRIX1_SETTING_3_1", 7)
mod.set_var(TEST_SUBSYSTEM + "RAMPMUXMATRIX1_SETTING_3_2", 8)
mod.set_var(TEST_SUBSYSTEM + "RAMPMUXMATRIX1_SETTING_3_3", 9)
mod.set_var(TEST_SUBSYSTEM + "RAMPMUXMATRIX1_SETTING_4_1", 10)
mod.set_var(TEST_SUBSYSTEM + "RAMPMUXMATRIX1_SETTING_4_2", 11)
mod.set_var(TEST_SUBSYSTEM + "RAMPMUXMATRIX1_SETTING_4_3", 12)
mod.set_var(TEST_SUBSYSTEM + "RAMPMUXMATRIX1_LOAD_MATRIX", 1) #load above
mod.set_var(TEST_SUBSYSTEM + "RAMPMUXMATRIX1_TRAMP", 5)

mod.record_model_var(TEST_SUBSYSTEM + "RAMPMUXMATRIX1_OUT_1", mod.get_model_rate_Hz())
mod.record_model_var(TEST_SUBSYSTEM + "RAMPMUXMATRIX1_OUT_2", mod.get_model_rate_Hz())
mod.record_model_var(TEST_SUBSYSTEM + "RAMPMUXMATRIX1_OUT_3", mod.get_model_rate_Hz())
mod.record_model_var(TEST_SUBSYSTEM + "RAMPMUXMATRIX1_OUT_4", mod.get_model_rate_Hz())


mod.run_model( mod.get_model_rate_Hz() * 3)

out_1 = mod.get_recorded_var_double(TEST_SUBSYSTEM + "RAMPMUXMATRIX1_OUT_1");
out_2 = mod.get_recorded_var_double(TEST_SUBSYSTEM + "RAMPMUXMATRIX1_OUT_2");
out_3 = mod.get_recorded_var_double(TEST_SUBSYSTEM + "RAMPMUXMATRIX1_OUT_3");
out_4 = mod.get_recorded_var_double(TEST_SUBSYSTEM + "RAMPMUXMATRIX1_OUT_4");

p1 = plt.figure(1)
plt.plot(out_1)
plt.plot(out_2)
plt.plot(out_3)
plt.plot(out_4)
plt.title("MATRIX OUTPUTS")

plt.show()

