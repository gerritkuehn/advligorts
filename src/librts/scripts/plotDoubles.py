#!/bin/python3
import sys
import numpy as np
import matplotlib.pyplot as plt


def testFunc(pathname):
    data = np.fromfile(pathname, dtype='double')

    p1 = plt.figure(1)
    #plt.plot(data)
    plt.plot(data,linestyle="",marker="o")
    plt.title("TD Samples")

    p2 =  plt.figure(2)
    plt.plot(10*np.log(np.abs(np.fft.rfft(data))))
    plt.title("Mag of Real Side FFT")
    plt.show()


if __name__ == "__main__":
    testFunc(sys.argv[1])

