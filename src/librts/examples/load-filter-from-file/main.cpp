#include "Model.hh"
#include "LIGO_FilterModule.hh"

#include "RampGenerator.hh"

#include <iostream>
#include <fstream>

/**
 * There are example .txt and .fir files stored in this repository.
 * <path>/librts/examples/data/
 *
 * Those filter files can be loaded when the x1firtst model has 
 * been built. The get_model_name() check below is trying to make
 * sure you run this with the correct model. 
 *
 */

int main(int argc, char** argv)
{

    if (argc < 2)
    {
        std::cout << "Error: Missing arguments, must pass the IIR filter pathname, and (optionally) the FIR filter pathname.\n";
        std::cout << "Usage: " << argv[0] << " <IIR FILTER PATH (.txt)> [FIR FILTER PATH (.fir)]\n";
        return 1;
    }

    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance( 0 );
    if(model_ptr == nullptr)
    {
        std::cout << "Error could not create Model. Exiting..."  << std::endl;
        return 2;
    }

    /* This model only works with a FIR (x1firtst.mdl) test model, 
     * because we don't have FIR filters in the unit test yet.
     */
    if (model_ptr->get_model_name() != "x1firtst")
    {
        std::cout << "Error: Model name is " << model_ptr->get_model_name() << " but x1firtst was expected." << std::endl;
        return 2;
    }

    std::string iir_pathname(argv[1]);
    std::string fir_pathname("");
    if(argv[2] != nullptr) fir_pathname = std::string(argv[2]);
    model_ptr->load_from_filter_file(iir_pathname, fir_pathname);

    std::cout << "FM0, stage 1 : ";
    for ( const auto & val : model_ptr->read_biquad_coefficients("FM0", 1) ) { 
        std::cout << val << " ";
    }
    std::cout << "\n";
    if ( model_ptr->read_biquad_coefficients("FM0", 1).size() == 4)
    {
        std::cout << "Pass : Have expected 0 coeffs.\n";
    } 
    else {
        std::cout << "Fail: has " << model_ptr->read_biquad_coefficients("FM0", 1).size() << " coeffs\n";
    }

    std::cout << "TAGSYSTEM_SENSCOR_X_FIR, stage 1 : ";
    for ( const auto & val : model_ptr->read_fir_coefficients("TAGSYSTEM_SENSCOR_X_FIR", 1) )
    {
        std::cout << val << " ";
    }
    std::cout << "\n";

    std::cout << "TAGSYSTEM_SENSCOR_X_FIR, stage 2 : ";
    for ( const auto & val : model_ptr->read_fir_coefficients("TAGSYSTEM_SENSCOR_X_FIR", 2) )
    {
        std::cout << val << " ";
    }
    std::cout << "\n";

    std::cout << "TAGSYSTEM_SENSCOR_Y_FIR, stage 1 : ";
    for ( const auto & val : model_ptr->read_fir_coefficients("TAGSYSTEM_SENSCOR_Y_FIR", 1) )
    {
        std::cout << val << " ";
    }
    std::cout << "\n";




    /*
    for (const auto & name : model_ptr->get_all_filter_names())
    {
        rts::LIGO_FilterModule filt =  model_ptr->get_filter_module_by_name( name ).value();
        std::cout << "Mod: " << name  << ", isFIR() : " <<  filt.isFIR() << std::endl;
    }*/

    return 0;
}
