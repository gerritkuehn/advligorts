#include "Model.hh"
#include "LIGO_FilterModule.hh"

#include "SnapFile.hh"

#include <iostream>
#include <fstream>

/**
 *
 */

int main(int argc, char** argv)
{

  
    
    if (argc < 2)
    {
        std::cout << "Error: Missing arguments, must pass the path to the snap file you would like to load.\n";
        std::cout << "Usage: " << argv[0] << " <SNAP FILE PATHNAME (.snap)> \n";
        return 1;
    }

    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance( 0 );
    if(model_ptr == nullptr)
    {
        std::cout << "Error could not create Model. Exiting..."  << std::endl;
        return 2;
    }


    int num_loaded = model_ptr->load_snap_file(argv[1]);
    std::cout << "Loaded " << num_loaded << " variables from the " << argv[1] << " file." << std::endl;


    return 0;
}
