# Lib RTS
The RTS library allows users to build advLIGO *models*, ie. `.mdl` files created with Simulink/cdsParts library, simulate inputs and captures outputs. It provides a c++ interface, as well as python bindings for use in either language. 

## Dependencies

### Debian

#### Required
The required packages that should be installed from your distribution's repositories are:
```
build-essential cmake perl libspdlog-dev catch2 python3-dev git python3-pybind11
```
On Debian 10 you will only have `catch` instead of `catch2`

#### Others
Some python scrips have ***python dependencies*** that you will need to run them
```
numpy, matplotlib 
```

### Mac

#### Required
```
cmake spdlog pybind11 numpy scipy matplotlib
```

If you want to run the unit tests you will need:
```
catch2
```


## Directory Structure

| Directory | Uses |
| ---------------------- | ----------------------------------------- |
| `docker/`     | Holds docker scripts showing how this repository might be cloned, built and run on a system |
| `env/`        | Contains config scripts holding environmental variables sourced by the build scripts |
| `examples`    | C++ examples on how to use the `librts` interface |
| `include/`    | `librts` header files |
| `models/`     | Directory that is included in the search path for models (`.mdl` files) |
| `python/`     | python pybind11 binding files and python example scripts on using the python interface |
| `scripts/`    | Utility scripts for plotting c++ output and finding model search paths |
| `src/`        | `librts` source files |
| `tests/`      | C++ tests ran against `librts` |

# Building Models
There are ***two*** ways to build models with librts. One (in-tree) is suited when you only want to clone this repository and not install LIGO packages. The other(rtcds command) is useful on systems where you have the LIGO ***cds-workstation*** or ***advligort-cymac*** packages installed. This is common on test stands and production systems. Use of the rtcds command for building is easier for production models as it sets up the required environment variables. 

## Building In-Tree

## Building Custom Models
Simple custom models are very simple to build. The general steps are to make sure the `Makefile` in this directory will be able to find your `.mdl` file. The simplest way is to just copy it into the `models/` directory, ex: `cp x1testmodel.mdl /home/$USER/git/advligorts/src/librts/models/` and then run `make <model_name>` from this directory. ex: `cd  /home/$USER/git/advligorts/src/librts/; make x1testmodel`.

## Building Production Models
In the `Makefile` uncomment the `include $(repo_dir)/env/USERAPPS_ENV_VARS.mk` line. 
In the `./env/USERAPPS_ENV_VARS.mk` file, update the `USERAPPS_BASE` at the top of the file to point to where you have the userapps checkout. If you have userapps installed at the standard location or a symlink in place, the default path should work.

After those changes you can build a production model:
```sh
make h1calcs
```

### Production Complications 
Some production models have hard coded paths to `/opt/rtcds/userapps/release/` so you are going to need to setup a symlink from that path to your userapps or install userapps to that standard location.

## Build Options
There are several build options that can be used to change how the models are build, and can also help the build work on more OS/architectures. Options are passed to the build by adding them to the `LIBRTS_CMAKE_FLAGS` environment variable.  Below are those options and what they do.

| Option                        | Default     | Function | 
| ----------------------------- | ----------- | -------- |
| `MODEL_NO_EXECUTABLE`         | False | Disables the build of the model's userspace application. (Not used by librts) |
| `USE_STDLIB_MATH`             | False |This option will cause c standard library math functions to replace the LIGO implemented (x86 Only) functions. |
| `FORCE_STATIC_INLINE`         | True  | Sets `LIGO_INLINE` in the model to `static inline`, for setter build compatibility. |
| `LIBRTS_NO_SHMEM`             | False | This option replaces the shared memory buffers with local memory buffers. |
| `LIBRTS_NO_EXAMPLES`          | False | When set, don't build librts c++ example applications |
| `LIBRTS_NO_UNIT_TESTS`        | False | When set, don't build librts c++ unit tests. (These depend on catch/catch2) |
| `LIBRTS_BINDING_ONLY`         | False | Only build the library and python bindings. This just sets `MODEL_NO_EXECUTABLE=1`, `LIBRTS_NO_EXAMPLES=1` and `LIBRTS_NO_UNIT_TESTS=1` |
 
### Example of using the Build Options
```
LIBRTS_NO_SHMEM=1 USE_STDLIB_MATH=1 LIBRTS_BINDING_ONLY=1 make x1unittest
```
Note: these options are sticky, so successive builds will have these parameters set until you do a `make clean`. 

## Building with the `rtcds` command
If the rtcds command can find the model for normal builds, all you need to do is pass the `--librts` option to the build command.

```sh
rtcds build h1calcs --librts --no-kernel-space
```
Above we pass `--no-kernel-space` so we don't build the kernel module (not required)


## Running Python Examples
All you should need to do is build the model, set your `PYTHONPATH`, and run the script. Ex (from the repo root dir):

### In-Tree
```
~/git/advligorts/src/librts$ make x1unittest
~/git/advligorts/src/librts$ source ./scripts/bash_setDefaultPythonPath.sh #If you are on zsh source zsh_setDefaultPythonPath.zsh
~/git/advligorts/src/librts$ ./python/examples/load-module-example.py
```

### `rtcds` Command
After building wit the `--librts` option to `rtcds build` the python bindings will be located at `$RCG_BUILDD/models/librts/python/pybind/`. You can view your `RCG_BUILDD` env var with `rtcds env`.

```sh
$ rtcds env
...
RCG_BUILDD=/var/cache/advligorts/rcg-snap-fix
...

$ export PYTHON_PATH=/var/cache/advligorts/rcg-snap-fix/models/librts/python/pybind/      #So you "import <MODELNAME>_pybind as model" works
$ ./load-module-example.py                                                                #Run your python script

```

## Parts Notes
### IPCs
IPCs dont work the same way with `librts`, because external sender/receiver models are not running. Some special considerations should be made. 
| IPC Setup     | Status | Notes |
| ----------- | ----------- | ------------ |
| Receiver only   | SUPPORTED      | Because there is no "sender" model running, the output of these can be configured with a Generator, the "rate" of these IPCs will be at the model rate as we don't have sender information. |
| Sender only   | SUPPORTED       | Sender IPCs that would normally be sent to another model can be collected by recording and trending the IPC like any other recordable variable |

## Limitations 

### Debian 10

Debian 10 packages have older versions of spdlog and pybind, making some functionality limited.
- You can't set librts log level through the envirnment.
- Generators only work when implimented in c++, so you can't use python implimented generators as shown [here](https://git.ligo.org/cds/software/advligorts/-/blob/master/src/librts/python/examples/generators-example.py)
    - NOTE: You can still use the default generators (or impliment new ones in c++, just not python)

## Generator Objects
Many parts (excitations, ADCs, IPC receivers, etc.) can have their outputs simulated by a `Generator` object. The C++ library supports a number of generators, and more can be easily added. Checkout `include/RampGenerator.hh` for an example of a C++ Generator, and `python/examples/generators-example.py` for usage in python, as well as how you would define a custom Generator in python.  

### Currently Implemented Generators
| Name     |            Function |   Prototype |
| -------  | ------------------- | ----------- |
| `CannedSignalGenerator` | The output is simulated by reading samples from the provided list. If repeat is set to false, when you run out of canned samples the generator will return 0 on successive calls. If repeat is true, the buffer is replayed from the first sample each time the end is reached. | `CannedSignalGenerator(const std::vector<double>& canned_data, bool repeat = true)` |
| `ConstantGenerator` | Feeds the constant value configured as output | `ConstantGenerator(double value)` |
| `GaussianNoiseGenerator` | Simulates Gaussian noise with the configured parameters | `GaussianNoiseGenerator(double mean, double stddev) ` |
| `RampGenerator` | Simulates a ramp output with the configured parameters | `RampGenerator(double start_val, double step)` |
| `UniformRealGenerator` | Generates real numbers in the range [a, b) | `UniformRealGenerator(double min, double max)` |


## Debugging 
You can turn on more verbose logging by changing the spdlog level (spdlog version > 1.4.0:
```
export SPDLOG_LEVEL=info
(run your app)
``` 
