#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

use std::ffi;
use std::os::fd::{AsFd, AsRawFd};
use std::os::raw::c_int;

#[derive(Debug)]
pub struct ModelRateInfo {
    pub dcuid: u32,
    pub rate: u32,
}

impl std::cmp::PartialEq for ModelRateInfo {
    fn eq(&self, other: &Self) -> bool {
        self.dcuid == other.dcuid && self.rate == other.rate
    }
}

impl std::cmp::Eq for ModelRateInfo {}

pub fn lookup_model_rate_and_dcuid(
    name: &str,
    gds_tp_dir: &Option<String>,
) -> Option<ModelRateInfo> {
    let mut rate: c_int = 0;
    let mut dcuid: c_int = 0;

    let cname = match ffi::CString::new(name) {
        Ok(s) => s,
        _ => return None,
    };
    let opt_dir;
    let mut p = std::ptr::null();
    if let Some(s) = gds_tp_dir {
        opt_dir = match ffi::CString::new(s.as_str()) {
            Ok(val) => val,
            _ => return None,
        };
        p = opt_dir.as_ptr();
    }
    unsafe {
        get_model_rate_dcuid(&mut rate, &mut dcuid, cname.as_ptr(), p);
    };
    Some(ModelRateInfo {
        dcuid: dcuid as u32,
        rate: rate as u32,
    })
}

pub fn ioctl_1<F, T>(f: &F, request: ffi::c_ulong, t: T) -> i32
where
    F: AsFd,
{
    unsafe { ioctl(f.as_fd().as_raw_fd(), request, t) as i32 }
}

pub fn get_hostname() -> Result<String, i32> {
    let name;
    let mut buffer: [i8; 1024] = [0; 1024];

    unsafe {
        let val = gethostname(buffer.as_mut_ptr(), 1024) as i32;
        if val != 0 {
            return Err(val);
        }
        buffer[1024 - 1] = 0;
        name = ffi::CStr::from_ptr(buffer.as_ptr()).to_owned();
    }
    match name.into_string() {
        Ok(s) => Ok(s),
        Err(_) => Err(-1),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
