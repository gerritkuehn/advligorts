#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

use std::ffi;
use std::io;
use std::os::raw::c_void;
use std::sync::Mutex;

pub const MbufPrefix: &str = "mbuf://";
pub const PosixPrefix: &str = "shm://";

pub struct ShMem {
    handle: *mut c_void,
}

struct vec_info {
    ptr: *mut u8,
    data: Box<Vec<u8>>,
}

static mut vec_buffers: Mutex<Vec<vec_info>> = Mutex::new(Vec::new());

impl ShMem {
    pub fn new(name: &str, size_mb: usize) -> io::Result<Self> {
        let handle;

        let cname = match ffi::CString::new(name) {
            Ok(s) => s,
            _ => return Err(io::Error::new(io::ErrorKind::InvalidData, "bad shmem name")),
        };
        unsafe {
            handle = shmem_open(cname.as_ptr(), size_mb);
            if handle == std::ptr::null_mut() {
                return Err(io::Error::new(
                    io::ErrorKind::Other,
                    "unable to open shmem segment",
                ));
            }
        };
        Ok(ShMem { handle: handle })
    }

    pub unsafe fn new_from_slice(data: &mut [u8]) -> Self {
        let mb = 1024 * 1024usize;
        if data.len() == 0 {
            panic!("new_from_slice called with empty input");
        }
        if data.len() % mb != 0 {
            panic!("new_from_slice called with data size that is not a multiple of 2**20")
        }
        let h = shmem_wrap(data.as_mut_ptr() as *mut c_void, data.len() / mb, None);
        ShMem { handle: h }
    }

    pub unsafe fn new_from_vec(mut data: Vec<u8>) -> Self {
        let mb = 1024 * 1024usize;
        let data_len = data.len();
        if data_len == 0 {
            panic!("new_from_vec called with an empty vector");
        }
        if data_len % mb != 0 {
            panic!("new_from_vec called with a data size that is not a multiple of 2**20")
        }
        data.resize(data_len, 0);
        let mut data = Box::new(data);
        let h;
        unsafe {
            let mut buffers = vec_buffers.lock().unwrap();

            let ptr = (*data).as_mut_ptr();
            buffers.push(vec_info { ptr, data });
            h = shmem_wrap(ptr as *mut c_void, data_len / mb, Some(_shmem_cleanup_vec));
        }
        ShMem { handle: h }
    }

    pub unsafe fn mapping(&self) -> *mut c_void {
        shmem_mapping(self.handle)
    }

    pub unsafe fn mapping_as<T>(&self) -> *mut T {
        shmem_mapping(self.handle) as *mut T
    }

    pub fn get_ptr_at_offset<T>(&self, offset: usize) -> Result<*mut T, String> {
        let size = self.size_mb() * 1024 * 1024 as usize;
        if offset + std::mem::size_of::<T>() > size {
            return Err("Overflow".to_string());
        }
        unsafe {
            let raw = self.mapping_as::<ffi::c_char>();
            return Ok(raw.add(offset) as *mut T);
        }
    }

    pub fn size_mb(&self) -> usize {
        unsafe { shmem_size_mb(self.handle) as usize }
    }
}

pub extern "C" fn _shmem_cleanup_vec(p: *mut c_void) {
    if p == std::ptr::null_mut() {
        return;
    }
    let p = p as *mut u8;
    unsafe {
        let mut buffers = vec_buffers.lock().unwrap();
        if let Some(index) = buffers.iter().position(|entry| entry.ptr == p) {
            buffers.swap_remove(index);
        }
    }
}

impl Drop for ShMem {
    fn drop(&mut self) {
        if self.handle == std::ptr::null_mut() {
            return;
        }
        unsafe {
            shmem_close(self.handle);
        }
        self.handle = std::ptr::null_mut();
    }
}
#[cfg(test)]
mod tests {}
