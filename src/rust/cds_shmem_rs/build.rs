use std::env;
use std::path::PathBuf;

fn main() {
    let mut shmem_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    for _i in 0..6 {
        if !shmem_path.pop() {
            panic!("cannot create the path to cds-shmem")
        }
    }
    shmem_path.push("cds-shmem");
    println!("cargo:rustc-link-search={}", shmem_path.to_str().unwrap());
    println!("cargo:rustc-link-lib=static=cds-shmem");
    //println!("cargo:rustc-link-lib=rt");
    println!("cargo:rerun-if-change=wrapper.h");

    let bindings = bindgen::Builder::default()
        .clang_args(["-I../../", "-I../../include"])
        .header("wrapper.h")
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .generate()
        .expect("Unable to generate bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings");
}
