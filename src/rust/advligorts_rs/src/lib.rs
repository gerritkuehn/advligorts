#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

use std::sync::atomic::{AtomicU32, Ordering};

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

impl rmIpcStr {
    /// Get the cycle counter from the rmIpcStr via an atomic operation.
    pub fn get_cycle(&self) -> u32 {
        unsafe {
            let p: *const AtomicU32 = std::mem::transmute::<
                *const ::std::os::raw::c_uint,
                *const AtomicU32,
            >(&(self.cycle));
            (*p).load(Ordering::SeqCst)
        }
    }

    /// Atomically xor a response value into the reqAck field
    pub fn xor_req_ack(&self, val: u32) {
        unsafe {
            let p: *const AtomicU32 = std::mem::transmute::<
                *const ::std::os::raw::c_uint,
                *const AtomicU32,
            >(&(self.reqAck));
            (*p).fetch_xor(val, Ordering::SeqCst);
        }
    }
}

impl daq_multi_cycle_header_t {
    pub fn set_cur_cycle(&mut self, val: u32) {
        unsafe {
            let p: *const AtomicU32 = std::mem::transmute::<
                *const ::std::os::raw::c_uint,
                *const AtomicU32,
            >(&(self.curCycle));
            (*p).store(val, Ordering::SeqCst);
        }
    }
    pub fn get_cur_cycle(&self) -> u32 {
        unsafe {
            let p: *const AtomicU32 = std::mem::transmute::<
                *const ::std::os::raw::c_uint,
                *const AtomicU32,
            >(&(self.curCycle));
            (*p).load(Ordering::SeqCst)
        }
    }
    pub fn set_max_cycle(&mut self, val: u32) {
        unsafe {
            let p: *const AtomicU32 = std::mem::transmute::<
                *const ::std::os::raw::c_uint,
                *const AtomicU32,
            >(&(self.maxCycle));
            (*p).store(val, Ordering::SeqCst);
        }
    }

    pub fn set_cycle_data_size(&mut self, val: u32) {
        unsafe {
            let p: *const AtomicU32 = std::mem::transmute::<
                *const ::std::os::raw::c_uint,
                *const AtomicU32,
            >(&(self.cycleDataSize));
            (*p).store(val, Ordering::SeqCst);
        }
    }

    pub fn get_cycle_data_size(&self) -> u32 {
        unsafe {
            let p: *const AtomicU32 = std::mem::transmute::<
                *const ::std::os::raw::c_uint,
                *const AtomicU32,
            >(&(self.cycleDataSize));
            (*p).load(Ordering::SeqCst)
        }
    }

    pub fn set_msg_crc(&mut self, val: u32) {
        unsafe {
            let p: *const AtomicU32 = std::mem::transmute::<
                *const ::std::os::raw::c_uint,
                *const AtomicU32,
            >(&(self.msgcrc));
            (*p).store(val, Ordering::SeqCst);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn do_test() {
        let bp = blockProp {
            status: 0,
            timeSec: 0,
            timeNSec: 0,
            run: 0,
            cycle: 0,
            crc: 0,
        };

        let r = rmIpcStr {
            cycle: 0,
            dcuId: 0,
            crc: 0,
            command: 0,
            cmdAck: 0,
            request: 0,
            reqAck: 0,
            status: 0,
            channelCount: 0,
            dataBlockSize: 0,
            bp: [
                bp.clone(),
                bp.clone(),
                bp.clone(),
                bp.clone(),
                bp.clone(),
                bp.clone(),
                bp.clone(),
                bp.clone(),
                bp.clone(),
                bp.clone(),
                bp.clone(),
                bp.clone(),
                bp.clone(),
                bp.clone(),
                bp.clone(),
                bp.clone(),
            ],
        };

        assert_eq!(r.get_cycle(), 0);
    }
}
