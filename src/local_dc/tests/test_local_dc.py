
import os
import os.path
import time
import typing

import integration

integration.Executable(name="local_dc", hints=[], description="local_dc")
integration.Executable(name="fe_stream_check", hints=["../fe_stream_test",], description="verification program")
integration.Executable(name="fe_simulated_streams", hints=["../fe_stream_test/fe_simulation",], description="data stream generator")

ini_dir = integration.state.temp_dir('ini_files')
master_file = os.path.join(ini_dir, "master")


def ini(dcuid: int) -> str:
    global ini_dir
    return os.path.join(ini_dir, "mod{0}.ini".format(dcuid))


def par(dcuid: int) -> str:
    global ini_dir
    return os.path.join(ini_dir, "tpchn_mod{0}.par".format(dcuid))


fe_simulated_streams = integration.Process("fe_simulated_streams",
                                           ["-S",
                                            "-i", ini_dir,
                                            "-M", master_file,
                                            "-k", "300",
                                            "-D", "5,6,7,250,255"])

local_dc = integration.Process("local_dc",
                               ["-m", "100",
                                "-s", "mod5 mod6 mod7 mod250 mod255",
                                "-d", ini_dir,
                                "-b", "local_dc"])

fe_stream_check = integration.Process("fe_stream_check",
                                      ["-m", "local_dc",
                                       "-s", "100",
                                       "-v",
                                       "-c", ini(5), par(5),
                                       ini(6), par(6),
                                       ini(7), par(7),
                                       ini(250), par(250),
                                       ini(255), par(255)])


def check_ok(timeout:float):
    end_time = time.time() + timeout

    def do_check():
        global fe_stream_check
        while True:
            state = fe_stream_check.state()
            if state == integration.Process.STOPPED:
                fe_stream_check.ignore()
                return
            elif state == integration.Process.FAILED:
                raise RuntimeError('Data check failed')
            print("state is {0}".format(state))

            if time.time() > end_time:
                raise RuntimeError('Timed out while waiting for check to complete')
            time.sleep(0.5)

    return do_check

integration.Sequence(
    [
        # integration.state.preserve_files,
        integration.require_readable(files=["/dev/gpstime", "/dev/mbuf"], description="required device files"),
        fe_simulated_streams.run,
        integration.wait(),
        local_dc.run,
        integration.wait(2),
        fe_stream_check.run,
        check_ok(timeout=30),
        fe_simulated_streams.ignore,
        local_dc.ignore,
    ]
)