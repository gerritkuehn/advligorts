#ifndef LIGO_MBUF_KERNEL_H
#define LIGO_MBUF_KERNEL_H
/** @brief This header is for the kernel module to mbuf, kernel 
 *        space interface. Mbuf exports the below symbols.
 */

/** Used by the IOP to store Dolphin memory state */
extern int iop_rfm_valid;

/** @brief This enum defines all possible error codes that can be
 *        returned by the kernel interface. You can use mbuf_lookup_error_msg()
 *        to retrieve a string version of the error code.
 */
enum MBUF_KERNEL_CODE
{
    MBUF_KERNEL_CODE_OK = 0,
    MBUF_KERNEL_CODE_NO_BUFFERS = 1,
    MBUF_KERNEL_CODE_ALLOC_FAILED = 2,
    MBUF_KERNEL_CODE_SZ_ERROR = 3,
    MBUF_KERNEL_CODE_NAME_NOT_FOUND = 4,
    MBUF_KERNEL_CODE_LAST 
};

/** A buffer of MBUF_ERROR_MSG_ALLOC_LEN must be passed as the second argument to mbuf_lookup_error_msg(...) */
#define MBUF_ERROR_MSG_MAX_LEN 128
#define MBUF_ERROR_MSG_ALLOC_LEN (MBUF_ERROR_MSG_MAX_LEN+1)

/** @brief Requests that a shared memory buffer be allocated with the name requested.
 *         If a buffer already exists with the requested name, then a pointer to that 
 *         buffer is returned. If the buffer does not exist, it is allocated and 
 *         a pointer to it returned.  
 *         
 *  @param name The name of the shared memory buffer to create/lookup
 *         
 *  @param size The size (in bytes) of the buffer to create/lookup
 *
 *  @param buffer_ptr The address of a pointer where the shared buffer's address
 *                    should be written
 *         
 *  @return MBUF_KERNEL_CODE_OK : no errors were encountered
 *          MBUF_KERNEL_CODE_NO_BUFFERS : mbuf is out of shared buffer spaces and cannot you a new one
 *          MBUF_KERNEL_CODE_ALLOC_FAILED : The system call to allocate memory failed, check the size you 
 *                                          are requesting, or the system might be out of memory
 *          MBUF_KERNEL_CODE_SZ_ERROR : Returned if the size used to lookup a buffer does not match
 *                                      the size used in the first allocation for the buffer of that name
 */
enum MBUF_KERNEL_CODE mbuf_allocate_area(const char *name, unsigned int size, volatile void ** buffer_ptr);

/** @brief Releases the callers used of the shared memory. When all users of a shared buffer
 *         have released it, this function will free the memory on the last release.
 *         
 *  @param name The name of the shared memory buffer to release
 *         
 *  @return MBUF_KERNEL_CODE_OK : no errors were encountered
 *          MBUF_KERNEL_CODE_NAME_NOT_FOUND : A shared buffer with the given name was not found
 */
enum MBUF_KERNEL_CODE mbuf_release_area(const char *name);

/** @brief Allows callers to lookup error messages from their returned error codes
 *         Useful for error messages
 *
 *  @param code The error code that should be looked up
 *         
 *  @param error_msg A buffer for the retrieved error code to be written. Should have at
 *                   least MBUF_ERROR_MSG_ALLOC_LEN bytes allocated for it
 *         
 *  @return Void.
 */
void mbuf_lookup_error_msg( enum MBUF_KERNEL_CODE code, char* error_msg);

#endif // LIGO_MBUF_KERNEL_H

