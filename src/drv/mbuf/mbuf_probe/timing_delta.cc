//
// Created by jonathan.hanks on 5/19/21.
//

#include "timing_delta.hh"

#include <atomic>
#include <chrono>
#include <csignal>
#include <functional>
#include <iostream>
#include <map>
#include <mutex>
#include <thread>

#include "rmipc_intl.hh"

namespace
{
    struct model_time
    {
        std::int64_t seconds{ 0 };
        unsigned int cycle{ 0 };

        model_time( ) : seconds{ 0 }, cycle{ 0 }
        {
        }
        model_time( std::int64_t s, unsigned int c ) : seconds{ s }, cycle{ c }
        {
        }
        model_time( const model_time& other ) noexcept = default;

        model_time& operator=( const model_time& ) noexcept = default;

        bool
        operator==( const model_time& other ) const noexcept
        {
            return seconds == other.seconds && cycle == other.cycle;
        }

        bool
        operator!=( const model_time& other ) const noexcept
        {
            return seconds != other.seconds || cycle != other.cycle;
        }

        bool
        operator<( const model_time& other ) const noexcept
        {
            if ( seconds < other.seconds )
            {
                return true;
            }
            if ( seconds == other.seconds )
            {
                return cycle < other.cycle;
            }
            return false;
        }
        bool
        operator<=( const model_time& other ) const noexcept
        {
            if ( seconds < other.seconds )
            {
                return true;
            }
            if ( seconds == other.seconds )
            {
                return cycle <= other.cycle;
            }
            return false;
        }
    };

    using timing_callback_t = std::function< void( model_time& ) >;

    class rmipc_peek
    {

        static model_time
        get_time( rmipc::memory_layout& layout )
        {
            auto cur_cycle = layout.ipc->cycle;
            return model_time{
                layout.ipc->bp[ cur_cycle ].timeSec,
                cur_cycle,
            };
        }

    public:
        rmipc_peek( volatile void*       buffer,
                    timing_callback_t    callback,
                    std::atomic< bool >* stop_signal )
            : layout_( reinterpret_cast< volatile char* >( buffer ) ),
              prev_time_{ get_time( layout_ ) },
              callback_{ std::move( callback ) }, stop_{ stop_signal }
        {
        }
        rmipc_peek( const rmipc_peek& ) noexcept = default;
        rmipc_peek( rmipc_peek&& ) noexcept = default;

        rmipc_peek& operator=( const rmipc_peek& ) noexcept = default;
        rmipc_peek& operator=( rmipc_peek&& ) noexcept = default;

        void
        operator( )( )
        {
            while ( !*stop_ )
            {
                auto cur_time = get_time( layout_ );
                if ( cur_time != prev_time_ )
                {
                    callback_( cur_time );
                    prev_time_ = cur_time;
                }
                std::this_thread::sleep_for( std::chrono::milliseconds( 1 ) );
            }
        }

    private:
        rmipc::memory_layout layout_;
        model_time           prev_time_;
        timing_callback_t    callback_;
        std::atomic< bool >* stop_;
    };

    class daq_multi_cycle_peek
    {

        static model_time
        get_time( volatile daq_multi_cycle_data_t* multi_data )
        {
            auto         cur_cycle = multi_data->header.curCycle;
            unsigned int data_size = multi_data->header.cycleDataSize;
            std::size_t  cycle_offset = data_size * cur_cycle;

            auto* dc_data = reinterpret_cast< volatile daq_dc_data_t* >(
                &( multi_data->dataBlock[ 0 ] ) + cycle_offset );

            return model_time{
                dc_data->header.dcuheader[ 0 ].timeSec,
                cur_cycle,
            };
        }

    public:
        daq_multi_cycle_peek( volatile void*       buffer,
                              timing_callback_t    callback,
                              std::atomic< bool >* stop_signal )
            : multi_data_( reinterpret_cast< volatile daq_multi_cycle_data_t* >(
                  buffer ) ),
              prev_time_{ get_time( multi_data_ ) },
              callback_{ std::move( callback ) }, stop_{ stop_signal }
        {
        }
        daq_multi_cycle_peek( const daq_multi_cycle_peek& ) noexcept = default;
        daq_multi_cycle_peek( daq_multi_cycle_peek&& ) noexcept = default;

        daq_multi_cycle_peek&
        operator=( const daq_multi_cycle_peek& ) noexcept = default;
        daq_multi_cycle_peek&
        operator=( daq_multi_cycle_peek&& ) noexcept = default;

        void
        operator( )( )
        {
            while ( !*stop_ )
            {
                auto cur_time = get_time( multi_data_ );
                if ( cur_time != prev_time_ )
                {
                    callback_( cur_time );
                    prev_time_ = cur_time;
                }
                std::this_thread::sleep_for( std::chrono::milliseconds( 1 ) );
            }
        }

    private:
        volatile daq_multi_cycle_data_t* multi_data_;
        model_time                       prev_time_;
        timing_callback_t                callback_;
        std::atomic< bool >*             stop_;
    };

    using time_point = std::chrono::time_point< std::chrono::steady_clock >;
    time_point
    now( )
    {
        return std::chrono::steady_clock::now( );
    }

    void
    record_timestamp( std::map< model_time, time_point >& seq,
                      model_time                          time_cycle,
                      time_point                          cur_time,
                      int                                 position )
    {
        auto it = seq.find( time_cycle );
        if ( it != seq.end( ) )
        {
            time_point a, b;
            if ( position == 0 )
            {
                a = cur_time;
                b = it->second;
            }
            else
            {
                a = it->second;
                b = cur_time;
            }
            auto delta =
                std::chrono::duration_cast< std::chrono::milliseconds >( a -
                                                                         b );
            std::cout << time_cycle.seconds << ":" << time_cycle.cycle
                      << " delta(ms) = " << delta.count( ) << "\n";
            seq.erase( it );
        }
        else
        {
            seq[ time_cycle ] = cur_time;
            if ( seq.size( ) > 1024 )
            {
                seq.erase( seq.begin( ) );
            }
        }
    }

    std::atomic< bool > stopping{ false };

    extern "C" void
    signal_handler( int ignored )
    {
        stopping = true;
    }

    template < typename Peek >
    int
    timing_loop( volatile void* buffer1, volatile void* buffer2 )
    {

        std::mutex m;
        auto       sequence = std::map< model_time, time_point >( );

        timing_callback_t callback1 = [&m, &sequence]( model_time& t ) {
            auto                          cur_time = now( );
            std::lock_guard< std::mutex > l_{ m };
            record_timestamp( sequence, t, cur_time, 0 );
        };

        timing_callback_t callback2 = [&m, &sequence]( model_time& t ) {
            auto                          cur_time = now( );
            std::lock_guard< std::mutex > l_{ m };
            record_timestamp( sequence, t, cur_time, 1 );
        };

        Peek buf1( buffer1, callback1, &stopping );
        Peek buf2( buffer2, callback2, &stopping );

        std::signal( SIGINT, signal_handler );
        std::signal( SIGTERM, signal_handler );

        std::thread t( [&buf1]( ) { buf1( ); } );
        buf2( );

        t.join( );
        return 0;
    }
} // namespace

namespace timing_delta
{
    int
    calculate_timing_delta( volatile void*    buffer1,
                            volatile void*    buffer2,
                            const ConfigOpts& opts )
    {
        if ( !buffer1 || !buffer2 )
        {
            std::cerr << "Unable to read memory buffers" << std::endl;
            return 1;
        }
        switch ( opts.analysis_type )
        {
        case MBUF_RMIPC:
            return timing_loop< rmipc_peek >( buffer1, buffer2 );
            break;
        case MBUF_DAQ_MULTI_DC:
            return timing_loop< daq_multi_cycle_peek >( buffer1, buffer2 );
            break;
        default:
            std::cout
                << "Unsupported structure type for calculating timing deltas"
                << std::endl;
            return 1;
        }
        return 0;
    }
} // namespace timing_delta
