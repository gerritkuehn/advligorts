#include "../../mbuf_kernel.h"


#include <linux/module.h>
#include <linux/init.h>


//  Define the module metadata.
#define MODULE_NAME "mbuf-test"

#define MBUFS_TO_CREATE 25

MODULE_DESCRIPTION("Module that tests the kernel space interface of mbuf");
MODULE_AUTHOR("Ezekiel Dohmen ezekiel.dohmen@ligo.org");
MODULE_LICENSE("Dual BSD/GPL");

typedef bool (*testFunctionPtr_t)(void);

void print_test_result(const char * name, testFunctionPtr_t tFunc)
{
    char msg[128];
    if( tFunc() == false) 
        strcpy(msg, "Failed");
    else
        strcpy(msg, "Passed");
        
    printk("Test %s: %s\n", name, msg);
}


bool typical_use( void )
{
    volatile void * buffer = NULL;
    enum MBUF_KERNEL_CODE res;
    int i;

    for(i=0; i<10; ++i)
    {
        //Allocate buffer
        res = mbuf_allocate_area(MODULE_NAME, 64 * 1024 * 1024, &buffer);
        if (res != MBUF_KERNEL_CODE_OK ) return false;
        if (buffer == NULL) return false;

        //Clean Up
        res = mbuf_release_area(MODULE_NAME);
        if( res != MBUF_KERNEL_CODE_OK ) return false;
    }

    return true;
}

bool too_small_alloc( void )
{
    volatile void * buffer = NULL;
    enum MBUF_KERNEL_CODE res;

    //Allocate buffer
    res = mbuf_allocate_area(MODULE_NAME, 64 * 1024 * 1024, &buffer);
    if (res != MBUF_KERNEL_CODE_OK ) return false;
    if (buffer == NULL) return false;

    //Generate some size errors
    res = mbuf_allocate_area(MODULE_NAME, 64 * 1024 * 1024 * 2, &buffer);
    if (res != MBUF_KERNEL_CODE_SZ_ERROR) return false;

    res = mbuf_allocate_area(MODULE_NAME, (62 * 1024 * 1024) , &buffer);
    if (res != MBUF_KERNEL_CODE_SZ_ERROR) return false;
   
    //Clean up
    res = mbuf_release_area(MODULE_NAME);
    if( res != MBUF_KERNEL_CODE_OK ) return false;


    return true;
}


bool alloc_and_lookup_base( size_t size )
{
    volatile void * buffer = NULL;
    volatile void *  last_buffer;
    enum MBUF_KERNEL_CODE res;
    int i;

    //Allocate buffer
    res = mbuf_allocate_area(MODULE_NAME, size, &buffer);
    if (res != MBUF_KERNEL_CODE_OK ) return false;
    if (buffer == NULL) return false;

    last_buffer = buffer;

    for(i=0; i<1024; ++i)
    {
        res = mbuf_allocate_area(MODULE_NAME, size, &buffer);
        if(buffer != last_buffer) return false;
        last_buffer = buffer;
    }

    //Release all buffers
    for(i=0; i<1025; ++i)
    {
        res = mbuf_release_area(MODULE_NAME);
        if( res != MBUF_KERNEL_CODE_OK ) return false;
    }

    //Make sure the usage count is at 0
    for(i=0; i<1024; ++i)
    {
        res = mbuf_release_area(MODULE_NAME);
        //Expect to not find it
        if( res != MBUF_KERNEL_CODE_NAME_NOT_FOUND ) return false;
    }//We look here to make sure we don't change anything in the module when we miss

    return true;
}

bool alloc_and_lookup( void )
{
    return alloc_and_lookup_base(64 * 1024 * 1024);
}

bool non_page_sz_check( void )
{
    return alloc_and_lookup_base(64);
}

bool fill_all_areas( void )
{
    char name[128];
    volatile void * buffer = NULL;
    enum MBUF_KERNEL_CODE res;
    int i;

    //Fill all the areas
    for(i=0; i<MBUFS_TO_CREATE; ++i)
    {
        snprintf(name, 128, "shmem_%d", i);
        //printk("%d, name: %s\n",i,name);
        res = mbuf_allocate_area(name, 64 * 1024 * 1024, &buffer);
        if (res != MBUF_KERNEL_CODE_OK ) return false;
    }


    //Free all the areas
    for(i=0; i<MBUFS_TO_CREATE; ++i)
    {
        snprintf(name, 128, "shmem_%d", i);
        res = mbuf_release_area(name);
        if (res != MBUF_KERNEL_CODE_OK ) return false;
    }


    return true;
}



bool error_lookup( void )
{
    char error_msg[MBUF_ERROR_MSG_ALLOC_LEN];

    mbuf_lookup_error_msg( MBUF_KERNEL_CODE_OK, error_msg);
    printk("%s Return code messages:\n", MODULE_NAME);
    printk("%s : MBUF_KERNEL_CODE_OK : %s", MODULE_NAME, error_msg);

    mbuf_lookup_error_msg( MBUF_KERNEL_CODE_NO_BUFFERS, error_msg);
    printk("%s : MBUF_KERNEL_CODE_NO_BUFFERS : %s", MODULE_NAME, error_msg);

    mbuf_lookup_error_msg( MBUF_KERNEL_CODE_ALLOC_FAILED, error_msg);
    printk("%s : MBUF_KERNEL_CODE_ALLOC_FAILED : %s", MODULE_NAME, error_msg);

    mbuf_lookup_error_msg( MBUF_KERNEL_CODE_SZ_ERROR, error_msg);
    printk("%s : MBUF_KERNEL_CODE_SZ_ERROR : %s", MODULE_NAME, error_msg);

    mbuf_lookup_error_msg( MBUF_KERNEL_CODE_NAME_NOT_FOUND, error_msg);
    printk("%s : MBUF_KERNEL_CODE_NAME_NOT_FOUND : %s", MODULE_NAME, error_msg);

    mbuf_lookup_error_msg( MBUF_KERNEL_CODE_LAST, error_msg);
    printk("%s : MBUF_KERNEL_CODE_LAST : %s", MODULE_NAME, error_msg);


    return true;
}

bool alloc_fill_read ( void )
{
    volatile void * buffer = NULL;
    enum MBUF_KERNEL_CODE res;
    int i;
    size_t size = 64 * 1024 * 1024;
    volatile unsigned char* mem;

    //Allocate buffer
    res = mbuf_allocate_area(MODULE_NAME, size, &buffer);
    if (res != MBUF_KERNEL_CODE_OK ) return false;
    if (buffer == NULL) return false;


    mem = buffer;
    unsigned char val = 0;
    for(i=0; i<size; ++i) //Fill up buffer
    {
        mem[i] = val++;
    }

    //Verify nothing has changed
    for(i=0; i<size-1; ++i)
    {
        if ( (mem[i+1] != mem[i] + 1) && mem[i+1] != 0)
            return false;
    }

    //Clean Up
    res = mbuf_release_area(MODULE_NAME);
    if( res != MBUF_KERNEL_CODE_OK ) return false;
    
    return true;
}

bool alloc_1M_buffer( void )
{
    volatile void * buffer = NULL;
    enum MBUF_KERNEL_CODE res;
    size_t size = 1 * 1024 * 1024;

    //Allocate buffer
    res = mbuf_allocate_area(MODULE_NAME, size, &buffer);
    if (res != MBUF_KERNEL_CODE_OK ) return false;
    if (buffer == NULL) return false;

    //Clean Up
    res = mbuf_release_area(MODULE_NAME);
    if( res != MBUF_KERNEL_CODE_OK ) return false;

    return true;

}

static int __init test_init(void)
{
    pr_info("%s: module starting tests.\n", MODULE_NAME);

    //Run all tests
    print_test_result("typical_use", typical_use);
    print_test_result("too_small_alloc", too_small_alloc);
    print_test_result("alloc_and_lookup", alloc_and_lookup);
    print_test_result("non_page_sz_check", non_page_sz_check);
    print_test_result("alloc_1M_buffer", alloc_1M_buffer);


    return 0;
}

static void __exit test_exit(void)
{
    print_test_result("fill_all_areas", fill_all_areas);
    print_test_result("error_lookup", error_lookup);
    print_test_result("alloc_fill_read", alloc_fill_read);

    pr_info("%s: module unloaded from 0x%p\n", MODULE_NAME, test_exit);
}



module_init(test_init);
module_exit(test_exit);

