# RTS CPU Isolator
This module's main use, is isolating kernel space models, allowing them run uninterrupted, then allowing a real time module's exit() code to clean up when done. There is a kernel space, and userspace interfaces, with differences explained below.  

## Common (Kernel/Userspace) Functions
When the isolator module is loaded it queries the kernel for the CPUs that have been isolated with the `isolcpus=...` kernel command line parameter. These CPUs are interpreted as being valid for isolation, and the module keeps track of the CPUs and allocates them to kernel and userspace requesters. 

## Kernel Module Isolation
This is how LIGO achieves real time performance with the linux operating system. This is achieved by overwriting the `play_dead()` function in the `smp_ops` structure [link](https://elixir.bootlin.com/linux/v5.10.154/source/arch/x86/include/asm/smp.h#L48). When the isolator later calls `remove_cpu()` (to isolate the core) linux calls the `play_dead()` function after removing the CPU. Our replaced `play_dead()` function then starts execution of the real time function on the downed CPU. This achieves the function of having Linux believe the CPU is offline, and thus it will not interrupt the running LIGO isolated (real time) functions, or complain about the kernel space code hogging the CPU. 

When we want the real time code to exit, we signal as such, and wait for it to exit. The isolator module catches the return and allows execution to continue into the original `play_dead()` function. From that point the CPU can be brought back up with the `add_cpu()` Linux call, which brings the CPU back online. It can then be used by Linux for processes again or re-isolated for another real time kernel module. 

### Code Example
```c
/// The general usage for getting your module to run in "real-time"
/// mode with this driver is:
///
/// == In your module init ==
///
/// 1. Start the real time function on a isolated CPU with:

    ISOLATOR_ERROR ret = rts_isolator_run( rt_runner_func, -1, g_core_allocated); 
    if( ret != ISOLATOR_OK ) ERROR;

/// == End module init ==

/// 4. Allow your "real-time" module to run and do its job.

/// 5. When you want to stop the "real-time" module and bring the CPU back.
///    First signal your "real-time" module to exit. This is usually done
///    through the use of a global varable that your module_exit() function
///    and your "real-time" runner function (rt_runner_func above) can both
///    see. Second call the rts_isolator_cleanup() function. Ex:

    void rt_user_mod_exit( void ) {
      g_stop_rt_function = 1; //The rt_runner_func is checking this
                                each iteration to see if it should exit
      msleep( 1000 ); //Wait for some time to let it exit,
                      //or use another var to signal an exit

      rts_isolator_cleanup( g_core_allocated ); //Cleanup the core that was allocated to us
    }
    module_exit( rt_user_mod_exit );

```


## Userspace CPU Allocation
The userspace interface only provides the ability to ask the CPU for a free isolated core (on the `isolcpus=...` list), and then allocate that core to the requesting userspace application, preventing it from being given to any other userspace application or being used to isolate a kernel module. 

### Code Example
```c
#define _GNU_SOURCE //sched_setaffinity and CPU_* macros
#include "../drv/rts-cpu-isolator/usp-allocate.h"
...

int allocated_cpu = -1;

allocated_cpu = rts_cpu_isolator_alloc_core(allocated_cpu);
if ( allocated_cpu == -1 )
{
    RTSLOG_ERROR("There was an error getting a free isolated core\n");
    return 1;
}

//Lock our thread to the allocated core
cpu_set_t my_set;
CPU_ZERO(&my_set);
CPU_SET(allocated_cpu, &my_set);
if ( sched_setaffinity(0, sizeof(cpu_set_t), &my_set) != 0)
{
    RTSLOG_ERROR("There was an error locking to the allocated core\n");
    return 1;
}


```
