project(get_time_from_dev)

add_executable(${PROJECT_NAME}
        stest.c)

target_include_directories(${PROJECT_NAME} PUBLIC
        "${CMAKE_CURRENT_SOURCE_DIR}/../../")

add_test(NAME ${PROJECT_NAME}
        COMMAND ${PROJECT_NAME}
        WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}")
