//
// Created by jonathan.hanks on 2/28/22.
//
#include "catch.hpp"
#include <cstdint>
#include <vector>
#include <time.h>
#define LIGO_TIMESPEC timespec
#include "timespec_utils.h"

static inline LIGO_TIMESPEC
ts(std::int64_t sec, std::int64_t nsec)
{
    LIGO_TIMESPEC t{};
    t.tv_sec = sec;
    t.tv_nsec = nsec;
    return t;
}

static inline ligo_duration
dur(std::int64_t sec, std::int64_t nsec)
{
    ligo_duration t{};
    t.tv_sec = sec;
    t.tv_nsec = nsec;
    return t;
}

TEST_CASE("You can add LIGO_TIMESPECs together")
{
    struct test_case_def {
        LIGO_TIMESPEC t0;
        ligo_duration duration;
        LIGO_TIMESPEC expected;
    };
    std::vector<test_case_def> test_cases{
        {ts(0,0), dur(0,0), ts(0,0)},
        {ts(1,0), dur(0,1), ts(1,1)},
        {ts(1,999999999), dur(0,1), ts(2,0)},
        {ts(1,999999999), dur(5,2), ts(7,1)},
        {ts(1,1), dur(0,999999999), ts(2,0)},
        {ts(1,2), dur(5,999999999), ts(7,1)},
    };
    for (const auto& test_case:test_cases)
    {
        auto results = ltimespec_add(test_case.t0, test_case.duration);
        REQUIRE(results.tv_sec == test_case.expected.tv_sec);
        REQUIRE(results.tv_nsec == test_case.expected.tv_nsec);
    }
}

TEST_CASE("You can take the subtract a duration from a LIGO_TIMESPEC")
{
    struct test_case_def {
        LIGO_TIMESPEC t0;
        ligo_duration duration;
        LIGO_TIMESPEC expected;
    };
    std::vector<test_case_def> test_cases{
        {ts(0,0), dur(0,0), ts(0,0)},
        {ts(1,0), dur(0,1), ts(0,999999999)},
        {ts(1,999999999), dur(0,1), ts(1,999999998)},
        {ts(1,999999999), dur(5,2), ts(-4,999999997)},
        { ts(5, 3), dur(3, 7), ts(1, 999999996)},
    };
    for (const auto& test_case:test_cases)
    {
        auto results = ltimespec_sub(test_case.t0, test_case.duration);
        REQUIRE(results.tv_sec == test_case.expected.tv_sec);
        REQUIRE(results.tv_nsec == test_case.expected.tv_nsec);
    }
}

TEST_CASE("You can take the difference between LIGO_TIMESPECs")
{
    struct test_case_def {
        LIGO_TIMESPEC t0;
        LIGO_TIMESPEC t1;
        ligo_duration expected;
    };
    std::vector<test_case_def> test_cases{
        {ts(0,0), ts(0,0), dur(0,0)},
        {ts(1,0), ts(0,1), dur(0,999999999)},
        {ts(1,999999999), ts(0,1), dur(1,999999998)},
        {ts(1,999999999), ts(5,2), dur(-4,999999997)},
        { ts(5, 3), ts(3, 7), dur(1, 999999996)},
    };
    for (const auto& test_case:test_cases)
    {
        auto results = ltimespec_difference(test_case.t0, test_case.t1);
        REQUIRE(results.tv_sec == test_case.expected.tv_sec);
        REQUIRE(results.tv_nsec == test_case.expected.tv_nsec);
    }
}
